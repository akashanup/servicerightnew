<?php

use App\Models\Garage;
use Illuminate\Database\Seeder;

class TestGaragesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Garage::class, 10)->create();
    }
}
