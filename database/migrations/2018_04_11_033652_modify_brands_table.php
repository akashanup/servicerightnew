<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->renameColumn('brand', 'name');
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->string('name', 50)->change();
            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->dropUnique('brands_name_unique');
            $table->string('name', 255)->change();
        });

        Schema::table('brands', function (Blueprint $table) {
            $table->renameColumn('name', 'brand');
        });
    }
}
