<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->string('model');
            $table->json('services')->nullable();
            $table->date('build_year_start')->nullable();
            $table->date('build_year_end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('model');
            $table->dropColumn('services');
            $table->dropColumn('build_year_start');
            $table->dropColumn('build_year_end');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
