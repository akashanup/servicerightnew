<?php

namespace Tests\Unit;

use App\Models\City;
use App\Models\State;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class CityTest extends UnitTestCase
{
    /**
     * City createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueForExistingCity()
    {
        $city = City::inRandomOrder()->first();
        $data = City::createUnique($city->state_id, $city->name);
        $this->assertEquals($city->id, $data->id);
    }

    /**
     * City createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueForNonExistingCity()
    {
        $faker = Faker::create();
        $name = $faker->city;
        $state = State::inRandomOrder()->first();
        $stateId = $state->id;
        $data = City::createUnique($stateId, $name);
        $city = City::where('name', $name)->where('state_id', $state->id)->first();
        $this->assertEquals($city->id, $data->id);
    }
}
