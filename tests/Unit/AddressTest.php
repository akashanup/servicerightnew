<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\Country;
use App\Models\PostalCode;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class AddressTest extends UnitTestCase
{
    /**
     * Address fetch test example.
     *
     * @return void
     */
    public function testFetchExistingAddress()
    {
        $address = Address::inRandomOrder()->first();
        $postalCode = $address->postalCode;
        $city = $postalCode->city;
        $state = $city->state;
        $data = Address::fetch(
            array(
                'houseNumber' => $address->house_number, 'postalCode' => $postalCode->postal_code,
                'countryId' => $state->country_id,
            )
        );
        $this->assertEquals(
            array(
                'response' => true, 'addressId' => $address->id, 'state' => $state->name,
                'city' => $city->name, 'complete_address' => $address->complete_address,
            ),
            $data
        );
    }

    /**
     * Address fetch test example.
     *
     * @return void
     */
    public function testFetchNonExistingAddress()
    {
        $countryNetherands = Country::where('name', Address::NETHERLANDS)->first();
        $houseNumber = '13';
        $postalCode = '1333GE';
        $data = Address::fetch(
            array('houseNumber' => $houseNumber, 'postalCode' => $postalCode, 'countryId' => $countryNetherands->id)
        );
        $postalCode = PostalCode::where('postal_code', $postalCode)->first();
        $address = Address::where('house_number', $houseNumber)->where('postal_code_id', $postalCode->id)->first();
        $city = $address->postalCode->city;
        $this->assertEquals(
            array(
                'response' => true, 'addressId' => $address->id, 'state' => $city->state->name,
                'city' => $city->name, 'complete_address' => $address->complete_address,
                'latitude' => $address->latitude, 'longitude' => $address->longitude, 'message' => null,
            ),
            $data
        );
    }

    /**
     * Address fetch test example.
     *
     * @return void
     */
    public function testFetchInvalidAddressForOutsideNetherland()
    {
        $faker = Faker::create();
        $country = Country::create(['name', $faker = Faker::create()]);
        $houseNumber = 'XX';
        $postalCode = '201301';
        $data = Address::fetch(
            array(
                'houseNumber' => $houseNumber, 'postalCode' => $postalCode,
                'countryId' => $country->id,
            )
        );
        $this->assertEquals(array('response' => false), $data);
    }

    /**
     * Address fetch test example.
     *
     * @return void
     */
    public function testFetchInvalidAddressForNetherland()
    {
        $faker = Faker::create();
        $country = Country::where('name', Address::NETHERLANDS)->first();
        $houseNumber = 'XX';
        $postalCode = '201301';

        $this->expectException(\GuzzleHttp\Exception\ClientException::class);
        $data = Address::fetch(
            array('houseNumber' => $houseNumber, 'postalCode' => $postalCode, 'countryId' => $country->id)
        );
    }

    /**
     * Address createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueForExistingAddress()
    {
        $address = Address::inRandomOrder()->first();
        $postalCode = $address->postalCode;
        $city = $postalCode->city;
        $data = Address::createUnique(
            $address->house_number, $address->complete_address, $address->type, $address->latitude,
            $address->longitude, $postalCode->postal_code, $city->name, $city->state->name
        );
        $this->assertEquals($address->id, $data->id);
    }

    /**
     * Address createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueForNonExistingAddress()
    {
        $faker = Faker::create();
        $completeAddress = $faker->streetAddress . ',' . $faker->streetName;
        $postalCode = PostalCode::inRandomOrder()->first();
        $houseNumber = $faker->buildingNumber;
        $type = 'client';
        $city = $postalCode->city;
        $data = Address::createUnique($houseNumber, $completeAddress, $type, null, null, $postalCode->postal_code,
            $city->name, $city->state->name
        );
        $address = Address::where('postal_code_id', $postalCode->id)->where('type', $type)
            ->where('house_number', $houseNumber)->where('complete_address', $completeAddress)->first();
        $this->assertEquals($address->id, $data->id);
    }
}
