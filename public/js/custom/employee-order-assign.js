$(function(){
  
  $(document).on('click', '.assignGarageOrderBtn', function(){
    var thisBtn = $(this);
    var garageName = thisBtn.attr('data-garageName');
    var garageId = thisBtn.attr('data-garageId');
    $('#garageModalDiv').html(`<h5>${garageName}</h5>`);
    $('#garageIdInpt').val(garageId);
    $('#orderModal').modal('show');
  });

  $('#orderModal').on('hidden.bs.modal', function(){
    $('#garageModalDiv').html('');
    $('#garageIdInpt').val('');
  });

  $(document).on('click', '.updateGarageOrderBtn', function(){
    var thisBtn = $(this);
    var status = thisBtn.attr('data-status');
    var data = {
      "orderId": $('#modalOrderId').html().trim(),
      "garageId": $('#orderModal').find('#garageIdInpt').val(),
      "status": status
    };
    var url = $('#garageOrderRoute').val();
    thisBtn.attr('disabled', 'disabled');
    $.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
      thisBtn.removeAttr('disabled');
      if(successResponse.response) {
        if (successResponse.route) {
          window.location = successResponse.route;
        } else {
          makeGarageMarker(map, clientLatitude, clientLongitude, successResponse.locations);
          $('#orderModal').modal('hide');
        }
      }
    }, (errorResponse) => {
      thisBtn.removeAttr('disabled');
      $('#orderModal').modal('hide');
      $.unblockUI();
      $.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
    });
  });

  $('.categoryCheckboxInpt, #members_only, .typeCheckboxInpt').on('click', function(){
    initMap();
  });

  $(document).on('click', '.saveGarageNoteBtn', function(){
    var thisBtn = $(this);
    var garageNote = thisBtn.parent('td').find('textarea');
    var notes = garageNote.val();
    var data = {
      "garageId": thisBtn.attr('data-garageId'),
      "notes": notes
    };
    var url = $('#garageNotesRoute').val();
    $.fn.ajaxBlockUI();
    $.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
      makeGarageMarker(map, clientLatitude, clientLongitude, successResponse.locations);
      $.unblockUI();
    }, (errorResponse) => {
      $.unblockUI();
      $.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
    });
  });

  $(document).on('click', '.garageCheckBoxInpt', function(){
    var thisCheckbox = $(this);
    var categoryIds = [];
    $.each(thisCheckbox.parent('td').find('.garageCheckBoxInpt:checked'),function(i, v){
      categoryIds.push($(v).attr('data-categoryId'));
    });
    var data = {
      "garageId": thisCheckbox.attr('data-garageId'),
      "categoryIds": categoryIds
    };

    var url = $('#garageCategoryRoute').val();
    $.fn.ajaxBlockUI();
    $.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
      $.unblockUI();
      makeGarageMarker(map, clientLatitude, clientLongitude, successResponse.locations);
    }, (errorResponse) => {
      $.unblockUI();
      $.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
    });
  });
});

