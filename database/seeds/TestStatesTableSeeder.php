<?php

use App\Models\Address;
use App\Models\Country;
use App\Models\State;
use Illuminate\Database\Seeder;

class TestStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(State::class, 10)->create();
        DB::table('states')->insert(
            [
                'name' => State::NOT_FOUND,
                'country_id' => Country::where('name', Address::NETHERLANDS)->first()->id,
            ]
        );
    }
}
