<?php

use App\Models\Vehicle;
use Illuminate\Database\Seeder;

class TestVehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Vehicle::class, 50)->create();

        foreach (Vehicle::all() as $key => $vehicle) {
            Vehicle::create([
                'brand_id' => $vehicle->brand_id,
                'fuel_type' => $vehicle->fuel_type,
                'model' => $vehicle->model,
                'services' => $vehicle->services,
                'parent_id' => $vehicle->id,
            ]);
        }
    }
}
