<?php

namespace Tests\Unit;

use App\Models\Category;
use App\Models\Garage;
use App\Models\Order;
use Carbon\Carbon;
use DB;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class GarageTest extends UnitTestCase
{
    /**
     * Garage store test example.
     *
     * @return void
     */
    public function testStoreForUpdating()
    {

        $faker = Faker::create();
        $testData = array(
            'completeAddress' => $faker->streetAddress . ',' . $faker->streetName,
            'latitude' => (string) $faker->latitude(-90, 90),
            'longitude' => (string) $faker->longitude(-180, 180),
            'postalCode' => $faker->postcode,
            'city' => $faker->city,
            'contactPerson' => $faker->name,
            'email' => $faker->safeEmail,
            'phone' => $faker->e164PhoneNumber,
            'name' => $faker->word,
            'website' => $faker->url,
            'type' => null,
            'moneyBirdId' => null,
            'categories' => [],
            'companies' => [],

        );
        $garage = Garage::inRandomOrder()->first();
        $data = Garage::store($testData, $garage);
        $address = $garage->address;
        $postalCode = $address->postalCode;
        $user = $garage->user;
        $this->assertEquals(
            $testData,
            array(
                'completeAddress' => $address->complete_address,
                'latitude' => $address->latitude,
                'longitude' => $address->longitude,
                'postalCode' => $postalCode->postal_code,
                'city' => $postalCode->city->name,
                'contactPerson' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
                'name' => $garage->name,
                'website' => $garage->website,
                'type' => $garage->type,
                'moneyBirdId' => $garage->moneybird_id,
                'categories' => [],
                'companies' => [],
            )
        );
    }

    /**
     * Garage store test example.
     *
     * @return void
     */
    public function testStoreForStoring()
    {

        $faker = Faker::create();
        $testData = array(
            'completeAddress' => $faker->streetAddress . ',' . $faker->streetName,
            'latitude' => (string) $faker->latitude(-90, 90),
            'longitude' => (string) $faker->longitude(-180, 180),
            'postalCode' => $faker->postcode,
            'city' => $faker->city,
            'contactPerson' => $faker->name,
            'email' => $faker->safeEmail,
            'phone' => $faker->e164PhoneNumber,
            'name' => $faker->word,
            'website' => $faker->url,
            'type' => null,
            'moneyBirdId' => null,
            'categories' => [],
            'companies' => [],

        );

        $data = Garage::store($testData);
        $garage = Garage::latest()->first();
        $address = $garage->address;
        $postalCode = $address->postalCode;
        $user = $garage->user;
        $this->assertEquals(
            $testData,
            array(
                'completeAddress' => $address->complete_address,
                'latitude' => $address->latitude,
                'longitude' => $address->longitude,
                'postalCode' => $postalCode->postal_code,
                'city' => $postalCode->city->name,
                'contactPerson' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
                'name' => $garage->name,
                'website' => $garage->website,
                'type' => $garage->type,
                'moneyBirdId' => $garage->moneybird_id,
                'categories' => [],
                'companies' => [],
            )
        );
    }

    /**
     * Garage store order test example.
     *
     * @return void
     */
    public function testStoreOrderFirst()
    {
        DB::rollback();
        $order = Order::inRandomOrder()->first();
        $orderInitialStatus = $order->status;
        $garage = Garage::inRandomOrder()->first();
        $garage->storeOrder($order->id, Order::ACCEPTED_BY_GARAGE);
        $orderId = $garage->orders()->orderBy('garage_orders.id', 'DESC')->first()->pivot->order_id;
        $garageOrderId = $garage->orders()->orderBy('garage_orders.id', 'DESC')->first()->pivot->id;
        unset($order);
        $order = Order::find($orderId);
        $orderStatus = $order->status;
        $order->update(['status' => $orderInitialStatus]);
        DB::table('garage_orders')->delete($garageOrderId);
        $this->assertEquals(
            array(
                'orderStatus' => Order::ACCEPTED_BY_GARAGE,
                'orderLockedStatus' => Order::UNLOCKED,
                'garageOrders' => $order->id,
            ),
            array(
                'orderStatus' => $orderStatus,
                'orderLockedStatus' => $order->locked_status,
                'garageOrders' => $orderId,
            )
        );
    }

    /**
     * Garage store order test example.
     *
     * @return void
     */
    public function testStoreOrderSecond()
    {
        $garage = Garage::inRandomOrder()->first();
        $data = $garage->storeOrder(null, Order::ACCEPTED_BY_GARAGE);
        $this->assertEquals(['response' => false], $data);
    }

    /**
     * Garage store order test example.
     *
     * @return void
     */
    public function testStoreOrderThird()
    {
        DB::rollback();
        $order = Order::inRandomOrder()->first();
        $orderInitialStatus = $order->status;
        $garage = Garage::inRandomOrder()->first();
        $garage->storeOrder($order->id, Order::REJECTED_BY_GARAGE);
        $orderId = $garage->orders()->orderBy('garage_orders.id', 'DESC')->first()->pivot->order_id;
        $garageOrderId = $garage->orders()->orderBy('garage_orders.id', 'DESC')->first()->pivot->id;
        unset($order);
        $order = Order::find($orderId);
        $order->update(['status' => $orderInitialStatus]);
        DB::table('garage_orders')->delete($garageOrderId);
        $this->assertEquals(
            array(
                'garageOrders' => $order->id,
            ),
            array(
                'garageOrders' => $orderId,
            )
        );
    }

    /**
     * Garage fetchAll test example.
     *
     * @return void
     */
    public function testfetchAll()
    {
        $member = 'true';
        $categories = array(
            Category::inRandomOrder()->first()->id,
            Category::inRandomOrder()->first()->id,
            Category::inRandomOrder()->first()->id,
        );
        $types = array('Bovag autobedrijf', 'SR cleaningbedrijf');
        $garages = Garage::whereNotNull('garages.member_expiry')->whereIn('type', $types)
            ->where('garages.status', Garage::ACTIVE)
            ->leftjoin('garage_categories as gc', 'garages.id', 'gc.garage_id')
            ->leftjoin('categories as c', 'gc.category_id', 'c.id')
            ->whereIn('c.id', $categories)
            ->distinct('garages.id')->groupBy('garages.id');
        $data = Garage::fetchAll($member, $categories, $types);
        $this->assertEquals($garages->count(), sizeof($data['locations']));
    }

    /**
     * Garage updatePassword test example.
     *
     * @return void
     */
    public function testUpdatePassword()
    {
        $faker = Faker::create();
        $password = $faker->word;
        $garage = Garage::inRandomOrder()->first();
        $data = $garage->updatePassword($password);
        $this->assertEquals($password, $data);
    }

    /**
     * Garage saleNotesData test example.
     *
     * @return void
     */
    public function testSaleNotesDataFirst()
    {
        $garage = Garage::inRandomOrder()->first();
        $data = $garage->saleNotesData();
        $this->assertEquals($garage->saleNotes()->count(), sizeof($data['saleNotes']));
    }

    /**
     * Garage saleNotesData test example.
     *
     * @return void
     */
    public function testSaleNotesDataSecond()
    {
        $garage = Garage::inRandomOrder()->first();
        $data = $garage->saleNotesData();
        $testData = Garage::leftjoin('garage_sale_notes', 'garage_sale_notes.garage_id', 'garages.id')
            ->join('users', 'garage_sale_notes.by_user_id', 'users.id')
            ->where('garages.id', $garage->id)
            ->select(
                'garage_sale_notes.sale_note as note', 'users.name as createdBy',
                DB::raw('DATE_FORMAT(garage_sale_notes.created_at, "%d-%m-%Y %h:%m:%s") as createdAt')
            )
            ->orderBy('garages.id', 'desc')
            ->get()->toArray();
        $this->assertEquals($testData, $data['saleNotes']);
    }

    /**
     * Garage sendDemo test example.
     *
     * @return void
     */
    public function testSendDemoFirst()
    {
        $garage = Garage::inRandomOrder()->first();
        $data = $garage->sendDemo();
        $this->assertEquals(
            array('response' => true, 'message' => \Lang::get('manage-garage.login_credentials')),
            $data
        );
    }

    /**
     * Garage sendDemo test example.
     *
     * @return void
     */
    public function testSendDemoSecond()
    {
        $garage = Garage::inRandomOrder()->first();
        $garage->update(['membership_status' => Garage::ACTIVE]);
        $data = $garage->sendDemo();
        $this->assertEquals(
            array('response' => false, 'message' => null),
            $data
        );
    }

    /**
     * Garage generateActivationToken test example.
     *
     * @return void
     */
    public function testGenerateActivationToken()
    {
        $garage = Garage::inRandomOrder()->first();
        $token = $garage->generateActivationToken();
        $uniqueTokenCount = Garage::where('activation_token', $token)->get();
        $this->assertEquals($uniqueTokenCount->count(), 1);
    }

    /**
     * Garage createMembership test example.
     *
     * @return void
     */
    public function testCreateMembershipFirst()
    {
        $garage = Garage::inRandomOrder()->first();
        $data = $garage->createMembership(array('discount' => 10.0));
        $this->assertEquals(
            array('response' => true, 'message' => \Lang::get('manage-garage.membership_added')),
            $data
        );
    }

    /**
     * Garage createMembership test example.
     *
     * @return void
     */
    public function testCreateMembershipSecond()
    {
        $garage = Garage::inRandomOrder()->first();
        $garage->update(['membership_status' => Garage::ACTIVE]);
        $data = $garage->createMembership(array('discount' => 10.0));
        $this->assertEquals(
            array('response' => true, 'message' => \Lang::get('manage-garage.membership_already_active')),
            $data
        );
    }

    /**
     * Garage createMembership test example.
     *
     * @return void
     */
    public function testGetMembershipTextFirst()
    {
        $garage = Garage::inRandomOrder()->first();
        $garage->update(['membership_status' => Garage::ACTIVE]);
        $member_expiry = $garage->member_expiry ? Carbon::parse($garage->member_expiry)->format('d-m-Y')
        : $garage->member_expiry;
        $data = Garage::getMembershipText($garage);
        $this->assertEquals(
            \Lang::get('manage-garage.member_to') . ': ' . $member_expiry,
            $data
        );
    }

    /**
     * Garage createMembership test example.
     *
     * @return void
     */
    public function testGetMembershipTextSecond()
    {
        $garage = Garage::inRandomOrder()->first();
        $garage->update(['member_expiry' => Carbon::now()->toDateString()]);
        $member_expiry = $garage->member_expiry ? Carbon::parse($garage->member_expiry)->format('d-m-Y')
        : $garage->member_expiry;
        $data = Garage::getMembershipText($garage);
        $this->assertEquals(
            \Lang::get('manage-garage.expired_on') . ': ' . $member_expiry,
            $data
        );
    }

    /**
     * Garage createMembership test example.
     *
     * @return void
     */
    public function testGetMembershipTextThird()
    {
        $garage = Garage::inRandomOrder()->first();
        $data = Garage::getMembershipText($garage);
        $this->assertEquals(
            \Lang::get('globals.not_a_member'),
            $data
        );
    }
}
