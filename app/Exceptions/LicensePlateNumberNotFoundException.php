<?php

namespace App\Exceptions;

use Exception;
use Lang;

class LicensePlateNumberNotFoundException extends Exception
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        $json = [
            "response" => false,
            "errors" => ["error" => [Lang::get('globals.vehicle_not_found')]],
        ];
        return response()->json($json, 422);
    }
}
