<?php

namespace App\Models;

use App\Models\Address;
use App\Models\Feedback;
use App\Models\Garage;
use App\Models\LicensePlateNumber;
use App\Models\LockOrder;
use App\Models\OrderHistory;
use App\Models\OrderNote;
use App\Models\Service;
use App\Models\User;
use App\Utils\Curl;
use App\Utils\OrderService;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Lang;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'license_plate_number_id', 'pickup_date', 'alternative_pickup_date', 'pickup_time',
        'pickup_type', 'additional_work', 'user_id', 'address_id', 'total_price', 'status',
        'verification_token', 'client_verified', 'locked_status', 'invoice',
    ];

    const PICKUP_FROM_CLIENT = 'pickup_from_client';
    const CLIENT_BRING_ITSELF = 'client_brings_itself';
    const REPAIR_AT_LOCATION = 'repair_at_client_location';

    const DRAFTED = 'drafted';
    const QUOTED = 'quoted';
    const CONFIRMED = 'confirmed';
    const CANCELLED = 'cancelled';
    const REMOVED = 'removed';
    const ACCEPTED_BY_GARAGE = 'accepted_by_garage';
    const REJECTED_BY_GARAGE = 'rejected_by_garage';
    const COMPLETED = 'completed';

    const EDITABLE_STATUS = ['drafted', 'quoted', 'confirmed'];
    const SHOWABLE_STATUS = ['drafted', 'quoted', 'confirmed', 'accepted_by_garage', 'completed'];

    // Virtual statuses.
    const UPDATED = 'updated';
    const CALLBACK_REQUESTED = 'callback_requested';

    // Garage statuses.
    const CANCELLED_WITH_NO_CANCELLATION_COST = 'cancelled_with_no_cancellation_cost';
    const CANCELLED_GARAGE_GETS_CANCELLATION_COST = 'cancelled_garage_gets_cancellation_cost';
    const CANCELLED_CLIENT_GETS_CANCELLATION_COST = 'cancelled_client_gets_cancellation_cost';

    // Locked status.
    const LOCKED = 'locked';
    const UNLOCKED = 'unlocked';

    const TOKEN_EXPIRY_DAYS = 30;

    /**
     * Format pickup_date from yyyy-mm-dd to dd-mm-yyyy
     *
     * @return date $pickup_date
     */
    public function pickup_date()
    {
        return Carbon::parse($this->pickup_date)->format('d-m-Y');
    }

    /**
     * Format alternative_pickup_date from yyyy-mm-dd to dd-mm-yyyy
     *
     * @return date $alternative_pickup_date
     */
    public function alternative_pickup_date()
    {
        return Carbon::parse($this->alternative_pickup_date)->format('d-m-Y');
    }

    /**
     * Get the the user of the order.
     *
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    } //end user()

    /**
     * Get the the postal code of the address.
     *
     * @return App\Models\Address
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    } //end address()

    /**
     * Get the the licensePlateNumber of the order.
     *
     * @return App\Models\LicensePlateNumber
     */
    public function licensePlateNumber()
    {
        return $this->belongsTo(LicensePlateNumber::class);
    } //end licensePlateNumber()

    /**
     * Get the order services for the order.
     *
     * @return App\Models\Service
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'order_services', 'order_id', 'service_id')
            ->withPivot('id', 'price')->withTimestamps();
    } //end services()

    /**
     * Get the histories of the order.
     *
     * @return App\Models\OrderHistory
     */
    public function histories()
    {
        return $this->hasMany(OrderHistory::class);
    } //end histories()

    /**
     * Get the feedback of the order.
     *
     * @return App\Models\Feedback
     */
    public function feedback()
    {
        return $this->hasOne(Feedback::class);
    } //end feedback()

    /**
     * Get the lock statuses of the order.
     *
     * @return App\Models\LockOrder
     */
    public function lock_statuses()
    {
        return $this->hasMany(LockOrder::class);
    } //end lock_statuses

    /**
     * Get the notes of the order.
     *
     * @return App\Models\OrderNote
     */
    public function notes()
    {
        return $this->hasMany(OrderNote::class);
    } //end notes()

    /**
     * Add services to the order.
     *
     * @param  array $serviceIds
     * @return void
     */
    public function addServices($serviceIds)
    {
        if ($this->services->count()) {
            $this->services()->detach();
        }

        foreach ($serviceIds as $serviceId) {
            $service = Service::find($serviceId);
            if ($service) {
                $this->services()->attach(
                    $service->id,
                    ['price' => $this->licensePlateNumber->vehicle->servicePrice($serviceId)]
                );
            }
        }
    } //end addServices()

    /**
     * Add note to the order.
     *
     * @param  text $note
     * @return App\Models
     */
    public function addNote($note)
    {
        return $this->notes()->create(
            [
                'note' => $note,
                'user_id' => auth()->user()->id,
            ]
        );
    } //end addNote()

    /**
     * Get note data in a specific format.
     *
     * @return array
     */
    public function notesData()
    {
        $notes = [];
        foreach ($this->notes()->orderBy('id', 'desc')->get() as $key => $note) {
            $notes[$key]['note'] = $note->note;
            $notes[$key]['createdBy'] = User::find($note->user_id)->name;
            $notes[$key]['createdAt'] = $note->created_at->format('d-m-Y h:m:s');
        }
        return $notes;
    } //end notesData()

    /**
     * Add order history.
     *
     * @return void
     */
    public function addHistory($comment = null)
    {
        // To keep the logs we save the order's current details.
        $detail = [
            'user_id' => $this->user->id,
            'address_id' => $this->address_id,
            'license_plate_number_id' => $this->license_plate_number_id,
            'pickup_date' => $this->pickup_date,
            'alternative_pickup_date' => $this->alternative_pickup_date,
            'pickup_time' => $this->pickup_time,
            'pickup_type' => $this->pickup_type,
            'additional_work' => $this->additional_work,
            'total_price' => $this->total_price,
            'service_ids' => $this->services()->select('order_services.service_id', 'order_services.price')->get()->toArray(),
            'status' => $this->status,
        ];

        // If the sr_employee updates the order then he must be authenticated else the client must be updating the order via email.
        $this->histories()->create(
            [
                'detail' => json_encode($detail),
                'comment' => $comment ? $comment : $this->status,
                'created_by_user_id' => auth()->user() ? auth()->user()->id : $this->user_id,
            ]
        );
    } //end addHistory()

    /**
     * Get the garages for the order.
     */
    public function garages()
    {
        return $this->belongsToMany(Garage::class, 'garage_orders', 'order_id', 'garage_id')
            ->withPivot('status', 'id')->withTimestamps();
    } //end garages()

    /**
     * Get the last garages that has accepted the order.
     */
    public function garage()
    {
        return $this->garages()->latest()->first();
    } //end garage()

    /**
     * Save an order
     *
     * @param array $order
     * @return array
     */
    public static function store($order)
    {
        $address = OrderService::getAddress($order['addressId']);
        $licensePlateNumber = OrderService::getLicensePlateNumber($order['licensePlateNumberId']);

        // Check for the existing user. If not found then add it.
        $user = User::createUnique(
            $order['name'], $order['email'], $order['phone'],
            User::CLIENT, $order['salutation']
        );

        // Update Vehicle service prices if required.
        $serviceIds = json_decode($order['serviceIds'], true);
        $vehicle = $licensePlateNumber->vehicle;
        $vehicle->updateServices($serviceIds, true);
        // Get only the serviceIds
        $serviceIds = array_column($serviceIds, 'serviceId');

        // Save new order.
        $totalPrice = OrderService::getTotalPrice($serviceIds, $vehicle->id);
        $thisorder = static::create(
            [
                'user_id' => $user->id,
                'address_id' => $address->id,
                'license_plate_number_id' => $licensePlateNumber->id,
                'pickup_date' => Carbon::parse($order['pickupDate']),
                'alternative_pickup_date' => array_key_exists('alternativePickupDate', $order) ?
                Carbon::parse($order['alternativePickupDate']) : null,
                'pickup_time' => $order['pickupTime'],
                'pickup_type' => $order['pickupType'],
                'additional_work' => $order['additionalWork'],
                'total_price' => $totalPrice,
                'status' => $order['orderStatus'],
            ]
        );

        // Save order services.
        $thisorder->addServices($serviceIds);

        // Add the order to order history.
        $thisorder->addHistory();

        // If order status is quoted_for_client then just send the email.
        // If order status is confirmed_by_client then send the email with PDF attachment.
        if ($thisorder->status == static::QUOTED || $thisorder->status == static::CONFIRMED) {
            $user->sendOrderStatusNotification($thisorder);
        }

        return ['response' => true, 'route' => route('orders.statuses', $thisorder->status)];
    } // end store

    /**
     * Update an order
     *
     * @param array $order
     * @return array
     */
    public function updateOrder($order)
    {

        $address = OrderService::getAddress($order['addressId']);
        $licensePlateNumber = OrderService::getLicensePlateNumber($order['licensePlateNumberId']);
        $comment = null;

        // Set redirect url.
        // If client updates the order then redirect to its corresponding page
        // else if SR employee/admin edits the order then redirect to order statuses page.
        $userType = auth()->user() ? auth()->user()->type : null;
        if (!$userType || $userType == User::CLIENT) {
            $route = route('home');
        } elseif (in_array($userType, [User::SR_EMPLOYEE, User::SR_ADMIN])) {
            $route = route('orders.statuses', $this->status);
        }

        // If the details are just updated or employee requested client for callback then the status of order wouldn't be changed.
        if ($order['orderStatus'] == static::UPDATED || $order['orderStatus'] == static::CALLBACK_REQUESTED) {
            $comment = $order['orderStatus'];
            $order['orderStatus'] = $this->status;
        }

        // Update the order.
        $this->update(
            [
                'address_id' => $address->id,
                'license_plate_number_id' => $licensePlateNumber->id,
                'pickup_date' => Carbon::parse($order['pickupDate']),
                'alternative_pickup_date' => array_key_exists('alternativePickupDate', $order) ?
                Carbon::parse($order['alternativePickupDate']) : null,
                'pickup_time' => $order['pickupTime'],
                'status' => array_key_exists('orderStatus', $order) ? $order['orderStatus'] : $this->status,
                'pickup_type' => array_key_exists('pickupType', $order) ? $order['pickupType'] : $this->pickup_type,
                'additional_work' => array_key_exists('additionalWork', $order) ? $order['additionalWork'] : $this->additional_work,
            ]
        );

        // Check whether the user details need to be updated.
        if (array_key_exists('email', $order)) {
            User::createUnique($order['name'], $order['email'],
                $order['phone'], User::CLIENT, $order['salutation']
            );
        }

        // Check whether the services need to be updated.
        if (array_key_exists('serviceIds', $order)) {
            $serviceIds = json_decode($order['serviceIds'], true);
            $vehicle = $licensePlateNumber->vehicle;
            $vehicle->updateServices($serviceIds, true);
            // Get only the serviceIds
            $serviceIds = array_column($serviceIds, 'serviceId');
            $totalPrice = OrderService::getTotalPrice($serviceIds, $vehicle->id);
            $this->update(['total_price' => $totalPrice]);
            // Save order services.
            $this->addServices($serviceIds);
        }

        // Send callback request else order status notification.
        if ($comment == static::CALLBACK_REQUESTED) {
            $this->user->sendOrderCallbackNotification($this);
        } elseif (in_array($this->status, [static::QUOTED, static::CONFIRMED, static::CANCELLED])
            && $comment != static::UPDATED) {
            $this->user->sendOrderStatusNotification($this);
        }

        // Add the order to order history.
        $this->addHistory($comment);

        return ['response' => true, 'route' => $route];
    } // end updateOrder

    /**
     * Verifies lock status.
     *
     * @return array $json
     */
    public function verifyLock()
    {
        // Check whether the order is confirmed and locked then only the admin or the locked employee can edit/update the order.
        $status = false;
        $user = auth()->user();
        if ($this->locked_status == static::UNLOCKED ||
            ($this->status == static::CONFIRMED &&
                ($user->type == User::SR_ADMIN || $this->lock_statuses->last()->to_user_id == $user->id)
            )
        ) {
            $status = true;
        }
        return $status;
    } // end verifyLock

    /**
     * Update the lock status.
     *
     * @return array $json
     */
    public function lockStatus()
    {
        // If logged in user is admin then byUserId is admin else it the the loggedIn employee.
        $json['response'] = true;
        $user = auth()->user();
        $toUserId = $user->id;
        $byUserId = $user->id;

        if ($this->locked_status == static::UNLOCKED) {
            $this->update(['locked_status' => static::LOCKED]);
            $this->lock_statuses()->create(
                [
                    'status' => static::LOCKED,
                    'to_user_id' => $toUserId,
                    'by_user_id' => $byUserId,
                ]
            );
        } else {
            $this->update(['locked_status' => static::UNLOCKED]);
            $this->lock_statuses()->create(
                [
                    'status' => static::UNLOCKED,
                    'to_user_id' => $this->lock_statuses->last()->to_user_id,
                    'by_user_id' => $byUserId,
                ]
            );
        }

        return $json;
    } // end lockStatus

    /**
     * Complete an order.
     *
     * @param array $data
     * @return array $json
     */
    public function completeOrder($data)
    {
        $json['response'] = true;
        $this->update(['status' => $data['status']]);
        // Store type of cancellation.
        if ($data['status'] == static::CANCELLED) {
            $data['status'] = array_key_exists('type', $data) ? $data['type'] : static::CANCELLED_WITH_NO_CANCELLATION_COST;
        }
        $this->garages()->attach($this->garage()->id, ['status' => $data['status']]);
        $this->addHistory($data['status']);

        return $json;
    } // end completeOrder

    /**
     * Update price of an order due to additional work.
     *
     * @param array $data
     * @return array $json
     */
    public function updatePrice($data)
    {
        $json['response'] = true;
        $json['message'] = Lang::get('globals.price_updated');

        $file = $data['invoice'];
        $ext = $file->getClientOriginalExtension();
        $name = 'invoice_' . $this->id . ".$ext";
        $this->update(
            [
                'total_price' => $data['totalPrice'],
                'additional_work' => $data['additionalWork'],
                'invoice' => $name,
            ]
        );
        $this->addHistory();
        if ($data['note']) {
            $this->addNote($data['note']);
        }
        // Save Invoice
        Storage::disk('invoice')->put($name, file_get_contents($file));
        return $json;
    } // end updatePrice

    /**
     * The order data based on status.
     *
     * @param string $status
     * @return array
     */
    public static function ordersData($status, $orderId = null)
    {
        $orders = static::leftjoin('order_notes', 'orders.id', 'order_notes.order_id')
            ->join('license_plate_numbers', 'orders.license_plate_number_id', 'license_plate_numbers.id')
            ->join('vehicles', 'license_plate_numbers.vehicle_id', 'vehicles.id')
            ->join('brands', 'vehicles.brand_id', 'brands.id')
            ->join('addresses', 'orders.address_id', 'addresses.id')
            ->join('postal_codes', 'addresses.postal_code_id', 'postal_codes.id')
            ->join('cities', 'postal_codes.city_id', 'cities.id')
            ->join('users', 'orders.user_id', 'users.id')
            ->leftjoin('lock_orders', 'orders.id', 'lock_orders.order_id')
            ->leftjoin('order_services', 'orders.id', 'order_services.order_id')
            ->leftjoin('services', 'order_services.service_id', 'services.id')
            ->leftjoin('garage_orders', function ($join) {
                $join->on('orders.id', 'garage_orders.order_id')
                    ->where('garage_orders.status', static::COMPLETED);
            });
        if ($orderId) {
            $orders = $orders->where('orders.id', $orderId);
        }
        $orders = $orders->where('orders.status', $status)
            ->groupBy('orders.id')
            ->groupBy('garage_orders.created_at')
            ->select(
                'orders.id',
                'orders.status',
                'orders.locked_status',
                DB::raw('COUNT(DISTINCT order_notes.id) as notes'),
                'license_plate_numbers.license_plate_number',
                'vehicles.model',
                'brands.name',
                'addresses.complete_address',
                'addresses.house_number',
                'postal_codes.postal_code',
                'cities.name as city',
                'orders.pickup_date',
                'orders.pickup_time',
                'orders.total_price',
                'orders.additional_work',
                'users.phone',
                'garage_orders.created_at',
                DB::raw('GROUP_CONCAT(DISTINCT services.name SEPARATOR " | ") as services'),
                DB::raw('GROUP_CONCAT(DISTINCT order_services.price  SEPARATOR " | ") as servicePrices')
            );
        return $orders;
    } // end ordersData

    /**
     * Check and update client lat and lang
     *
     * @return void
     */
    public function checkClientLatLng()
    {
        $address = $this->address;
        if (!($address->latitude && $address->longitude)) {
            $latLang = Curl::fetchLatLang($address->postalCode->postal_code);
            $address->update(['latitude' => $latLang['lat'], 'longitude' => $latLang['lng']]);
        }
    }
}
