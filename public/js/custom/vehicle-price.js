$(function(){
	const UPDATE_VEHICLE_PRICE_ROUTE = $('#updateVehicleServicePrice').val();
	const INPUT_SAVED = $('#inputSavedTxt').val();
	const INPUT_NOT_SAVED = $('#inputNotSavedTxt').val();
	var inputSavedTimerTimer =	null;   //timer identifier
	var doneTypingInterval 	= 	1500;   //time in ms
	var requestArray = [];
	var pushRequestStatus = false;

	$.fn.loadVehicles = () => {
		$.fn.ajaxBlockUI();
		var model = $('#vehicleId').val();
		var licensePlateNumber = $('#licensePlateNumber').val();
		var homeRoute = $('#homeRoute').val();
		var url = `${homeRoute}/vehicles/prices-data/${model}/${licensePlateNumber}`;
		$.fn.ajaxCall(REQUEST_TYPE_GET, url, {}, DATA_TYPE_JSON).then((successResponse) => {
			if (successResponse.response) {
				$('#vehicleData').html(successResponse.data);
				$.unblockUI();
			} else {
				window.location = successResponse.redirect;
			}
		}, (errorResponse) => {
				$.unblockUI();
				$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	};

	$.fn.updatePrice = () => {
		pushRequestStatus = true;
		var thisInpt = requestArray.shift();
		var parent = thisInpt.parent('td');
		var data = {
			"vehicleId": parent.attr('data-vehicleId'),
			"service": JSON.stringify([{
				"serviceId": parent.attr('data-serviceId'),
                "price": parent.find('.servicePrice').val(),
                "dieselPercentage": parent.find('.dieselPercentage').val()
			}])
		};
		$.fn.ajaxCall(REQUEST_TYPE_POST, UPDATE_VEHICLE_PRICE_ROUTE, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			$.fn.loadVehicles();
			if(requestArray.length){
				$.fn.updatePrice()
			} else {
				pushRequestStatus = false;
				if (inputSavedTimerTimer) 
			    {
			    	// Clear if already set
			    	window.clearTimeout(inputSavedTimerTimer);  
			    }
		    	inputSavedTimerTimer = window.setTimeout(function(){
		    		$.blockUI({ message: INPUT_SAVED, timeout: 1500,baseZ: 1005 });
		    	}, doneTypingInterval);
			}
		}, (errorResponse) => {
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	};

	$(document).on('keyup', '.serviceInpt', function(){
		var thisInpt = $(this);
		if (thisInpt.attr('typingTimer') != undefined) 
	    {
	    	// Clear if already set
	    	window.clearTimeout(thisInpt.attr('typingTimer'));  
	    }
		if (thisInpt.val()) {
	    	thisInpt.attr('typingTimer', window.setTimeout(function(){
	    		requestArray.push(thisInpt);
		    	if(!pushRequestStatus) {
		    		$.fn.updatePrice();
		    	}
	    	}, doneTypingInterval));
		}
	});

	$(window).unload(function(){
		if (requestArray.length) {
			$.blockUI({ message: INPUT_NOT_SAVED, timeout: 5000,baseZ: 1005 });
			return false;
		} else {
			return true;
		}
	});

	$.fn.loadVehicles();
});