<?php

namespace App\Http\Middleware;

use App\Models\Order;
use Carbon\Carbon;
use Closure;
use Lang;

class VerifyQuotedOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $order = Order::where('verification_token', request()->route('token'))->first();
        // Validity of a quoted order is maximum for 30 days.
        if ($order && $order->status == Order::QUOTED) {
            if (Carbon::now()->diffInDays($order->created_at) > Order::TOKEN_EXPIRY_DAYS) {
                $response = redirect()->route('home')->withErrors(Lang::get('globals.link_expired'));
            } else {
                $response = $next($request);
            }
            return $response;
        }

        return redirect()->route('home')->withErrors(Lang::get('globals.request_error_message'));
    }
}
