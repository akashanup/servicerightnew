<?php

namespace App\Exceptions;

use Exception;
use Lang;

class ServiceNotFoundException extends Exception
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        $json = [
            "response" => false,
            "errors" => ["error" => [Lang::get('globals.service_not_found')]],
        ];
        return response()->json($json, 422);
    }
}
