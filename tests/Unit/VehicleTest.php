<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Models\Vehicle;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class VehicleTest extends UnitTestCase
{

    /**
     * Vehicle createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueFirst()
    {
        $faker = Faker::create();
        $vehicle = Vehicle::whereNotNull('parent_id')->inRandomOrder()->first();
        $data = Vehicle::createUnique($vehicle->brand_id, $vehicle->fuel_type,
            $vehicle->model, $vehicle->build_year_start);
        $this->assertEquals($vehicle->id, $data->id);
    }

    /**
     * Vehicle createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueSecond()
    {
        $faker = Faker::create();
        $vehicle = Vehicle::whereNull('parent_id')->inRandomOrder()->first();
        $data = Vehicle::createUnique($vehicle->brand_id, $vehicle->fuel_type,
            $vehicle->model, $vehicle->build_year_start);
        $this->assertEquals($vehicle->id, $data->parent_id);
    }

    /**
     * Vehicle createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueThird()
    {
        $faker = Faker::create();
        $brandId = Brand::inRandomOrder()->first()->id;
        $fuelType = 'benzine';
        $model = $faker->word;
        $buildYearStart = Carbon::now()->toDateString();

        $data = Vehicle::createUnique($brandId, $fuelType, $model, $buildYearStart);
        $this->assertEquals(
            array($brandId, $fuelType, $model, $buildYearStart),
            array($data->brand_id, $data->fuel_type, $data->model, $data->build_year_start)
        );
    }

    /**
     * Vehicle updateServices test example.
     *
     * @return void
     */
    public function testUpdateServicesFirst()
    {
        $faker = Faker::create();
        $price1 = $faker->numberBetween($min = 100, $max = 500);
        $price2 = $faker->numberBetween($min = 100, $max = 500);
        $price3 = $faker->numberBetween($min = 100, $max = 500);
        $price4 = $faker->numberBetween($min = 100, $max = 500);
        $price5 = $faker->numberBetween($min = 100, $max = 500);
        $testServiceIds = array(
            '1' => $price1, '2' => $price2, '3' => $price3,
            '4' => $price4, '5' => $price5,
        );
        $serviceIds = array(
            array(
                'serviceId' => '1',
                'price' => $price1,
            ),
            array(
                'serviceId' => '2',
                'price' => $price2,
            ),
            array(
                'serviceId' => '3',
                'price' => $price3,
            ),
            array(
                'serviceId' => '4',
                'price' => $price4,
            ),
            array(
                'serviceId' => '5',
                'price' => $price5,
            ),
        );

        $vehicle = Vehicle::whereNull('parent_id')->inRandomOrder()->first();
        $vehicle->updateServices($serviceIds);
        $this->assertEquals(
            $testServiceIds,
            json_decode($vehicle->services, true)
        );
    }

    /**
     * Vehicle updateServices test example.
     *
     * @return void
     */
    public function testUpdateServicesSecond()
    {
        $faker = Faker::create();
        $price1 = $faker->numberBetween($min = 100, $max = 500);
        $price2 = $faker->numberBetween($min = 100, $max = 500);
        $price3 = $faker->numberBetween($min = 100, $max = 500);
        $price4 = $faker->numberBetween($min = 100, $max = 500);
        $price5 = $faker->numberBetween($min = 100, $max = 500);
        $testServiceIds = array(
            '1' => $price1, '2' => $price2, '3' => $price3,
            '4' => $price4, '5' => $price5,
        );
        $serviceIds = array(
            array(
                'serviceId' => '1',
                'price' => $price1,
            ),
            array(
                'serviceId' => '2',
                'price' => $price2,
            ),
            array(
                'serviceId' => '3',
                'price' => $price3,
            ),
            array(
                'serviceId' => '4',
                'price' => $price4,
            ),
            array(
                'serviceId' => '5',
                'price' => $price5,
            ),
        );

        $vehicle = Vehicle::whereNotNull('parent_id')->inRandomOrder()->first();
        $vehicle->updateServices($serviceIds);
        $this->assertEquals(
            $testServiceIds,
            json_decode($vehicle->services, true)
        );
    }

    /**
     * Vehicle servicePrice test example.
     *
     * @return void
     */
    public function testServicePrice()
    {
        $vehicle = Vehicle::inRandomOrder()->first();
        $serviceId = Faker::create()->numberBetween($min = 1, $max = 5);
        $services = json_decode($vehicle->services, true);
        $servicePrice = $services[$serviceId];
        $this->assertEquals(
            $servicePrice,
            $vehicle->servicePrice($serviceId)
        );
    }

    /**
     * Vehicle zeroservicePrice test example.
     *
     * @return void
     */
    public function testZeroservicePrice()
    {
        Vehicle::whereNull('parent_id')->inRandomOrder()->first()
            ->update(['services' => '{"1": 0, "2": 0, "3": 0, "4": 0, "5": 0}']);
        $vehicles = Vehicle::whereNull('parent_id')
            ->where('services', '{"1": 0, "2": 0, "3": 0, "4": 0, "5": 0}');
        $data = Vehicle::zeroservicePrice();
        \Log::info($data);
        $this->assertEquals($vehicles->count() + 1, sizeof($data));
    }

    /**
     * Vehicle getBaseModel test example.
     *
     * @return void
     */
    public function testGetBaseModelFirst()
    {
        $vehicle = Vehicle::whereNull('parent_id')->inRandomOrder()->first();
        $data = Vehicle::getBaseModel($vehicle->id);
        $this->assertEquals($vehicle->id, $data->id);
    }

    /**
     * Vehicle getBaseModel test example.
     *
     * @return void
     */
    public function testGetBaseModelSecond()
    {
        $vehicle = Vehicle::whereNotNull('parent_id')->inRandomOrder()->first();
        $data = Vehicle::getBaseModel($vehicle->id);
        $this->assertEquals($vehicle->parent_id, $data->id);
    }
}
