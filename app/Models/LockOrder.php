<?php

namespace App\Models;

use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class LockOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'status', 'to_user_id', 'by_user_id',
    ];

    /**
     * Get the order for the lock.
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
