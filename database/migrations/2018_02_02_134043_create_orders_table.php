<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('address_id');
            $table->unsignedInteger('license_plate_number_id');
            $table->date('pickup_date');
            $table->date('alternative_pickup_date')->nullable();
            $table->string('pickup_time');
            $table->enum('pickup_type', ['pickup_from_client', 'client_brings_itself', 'repair_at_client_location']);
            $table->text('additional_work')->nullable();
            $table->decimal('total_price')->default(0.00);
            $table->enum('status', ['drafted', 'quoted', 'confirmed', 'cancelled', 'removed', 'waiting_for_garage', 'confirmed_by_garage', 'pending', 'in_process', 'complete']);
            $table->string('verification_token')->nullable(); // Can't keep it unique as it would be used only for quoted orders and would be null for others.
            $table->enum('client_verified', ['yes', 'no'])->default('yes');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('license_plate_number_id')->references('id')->on('license_plate_numbers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropForeign('orders_address_id_foreign');
            $table->dropColumn('address_id');
            $table->dropForeign('orders_license_plate_number_id_foreign');
            $table->dropColumn('license_plate_number_id');
        });
        Schema::dropIfExists('orders');
    }
}
