<?php

namespace App\Models;

use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the vehicles of the brand.
     *
     * @return Collection
     */
    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    } //end vehicles()

    /**
     * Create a brand if its not found.
     *
     * @param string $brand
     * @return void
     */
    public static function createUnique($brand)
    {
        $thisBrand = static::where('name', $brand)->first();
        if (!$thisBrand) {
            $thisBrand = static::create(
                [
                    'name' => $brand,
                ]
            );
        }
        return $thisBrand;
    } //end createUnique()
}
