@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $salutation }} {{ $name }},
	<br/>
	<br/>
	{{ __('employee-assign-order.on') }} {{ $pickupDate }} {{__('employee-order-status.email_text_1') }} {{ $licensePlateNumber }} {{__('employee-order-status.email_text_2') }} {{ $garageName }}.
	<br/>
	{{__('employee-order-status.email_text_3') }}
	<br/><br/>
	<a href="#" style="color:#222; margin:5px 0; padding:15px; background:#ffc800; display:inline-block;" target="_blank">{{__('employee-order-status.email_text_3') }} &raquo;</a>
@endsection
