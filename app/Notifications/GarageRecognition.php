<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GarageRecognition extends Notification
{
    use Queueable;

    /**
     * The garage.
     *
     * @var App\Models
     */
    public $garage;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($garage)
    {
        $this->garage = $garage;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $garage = $this->garage;
        return (new MailMessage)
            ->view('emails.garage-recognition', [
                'title' => '',
                'garageName' => $garage->name,
                'garageId' => $garage->id,
                'garageCity' => $garage->address->postalCode->city->name,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
