@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $salutation }} {{ $name }},
	<br/>
	<br/>
	{{ __('employee-place-order.order_cancel_text_1') }} {{ $orderId }}
	<br/>
	<br/>
	<i>
		{{ __('employee-place-order.order_cancel_text_2') }} <a href="{{route('home')}}" target="_blank">{{ __('employee-place-order.order_cancel_text_3') }}</a>
		<br>
		{{ __('employee-place-order.order_cancel_text_4') }}: {{ config('constants.CUSTOMER_CARE_NUMBER') }}.
	</i>

@endsection
