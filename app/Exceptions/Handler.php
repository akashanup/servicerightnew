<?php

namespace App\Exceptions;

use App\Exceptions\AddressNotFoundException;
use App\Exceptions\LicensePlateNumberNotFoundException;
use App\Exceptions\PhoneNumberAlreadyExistsException;
use App\Exceptions\ServiceNotFoundException;
use App\Exceptions\VehicleNotFoundException;
use App\Mail\ExceptionEmail;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Mail;
use Lang;
use Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [

        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        // \Illuminate\Validation\ValidationException::class,
        \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if (config('app.env') != config('constants.LOCAL') && $this->shouldReport($exception)) {
            $request = json_encode(request()->input());
            $developers = json_decode(config('app.developers'), true);
            Mail::to(current($developers))->cc($developers)->send(new ExceptionEmail($exception, $request));
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->ajax() && !($exception instanceof \Illuminate\Validation\ValidationException)
            && !($exception instanceof LicensePlateNumberNotFoundException)
            && !($exception instanceof AddressNotFoundException)
            && !($exception instanceof PhoneNumberAlreadyExistsException)
            && !($exception instanceof ServiceNotFoundException)
            && !($exception instanceof VehicleNotFoundException)) {
            $json = [
                "response" => false,
                "errors" => ["error" => [Lang::get('globals.request_error_message')]],
            ];
            return response()->json($json, 500);
        }
        return parent::render($request, $exception);
    }
}
