<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyLicensePlateNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('license_plate_numbers', function (Blueprint $table) {
            // Since char data type isn't present in \Doctrine\DBAL\Types\Type.
            // DB::statement is a workaround for this.
            DB::statement('ALTER TABLE license_plate_numbers MODIFY COLUMN license_plate_number char(8)');
            DB::statement('CREATE UNIQUE INDEX license_plate_numbers_license_plate_number_unique ON license_plate_numbers (license_plate_number)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('license_plate_numbers', function (Blueprint $table) {
            $table->dropUnique('license_plate_numbers_license_plate_number_unique');
            $table->string('license_plate_number', 255)->change();
        });
    }
}
