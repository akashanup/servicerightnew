<?php

return [
"manage_price" => "Prijzen beheren",
"note" => "Let op",
"there_are" => "Er zijn",
"brands_without_prices" => "merken zonder prijzen",
"search_brand" => "Zoek prijzen op Merk",
"search_license_plate" => "Zoek prijzen op kenteken",
"edit_price" => "Bewerk prijzen",
"manage" => "beheren",
"basic_prices" => "Basisprijzen",
"text1" => "Als er geen prijzen zijn voor een specifiek bouwjaar dan valt het systeem terug op de basisprijzen. Zorg dus dat deze in ieder geval zo veel mogelijk zijn ingevuld.",
"text2" => "Voor dieselautos wordt een toeslag berekend aan de hand van het percentage wat onder elke dienst staat. Standaard is dit 5%.",
"text3" => "Wanneer prijzen voor dit bouwjaar verschillen van de basisprijzen dienen de prijzen hier te worden ingevuld. Als een prijs niet is ingevuld valt het systeem terug op de basisprijs.",
"waterpump" => "Inclusief waterpomp?",
];