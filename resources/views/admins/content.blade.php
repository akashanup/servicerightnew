@extends('layouts.app')

@section('stylesheets')
	@yield('admin-css')
@endsection

@section('content')
    @include('admins.header')
    	@include('errors')
        @yield('admin-content')
    @include('admins.footer')
@endsection

@section('scripts')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    @yield('admin-scripts')
@endsection
