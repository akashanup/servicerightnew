<?php

use App\Models\Company;
use App\Models\Garage;
use App\Models\GarageCompany;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(GarageCompany::class, function (Faker $faker) {
    return [
        'company_id' => Company::inRandomOrder()->first()->id,
        'garage_id' => Garage::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
