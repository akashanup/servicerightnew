<?php

namespace App\Console\Commands;

use App\Models\Garage;
use Illuminate\Console\Command;

class GarageActivationReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'garage:remind-activation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder email to garage to activate their account.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Deleted garages' status is inactive.
        $garages = Garage::where('status', Garage::ACTIVE)->where('membership_status', Garage::INACTIVE)->get();
        foreach ($garages as $garage) {
            // Expired membership of a garage won't have null value in start_date.
            $membership = $garage->memberships()->whereNull('start_date')->latest()->first();
            if ($membership) {
                // Send Reminder mail.
                $garage->user->sendGarageActivationReminder($garage);
            }
        }
    }
}
