<?php

use App\Models\Order;
use App\Models\OrderService;
use App\Models\Service;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(OrderService::class, function (Faker $faker) {
    return [
        'price' => $faker->numberBetween($min = 1, $max = 200),
        'order_id' => Order::inRandomOrder()->first()->id,
        'service_id' => Service::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
