@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $salutation }} {{ $name }},
	<br/>
	<br/>
	<div style="padding:15px; background:#dff395; border:1px dashed #95b41c; text-align:center;">
		<b>{{ __('manage-garage-email.email_text_20') }}<br/>{{ __('manage-garage-email.email_text_21') }}:</b>
		<br/><br/>
	    <a href="{{ config('app.url') }}{{ config('app.fallback_locale') }}/garages/activate-membership/{{ $token }}" style="padding:15px; font-size:14px; text-decoration:none; color:white; display:inline-block; background:#95b41c;" target="_blank">
	    	{{ __('globals.login') }} &raquo;
	    </a>
	    <br/><br/>
	    <strong> {{ __('manage-garage.company') }}: {{$garageId}}</strong><br/>
		<strong>{{ __('globals.password') }}: {{$password}}</strong><br/>
	</div>
	<br>
	<h2>{{ __('manage-garage-email.email_text_6') }}:</h2>
	&bull; {{ __('manage-garage-email.email_text_7') }}<br/>
	&bull; {{ __('manage-garage-email.email_text_8') }}<br/>
	&bull; {{ __('manage-garage-email.email_text_9') }}<br/>
	&bull; {{ __('manage-garage-email.email_text_10') }}<br/>
	&bull; {{ __('manage-garage-email.email_text_11') }}<br/>
	&bull; No-cure no pay ({{config('constants.PROVISION')}}{{ __('manage-garage-email.email_text_12') }})<br/>
	<br/><br/>

	{{ __('manage-garage-email.email_text_15') }} {{ config('constants.CUSTOMER_CARE_NUMBER') }} {{ __('manage-garage-email.email_text_16') }} {{ config('constants.CUSTOMER_CARE_EMAIL') }}.
@endsection
