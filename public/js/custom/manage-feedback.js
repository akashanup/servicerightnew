$(function(){
	const FEEDBACK_DATA_URL = $('#feedbackDataRoute').val();
	const FEEDBACK_UPDATE_URL = $('#feedbackUpdateRoute').val();
	
	$.fn.loadFeedback = (draw = 1) => {
		$.fn.ajaxBlockUI();
		$.fn.ajaxCall(REQUEST_TYPE_GET, `${FEEDBACK_DATA_URL}/${draw}`, {}, DATA_TYPE_JSON).then((successResponse) => {
			$('#feedbackData').html(successResponse.feedback);
			$(document).find('.popp').popover();
			$.unblockUI();
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000, baseZ: 1005 });
		});
	};

	$(document).on('click', '.feedbackPaginateBtn', function(){
		$.fn.loadFeedback($(this).attr('data-draw'));
	});

	$(document).on('click', '.feedbackBtn', function(){
		$.fn.ajaxBlockUI();
		var thisBtn = $(this);
		var feedbackId = thisBtn.attr('data-id');
		var data = {
			"feedbackId": thisBtn.attr('data-id'),
			"type": thisBtn.attr('data-type'),
			"socialMediaPost": thisBtn.parent().parent().find('input[type=checkbox]').is(':checked'),
		};
		$.fn.ajaxCall(REQUEST_TYPE_POST, FEEDBACK_UPDATE_URL, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			$.unblockUI();
			$.fn.loadFeedback();
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});

	$.fn.loadFeedback();
});