<?php

use App\Models\PostalCode;
use Illuminate\Database\Seeder;

class TestPostalCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PostalCode::class, 40)->create();
    }
}
