<?php

namespace Tests\Unit;

use App\Models\Country;
use App\Models\State;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class StateTest extends UnitTestCase
{
    /**
     * State createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueFirst()
    {
        $faker = Faker::create();
        $country = Country::inRandomOrder()->first();
        $state = $faker->state;
        $data = State::createUnique($country->id, $state);
        $state = State::where('country_id', $country->id)
            ->where('name', $state)->first();
        $this->assertEquals($state->id, $data->id);
    }

    /**
     * State createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueSecond()
    {
        $state = State::inRandomOrder()->first();
        $data = State::createUnique($state->country_id, $state->name);
        $this->assertEquals($state->id, $data->id);
    }
}
