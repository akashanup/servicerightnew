<?php

namespace Tests\Unit;

use App\Models\City;
use App\Models\PostalCode;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class PostalCodeTest extends UnitTestCase
{
    /**
     * PostalCode createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueFirst()
    {
        $faker = Faker::create();
        $city = City::inRandomOrder()->first();
        $postalCode = $faker->postcode;
        $data = PostalCode::createUnique($city->id, $postalCode);
        $postalCode = PostalCode::where('city_id', $city->id)
            ->where('postal_code', $postalCode)->first();
        $this->assertEquals($postalCode->id, $data->id);
    }

    /**
     * PostalCode createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueSecond()
    {
        $postalCode = PostalCode::inRandomOrder()->first();
        $data = PostalCode::createUnique($postalCode->city_id, $postalCode->postal_code);
        $this->assertEquals($postalCode->id, $data->id);
    }
}
