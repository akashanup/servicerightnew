<?php

namespace Tests\Unit;

use App\Models\Brand;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class BrandTest extends UnitTestCase
{
    /**
     * Brand createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueForExistingBrand()
    {
        $brand = Brand::inRandomOrder()->first();
        $data = Brand::createUnique($brand->name);
        $this->assertEquals($brand->id, $data->id);
    }

    /**
     * Brand createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueForNonExistingBrand()
    {
        $faker = Faker::create();
        $name = $faker->word;
        $data = Brand::createUnique($name);
        $brand = Brand::where('name', $name)->first();
        $this->assertEquals($brand->id, $data->id);
    }
}
