<?php

use App\Models\Feedback;
use App\Models\Order;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Feedback::class, function (Faker $faker) {
    return [
        'rating' => $faker->numberBetween($min = 1, $max = 5),
        'review' => $faker->text,
        'status' => 'new',
        'order_id' => Order::where('status', Order::COMPLETED)->inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
