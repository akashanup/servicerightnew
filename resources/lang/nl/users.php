<?php

return [
"inactive_account" => "Uw account is inactief. Neem contact op met onze ondersteuningsmedewerkers.",
"last_login" => "Laatst ingelogd",
"nickname" => "Bijnaam",
"active" => "Actief",
"inactive" => "Inactief",
"admin" => "Beheerder",
"employee" => "Werknemer",
"password" => "Wachtwoord",
"add_staff" => "Voeg personeel toe",
];