<?php

use Illuminate\Database\Seeder;

class TestCountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert(
            [
                'name' => 'Netherlands',
            ]
        );
    }
}
