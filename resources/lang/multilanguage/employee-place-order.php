<?php

include_once 'generate-file.php';

$dictionary = [
    'schedule_new_assignment' => [
        'en' => 'Schedule a new assignment',
        'nl' => 'Nieuwe opdracht inplannen',
    ],
    'customer_data' => [
        'en' => 'Customer data',
        'nl' => 'Gegevens klant',
    ],
    'salutation' => [
        'en' => 'Salutation',
        'nl' => 'Aanhef',
    ],
    'mr' => [
        'en' => 'Mr',
        'nl' => 'Dhr',
    ],
    'miss' => [
        'en' => 'Miss',
        'nl' => 'Mevr',
    ],
    'name' => [
        'en' => 'Name',
        'nl' => 'Naam',
    ],
    'phone_number' => [
        'en' => 'Phone number',
        'nl' => 'Telefoonnummer',
    ],
    'address' => [
        'en' => 'Address',
        'nl' => 'Adres',
    ],
    'city' => [
        'en' => 'City',
        'nl' => 'Plaatsnaam',
    ],
    'data_auto' => [
        'en' => 'Data Auto',
        'nl' => 'Gegevens Auto',
    ],
    'license_plate' => [
        'en' => 'License Plate',
        'nl' => 'Kenteken',
    ],
    'pick_up' => [
        'en' => 'Pick up',
        'nl' => 'Ophalen',
    ],
    'brand' => [
        'en' => 'Brand',
        'nl' => 'Merk',
    ],
    'select' => [
        'en' => 'Select',
        'nl' => 'Selecteer',
    ],
    'fuel' => [
        'en' => 'Fuel',
        'nl' => 'Brandstof',
    ],
    'construction_year' => [
        'en' => 'Construction year',
        'nl' => 'Bouwjaar',
    ],
    'data_maintenance' => [
        'en' => 'Data maintenance',
        'nl' => 'Gegevens onderhoud',
    ],
    'maintenance' => [
        'en' => 'Maintenance',
        'nl' => 'Onderhoud',
    ],
    'overview' => [
        'en' => 'Overview',
        'nl' => 'Overzicht',
    ],
    'date' => [
        'en' => 'Date',
        'nl' => 'Datum',
    ],
    'alternative' => [
        'en' => 'Alternative',
        'nl' => 'Alternatieve',
    ],
    'time' => [
        'en' => 'Time',
        'nl' => 'Tijd',
    ],
    'pick_up_bring' => [
        'en' => 'Pick up / bring',
        'nl' => 'Ophalen/brengen',
    ],
    'comments_additional_work' => [
        'en' => 'Comments / additional work',
        'nl' => 'Opmerkingen/meerwerk',
    ],
    'send_as_an_offer' => [
        'en' => 'Send as an offer',
        'nl' => 'Toesturen als offerte',
    ],
    'save_as_draft' => [
        'en' => 'Save as draft',
        'nl' => 'Opslaan als concept',
    ],
    'total_price' => [
        'en' => 'Total price',
        'nl' => 'Totaalprijs',
    ],
    'car_tires' => [
        'en' => 'Car tires',
        'nl' => 'Autobanden',
    ],
    'damage_repair' => [
        'en' => 'Damage repair',
        'nl' => 'Schadeherstel',
    ],
    'remaining' => [
        'en' => 'Remaining',
        'nl' => 'Overig',
    ],
    'pickup_from_client' => [
        'en' => 'Pick up car from the customer.',
        'nl' => 'Auto ophalen bij klant.',
    ],
    'client_brings_itself' => [
        'en' => 'Customer brings car itself.',
        'nl' => 'Klant brengt auto zelf.',
    ],
    'repair_at_client_location' => [
        'en' => 'Repair at customer location.',
        'nl' => 'Reparatie bij klant op locatie.',
    ],
    'order_quote_text_1' => [
        'en' => 'Below I will give you the offer for the maintenance of your',
        'nl' => 'Onderstaand breng ik u de offerte uit betreft het onderhoud aan uw',
    ],
    'order_quote_text_2' => [
        'en' => 'with the license plate',
        'nl' => 'met het kenteken',
    ],
    'order_quote_text_3' => [
        'en' => 'The discount price in this offer is only valid for 7 days. If you want to proceed to repair, you can confirm this quotation online by clicking on the link above.
        A consultant will then contact you to answer any questions for you.',
        'nl' => 'De voordeelprijs in deze offerte is slechts 7 dagen geldig.Indien u over wilt gaan tot reparatie kunt u deze offerte online bevestigen door op de bovenstaande link te klikken.
        Een adviseur zal dan contact met u opnemen om eventuele vragen voor u te beantwoorden.',
    ],
    'order_quote_text_4' => [
        'en' => 'Share the convenience',
        'nl' => 'Deel het gemak',
    ],
    'services_list' => [
        'en' => 'Services list',
        'nl' => 'Diensten Lijst',
    ],
    'confirm_quote' => [
        'en' => 'Confirm quote',
        'nl' => 'Bevestig citaat',
    ],
    'order_status_email_title' => [
        'en' => 'The maintenance of your vehicle is planned.',
        'nl' => 'Het onderhoud van uw voertuig is gepland.',
    ],
    'order_confirm_text_1' => [
        'en' => 'Below the data as you have specified and now known to us.',
        'nl' => 'Onderstaand de gegevens zoals door u opgegeven en nu bij ons bekend.',
    ],
    'order_confirm_text_2' => [
        'en' => 'You will receive a confirmation with the details of the garage as soon as one of our garages has accepted your maintenance.',
        'nl' => 'U ontvangt een bevestiging met de gegevens van de garage zodra eenn van onze garages uw onderhoud heeft geaccepteerd.',
    ],
    'order_confirm_text_3' => [
        'en' => 'Yes, the data in the order is correct.',
        'nl' => 'Ja, de gegevens in de order zijn juist.',
    ],
    'order_confirm_text_4' => [
        'en' => 'A copy of the order can be found in the appendix.',
        'nl' => 'Een kopie van de order vind u in de bijlage.',
    ],
    'order_confirm_text_5' => [
        'en' => 'If you have any questions you can call.',
        'nl' => 'Mocht u vragen hebben kunt u bellen met',
    ],
    'order_confirm_text_6' => [
        'en' => 'Your digital confirmation of the data will speed up the progress of the assignment.
If you prefer to speak to one of our advisers, you do not have to do anything.
One of them will contact you as soon as possible.',
        'nl' => 'Uw digitale bevestiging van de gegevens zal het verloop van de opdracht bespoedigen.
Spreekt u liever nog met één van onze adviseurs dan hoeft u niets te doen.
Eén van hen zal zo spoedig mogelijk contact met u opnemen.',
    ],
    'your_data' => [
        'en' => 'Your data',
        'nl' => 'Uw gegevens',
    ],
    'comments' => [
        'en' => 'Comments',
        'nl' => 'OPMERKINGEN',
    ],
    'car_data' => [
        'en' => 'Car data',
        'nl' => 'Gegevens auto',
    ],
    'order_callback_text_1' => [
        'en' => 'Recently you have scheduled the maintenance for your vehicle with the license plate',
        'nl' => 'Onlangs heeft u het onderhoud voor uw voertuig met het kenteken',
    ],
    'order_callback_text_2' => [
        'en' => 'via our website.',
        'nl' => 'ingepland via onze website.',
    ],
    'order_callback_text_3' => [
        'en' => 'We tried to contact you but unfortunately we did not hear.',
        'nl' => 'Wij hebben proberen contact met u op te nemen maar kregen helaas geen gehoor.',
    ],
    'order_callback_text_4' => [
        'en' => 'We ask you to contact us by telephone via',
        'nl' => 'Wij verzoeken u telefonisch contact met ons op te nemen via',
    ],
    'order_callback_text_5' => [
        'en' => 'or click on the button below to confirm that the information, in the order confirmation sent to you earlier, is correct.',
        'nl' => 'Wof op de onderstaande knop te klikken om te bevestigen dat de gegevens, in de al eerder aan u verstuurde orderbevestiging, juist zijn.',
    ],
    'order_cancel_text_1' => [
        'en' => 'In this case our confirmation concerns the cancellation of your order with order number',
        'nl' => 'Bij deze onze bevestiging betreft de annulering van uw opdracht met order nummer',
    ],
    'order_cancel_text_2' => [
        'en' => 'You can place a new order at any time via',
        'nl' => 'U kunt te allen tijde een nieuwe order plaatsen via',
    ],
    'order_cancel_text_3' => [
        'en' => 'our website',
        'nl' => 'onze website',
    ],
    'order_cancel_text_4' => [
        'en' => 'If you have any questions, our team would be happy to answer them for you',
        'nl' => 'Mocht u vragen hebben dan wil ons team deze graag voor u beantwoorden',
    ],
];

generateFile($dictionary, basename(__FILE__));
