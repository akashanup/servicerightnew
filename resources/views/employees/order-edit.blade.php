@extends('employees.content')

@section('employee-content')
    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ __('employee-edit-order.check') }} #{{ $order->id }} {{ __('employee-edit-order.assignment') }}</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding bg-gray">
        <div class="container">
            <div class="row" id="userDetails">
                <div class="col-md-12">
                    <h3>1. {{ __('employee-place-order.customer_data') }}</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.salutation') }}:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="salutationSelect">
                                @if($user->salutation == 'Mr.')
                                    <option value="Mr." selected>{{ __('employee-place-order.mr') }}.</option>
                                    <option value="Ms.">{{ __('employee-place-order.miss') }}.</option>
                                @else
                                    <option value="Mr.">{{ __('employee-place-order.mr') }}.</option>
                                    <option value="Ms." selected>{{ __('employee-place-order.miss') }}.</option>
                                @endif
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.name') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control required" type="text" placeholder="{{ __('employee-place-order.name') }}..." id="nameInpt" value="{{ $user->name }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.phone_number') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control required" type="tel" placeholder="{{ __('employee-place-order.phone_number') }}..." id="phoneInpt" value="{{ $user->phone }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Email:</label>
                        <div class="col-sm-8">
                            <input class="form-control required" type="email" placeholder="Email..." id="emailInpt"  value="{{ $user->email }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">Country:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="countryInpt">
                                    <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                @foreach($otherCountries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Postcode + nr:</label>
                        <div class="col-sm-6">
                            <input class="form-control required" type="text" placeholder="Postcode..." id="postalCodeInpt" value="{{$postalCode->postal_code}}">
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control homenr required" type="text" placeholder="Nr.." id="houseNumberInpt" value="{{$address->house_number}}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.address') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" placeholder="{{ __('employee-place-order.address') }}..." id="addressInpt" value="{{ $address->complete_address }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.city') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" placeholder="{{ __('employee-place-order.city') }}..." id="cityInpt" value="{{ $city->city }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row" id="vehicleDetails">
                <div class="col-md-12">
                    <h3>2. {{ __('employee-place-order.data_auto') }}</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.license_plate') }}:</label>
                        <div class="col-sm-4">
                            <input class="form-control licence-plate required" type="text" placeholder="{{ __('employee-place-order.license_plate') }}..." id="licensePlateInpt" value=" {{ $licensePlateNumber->license_plate_number }}">
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-primary getCarDetails" id="carPickUpBtn">{{ __('employee-place-order.pick_up') }}</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.brand') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" disabled="disabled" id="carBrandInpt" style="display: none">
                            <select class="form-control car-brand" id="carBrandSelect">
                                <option value="">{{ __('employee-place-order.select') }} {{ __('employee-place-order.brand') }}</option>
                                @foreach($brands as $brand)
                                    @if($vehicle->brand_id == $brand->id)
                                        <option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
                                    @else
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Model:</label>
                        <div class="col-sm-8">
                            <input type="text" id="carModelInpt" class="form-control" disabled="disabled" style="display: none">
                            <select class="form-control car-model" id="carModelSelect">
                                <option value="">{{ __('employee-place-order.select') }} model</option>
                                <option value="{{ $vehicle->id }}" selected="true"> {{ $vehicle->model }} </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.fuel') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" id="carFuelTypelInpt" disabled="disabled" class="form-control" style="display: none">
                            <select class="form-control car-fuel" id="carFuelTypeSelect">
                                <option value="{{ $vehicle->fuel_type }}" selected>{{ $vehicle->fuel_type }}</option>
                                @foreach($fuelTypes as $fuel)
                                    @if($fuel != $vehicle->fuel_type)
                                        <option value="{{ $fuel }}">{{ $fuel }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.construction_year') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control car-build-year required" type="text" placeholder="{{ __('employee-place-order.construction_year') }}..." id="buildYearInpt" value="{{ $vehicle->build_year_start() }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">APK verloopt op:</label>
                        <div class="col-sm-8">
                            <input class="form-control car-apk required" type="text" placeholder="APK verloopt op..." id="mandatoryServiceExpiryDateInpt" value="{{ $licensePlateNumber->mandatory_service_expiry_date() }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding bg-gray">
        <div class="container service-selection">
            <div class="row">
                <h3>3. {{ __('employee-place-order.data_maintenance') }}</h3>
            </div>
            @foreach($parentCategories as $category)
                    <div class="row">
                        <h4 style="margin: 0.5em 0 0.1em 0">{{ $category->name }}</h4>
                        @foreach($category->services as $service)
                            <div class="col-md-4">
                                <div class="form-check">
                                    @if($orderService = $order->services()->where('services.id', $service->id)->first())
                                        <input class="form-check-input serviceCheckBox" checked type="checkbox" value="{{ $service->id }}">
                                    @else
                                        <input class="form-check-input serviceCheckBox" type="checkbox" value="{{ $service->id }}">
                                    @endif
                                    <label class="form-check-label">{{ $service->name }}</label>
                                    <div class="input-group price-hidden">
                                        <div class="row">
                                            <div class="col-md-1 prepend-euro">
                                                &euro;
                                            </div>
                                            <div class="col-md-4">
                                                <input class="form-control form-control-sm item-price servicePrice" type="number" min="0.00" step="0.1" value="{{$orderService?$orderService->pivot->price:'0.00'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row" id="orderDetails">
                <div class="col-md-12">
                    <h3>4. {{ __('employee-place-order.overview') }} &amp; planning</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.date') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control datepicker required" type="tel" value="{{ $order->pickup_date() }}" id="pickupDateInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.alternative') }} {{ __('employee-place-order.date') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control datepicker" type="tel" value="{{ $order->alternative_pickup_date() }}" id="alternativePickupDateInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.time') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control required" type="tel" value="{{ $order->pickup_time }}" id="pickupTimeInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.pick_up_bring') }}:</label>
                        <div class="col-sm-8">
                            <select class="form-control required" id="pickupTypeSelect">
                                <option value="">{{ __('employee-place-order.select') }}</option>
                                @foreach($pickupTypes as $key => $pickupType)
                                    <option value="{{ $key }}" {{ $key == $order->pickup_type ? 'selected' : ''}}>{{ $pickupType }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.comments_additional_work') }}:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="additionalWorkText">{{ $order->additional_work }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    @if($order->status == \App\Models\Order::QUOTED)
                        <button type="button" class="btn btn-primary btn-block orderUpdateBtn" data-type="confirmed">{{ __('globals.confirm') }}</button>
                        <button type="button" class="btn btn-info btn-block orderUpdateBtn" data-type="updated">{{ __('globals.save') }}</button>
                        <button type="button" class="btn btn-secondary btn-block orderUpdateBtn" data-type="callback_requested">{{ __('employee-edit-order.callback_request') }}
                            @if($callbackCount)
                                ({{$callbackCount}})
                            @endif
                        </button>
                        <button type="button" class="btn btn-secondary btn-block orderUpdateBtn" data-type="cancelled">{{ __('globals.cancel') }}</button>
                        <button type="button" class="btn btn-secondary btn-block orderUpdateBtn" data-type="removed">{{ __('globals.remove') }}</button>
                    @else
                        <button type="button" class="btn btn-info btn-block orderUpdateBtn" data-type="updated">{{ __('globals.save') }}</button>
                    @endif
                    <h4>{{ __('employee-place-order.total_price') }}: &euro; <span id="totalPrice">{{ $order->total_price }}</span></h4>
                </div>
            </div>
        </div>
    </section>

    <!-- Hidden fields required for javascript -->
    <input type="hidden" id="fetchAddressRoute" value="{{ route('addresses.client') }}">
    <input type="hidden" id="fetchedAddressId" value="{{ $order->address_id }}">
    <input type="hidden" id="fetchVehicleRoute" value="{{ route('licensePlateNumbers.vehicle') }}">
    <input type="hidden" id="fetchedlicensePlateNumberId" value="{{ $order->license_plate_number_id }}">
    <input type="hidden" id="fetchedServices" value="{{ $vehicle->services }}">
    <input type="hidden" id="updateOrderRoute" value="{{ route('orders.update', $order->id) }}">
    <input type="hidden" id="redirectRoute" value="{{ route('orders.statuses', \App\Models\Order::QUOTED) }}">

    <!-- Scripts -->
    @section('employee-scripts')
        @include('scripts.save-order')
        <script type="text/javascript" src="{{ asset('js/custom/employee-service-selection.js').'?v='.config('app.version', '1') }}"></script>
        <script type="text/javascript" src="{{ asset('js/custom/employee-update-order.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
