@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $garageName }},
	<br/>
	<br/>
	{{__('employee-assign-order.email_text_5') }} (Order ID:{{ $orderId }}).
	<br/>
	<br/>
	<hr/><b>{{__('employee-assign-order.customer_data') }}:</b><hr/>
	{{ $salutation }} {{ $name }}<br/>
	{{ $phone }}<br/>
	{{ $email }}<br/>
	{{ $houseNumber }} {{ $completeAddress }}<br/>
	{{ $postalCode }} {{ $city }}<br/>

	<hr/><b>{{__('employee-assign-order.vehicle_data') }}:</b><hr/>
	{{__('employee-place-order.license_plate') }}: {{ $licensePlateNumber }}<br/>
	{{__('employee-place-order.brand') }}: {{ $brand }}<br/>
	Model: {{ $model }}<br/>

	<hr/><b>{{__('employee-assign-order.maintenance_data') }}:</b><hr/>

	{{__('employee-assign-order.on') }} <b>{{ $pickupDate }}</b><br/>
	{{__('employee-assign-order.pickup_between') }} <b>{{ $pickupTime }}</b><br/>
	<br/>

	{{ $additionalWork }}
	<br/>
	{{ __('employee-place-order.services_list') }}:
	<ul>
		@foreach($servicesList as $service)
			<li>{{ $service }}</li>
		@endforeach
	</ul><br/>

	<small>{{__('employee-assign-order.email_text_8') }}.</small>
	<br/><br/>
	@include('emails.garage-payment-details')
@endsection
