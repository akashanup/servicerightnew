<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGarageCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('garage_id');
            $table->unsignedInteger('category_id');
            $table->timestamps();
            $table->foreign('garage_id')->references('id')->on('garages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garage_categories', function (Blueprint $table) {
            $table->dropForeign('garage_categories_garage_id_foreign');
            $table->dropColumn('garage_id');
            $table->dropForeign('garage_categories_category_id_foreign');
            $table->dropColumn('category_id');
        });
        Schema::dropIfExists('garage_categories');
    }
}
