<?php

include_once 'generate-file.php';

$dictionary = [
    'email_text_1' => [
        'en' => 'As a result of your telephone agreement, the membership for the period of 12 months will, as agreed, receive your',
        'nl' => 'Naar aanleiding van uw telefonisch akkoord betreft de lidmaatschap voor de periode van 12 maanden ontvangt u zoals afgesproken hierbij uw',
    ],
    'email_text_2' => [
        'en' => ' company code and password.',
        'nl' => ' bedrijfscode en wachtwoord.',
    ],
    'email_text_3' => [
        'en' => 'By clicking on log in you agree with the',
        'nl' => 'Door op inloggen te klikken gaat u akkoord met de',
    ],
    'email_text_4' => [
        'en' => 'general terms and conditions for companies',
        'nl' => 'algemene voorwaarden bedrijven',
    ],
    'email_text_5' => [
        'en' => 'and you can immediately get the local assignments',
        'nl' => 'en kunt u direct de locale opdrachten binnen halen',
    ],
    'email_text_6' => [
        'en' => 'Your membership of 12 months contains',
        'nl' => 'Uw lidmaatschap van 12 maanden bevat',
    ],
    'email_text_7' => [
        'en' => 'ServiceRight recognition (A4 format sticker)',
        'nl' => 'ServiceRight erkenning (A4 formaat sticker)',
    ],
    'email_text_8' => [
        'en' => 'Digital ServiceRight recognition (for on your own website)',
        'nl' => 'Digitale ServiceRight erkenning (voor op uw eigen website)',
    ],
    'email_text_9' => [
        'en' => 'Unlimited local assignments',
        'nl' => 'Onbeperkt lokale opdrachten binnenhalen',
    ],
    'email_text_10' => [
        'en' => 'Your own ServiceRight company page',
        'nl' => 'Uw eigen ServiceRight bedrijfspagina',
    ],
    'email_text_11' => [
        'en' => 'Monitoring customer references',
        'nl' => 'Monitoring van klanten referenties',
    ],
    'email_text_12' => [
        'en' => ' commission per executed assignment',
        'nl' => ' provisie per uitgevoerde opdracht',
    ],
    'email_text_13' => [
        'en' => 'You receive from us by e-mail the invoice concerning the contribution a',
        'nl' => 'U ontvangt van ons per e-mail de factuur betreft de contributie a',
    ],
    'email_text_14' => [
        'en' => 'for these 12 months',
        'nl' => 'voor deze 12 maanden',
    ],
    'email_text_15' => [
        'en' => 'If you have any questions regarding the membership you can call',
        'nl' => 'Mocht u nog vragen hebben betreft het lidmaatschap dan kunt u bellen met',
    ],
    'email_text_16' => [
        'en' => 'or e-mail to',
        'nl' => 'of e-mailen naar',
    ],
    'email_text_17' => [
        'en' => 'With this we confirm that your membership has been extended to',
        'nl' => 'Bij deze bevestigen wij dat uw lidmaatschap is verlengd tot en met',
    ],
    'email_text_18' => [
        'en' => 'You can immediately continue with collecting assignments via the ServiceRight online assignments module.
        We wish you many assignments this year too.',
        'nl' => 'U kunt direct weer verder gaan met opdrachten binnenhalen via de ServiceRight online opdrachten module.
        Wij wensen u ook dit jaar weer veel opdrachten toe.',
    ],
    'email_text_19' => [
        'en' => 'Your account is activated. Login to continue.',
        'nl' => 'Uw account is geactiveerd. Log in om verder te gaan.',
    ],
    'email_text_20' => [
        'en' => 'Log in now to accept orders directly',
        'nl' => 'Log nu in om direct opdrachten te accepteren',
    ],
    'email_text_21' => [
        'en' => 'and confirm your membership',
        'nl' => 'en uw lidmaatschap te bevestigen',
    ],
    'email_text_22' => [
        'en' => 'Congratulations, your membership has been confirmed by you',
        'nl' => 'Gefeliciteerd, uw lidmaatschap is door u bevestigd op',
    ],
    'email_text_23' => [
        'en' => 'From now on you can use your login details to retrieve the orders entered via ServiceRight online.',
        'nl' => 'U mag vanaf heden met uw inloggegevens de via ServiceRight ingeboekte opdrachten online binnenhalen.',
    ],
    'email_text_24' => [
        'en' => 'Your membership, ServiceRight company code and password are valid from',
        'nl' => 'Uw lidmaatschap, ServiceRight bedrijfscode en wachtwoord zijn geldig vanaf',
    ],
    'email_text_25' => [
        'en' => 'and valid upto',
        'nl' => 'en lopen tot',
    ],
    'email_text_26' => [
        'en' => 'This means that you will, unless you have the ServiceRight membership for it',
        'nl' => 'Dit betekent dat u, tenzij u de ServiceRight lidmaatschap voor',
    ],
    'email_text_27' => [
        'en' => 'have extends by one year, until the mentioned period can log in unlimited on the ServiceRight website',
        'nl' => 'hebt verlengt met een jaar, tot de genoemde periode onbeperkt kunt inloggen op de website van ServiceRight',
    ],
    'email_text_28' => [
        'en' => 'Your acknowledgment will be received by post within 30 days on the',
        'nl' => 'Uw erkenning zult u binnen 30 dagen per post ontvangen op de',
    ],
    'email_text_29' => [
        'en' => 'We hereby send you written confirmation of the cancellation of your membership at ServiceRight.',
        'nl' => 'Hierbij sturen wij u de schriftelijke bevestiging van de opzegging van uw lidmaatschap bij ServiceRight.',
    ],
    'email_text_30' => [
        'en' => 'Your membership will expire per ',
        'nl' => 'Uw lidmaatschap zal verlopen per ',
    ],
    'email_text_31' => [
        'en' => 'You must still do the following',
        'nl' => 'U dient het volgende nog te doen',
    ],
    'email_text_32' => [
        'en' => 'Removing all placed ServiceRight (digital) approvals.',
        'nl' => 'Het verwijderen van alle geplaatste ServiceRight (digitale)erkenningen.',
    ],
    'email_text_33' => [
        'en' => 'Returning your ServiceRight membership only.',
        'nl' => 'Het retour sturen van uw ServiceRight lidmaatschap pas.',
    ],
    'email_text_34' => [
        'en' => 'The payment of all outstanding invoices.',
        'nl' => 'Het voldoen van alle nog openstaande facturen.',
    ],
    'email_text_35' => [
        'en' => 'Return address',
        'nl' => 'Retouradres',
    ],
    'email_text_36' => [
        'en' => 'Congratulations again with your ServiceRight recognition!',
        'nl' => 'Nogmaals gefeliciteerd met uw ServiceRight erkenning!',
    ],
    'email_text_37' => [
        'en' => 'You will receive the digital version of your ServiceRight recognition.',
        'nl' => 'Hierbij ontvangt u de digitale variant van uw ServiceRight erkenning.',
    ],
    'email_text_38' => [
        'en' => 'By placing this recognition on your website, the visitors immediately see that you meet all service and quality requirements.',
        'nl' => 'Door deze erkenning op uw website te plaatsen zien de bezoekers direct dat u voldoet aan alle ServiceRight gestelde service- en kwaliteits- eisen.',
    ],
    'email_text_39' => [
        'en' => 'They also come to your personal',
        'nl' => 'Tevens komen zij op uw persoonlijke',
    ],
    'email_text_40' => [
        'en' => 'business page',
        'nl' => 'bedrijfspagina',
    ],
    'email_text_41' => [
        'en' => ' and they can immediately become a transparent one ',
        'nl' => ' en kunnen zij direct een transparante ',
    ],
    'email_text_42' => [
        'en' => 'online quote',
        'nl' => 'online offerte',
    ],
    'email_text_43' => [
        'en' => 'see',
        'nl' => 'zien',
    ],
    'email_text_44' => [
        'en' => 'In addition to these benefits, the mission of this visitor is also once again',
        'nl' => 'Bovenop deze voordelen wordt de opdracht van deze bezoeker ook nog eens',
    ],
    'email_text_45' => [
        'en' => 'directly at your company',
        'nl' => 'direct bij uw bedrijf',
    ],
    'email_text_46' => [
        'en' => 'planned',
        'nl' => 'ingepland',
    ],
    'email_text_47' => [
        'en' => 'You only need to copy & paste one of the two codes below into your website',
        'nl' => 'U hoeft enkel &eacute;&eacute;n van de twee onderstaande codes te kopiëren en te plakken in uw website',
    ],
    'email_text_48' => [
        'en' => 'Example',
        'nl' => 'Voorbeeld',
    ],
    'email_text_49' => [
        'en' => 'No idea of websites and codes?',
        'nl' => 'Geen idee van websites en codes?',
    ],
    'email_text_50' => [
        'en' => 'ServiceRight now offers temporarily',
        'nl' => 'ServiceRight biedt nu tijdelijk',
    ],
    'email_text_51' => [
        'en' => 'free integration aid',
        'nl' => 'integratiehulp',
    ],
    'email_text_52' => [
        'en' => 'to',
        'nl' => 'aan',
    ],
    'email_text_53' => [
        'en' => 'Our technical team will place the code completely free on your website',
        'nl' => 'Ons technische team zal de code geheel gratis op uw website plaatsen',
    ],
    'email_text_54' => [
        'en' => 'For more information call',
        'nl' => 'Voor meer informatie bel',
    ],
];

generateFile($dictionary, basename(__FILE__));
