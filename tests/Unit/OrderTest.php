<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\LicensePlateNumber;
use App\Models\Order;
use App\Models\Service;
use App\Models\User;
use DB;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class OrderTest extends UnitTestCase
{
    /**
     * Order addServices test example.
     *
     * @return void
     */
    public function testAddServicesFirst()
    {
        DB::rollback();
        $order = Order::inRandomOrder()->first();
        $initialServices = $order->services()->pluck('services.id')->toArray();
        $serviceIds = array(
            Service::inRandomOrder()->first()->id,
            Service::inRandomOrder()->first()->id,
        );
        $order->addServices($serviceIds);
        $updatedServices = $order->services()->pluck('services.id')->toArray();
        $order->addServices($initialServices);
        $this->assertEquals($serviceIds, $updatedServices);
    }

    /**
     * Order addServices test example.
     *
     * @return void
     */
    public function testAddServicesSecond()
    {
        DB::rollback();
        $order = Order::inRandomOrder()->first();
        $initialServices = $order->services()->pluck('services.id')->toArray();
        $serviceIds = array();
        $order->addServices($serviceIds);
        $updatedServices = $order->services()->pluck('services.id')->toArray();
        $order->addServices($initialServices);
        $this->assertEquals($serviceIds, $updatedServices);
    }

    /**
     * Order notesData test example.
     *
     * @return void
     */
    public function testNotesData()
    {
        $order = Order::inRandomOrder()->first();
        $data = $order->notesData();
        $testData = Order::leftjoin('order_notes', 'order_notes.order_id', 'orders.id')
            ->join('users', 'order_notes.user_id', 'users.id')
            ->where('orders.id', $order->id)
            ->select(
                'order_notes.note as note', 'users.name as createdBy',
                DB::raw('DATE_FORMAT(order_notes.created_at, "%d-%m-%Y %h:%m:%s") as createdAt')
            )
            ->orderBy('orders.id', 'desc')
            ->get()->toArray();
        $this->assertEquals($testData, $data);
    }

    /**
     * Order store test example.
     *
     * @return void
     */
    public function testStoreFirst()
    {
        $faker = Faker::create();
        $user = User::where('type', User::CLIENT)->inRandomOrder()->first();
        $orderData = array(
            'addressId' => Address::inRandomOrder()->first()->id,
            'licensePlateNumberId' => LicensePlateNumber::inRandomOrder()->first()->id,
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'salutation' => $user->salutation,
            'serviceIds' => json_encode(array(
                array(
                    'serviceId' => Service::inRandomOrder()->first()->id,
                    'price' => $faker->numberBetween($min = 100, $max = 500),
                ),
                array(
                    'serviceId' => Service::inRandomOrder()->first()->id,
                    'price' => $faker->numberBetween($min = 100, $max = 500),
                ),
            )),
            'pickupDate' => $faker->date,
            'alternativePickupDate' => $faker->date,
            'pickupTime' => '10:00-12:00',
            'pickupType' => 'client_brings_itself',
            'additionalWork' => $faker->text,
            'orderStatus' => 'drafted',
        );
        $data = Order::store($orderData);
        $this->assertEquals(['response' => true, 'route' => route('orders.statuses', 'drafted')], $data);
    }

    /**
     * Order store test example.
     *
     * @return void
     */
    public function testStoreSecond()
    {
        $faker = Faker::create();
        $orderData = array(
            'addressId' => Address::inRandomOrder()->first()->id,
            'licensePlateNumberId' => LicensePlateNumber::inRandomOrder()->first()->id,
            'name' => $faker->name,
            'email' => $faker->safeEmail,
            'phone' => $faker->e164PhoneNumber,
            'salutation' => 'Mr.',
            'serviceIds' => json_encode(array(
                array(
                    'serviceId' => Service::inRandomOrder()->first()->id,
                    'price' => $faker->numberBetween($min = 100, $max = 500),
                ),
                array(
                    'serviceId' => Service::inRandomOrder()->first()->id,
                    'price' => $faker->numberBetween($min = 100, $max = 500),
                ),
            )),
            'pickupDate' => $faker->date,
            'alternativePickupDate' => $faker->date,
            'pickupTime' => '10:00-12:00',
            'pickupType' => 'client_brings_itself',
            'additionalWork' => $faker->text,
            'orderStatus' => 'drafted',
        );
        $data = Order::store($orderData);
        $this->assertEquals(['response' => true, 'route' => route('orders.statuses', 'drafted')], $data);
    }

    /**
     * Order updateOrder test example.
     *
     * @return void
     */
    public function testUpdateOrderFirst()
    {
        $faker = Faker::create();
        $order = Order::inRandomOrder()->first();
        $userName = $faker->name;
        $userPhone = $faker->e164PhoneNumber;
        $orderData = array(
            'addressId' => Address::inRandomOrder()->first()->id,
            'licensePlateNumberId' => LicensePlateNumber::inRandomOrder()->first()->id,
            'name' => $userName,
            'email' => $order->user->email,
            'phone' => $userPhone,
            'salutation' => 'Mr.',
            'serviceIds' => json_encode(array(
                array(
                    'serviceId' => Service::inRandomOrder()->first()->id,
                    'price' => $faker->numberBetween($min = 100, $max = 500),
                ),
                array(
                    'serviceId' => Service::inRandomOrder()->first()->id,
                    'price' => $faker->numberBetween($min = 100, $max = 500),
                ),
            )),
            'pickupDate' => $faker->date,
            'alternativePickupDate' => $faker->date,
            'pickupTime' => '10:00-12:00',
            'pickupType' => 'client_brings_itself',
            'additionalWork' => $faker->text,
            'orderStatus' => 'quoted',
        );
        $data = $order->updateOrder($orderData);
        $order = Order::find($order->id);
        $this->assertEquals(
            array(
                'route' => route('home'),
                'orderStatus' => 'quoted',
                'userName' => $userName,
                'email' => $order->user->email,
                'phone' => $userPhone,
            ),
            array(
                'route' => $data['route'],
                'orderStatus' => $order->status,
                'userName' => $order->user->name,
                'email' => $order->user->email,
                'phone' => $order->user->phone,
            )
        );
    }

    /**
     * Order updateOrder test example.
     *
     * @return void
     */
    public function testUpdateOrderSecond()
    {
        $faker = Faker::create();
        $order = Order::inRandomOrder()->first();
        $userName = $faker->name;
        $userPhone = $faker->e164PhoneNumber;
        $initialStatus = $order->status;
        $orderData = array(
            'addressId' => Address::inRandomOrder()->first()->id,
            'licensePlateNumberId' => LicensePlateNumber::inRandomOrder()->first()->id,
            'name' => $userName,
            'email' => $order->user->email,
            'phone' => $userPhone,
            'salutation' => 'Mr.',
            'serviceIds' => json_encode(array(
                array(
                    'serviceId' => Service::inRandomOrder()->first()->id,
                    'price' => $faker->numberBetween($min = 100, $max = 500),
                ),
                array(
                    'serviceId' => Service::inRandomOrder()->first()->id,
                    'price' => $faker->numberBetween($min = 100, $max = 500),
                ),
            )),
            'pickupDate' => $faker->date,
            'alternativePickupDate' => $faker->date,
            'pickupTime' => '10:00-12:00',
            'pickupType' => 'client_brings_itself',
            'additionalWork' => $faker->text,
            'orderStatus' => 'callback_requested',
        );
        $data = $order->updateOrder($orderData);
        $order = Order::find($order->id);
        $this->assertEquals(
            array(
                'route' => route('home'),
                'orderStatus' => $initialStatus,
                'userName' => $userName,
                'email' => $order->user->email,
                'phone' => $userPhone,
            ),
            array(
                'route' => $data['route'],
                'orderStatus' => $order->status,
                'userName' => $order->user->name,
                'email' => $order->user->email,
                'phone' => $order->user->phone,
            )
        );
    }

    /**
     * Order updateOrder test example.
     *
     * @return void
     */
    public function testCompleteOrderFirst()
    {
        DB::rollback();
        $order = Order::where('status', 'accepted_by_garage')->inRandomOrder()->first();
        $completedOrderStatus = 'completed';
        $orderData = array(
            'status' => $completedOrderStatus,
        );
        $order->completeOrder($orderData);
        $garageOrder = DB::table('garage_orders')->where('order_id', $order->id)
            ->orderBy('id', 'DESC')->first();
        $finalOrderStatus = $garageOrder->status;
        $orderStatus = $order->status;
        $orderId = $order->id;
        unset($order);
        $order = Order::find($orderId);
        $order->update(['status' => 'accepted_by_garage']);
        DB::table('garage_orders')->delete($garageOrder->id);
        $this->assertEquals(
            array(
                'orderStatus' => $completedOrderStatus,
                'garageOrderStatus' => $completedOrderStatus,
            ),
            array(
                'orderStatus' => $orderStatus,
                'garageOrderStatus' => $finalOrderStatus,
            )
        );
    }

    /**
     * Order updateOrder test example.
     *
     * @return void
     */
    public function testCompleteOrderSecond()
    {
        DB::rollback();
        $order = Order::where('status', 'accepted_by_garage')->inRandomOrder()->first();
        $completedOrderStatus = 'cancelled';
        $orderData = array(
            'status' => $completedOrderStatus,
        );
        $order->completeOrder($orderData);
        $garageOrder = DB::table('garage_orders')->where('order_id', $order->id)
            ->orderBy('id', 'DESC')->first();
        $finalOrderStatus = $garageOrder->status;
        $orderStatus = $order->status;
        $orderId = $order->id;
        unset($order);
        $order = Order::find($orderId);
        $order->update(['status' => 'accepted_by_garage']);
        DB::table('garage_orders')->delete($garageOrder->id);
        $this->assertEquals(
            array(
                'orderStatus' => $completedOrderStatus,
                'garageOrderStatus' => Order::CANCELLED_WITH_NO_CANCELLATION_COST,
            ),
            array(
                'orderStatus' => $orderStatus,
                'garageOrderStatus' => $finalOrderStatus,
            )
        );
    }

    /**
     * Order updateOrder test example.
     *
     * @return void
     */
    public function testCompleteOrderThird()
    {
        DB::rollback();
        $order = Order::where('status', 'accepted_by_garage')->inRandomOrder()->first();
        $completedOrderStatus = 'cancelled';
        $garageOrderStatus = Order::CANCELLED_GARAGE_GETS_CANCELLATION_COST;
        $orderData = array(
            'status' => $completedOrderStatus,
            'type' => $garageOrderStatus,
        );
        $order->completeOrder($orderData);
        $garageOrder = DB::table('garage_orders')->where('order_id', $order->id)
            ->orderBy('id', 'DESC')->first();
        $finalOrderStatus = $garageOrder->status;
        $orderStatus = $order->status;
        $orderId = $order->id;
        unset($order);
        $order = Order::find($orderId);
        $order->update(['status' => 'accepted_by_garage']);
        DB::table('garage_orders')->delete($garageOrder->id);
        $this->assertEquals(
            array(
                'orderStatus' => $completedOrderStatus,
                'garageOrderStatus' => $garageOrderStatus,
            ),
            array(
                'orderStatus' => $orderStatus,
                'garageOrderStatus' => $finalOrderStatus,
            )
        );
    }

    /**
     * Order updateOrder test example.
     *
     * @return void
     */
    public function testCompleteOrderFourth()
    {
        DB::rollback();
        $order = Order::where('status', 'accepted_by_garage')->inRandomOrder()->first();
        $completedOrderStatus = 'cancelled';
        $garageOrderStatus = Order::CANCELLED_CLIENT_GETS_CANCELLATION_COST;
        $orderData = array(
            'status' => $completedOrderStatus,
            'type' => $garageOrderStatus,
        );
        $order->completeOrder($orderData);
        $garageOrder = DB::table('garage_orders')->where('order_id', $order->id)
            ->orderBy('id', 'DESC')->first();
        $finalOrderStatus = $garageOrder->status;
        $orderStatus = $order->status;
        $orderId = $order->id;
        unset($order);
        $order = Order::find($orderId);
        $order->update(['status' => 'accepted_by_garage']);
        DB::table('garage_orders')->delete($garageOrder->id);
        $this->assertEquals(
            array(
                'orderStatus' => $completedOrderStatus,
                'garageOrderStatus' => $garageOrderStatus,
            ),
            array(
                'orderStatus' => $orderStatus,
                'garageOrderStatus' => $finalOrderStatus,
            )
        );
    }

    /**
     * Order ordersData test example.
     *
     * @return void
     */
    public function testOrdersDataFirst()
    {
        $status = 'quoted';
        $data = Order::ordersData($status);
        $orderCount = Order::where('status', $status)->count();
        $this->assertEquals($orderCount, sizeof($data->get()));
    }

    /**
     * Order ordersData test example.
     *
     * @return void
     */
    public function testOrdersDataSecond()
    {
        $status = 'confirmed';
        $data = Order::ordersData($status);
        $orderCount = Order::where('status', $status)->count();
        $this->assertEquals($orderCount, sizeof($data->get()));
    }

    /**
     * Order ordersData test example.
     *
     * @return void
     */
    public function testOrdersDataThird()
    {
        $status = 'completed';
        $data = Order::ordersData($status);
        $orderCount = Order::where('status', $status)->count();
        $this->assertEquals($orderCount, sizeof($data->get()));
    }

    /**
     * Order ordersData test example.
     *
     * @return void
     */
    public function testOrdersDataFourth()
    {
        $status = 'accepted_by_garage';
        $data = Order::ordersData($status);
        $orderCount = Order::where('status', $status)->count();
        $this->assertEquals($orderCount, sizeof($data->get()));
    }

    /**
     * Order checkClientLatLng test example.
     *
     * @return void
     */
    public function testCheckClientLatLng()
    {
        $order = Order::inRandomOrder()->first();
        $address = $order->address;
        $address->update(['latitude' => null, 'longitude' => null]);
        $address->postalCode->update(['postal_code' => '201301']);
        $order->checkClientLatLng();
        $latLng = array($address->latitude, $address->longitude);
        $address->update(['latitude' => null, 'longitude' => null]);
        $order->checkClientLatLng();
        $address = $order->address;
        $this->assertEquals($latLng, array($address->latitude, $address->longitude));
    }
}
