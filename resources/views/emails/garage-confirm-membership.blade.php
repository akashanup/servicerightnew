@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $garageName }},
	<br>
	<br>
	{{ __('manage-garage-email.email_text_22') }} {{ $memberExpiry }}
	{{ __('manage-garage-email.email_text_23') }}
	<br>
	<br>
    <a href="{{ route('home') }}" style="padding:15px; font-size:14px; text-decoration:none; color:white; display:inline-block; background:#95b41c;" target="_blank">
            {{ __('globals.login') }} &raquo;
    </a>
    <br>
    <br>
    <p>
        {{ __('manage-garage-email.email_text_24') }} {{ $startDate }} {{  __('manage-garage-email.email_text_25')  }}
        {{ $memberExpiry }}.
    </p>
    <p>
        {{ __('manage-garage-email.email_text_26') }} {{ $memberExpiry }}
        {{  __('manage-garage-email.email_text_27')  }}
        <a href="{{ route('home') }}" target="_blank">ServiceRight</a>
    </p>
    <br>

    {{ __('manage-garage-email.email_text_28') }} {{ $garageAddress }}  {{ $garagrCity }}
    <br>

    {{ __('manage-garage-email.email_text_15') }} {{ config('constants.CUSTOMER_CARE_NUMBER') }} {{ __('manage-garage-email.email_text_16') }} {{ config('constants.CUSTOMER_CARE_EMAIL') }}.
@endsection
