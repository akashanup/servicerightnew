<?php

namespace App\Models;

use App\Models\State;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the states of the country.
     *
     * @return Collection
     */
    public function states()
    {
        return $this->hasMany(State::class);
    } //end states()
}
