@extends('layouts.app')

@section('content')
    @include('clients.header')
    	@include('errors')
        @yield('client-content')
    @include('clients.footer')
@endsection

@section('scripts')
    @yield('clients-scripts')
@endsection
