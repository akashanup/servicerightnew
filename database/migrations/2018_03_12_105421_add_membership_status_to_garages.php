<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMembershipStatusToGarages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages', function (Blueprint $table) {
            $table->string('activation_token')->nullable();
            $table->enum('membership_status', ['active', 'inactive', 'demo'])->default('inactive');
            $table->enum('deactivate_requested', ['true'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages', function (Blueprint $table) {
            $table->dropColumn('activation_token');
            $table->dropColumn('membership_status');
            $table->dropColumn('deactivate_requested');
        });
    }
}
