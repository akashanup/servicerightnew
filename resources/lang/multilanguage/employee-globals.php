<?php

include_once 'generate-file.php';

$dictionary = [
    'finder_company' => [
        'en' => 'Find company',
        'nl' => 'Bedrijvenzoeker',
    ],
    'login_aldoc' => [
        'en' => 'Login Aldoc',
        'nl' => 'Inloggen Aldoc',
    ],
    'oil_content' => [
        'en' => 'Oil content',
        'nl' => 'Olie inhoud',
    ],
    'looking_for_a_type' => [
        'en' => 'looking for a type',
        'nl' => 'type opzoeken',
    ],
    'calc_oil_price' => [
        'en' => 'Calculate oil price',
        'nl' => 'Olie prijs berekenen',
    ],
    'calc_tyre_price' => [
        'en' => 'Calculate tire price',
        'nl' => 'Bandenprijs berekenen',
    ],
    'exercises' => [
        'en' => 'Exercises',
        'nl' => 'Opdrachten',
    ],
    'schedule' => [
        'en' => 'Schedule',
        'nl' => 'Inplannen',
    ],
    'to_check' => [
        'en' => 'To check',
        'nl' => 'Controleren',
    ],
    'to_check' => [
        'en' => 'To check',
        'nl' => 'Controleren',
    ],
    'open' => [
        'en' => 'Open',
        'nl' => 'Openstaand',
    ],
    'accepted' => [
        'en' => 'Accepted',
        'nl' => 'Geaccepteerd',
    ],
    'rounded' => [
        'en' => 'Completed',
        'nl' => 'Afgerond',
    ],
    'company_management' => [
        'en' => 'Company management',
        'nl' => 'Bedrijf beheer',
    ],
    'references' => [
        'en' => 'References',
        'nl' => 'Referenties',
    ],
    'complaints' => [
        'en' => 'Complaints',
        'nl' => 'Klachten',
    ],
    'prices' => [
        'en' => 'Prices',
        'nl' => 'Tarieven',
    ],
    'edit_staff' => [
        'en' => 'Edit staff',
        'nl' => 'Personeel bewerken',
    ],
    'settings' => [
        'en' => 'Settings',
        'nl' => 'Instellingen',
    ],
    'log_out' => [
        'en' => 'Log out',
        'nl' => 'Uitloggen',
    ],
    'terms_and_conditions' => [
        'en' => 'Terms and Conditions',
        'nl' => 'Algemene voorwaarden',
    ],
    'privacy_policy' => [
        'en' => 'Privacy policy',
        'nl' => 'Privacybeleid',
    ],
    'contact' => [
        'en' => 'Contact',
        'nl' => 'Contact opnemen',
    ],
    'cars' => [
        'en' => 'cars',
        'nl' => 'autos',
    ],
    'couriers' => [
        'en' => 'couriers',
        'nl' => 'koeriers',
    ],
    'financial' => [
        'en' => 'financial',
        'nl' => 'financieel',
    ],
    'are_a_part_of' => [
        'en' => 'are a part of',
        'nl' => 'zijn onderdeel van',
    ],
    'all_rights_reserved' => [
        'en' => 'All rights reserved',
        'nl' => 'Alle rechten voorbehouden',
    ],
];

generateFile($dictionary, basename(__FILE__));
