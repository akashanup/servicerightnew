<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Utils\YajaraDataTable;
use DB;
use Illuminate\Validation\Rule;
use Lang;

class EmployeeController extends Controller
{
    /**
     * Display SR Employee dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('employees.dashboard');
    }

    /**
     * Display SR Employees list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.index');
    }

    /**
     * SR Employees data.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData()
    {
        return YajaraDataTable::employees(User::employees());
    }

    /**
     * SR Employees login.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $validatedData = request()->validate(['id' => 'required|numeric']);
        // Logs admin out
        $adminId = auth()->user()->id;
        auth()->logout();
        request()->session()->flush();
        // Keep admin id in session to switch back to admin dashboard
        session(['admin' => $adminId]);
        // Logs Employee in
        $employee = User::find($validatedData['id']);
        auth()->login($employee);
        // Redirect to employee dashboard
        return response()->json(['response' => true, 'route' => route('employees.dashboard')]);
    }

    /**
     * SR Admin login.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminLogin()
    {
        // Get admin id from session to switch back to admin dashboard
        $admin = User::find(session('admin'));
        // Logs employee out
        auth()->logout();
        request()->session()->flush();
        // Logs Admin in
        auth()->login($admin);
        // Redirect to employee dashboard
        return response()->json(['response' => true, 'route' => route('employees.dashboard')]);
    }

    /**
     * Show employee data
     *
     * @param integer $id user id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $json = [];
        $json['response'] = false;
        $user = User::find($id);
        if ($user) {
            $json['data'] = $user->toArray();
            $json['response'] = true;
            if (auth()->user()->id == $id) {
                $json['currentUser'] = true;
            }
        }
        return response()->json($json);
    }

    /**
     * Update employee data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'id' => 'required|numeric',
                'name' => 'required|string|max:50',
                'nickname' => 'required|string|max:50',
                'email' => 'required|email|max:50',
                'password' => 'nullable|string',
                'status' => 'nullable', Rule::in([User::ACTIVE, User::INACTIVE]),
                'type' => 'required', Rule::in([User::SR_ADMIN, User::SR_EMPLOYEE]),
            ]
        );

        $json['response'] = false;
        $json['message'] = Lang::get('globals.request_error_message');
        $employee = User::find($validatedData['id']);
        if ($employee) {
            $employee->adminUpdates($validatedData);
            $json['response'] = true;
            $json['message'] = Lang::get('globals.input_saved');
        }

        DB::commit();
        return response()->json($json);
    }

    /**
     * Save employee data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'name' => 'required|string|max:50',
                'nickname' => 'required|string|max:50',
                'email' => 'required|email|max:50',
                'password' => 'required|string',
                'status' => 'required', Rule::in([User::ACTIVE, User::INACTIVE]),
                'type' => 'required', Rule::in([User::SR_ADMIN, User::SR_EMPLOYEE]),
            ]
        );
        User::create($validatedData);
        DB::commit();
        return response()->json(['response' => true, 'message' => Lang::get('globals.staff_added')]);
    }
}
