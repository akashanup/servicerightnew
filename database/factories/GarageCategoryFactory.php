<?php

use App\Models\Category;
use App\Models\Garage;
use App\Models\GarageCategory;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(GarageCategory::class, function (Faker $faker) {
    return [
        'category_id' => Category::inRandomOrder()->first()->id,
        'garage_id' => Garage::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
