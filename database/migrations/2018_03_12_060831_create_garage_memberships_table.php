<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGarageMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage_memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('garage_id');
            $table->decimal('base_price');
            $table->decimal('discount')->nullable();
            $table->decimal('final_price');
            $table->date('start_date')->nullable();
            $table->timestamps();
            $table->foreign('garage_id')->references('id')->on('garages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garage_memberships', function (Blueprint $table) {
            $table->dropForeign('garage_memberships_garage_id_foreign');
            $table->dropColumn('garage_id');
        });
        Schema::dropIfExists('garage_memberships');
    }
}
