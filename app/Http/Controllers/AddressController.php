<?php

namespace App\Http\Controllers;

use App\Models\Address;
use DB;

class AddressController extends Controller
{
    /**
     * Fetch client address.
     *
     * @return \Illuminate\Http\Response
     */
    public function client()
    {
        DB::beginTransaction();
        $json = Address::fetch(request()->query());
        DB::commit();
        return response()->json($json);
    }
}
