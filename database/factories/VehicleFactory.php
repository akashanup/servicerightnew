<?php

use App\Models\Brand;
use App\Models\Vehicle;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Vehicle::class, function (Faker $faker) {
    $fuel_type = ['benzine', 'diesel', 'g3', 'elektriciteit', 'hybride'];
    return [
        'brand_id' => Brand::inRandomOrder()->first()->id,
        'fuel_type' => $fuel_type[rand(0, 4)],
        'model' => $faker->word,
        'services' => json_encode(['1' => 120, '2' => 420, '3' => 230, '4' => 90, '5' => 100]),
    ];
});
