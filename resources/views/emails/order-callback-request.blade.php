@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $salutation }} {{ $name }},
	<br/>
	<br/>
	{{ __('employee-place-order.order_callback_text_1') }} {{ $licensePlateNumber }}
	{{ __('employee-place-order.order_callback_text_2') }}
	<br/>
	{{ __('employee-place-order.order_callback_text_3') }}
	<br/>

	<b>{{ __('employee-place-order.order_callback_text_4') }} {{ config('constants.CUSTOMER_CARE_NUMBER') }} {{ __('employee-place-order.order_callback_text_5') }}</b>
	<br><br>
	<a href="{{ $confirmQuoteUrl }}"> {{ __('employee-place-order.confirm_quote') }}</a>
	<br>
	{{ __('employee-place-order.order_confirm_text_6') }} {{ config('constants.CUSTOMER_CARE_NUMBER') }}

@endsection
