# ServiceRight

## Technology
* [NGINX](https://www.nginx.com/) - NGINX Server
* [PHP](http://php.net/releases/7_1_0.php) - PHP 7.0
* [Laravel](https://laravel.com/docs/5.4) - Laravel 5.5
* [MySQL](https://dev.mysql.com/doc/relnotes/mysql/5.7/en/news-5-7-18.html) - MySQL 5.7.18

## Workflow
* [Project Workflow] - 
	1. Users: There are four types of users - 
		* client -> End customer.
		* garage_worker -> Garage's contact person.
		* sr_employee -> ServiceRight employees.
		* sr_admin -> ServiceRight admin.
	2. Addresses: There are three types of addresses -
		* client -> Address of customer from where the vehicle has to be picked.
		* garage -> Address of a garage.
	3. Order statuses: There are seven types of order statuses -
		* drafted
		* quoted
		* confirmed
		* cancelled
		* removed
		* accepted_by_garage
		* rejected_by_garage
		* completed
	4. Order logs: On every update of order, order_histories table should be updated.
	5. Since the prices for each service for each vehicle is different so in vehicles table there is a json column, services which holds the price for each service of the vehicle. Whenever there is a change in the price of a service for base model then we should change the price of that service for each of its varient if it has either the same price or no price. Diesel percentage should also be calculated for Diesel vehicles.

* [Git Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow) - The Git Workflow uses a central repository as the communication hub for all developers. And, as in the other workflows, developers work locally and push branches to the central repo. The only difference is the branch structure of the project.
    
    Instead of a single master branch, this workflow uses two branches to record the history of the project. The master branch stores the official release history, and the develop branch serves as an integration branch for features. The rest of this workflow revolves around the distinction between these two branches.
    
    Each new feature should reside in its own branch, which can be pushed to the central repository for backup/collaboration. But, instead of branching off of master, feature branches use develop as their parent branch. When a feature is complete, it gets merged back into develop. Features should never interact directly with master. It is advised to pull the develop branch before making a merge request.

    Once develop has acquired enough features for a release, you fork a release branch off of develop. Once it's ready to ship, the release gets merged into master and tagged with a version number.

    Maintenance or “hotfix” branches are used to quickly patch production releases. This is the only branch that should fork directly off of master. As soon as the fix is complete, it should be merged into both master and develop (or the current release branch), and master should be tagged with an updated version number.

##### Note
* A feature branch must be checkedout from demo branch and must me merged to both on develop and demo.
* A fix branch from demo server must be named as demo-hotfix/name-of-branch and must be merged to both on demo and develop.
* If there is a conflict in feature branch while merging to develop then a new branch must be created from develop with name as feature-conflict-fix/name-of-feature-branch and feature branch should be pulled in this and merge conflicts should be resolved.
* A fix branch from live server must be named as live-hotfix/name-of-branch and must be merged with all- live, demo and develop.
* If there is a conflict in the fix branch from live while merging to demo or develop then a new branch must be created from demo and develop with names live-hotfix-conflict-fix-demo/fix-name and live-hotfix-conflict-fix-dev/fix-name respectively and should be merged accordingly.

## Installation    
1. Clone the repository using one of the following commands-
* HTTP - git clone http://backup.timplicity.com:30001/timplicity/ServiceRight
* SSH - git clone ssh://git@backup.timplicity.com:30001/timplicity/ServiceRight.git

2. Install [php-mbstring](http://php.net/manual/en/mbstring.installation.php) (if not installed).
    
3. Install [php-xml](http://php.net/manual/en/dom.setup.php) (if not installed).

4. Install [php-ziparchive](http://php.net/manual/en/class.ziparchive.php) (if not installed).

5. Install [composer](https://getcomposer.org/) (if not installed).

6. Run the following commands -
    * composer install

7. Run migrations -
    * php artisan migrate --seed //UsersTableSeeder for admin user
    * php artisan db:seed --class=ImportOldData // Import data from old database. This might take several hours.

8. If you want test data then run -
	* php artisan db:seed --class=TestDataSeeder

## Naming conventions for routes(eg. PhotoController)
* Action - index
	* verb - GET
	* uri - /photos
	* route name - photos.index
* Action - create
	* verb - GET
	* uri - /photos/create
	* route name - photos.create
* Action - store
	* verb - POST
	* uri - /photos
	* route name - photos.store
* Action - show
	* verb - GET
	* uri - /photos/{photo}
	* route name - photos.show
* Action - edit
	* verb - GET
	* uri - /photos/{photo}/edit
	* route name - photos.edit
* Action - update
	* verb - PUT/PATCH
	* uri - /photos/{photo}
	* route name - photos.update
* Action - destroy
	* verb - DELETE
	* uri - /photos/{photo}
	* route name - photos.destroy

## Multilingual Support
* '/resources/lang/{}' - contains the locales and their respective dictionary of the application
	* It is advised to add all the key value pair here first.

## Crons
1. /app/Console/Commands/GarageActivationReminder - Reminds garage each day to activate their account.
2. /app/Console/Commands/InactivateGarage - Inactivate the garages whose membership is expired.