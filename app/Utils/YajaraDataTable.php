<?php

namespace App\Utils;

use App\Models\Garage;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Datatables;
use Lang;

class YajaraDataTable
{
    /**
     * Order datatable
     *
     * @param  array $orders order dataset
     * @return json
     */
    public static function orders($orders)
    {
        return Datatables::of($orders)
            ->editColumn(
                'popups',
                function ($order) {
                    $html = '';
                    $primaryColor = $order->notes ? 'primary-color' : '';
                    if ($order->status == Order::CONFIRMED) {
                        if ($order->locked_status == Order::LOCKED) {
                            $dataContent = Lang::get('employee-order-status.order_unlock_text');
                            $class = 'fa fa-lock primary-color popp';
                            $dataLockStatus = Order::LOCKED;
                        } else {
                            $dataContent = Lang::get('employee-order-status.order_lock_text');
                            $class = 'fa fa-unlock popp';
                            $dataLockStatus = Order::UNLOCKED;
                        }
                        $html .= '<span data-toggle="popover" data-placement="right" data-trigger="hover" data-content="' . $dataContent . '" class="' . $class . ' orderLock" data-lock-status="' . $dataLockStatus . '" data-route="' . route('orders.lock', $order->id) . '">&nbsp;</span>';
                    } elseif ($order->status == Order::ACCEPTED_BY_GARAGE) {
                        $html .= '<span data-toggle="popover" data-placement="right" data-trigger="hover" data-content="Email" data-route="' . route('orders.completeRequest', $order->id) . '" class="fa fa-envelope popp orderConfirmRequest">&nbsp;</span>';
                    }
                    $html .= '&nbsp; <span data-toggle="popover" data-placement="right" data-trigger="hover" data-content="Notities bekijken en toevoegen." data-id="' . $order->id . '" class="fa fa-sticky-note ' . $primaryColor . ' popp orderNote">&nbsp;</span>';

                    return $html;
                }
            )
            ->editColumn(
                'id',
                function ($order) {
                    return '<div class="row">' . $order->id . '</div>';
                }
            )
            ->editColumn(
                'vehicleDetails',
                function ($order) {
                    $html = '<div class="row"><span>' . $order->services . '</span><br>' .
                    '<span class="small">' .
                    '<strong>' .
                    $order->license_plate_number .
                    '</strong> | ' .
                    $order->name . ' ' . $order->model .
                        '</span></div>';
                    return $html;
                }
            )
            ->editColumn(
                'clientDetails',
                function ($order) {
                    $html = '<span class="small">' .
                    $order->complete_address . ' ' . $order->house_number .
                    '<br>' .
                    $order->postal_code . ' ' . $order->city .
                        '</span>';
                    return $html;
                }
            )
            ->editColumn(
                'pickupDetails',
                function ($order) {
                    $html = Carbon::parse($order->pickup_date)->format('d-m-Y') .
                    '<br>' .
                    '<span class="small">' .
                    $order->pickup_time .
                        '</span>';
                    return $html;
                }
            )
            ->editColumn(
                'priceDetails',
                function ($order) {
                    $html = '&euro; ' . ($order->total_price) .
                        '<span class="small">(incl. BTW)</span>';
                    if ($order->status == Order::COMPLETED || $order->status == Order::ACCEPTED_BY_GARAGE) {
                        $garage = Order::find($order->id)->garage()->name;
                        $html .= '<br><span class="small">(' . $garage . ')</span>';
                    } else {
                        $html .= '<br><span class="small">(Wat de klant betaald)</span>';
                    }
                    return $html;
                }
            )
            ->editColumn(
                'action',
                function ($order) {
                    $html = '<div class="row">';
                    if ($order->status == Order::QUOTED) {
                        $html .= '<a href="' . route('orders.edit', $order->id) . '" class="btn btn-primary btn-sm">' .
                        Lang::get('employee-order-status.check') .
                            '</a>';
                    } elseif ($order->status == Order::CONFIRMED) {
                        $user = auth()->user();
                        if ($order->locked_status == Order::LOCKED) {
                            $toUserId = Order::find($order->id)->lock_statuses()->latest()->first()->to_user_id;
                            if ($user->type == User::SR_ADMIN || $toUserId == $user->id) {
                                $html .= '<a href="' . route('orders.assign', $order->id) . '" class="btn btn-primary btn-sm">' .
                                Lang::get('employee-order-status.assign') .
                                '</a> &nbsp;' .
                                '<a href="' . route('orders.edit', $order->id) . '" class="btn btn-info btn-sm">' .
                                Lang::get('employee-order-status.edit') .
                                    '</a>';
                            } else {
                                $html .= '<a href="#" class="btn btn-primary btn-sm" disabled>' .
                                Lang::get('employee-order-status.assign') .
                                '</a> &nbsp;' .
                                '<a href="javascript:void(0)" class="btn btn-info btn-sm" disabled>' .
                                Lang::get('employee-order-status.edit') .
                                    '</a>';
                            }
                        } else {
                            $html .= '<a href="' . route('orders.assign', $order->id) . '" class="btn btn-primary btn-sm">' .
                            Lang::get('employee-order-status.assign') .
                            '</a>' .
                            '<a href="' . route('orders.edit', $order->id) . '" class="btn btn-info btn-sm">' .
                            Lang::get('employee-order-status.edit') .
                                '</a>';
                        }
                    } elseif ($order->status == Order::ACCEPTED_BY_GARAGE) {
                        $html .= '<a href="javascript:void(0)" data-phone="' . $order->phone . '" data-id="' . $order->id . '" data-price="' . $order->total_price . '" data-work="' . $order->additional_work . '" class="btn btn-primary btn-sm priceOrderModalBtn">
                            EXTRAS
                        </a>' .
                        '<a href="javascript:void(0)" data-licensePlateNumber="' . $order->license_plate_number . '" data-model="' . $order->model . '" data-pickupDate="' . $order->pickup_date . '" data-id="' . $order->id . '" data-serviceList="' . $order->services . '" data-servicePriceList="' . $order->servicePrices . '" data-price="' . $order->total_price . '" class="btn btn-info btn-sm completeOrderBtn">' .
                        Lang::get('employee-order-status.complete') .
                            '</a>';
                    } elseif ($order->status == Order::COMPLETED) {
                        $html .= '<span class="small">' . Lang::get('employee-globals.rounded') . ' ' . Lang::get('employee-assign-order.on') . '</span><br><span>' . Carbon::parse($order->created_at)->format('d-m-Y') . '</span>';
                    }
                    return '</div>' . $html;
                }
            )
            ->rawColumns(['popups', 'id', 'vehicleDetails', 'clientDetails', 'pickupDetails', 'priceDetails', 'action'])
            ->make(true);
    } //end orders()

    /**
     * Garage datatable
     *
     * @param  array $garages garages dataset
     * @return json
     */
    public static function garages($garages)
    {
        return Datatables::of($garages)
            ->editColumn(
                'popups',
                function ($garage) {
                    $primaryColor = $garage->saleNotes ? 'primary-color' : '';
                    $html = '<span data-toggle="modal">
                                <span data-toggle="popover" data-placement="top" data-trigger="hover" data-content="' . Lang::get('manage-garage.edit_company') . '" data-route="' . route('garages.edit', $garage->id) . '" data-type="edit" data-id="' . $garage->id . '" data-name="' . $garage->name . '" class="fa fa-cog popp modalBtn">&nbsp;</span>
                            </span>
                            <span data-toggle="modal">
                                <span data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="Login" class="fa fa-key popp">&nbsp;</span>
                            </span><br>
                            <span data-toggle="modal">
                                <span data-toggle="popover" data-placement="bottom" data-trigger="hover" data-route="' . route('garages.delete', $garage->id) . '" data-name="' . $garage->name . '" data-type="delete" data-content="' . Lang::get('manage-garage.delete') . '" class="fa fa-trash popp modalBtn">&nbsp;</span>
                            </span>
                            <span data-toggle="modal">
                                <span data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="' . Lang::get('manage-garage.sales_notes') . '" data-route="' . route('garages.sale-notes', $garage->id) . '" data-type="saleNote" data-id="' . $garage->id . '" data-name="' . $garage->name . '" class="fa fa-sticky-note ' . $primaryColor . ' popp modalBtn saleNotePopup">&nbsp;</span>
                            </span>';
                    return $html;
                }
            )
            ->editColumn(
                'id',
                function ($garage) {
                    return $garage->id;
                }
            )
            ->editColumn(
                'name',
                function ($garage) {
                    $html = '<div class="row">' . $garage->name . '</div>' .
                    '<div class="row">' . $garage->categories . '</div>';
                    return $html;
                }
            )
            ->editColumn(
                'location',
                function ($garage) {
                    $html = '<div class="row">' . $garage->complete_address . ',<br>'
                    . $garage->city . '-' . $garage->postal_code . '</div>' .
                    '<div class="row">[T] ' . $garage->phone . '</div>';
                    return $html;
                }
            )
            ->editColumn(
                'membership',
                function ($garage) {
                    $html = Garage::getMembershipText($garage);
                    $html .= $garage->deactivate_requested
                    ? '<br><span class="small" style="color: red;">' . Lang::get('globals.cancelled') . '</span>'
                    : '';
                    return $html;
                }
            )
            ->editColumn(
                'action',
                function ($garage) {
                    $html = '';

                    if (Garage::isRenewable($garage)) {
                        $html .= '<span data-toggle="modal" class="btn btn-primary btn-sm modalBtn" style="width: 100%; margin-bottom: 1em; text-align: center;" data-garageExpiry="' . Carbon::parse($garage->member_expiry)->addYear()->format('d-m-Y') . '" data-id="' . $garage->id . '" data-name="' . $garage->name . '" data-type="renewPassword" data-membership="' . Garage::getMembershipText($garage) . '">' . Lang::get('manage-garage.manage') . '</span>';
                        $temp = '<span data-toggle="modal" class="btn btn-secondary btn-sm modalBtn" style="width: 45%; text-align: center; float: right;" data-id="' . $garage->id . '" data-name="' . $garage->name . '" data-type="recognition">' . Lang::get('manage-garage.recognition') . '</span>';
                    } else {
                        $html .= '<span data-toggle="modal" class="btn btn-primary btn-sm modalBtn" style="width: 100%; margin-bottom: 1em; text-align: center;" data-id="' . $garage->id . '" data-name="' . $garage->name . '" data-type="createPassword">' . Lang::get('manage-garage.create_password') . '</span>';
                        $temp = '<span data-toggle="modal" class="btn btn-secondary btn-sm modalBtn" style="width: 45%; text-align: center; float: right;" data-id="' . $garage->id . '" data-name="' . $garage->name . '" data-type="demo">' . Lang::get('manage-garage.send') . ' demo</span>';
                    }
                    $html .= '<span data-toggle="modal" class="btn btn-secondary btn-sm modalBtn" style="width: 45%;  text-align: center;" data-route="' . route('garages.stats', $garage->id) . '" data-name="' . $garage->name . '" data-type="stats">Stats</span>' . $temp;
                    return $html;
                }
            )
            ->rawColumns(['popups', 'name', 'location', 'membership', 'action'])
            ->make(true);
    }

    /**
     * Employee datatable
     *
     * @param  array $employees employee dataset
     * @return json
     */
    public static function employees($employees)
    {
        return Datatables::of($employees)
            ->editColumn(
                'login',
                function ($employee) {
                    return '<span data-toggle="popover" data-placement="top" data-trigger="hover" data-content="' . Lang::get('globals.login') . '" data-id="' . $employee->id . '" class="fa fa-key popp employeeLogin">&nbsp;</span>';
                }
            )
            ->editColumn(
                'action',
                function ($employee) {
                    return '<a href="javascript:void(0)" data-route="' . route('employees.show', $employee->id) .
                    '" class="btn btn-primary btn-sm editEmployeeModalBtn">'
                    . Lang::get('employee-order-status.edit') . '</a>';
                }
            )
            ->rawColumns(['login', 'action'])
            ->make(true);
    } //end orders()
} //end class
