<?php

namespace App\Models;

use App\Models\Garage;
use App\Models\Service;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'name',
    ];

    /**
     * Get the services of the category.
     *
     * @return Collection
     */
    public function services()
    {
        return $this->hasMany(Service::class);
    } //end services()

    /**
     * Get the garages for the category.
     */
    public function garages()
    {
        return $this->belongsToMany(Garage::class, 'garage_categories', 'category_id', 'garage_id')
            ->withTimestamps();
    } //end garages()
}
