$(function(){
	$.fn.initOrderDatatable = (orderId='') => {
		$('#orderTable').DataTable().destroy();
	    var table = $('#orderTable').DataTable(
		    {
		        processing: true,
		        serverSide: true,
		        ajax: $('#ordersDataRoute').val() + (orderId ? ('/'+orderId) : ''),
		        columns: [
		        	{data: 'popups', orderable: false, searchable: false},
		            {data: 'id', name: 'orders.id'},
		            {data: 'vehicleDetails', name: 'license_plate_numbers.license_plate_number', orderable: false},
		            {data: 'clientDetails', orderable: false, searchable: false},
		            {data: 'pickupDetails', name: 'orders.pickup_date', orderable: false},
		            {data: 'priceDetails', orderable: false, searchable: false},
		            {data: 'action', orderable: false, searchable: false}
		        ],
		        fnDrawCallback: () => {
		        	$(document).find('.popp').popover();
		        },
		        columnDefs: [
		            { width: '1%', targets: 0 },
		            { width: '1%', targets: 1 },
		            { width: '15%', targets: 2 },
		            { width: '10%', targets: 3 },
		            { width: '10%', targets: 4 },
		            { width: '15%', targets: 5 },
		            { width: '45%', targets: 6 }
		        ],
		    }
		);
	};

	$(document).on('click', '.orderConfirmRequest', function(){
		var thisBtn = $(this);
		var data = {};
		var url = $(this).attr('data-route');
		$.fn.ajaxBlockUI();
	    $.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
	      $.unblockUI();
	      if (successResponse.response) {
	        $.blockUI({ message: successResponse.message, timeout: 2000,baseZ: 1005 });
	      }
	    }, (errorResponse) => {
	    	$.unblockUI();
	      	$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
	    });
	});

	$(document).on('click', '.priceOrderModalBtn', function(){
		var modal = $('#priceUpdateModal');
		modal.find('#priceUpdateModalOrderId').html($(this).attr('data-id'));
		modal.find('#clientPhoneModal').html($(this).attr('data-phone'));
		modal.find('#additionalWorkModalText').val($(this).attr('data-work'));
		modal.find('#priceModalInpt').val($(this).attr('data-price'));
		modal.find('#currentPriceInpt').val($(this).attr('data-price'));
		modal.modal('show');

	});

	$(document).on('hidden.bs.modal', '#priceUpdateModal', function(){
		var modal = $('#priceUpdateModal');
		modal.find('#priceUpdateModalOrderId').html('');
		modal.find('#clientPhoneModal').html('');
		modal.find('#additionalWorkModalText').val('');
		modal.find('#priceModalInpt').val('');
		modal.find('#currentPriceInpt').val('');
		modal.find('#priceDifferenceModalInpt').val('');
	});


	$(document).on('click', '#priceUpdateBtn', function(){
		var modal = $('#priceUpdateModal');
		var formData = 	new FormData();
		var invoice = modal.find('#invoiceModalInpt');
		var orderId = modal.find('#priceUpdateModalOrderId').html();
		if(invoice.val()){
			file = invoice[0].files[0];
			var filename = file.name.toLowerCase();
			var extension = filename.substr((filename.lastIndexOf('.')+1));
			if($.inArray(extension,['pdf', 'jpg', 'gif', 'jpeg', 'png']) != -1) {
				formData.append('invoice', file);
				formData.append('orderId', orderId);
				formData.append('totalPrice', modal.find('#priceModalInpt').val());
				formData.append('additionalWork', modal.find('#additionalWorkModalText').val());
				formData.append('note', modal.find('#noteModalInpt').val());	
				
				$.ajax({
				    url: $(document).find('#priceUpdateRoute').val(),
				    data: formData,
				    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				    type: 'POST',
				    // THIS MUST BE DONE FOR FILE UPLOADING
				    contentType: false,
				    processData: false,
				    dataType: 	'json',
				    success : 	function(response) {
				    	modal.find('#invoiceModalInpt').val('');
						modal.modal('hide');
						$.blockUI({ message: response.message, timeout: 2000,baseZ: 1005 });
						$.fn.initOrderDatatable(orderId);
					},
					error 	: 	function(response) {
						modal.modal('hide');
						$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
					}
				});
			} else {
				invoice.focus();
				invoice.css('border-color', 'red');
				invoice.parent('div').append('<span id="invoiceError" style="color:red">Invalid format!</span>');
			}
			
		} else {
			invoice.focus();
			invoice.css('border-color', 'red');
		}
	});

	$(document).on('click', '#invoiceModalInpt', function(){
		$(this).parent('div').find('#invoiceError').remove();
		$(this).css('border-color', 'lightgrey');
	});

	$(document).on('click', '.completeOrderBtn', function(){
		var modal = $('#completeOrderModal');
		modal.find('#modalLicensePlateNumber').html($(this).attr('data-licensePlateNumber'));
		modal.find('#modalModel').html($(this).attr('data-model'));
		modal.find('#modalPickupDate').html($(this).attr('data-pickupDate'));
		modal.find('#modalPriceInpt').html($(this).attr('data-price'));
		modal.find('#modalCompleteOrderId').val($(this).attr('data-id'));
		var serviceList = $(this).attr('data-serviceList').split(' | ');
		var servicePriceList = $(this).attr('data-servicePriceList').split(' | ');
		var services = '';
		for (var i = 0; i < serviceList.length; i++) {
			services += `<li>${serviceList[i]} (&euro; ${servicePriceList[i]})</li>`
		}
		modal.find('#modalServiceList').html(services);
		modal.modal('show');
	});

	$(document).on('hidden.bs.modal', '#completeOrderModal', function(){
		var modal = $('#completeOrderModal');
		modal.find('#modalLicensePlateNumber').html('');
		modal.find('#modalModel').html('');
		modal.find('#modalPickupDate').html('');
		modal.find('#modalCompleteOrderId').val('');
		$(document).find('#orderCalcellationTypeDiv').css('display', 'none');
	});

	$(document).on('click', '#cancelModalBtn', function(){
		$(document).find('#orderCalcellationTypeDiv').css('display', 'block');
	});

	$(document).on('click', '.completeOrderModalBtn', function(){
		var thisBtn = $(this);
		var modal = $(document).find('#completeOrderModal');
		var status = thisBtn.attr('data-status');
		var data = {};
		data.status = status;
		data.orderId = modal.find('#modalCompleteOrderId').val();
		if(status == 'cancelled') {
			data.type = modal.find('#cancellationTypeSelect').val();
		}
		var url = $('#completeOrderRoute').val();
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			if (successResponse.response) {
				modal.modal('hide');
				$.fn.initOrderDatatable();
			}
		}, (errorResponse) => {
			modal.modal('hide');
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});

	$(document).on('keypress keyup kewdown', '#priceDifferenceModalInpt', function(){
		var val = Number($(this).val());
		var modal = $('#priceUpdateModal');
		var currentPrice = Number(modal.find('#currentPriceInpt').val());
		modal.find('#priceModalInpt').val(currentPrice + val);
	});

	$('#showAllOrder').on('click', function(){
		$.fn.initOrderDatatable();
	});

	$.fn.initOrderDatatable();
});