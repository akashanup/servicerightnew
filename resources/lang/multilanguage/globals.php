<?php

include_once 'generate-file.php';

$dictionary = [
    'request_error_message' => [
        'en' => 'Something went wrong. Please try again.',
        'nl' => 'Er is iets fout gegaan. Probeer het opnieuw.',
    ],
    'address_not_found' => [
        'en' => 'Address not found!',
        'nl' => 'Adres niet gevonden!',
    ],
    'vehicle_not_found' => [
        'en' => 'Vehicle not found!',
        'nl' => 'Voertuig niet gevonden!',
    ],
    'service_not_found' => [
        'en' => 'Service not found!',
        'nl' => 'Service niet gevonden!',
    ],
    'address_curl_exception' => [
        'en' => 'Address curl failed!',
        'nl' => 'Adreskrul mislukt!',
    ],
    'license_number_plate_curl_exception' => [
        'en' => 'License plate number curl failed!',
        'nl' => 'Kentekennummer curl mislukt!',
    ],
    'sincerely' => [
        'en' => 'Sincerely',
        'nl' => 'Met vriendelijke groet',
    ],
    'terms_and_conditions' => [
        'en' => 'Terms and conditions',
        'nl' => 'Algemene voorwaarden',
    ],
    'companies' => [
        'en' => 'Companies',
        'nl' => 'bedrijven',
    ],
    'consumers' => [
        'en' => 'Consumers',
        'nl' => 'consumenten',
    ],
    'dear' => [
        'en' => 'Dear',
        'nl' => 'Geachte',
    ],
    'total' => [
        'en' => 'Total',
        'nl' => 'Totaal',
    ],
    'price' => [
        'en' => 'Price',
        'nl' => 'Prijs',
    ],
    'validation_error' => [
        'en' => 'The given data was invalid.',
        'nl' => 'De gegeven gegevens waren ongeldig.',
    ],
    'confirm' => [
        'en' => 'Confirm',
        'nl' => 'Bevestigen',
    ],
    'reject' => [
        'en' => 'Reject',
        'nl' => 'Afwijzen',
    ],
    'cancel' => [
        'en' => 'Cancel',
        'nl' => 'Annuleren',
    ],
    'save' => [
        'en' => 'Save',
        'nl' => 'Opslaan',
    ],
    'delete' => [
        'en' => 'Delete',
        'nl' => 'Verwijderen',
    ],
    'link_expired' => [
        'en' => 'Link has been expired.',
        'nl' => 'Link is verlopen.',
    ],
    'remove' => [
        'en' => 'Remove',
        'nl' => 'Verwijderen',
    ],
    'unauthorized_text' => [
        'en' => 'You are not authorized to perform this task.',
        'nl' => 'U bent niet gemachtigd om deze taak uit te voeren.',
    ],
    'accepted' => [
        'en' => 'Accepted',
        'nl' => 'Geaccepteerd',
    ],
    'rejected' => [
        'en' => 'Rejected',
        'nl' => 'Verworpen',
    ],
    'price_updated' => [
        'en' => 'Price successfully updated!',
        'nl' => 'Prijs succesvol bijgewerkt!',
    ],
    'email_has_been_sent' => [
        'en' => 'An email has been sent.',
        'nl' => 'Er is een e-mail verzonden.',
    ],
    'discount' => [
        'en' => 'Discount',
        'nl' => 'Korting',
    ],
    'deactivate' => [
        'en' => 'Deactivate',
        'nl' => 'Deactiveren',
    ],
    'renew' => [
        'en' => 'Renew',
        'nl' => 'Verlengen',
    ],
    'send' => [
        'en' => 'Send',
        'nl' => 'Versturen',
    ],
    'not_a_member' => [
        'en' => 'No SR member',
        'nl' => 'Geen SR lid',
    ],
    'company_added' => [
        'en' => 'Garage added successfully!',
        'nl' => 'Bedrijven succesvol toegevoegd!',
    ],
    'company_deleted' => [
        'en' => 'Garage deleted successfully!',
        'nl' => 'Bedrijven verwijderde toegevoegd!',
    ],
    'company_updated' => [
        'en' => 'Garage updated successfully!',
        'nl' => 'Bedrijven succesvol bijgewerkt!',
    ],
    'show_all' => [
        'en' => 'Show all',
        'nl' => 'Toon alles',
    ],
    'login' => [
        'en' => 'Login',
        'nl' => 'Inlogin',
    ],
    'password' => [
        'en' => 'Password',
        'nl' => 'Wachtwoord',
    ],
    'cancelled' => [
        'en' => 'Cancelled',
        'nl' => 'Opgezegd',
    ],
    'input_saved' => [
        'en' => 'Input saved',
        'nl' => 'Invoer opgeslagen',
    ],
    'input_not_saved' => [
        'en' => 'Input is not saved yet. Please wait!',
        'nl' => 'Invoer is nog niet opgeslagen. Even geduld aub!',
    ],
    'staff_added' => [
        'en' => 'Staff added successfully',
        'nl' => 'Het personeel is succesvol toegevoegd',
    ],
    'phone_already_exists' => [
        'en' => 'Phone number is already used by some other user.',
        'nl' => 'Telefoonnummer wordt al door een andere gebruiker gebruikt.',
    ],
    'last_year' => [
        'en' => 'Last year',
        'nl' => 'Afgelopen jaar',
    ],
];

generateFile($dictionary, basename(__FILE__));
