<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class OrderCallbackRequest extends Notification
{
    use Queueable;

    /**
     * The user's salutation.
     *
     * @var string
     */
    public $salutation;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * The vehicle's licensePlateNumber.
     *
     * @var string
     */
    public $licensePlateNumber;

    /**
     * The order quote url.
     *
     * @var string
     */
    public $confirmQuoteUrl;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        $salutation,
        $name,
        $licensePlateNumber,
        $confirmQuoteUrl
    ) {
        $this->salutation = $salutation;
        $this->name = $name;
        $this->licensePlateNumber = $licensePlateNumber;
        $this->confirmQuoteUrl = $confirmQuoteUrl;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->view('emails.order-callback-request', [
                'title' => Lang::get('employee-place-order.order_status_email_title'),
                'salutation' => $this->salutation,
                'name' => $this->name,
                'licensePlateNumber' => $this->licensePlateNumber,
                'confirmQuoteUrl' => $this->confirmQuoteUrl,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
