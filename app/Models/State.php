<?php

namespace App\Models;

use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'country_id',
    ];

    const NOT_FOUND = 'Not found';

    /**
     * Get the country of the state.
     *
     * @return App\Models\Country
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    } //end country()

    /**
     * Get the cities of the state.
     *
     * @return Collection
     */
    public function cities()
    {
        return $this->hasMany(City::class);
    } //end states()

    /**
     * Create a state if its not found.
     *
     * @param integer $countryId
     * @param string $state
     * @return void
     */
    public static function createUnique($countryId, $state)
    {
        $thisState = static::where('name', $state)->where('country_id', $countryId)->first();
        if (!$thisState) {
            $thisState = static::create(
                [
                    'name' => $state,
                    'country_id' => $countryId,
                ]
            );
        }
        return $thisState;
    } //end createUnique()
}
