<?php

use Illuminate\Database\Seeder;

class ImportOldDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(VehiclesTableSeeder::class);
        $this->call(GaragesTableSeeder::class);
    }
}
