$(function(){
	var typingTimer 		=	null;   //timer identifier
	var doneTypingInterval 	= 	1500;   //time in ms
	var totalPrice          =   0.00;
	const spinner 			=   '<span class="col-sm-1 dynamicSpinner">'+
									'<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'+
								'</span>';

	$.fn.toggleAddressFields = ($toggle=true) => {
		$toggle ? $('#addressInpt, #cityInpt, #fetchedAddressId').val('').attr('disabled', true) 
				: $('#addressInpt, #cityInpt, #fetchedAddressId').removeAttr('disabled');
	};

	$.fn.showSpinner = ($toggle=true) => {
		$parent = $('#addressInpt, #cityInpt').parent();
		if ($toggle) {
			$parent.removeClass('col-sm-8').addClass('col-sm-6');
			$(spinner).insertAfter($parent); 
		} else {
			$parent.removeClass('col-sm-6').addClass('col-sm-8');
			$(document).find('.dynamicSpinner').remove();
		}
				
	};

	$.fn.populateAddress = (countryId, postalCode, houseNumber) => {
		var data = {
			"countryId": countryId,
			"postalCode": postalCode,
			"houseNumber": houseNumber
		};
		var url = $('#fetchAddressRoute').val();

		$.fn.showSpinner(false);
		$.fn.showSpinner();
		$.fn.ajaxCall(REQUEST_TYPE_GET, url, data, DATA_TYPE_JSON).then((successResponse) => {
			$.fn.showSpinner(false);
			if (successResponse.response) {
				$('#addressInpt').val(successResponse.complete_address);
				$('#cityInpt').val(successResponse.city);
				$('#fetchedAddressId').val(successResponse.addressId);
				$.fn.toggleAddressFields(false);
			} else {
				$.fn.toggleAddressFields();
			}

		}, (errorResponse) => {
			$('#postalCodeInpt').val('');
			$('#houseNumberInpt').val('');
			$.fn.showSpinner(false);
			$.fn.toggleAddressFields();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	};

	$('#postalCodeInpt, #houseNumberInpt').on('keyup paste', (e) => {
		var keyCode = e.keyCode;
		if ( $.fn.allowedKey(keyCode) || keyCode == undefined){
			var countryId = $('#countryInpt').val();
			var postalCode = $('#postalCodeInpt').val().trim();
			var houseNumber = $('#houseNumberInpt').val().trim();
			if (typingTimer) 
		    {
		    	// Clear if already set
		    	window.clearTimeout(typingTimer);  
		    }
			if (postalCode.length && houseNumber.length) {
		    	typingTimer 		= 	window.setTimeout(function(){
			    	$.fn.populateAddress(countryId, postalCode, houseNumber);
		    }, doneTypingInterval);
			} else {
				$.fn.toggleAddressFields();
			}
		}
	});

});