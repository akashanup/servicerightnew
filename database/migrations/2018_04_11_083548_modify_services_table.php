<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            // Since it has enum columns so schema builder wont work.
            // DB::statement is a workaround for this.
            DB::statement('ALTER TABLE services CHANGE service name VARCHAR(100)');
            DB::statement('ALTER TABLE services CHANGE code code VARCHAR(100)');
            DB::statement('ALTER TABLE services CHANGE slug slug VARCHAR(100)');
            DB::statement('CREATE UNIQUE INDEX services_code_unique ON services (code)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            DB::statement('ALTER TABLE services DROP INDEX services_code_unique');
            DB::statement('ALTER TABLE services CHANGE name service VARCHAR(255)');
            DB::statement('ALTER TABLE services CHANGE code code VARCHAR(255)');
            DB::statement('ALTER TABLE services CHANGE slug slug VARCHAR(255)');
        });
    }
}
