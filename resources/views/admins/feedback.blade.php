@extends('admins.content')

@section('admin-content')
    <section class="small-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>{{ __('feedbacks.manage_references') }}</h2>
				</div>
			</div>
		</div>
	</section>

	<section class="small-padding bg-gray">
		<div class="container" id="feedbackData">
		</div>
		<input type="hidden" id="feedbackDataRoute" value="{{route('feedbacks.indexData', $type)}}">
		<input type="hidden" value="{{ route('feedbacks.update') }}" id="feedbackUpdateRoute">
	</section>

    <!-- Scripts -->
    @section('admin-scripts')
    	<script type="text/javascript" src="{{ asset('js/custom/manage-feedback.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
