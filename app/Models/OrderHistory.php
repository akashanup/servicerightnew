<?php

namespace App\Models;

use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'detail', 'comment', 'created_by_user_id',
    ];

    /**
     * Get the order for the status.
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
