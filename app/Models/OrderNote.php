<?php

namespace App\Models;

use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class OrderNote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'user_id', 'note',
    ];

    /**
     * Get the order for the note.
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
