@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $garageName }},
	<br>
	<br>
	{{ __('manage-garage-email.email_text_29') }}
    <br>
	{{ __('manage-garage-email.email_text_30') }}
    {{ $memberExpiry }}
	<br>
	<br>

    <b>{{ __('manage-garage-email.email_text_31') }}:</b><br/>
    &bull; {{ __('manage-garage-email.email_text_32') }}<br/>
    &bull; {{ __('manage-garage-email.email_text_33') }}<br/>
    &bull; {{ __('manage-garage-email.email_text_34') }}
    <br/><br/>
    <b>{{ __('manage-garage-email.email_text_35') }}:</b><br/>
    ServiceRight<br/>
    Afdeling Administratie<br/>
    Splijtbakweg 60<br/>
    1333 HJ, Almere
    <br/><br/>

    {{ __('manage-garage-email.email_text_15') }} {{ config('constants.CUSTOMER_CARE_NUMBER') }} {{ __('manage-garage-email.email_text_16') }} {{ config('constants.CUSTOMER_CARE_EMAIL') }}.
@endsection
