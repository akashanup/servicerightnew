<?php

return [
"check_jobs" => "Opdrachten controleren",
"paragraph_text_1" => "De opdrachten komen hier binnen en worden automatisch geupdate. U kunt dit scherm open laten staan en te allen tijde opdrachten doorvoeren.",
"paragraph_text_2" => "Hierbij ontvangt u de order voor het laten uitvoeren van de werkzaamheden zoals onderstaand
omschreven voor het voertuig",
"paragraph_text_3" => "ServiceRight verzorgd uw auto-onderhoud!",
"paragraph_text_4" => "Het doel van ServiceRight is om een zo vriendelijk, kosten efficiënt
            en eenvoudig mogelijke auto service te bieden aan onze klanten. Bij
            ServiceRight waarborgen we de garantie en bespaart u gegarandeerd in
            vergelijking met de hoofddealer prijzen. Ook wordt uw voertuig gratis
            opgehaald voor uw gewenste service en gratis weer terug gebracht naar
            uw huis of werkplek. U wordt tijdens elke stap door één van onze medewerkers
            op de hoogte gehouden.",
"shift" => "Dienst",
"notes_for" => "Notities",
"customer_address" => "Adres klant",
"open_assignments" => "Openstaande opdrachten",
"check" => "Controleren",
"assign" => "Toewijzen",
"edit" => "Bewerken",
"complete" => "AFRONDEN",
"order_unlock_text" => "Opdracht gelockt, alleen jij kunt deze opdracht verwerken. Klik om te unlocken.",
"order_lock_text" => "Lock opdracht, alleen jij kunt deze opdracht verwerken. Klik om te locken.",
"email_text_1" => "is het onderhoud van uw voertuig met het kenteken",
"email_text_2" => "onderhouden door ",
"email_text_3" => "Wij begrijpen maar al te goed dat u het bedrag van het onderhoud aan uw voertuig liever ergens anders aan besteed. Daarom maakt u nu kans om uw gehele factuurbedrag terug te winnen!
Klik op de onderstaande link, u hoeft enkel het bedrag in te vullen om deel te nemen.",
"email_text_4" => "Win uw factuurbedrag terug!",
"other_work" => "Overige werkzaamheden order ",
"modal_text_1" => "Voor meerwerk moet u eerst contact opnemen met de klant via ",
"modal_text_2" => "Overige werkzaamheden dienen hieronder vermeld te worden ",
"copy_of_invoice" => "Kopie van factuur",
"no_file_choosen" => "Geen bestand gekozen",
"modal_text_3" => "Let Op: Rekening ontbreekt. Factuur dient nog te worden geupload.",
"modal_text_4" => "En/of vul de omschrijving van de overige werkzaamheden in",
"note" => "Opmerking ",
"modal_text_5" => "Kosten voor de overige werkzaamheden",
"modal_text_6" => "Opdracht Afronden",
"modal_text_7" => "Het onderhoud voor het volgende voertuig is verricht.",
"confirm_agree" => "Bevesting en akkoord ",
"cancelled_with_no_cancellation_cost" => "Geen Handelingskosten",
"cancelled_garage_gets_cancellation_cost" => "Handelingskosten bij het bedrijf in rekening brengen",
"cancelled_client_gets_cancellation_cost" => "Handelingskosten bij de klant in rekening brengen",
];