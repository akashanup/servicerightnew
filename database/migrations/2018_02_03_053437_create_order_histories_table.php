<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->json('detail');
            $table->unsignedInteger('created_by_user_id');
            $table->enum('comment', ['drafted', 'quoted', 'confirmed', 'updated', 'callback_requested', 'cancelled', 'removed', 'accepted_by_garage', 'rejected_by_garage', 'completed', 'cancelled_with_no_cancellation_cost', 'cancelled_garage_gets_cancellation_cost', 'cancelled_client_gets_cancellation_cost']);
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_histories', function (Blueprint $table) {
            $table->dropForeign('order_histories_order_id_foreign');
            $table->dropColumn('order_id');
            $table->dropForeign('order_histories_created_by_user_id_foreign');
            $table->dropColumn('created_by_user_id');
        });
        Schema::dropIfExists('order_histories');
    }
}
