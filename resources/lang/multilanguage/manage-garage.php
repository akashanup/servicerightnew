<?php

include_once 'generate-file.php';

$dictionary = [
    'add_company' => [
        'en' => 'Add Company',
        'nl' => 'Bedrijf toevoegen',
    ],
    'edit_company' => [
        'en' => 'Edit Company',
        'nl' => 'Bewerk bedrijf',
    ],
    'company' => [
        'en' => 'Company',
        'nl' => 'Bedrijf',
    ],
    'membership' => [
        'en' => 'Membership',
        'nl' => 'Lidmaatschap',
    ],
    'delete' => [
        'en' => 'Delete',
        'nl' => 'Verwijder',
    ],
    'sales_notes' => [
        'en' => 'View and add sales notes',
        'nl' => 'Sales notities bekijken en toevoegen',
    ],
    'expired_on' => [
        'en' => 'Expired on',
        'nl' => 'Verlopen op',
    ],
    'create_password' => [
        'en' => 'Create Password',
        'nl' => 'Wachtwoord aanmaken',
    ],
    'send' => [
        'en' => 'Send',
        'nl' => 'Stuur',
    ],
    'new_ones' => [
        'en' => 'New ones',
        'nl' => 'Nieuwe',
    ],
    'statistics' => [
        'en' => 'statistics',
        'nl' => 'statistieken',
    ],
    'modal_text_1' => [
        'en' => 'Are you sure you want to create a password for Autobedrijf Catsman? This company will then receive an e-mail with the login code and the password for his account. After receipt of the e-mail an invoice will be created and the membership for this company will be activated.',
        'nl' => 'Weet je zeker dat je een wachtwoord wilt aanmaken voor Autobedrijf Catsman? Dit bedrijf zal dan een e-mail ontvangen met de inlogcode en het wachtwoord voor zijn account. Er zal na ontvangst van de e-mail ook een factuur worden aangemaakt en het lidmaatschap voor dit bedrijf wordt geactiveerd.',
    ],
    'edit' => [
        'en' => 'edit',
        'nl' => 'bewerken',
    ],
    'delete' => [
        'en' => 'delete',
        'nl' => 'verwijderen',
    ],
    'modal_text_2' => [
        'en' => 'Are you sure you want to remove the garage?',
        'nl' => 'Weet je zeker dat je wilt verwijderen de bedrijf?',
    ],
    'contact_person' => [
        'en' => 'Contact person',
        'nl' => 'Contact persoon',
    ],
    'modal_text_3' => [
        'en' => 'Extend membership (1 year) to',
        'nl' => 'Lidmaatschap (1 jaar) verlengen tot',
    ],
    'modal_text_4' => [
        'en' => 'Send demo to',
        'nl' => 'Stuur demo naa',
    ],
    'modal_text_5' => [
        'en' => 'Are you sure you want to send a demo email',
        'nl' => 'Weet je zeker dat je een demo-mail naar wilt versturen',
    ],
    'modal_text_6' => [
        'en' => 'This will also end the membership for the garage.',
        'nl' => 'Dit zal ook het lidmaatschap voor de bedrijf beëindigen.',
    ],
    'manage' => [
        'en' => 'Manage',
        'nl' => 'Beheer',
    ],
    'recognition' => [
        'en' => 'Recognition',
        'nl' => 'Erkenning',
    ],
    'member_to' => [
        'en' => 'Member to',
        'nl' => 'Lid tot',
    ],
    'services' => [
        'en' => 'Services',
        'nl' => 'Diensten',
    ],
    'affiliated_with' => [
        'en' => 'Affiliated with',
        'nl' => 'Aangesloten bij',
    ],
    'total_orders' => [
        'en' => 'Received orders',
        'nl' => 'Ontvangen opdrachten',
    ],
    'accepted_orders' => [
        'en' => 'Accepted orders',
        'nl' => 'Uitgevoerde opdrachten',
    ],
    'revenue' => [
        'en' => 'Revenue',
        'nl' => 'Omzet',
    ],
    'average_turnover' => [
        'en' => 'Average turnover',
        'nl' => 'Gemiddelde omzet',
    ],
    'login_credentials' => [
        'en' => 'Login credentials have been sent to the garage.',
        'nl' => 'Inloggegevens zijn verzonden naar de garage.',
    ],
    'membership_already_active' => [
        'en' => 'Membership is already active for the garage.',
        'nl' => 'Het lidmaatschap is al actief voor de bedrijf.',
    ],
    'membership_added' => [
        'en' => 'Membership added for the garage.',
        'nl' => 'Lidmaatschap toegevoegd voor de bedrijf.',
    ],
    'membership_updated' => [
        'en' => 'Membership updated for the garage.',
        'nl' => 'Lidmaatschap bijgewerkt voor de bedrijf.',
    ],
    'membership_deactivated' => [
        'en' => ' is no more a member.',
        'nl' => ' is geen lid meer.',
    ],
];

generateFile($dictionary, basename(__FILE__));
