const REQUEST_TYPE_POST = 'post';
const REQUEST_TYPE_GET  = 'get';
const DATA_TYPE_JSON = 'json';
const AJAX_ERROR_MESSAGE = document.getElementById('requestErrorMessageInpt').value;
const BORDER_COLOR = 'border-color';
const RED_COLOR = 'red';
const LIGHT_GREY_COLOR = 'lightgrey';

$(function(){

	$('.datepicker').datepicker({
        dayNames: [ "Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag"],
        dayNamesMin: [ "Zo", "Ma", "Di", "Wo", "Do", "Vr", "Za"],
        monthNames: [ "Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"],
        dateFormat: "dd-mm-yy",
        firstDay: 1,
        minDate: 0
    });

	$.fn.setError = (element, message) => {
		var parent = element.parents('.form-errors:eq(0)');
		if(parent.hasClass('has-error'))
			return;
		parent.addClass('has-error').append('<span class="help-block">' + message + '</span>')
	};

	$.fn.unsetError = (element) => {
		var parent = element.parents('.form-errors:eq(0)');
		parent.removeClass('has-error');	
		parent.find('.help-block').remove();
	};

	$.fn.displayErrors = (errors) => {
	    var html="";
	    $.each(errors, function(i, v) {
	        html += "<div class='errorMessage alert alert-danger'>"                                                 +
	                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"  +
	                    v                                                                                           +
	                "</div>";                
	    });
	    return html;
	};

	$.fn.ajaxCall = (type, url, data, dataType) => {
		$('#errorsDiv').html('');
		return new Promise( (resolve, reject) => {
			var ajaxRequest = {
				type: type,
				url: url,
				data: data,
		        dataType: dataType,
		        success: function (data) {
		            resolve(data);
		        },
		        error: function (data) {
                    response = JSON.parse(data.responseText);
                    var errors = [];
                    $.each(response.errors, function(i, v) {
                        $.each(v, function(x, e) {
                            errors.push(e);
                        });
                    });
                    console.log(errors);
                    var html = $.fn.displayErrors(errors);
                    $('#errorsDiv').html(html);
                    $('#errorsDiv').focus();
		        	reject(data);
		        }
			};
			if (type === REQUEST_TYPE_POST) {
		        var headers = {
		        	headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		        	contentType: 'application/json; charset=utf-8'
		        };
		        ajaxRequest = Object.assign(headers, ajaxRequest);
			}

			$.ajax(ajaxRequest);
		});
	};

	$.fn.ajaxBlockUI = (loadingMessage='') => {
		$.blockUI({ message: `<h3> ${loadingMessage} <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></h3>`, baseZ: 1005 });
	};

	$.fn.allowedKey = (keyCode) => {
		// backspace, enter, delete, dash
		var response = null;
		var allowedKeyCodes = [8, 13, 46, 189];
		if ( ($.inArray(keyCode, allowedKeyCodes) > -1) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) ){
			response = true;
		}
		return response;
	};

	$.fn.validateRequest = (form) => {
		form.find('.required').css(BORDER_COLOR, LIGHT_GREY_COLOR);
		var response = true;
		$.each(form.find('.required'), (i, v) => {
			if(!$(v).val().trim().length) {
				$(v).focus();
				$(v).css(BORDER_COLOR, RED_COLOR);
				response = false;
				return response;
			}
		});
		
		return response;
	};

	$.fn.generateNoteContent = (notes, div) => {
		var notesDiv = '';
		$.each(notes, function(key, note){
			notesDiv += `<div class='col-md-12 note'><p>${note.note}</p><p class='noteUser'>${note.createdBy} | ${note.createdAt}</p></div>`;
		});
		div.html(notesDiv);
	};

	$('#adminLoginBtn').on('click', function(){
		$.fn.ajaxBlockUI();
		var url = $(this).attr('data-route');
		var data = {};
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			$.unblockUI();
			if(successResponse.response){
				window.location = successResponse.route;
			}
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});
});