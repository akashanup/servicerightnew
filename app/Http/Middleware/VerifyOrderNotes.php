<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Closure;
use Lang;

class VerifyOrderNotes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $order = Order::find(request()->input('orderId'));
        if (in_array($order->status, Order::SHOWABLE_STATUS) && $order->verifyLock()) {
            return $next($request);
        }
        $controller = new Controller;
        return $controller->sendResponse(Lang::get('globals.unauthorized_text'));
    }
}
