<?php

use App\Models\Garage;
use App\Models\GarageOrder;
use App\Models\Order;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(GarageOrder::class, function (Faker $faker) {
    $statuses = ['accepted_by_garage', 'rejected_by_garage', 'completed'];
    return [
        'status' => $statuses[rand(0, 2)],
        'order_id' => Order::whereIn('status', $statuses)->inRandomOrder()->first()->id,
        'garage_id' => Garage::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
