<?php

namespace App\Providers;

use App\Models\Order;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('employees.header', function ($view) {
            $user = auth()->user();
            $userType = $user ? $user->type : null;
            $view->with([
                'quotedOrdersCount' => Order::where('status', Order::QUOTED)->count(),
                'confirmedOrdersCount' => Order::where('status', Order::CONFIRMED)->count(),
                'acceptedByGarageOrdersCount' => Order::where('status', Order::ACCEPTED_BY_GARAGE)->count(),
                'completedOrdersCount' => Order::where('status', Order::COMPLETED)->count(),
                'userType' => $userType,
                'authenticatedUser' => $user,
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
