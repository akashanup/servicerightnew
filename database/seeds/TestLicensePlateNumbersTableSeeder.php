<?php

use App\Models\LicensePlateNumber;
use Illuminate\Database\Seeder;

class TestLicensePlateNumbersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(LicensePlateNumber::class, 100)->create();
    }
}
