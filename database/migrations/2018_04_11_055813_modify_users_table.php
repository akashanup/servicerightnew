<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // Since it has enum columns so schema builder wont work.
            // DB::statement is a workaround for this.
            DB::statement('ALTER TABLE users CHANGE name name VARCHAR(50)');
            DB::statement('ALTER TABLE users CHANGE nickname nickname VARCHAR(50)');
            DB::statement('ALTER TABLE users CHANGE email email VARCHAR(50)');
            DB::statement('ALTER TABLE users CHANGE phone phone VARCHAR(15)');
            DB::statement('CREATE UNIQUE INDEX users_phone_unique ON users (phone)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            DB::statement('ALTER TABLE users CHANGE name name VARCHAR(255)');
            DB::statement('ALTER TABLE users CHANGE email email VARCHAR(255)');
            DB::statement('ALTER TABLE users CHANGE phone phone VARCHAR(255)');
            DB::statement('ALTER TABLE users CHANGE nickname nickname VARCHAR(255)');
            DB::statement('ALTER TABLE users DROP INDEX users_phone_unique');
        });
    }
}
