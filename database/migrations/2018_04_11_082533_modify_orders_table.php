<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            // Since it has enum columns so schema builder wont work.
            // DB::statement is a workaround for this.
            DB::statement('ALTER TABLE orders CHANGE pickup_time pickup_time VARCHAR(50)');
            DB::statement('ALTER TABLE orders CHANGE verification_token verification_token VARCHAR(100)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            DB::statement('ALTER TABLE orders CHANGE pickup_time pickup_time VARCHAR(255)');
            DB::statement('ALTER TABLE orders CHANGE verification_token verification_token VARCHAR(255)');
        });
    }
}
