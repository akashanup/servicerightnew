@extends('employees.content')

@section('employee-content')
    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ __('employee-place-order.schedule_new_assignment') }}</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding bg-gray">
        <div class="container">
            <div class="row" id="userDetails">
                <div class="col-md-12">
                    <h3>1. {{ __('employee-place-order.customer_data') }}</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.salutation') }}:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="salutationSelect">
                                <option value="Mr.">{{ __('employee-place-order.mr') }}.</option>
                                <option value="Ms.">{{ __('employee-place-order.miss') }}.</option>
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.name') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control required" type="text" placeholder="{{ __('employee-place-order.name') }}..." id="nameInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.phone_number') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control required" type="tel" placeholder="{{ __('employee-place-order.phone_number') }}..." id="phoneInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Email:</label>
                        <div class="col-sm-8">
                            <input class="form-control required" type="email" placeholder="Email..." id="emailInpt">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">Country:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="countryInpt">
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Postcode + nr:</label>
                        <div class="col-sm-6">
                            <input class="form-control required" type="text" placeholder="Postcode..." id="postalCodeInpt">
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control homenr required" type="text" placeholder="Nr.." id="houseNumberInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.address') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" placeholder="{{ __('employee-place-order.address') }}..." id="addressInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.city') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" placeholder="{{ __('employee-place-order.city') }}..." id="cityInpt">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row" id="vehicleDetails">
                <div class="col-md-12">
                    <h3>2. {{ __('employee-place-order.data_auto') }}</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.license_plate') }}:</label>
                        <div class="col-sm-4">
                            <input class="form-control licence-plate required" type="text" placeholder="{{ __('employee-place-order.license_plate') }}..." id="licensePlateInpt">
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-primary getCarDetails" id="carPickUpBtn">{{ __('employee-place-order.pick_up') }}</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.brand') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" disabled="disabled" id="carBrandInpt" style="display: none">
                            <select class="form-control car-brand" id="carBrandSelect">
                                <option value="">{{ __('employee-place-order.select') }} {{ __('employee-place-order.brand') }}</option>
                                @foreach($brands as $brand)
                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Model:</label>
                        <div class="col-sm-8">
                            <input type="text" id="carModelInpt" class="form-control" disabled="disabled" style="display: none">
                            <select class="form-control car-model" disabled="disabled"  id="carModelSelect">
                                <option value="">{{ __('employee-place-order.select') }} model</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.fuel') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" id="carFuelTypelInpt" disabled="disabled" class="form-control" style="display: none">
                            <select class="form-control car-fuel" id="carFuelTypeSelect">
                                @foreach($fuelTypes as $fuelType)
                                    <option value="{{ $fuelType }}">{{ $fuelType }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.construction_year') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control car-build-year required" placeholder="{{ __('employee-place-order.construction_year') }}..." type="text" id="buildYearInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">APK verloopt op:</label>
                        <div class="col-sm-8">
                            <input class="form-control datepicker car-apk required" type="text" placeholder="APK verloopt op..." id="mandatoryServiceExpiryDateInpt">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding bg-gray">
        <div class="container service-selection">
            <div class="row">
                <div class="col-md-12">
                    <h3>3. {{ __('employee-place-order.data_maintenance') }}</h3>
                </div>

                @foreach($parentCategories as $category)
                    <div class="row">
                        <h4 style="margin: 0.5em 0 0.1em 0">{{ $category->name }}</h4>
                        @foreach($category->services as $service)
                            <div class="col-md-4">
                                <div class="form-check">
                                    <input class="form-check-input serviceCheckBox" type="checkbox" value="{{ $service->id }}">
                                    <label class="form-check-label">{{ $service->name }}</label>
                                    <div class="input-group price-hidden">
                                        <div class="row">
                                            <div class="col-md-1 prepend-euro">
                                                &euro;
                                            </div>
                                            <div class="col-md-4">
                                                <input class="form-control form-control-sm item-price servicePrice" type="number" min="0.00" step="0.1" value="0.00">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row" id="orderDetails">
                <div class="col-md-12">
                    <h3>4. {{ __('employee-place-order.overview') }} &amp; planning</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.date') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control datepicker required" type="tel" placeholder="{{ __('employee-place-order.date') }}..." id="pickupDateInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.alternative') }} {{ __('employee-place-order.date') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control datepicker" type="tel" placeholder="{{ __('employee-place-order.alternative') }} {{ __('employee-place-order.date') }}..." id="alternativePickupDateInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.time') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control required" type="tel" placeholder="08:00 - 09:00" id="pickupTimeInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.pick_up_bring') }}:</label>
                        <div class="col-sm-8">
                            <select class="form-control required" id="pickupTypeSelect">
                                <option value="">{{ __('employee-place-order.select') }}</option>
                                <option value="pickup_from_client">{{ __('employee-place-order.pickup_from_client') }}</option>
                                <option value="client_brings_itself">{{ __('employee-place-order.client_brings_itself') }}</option>
                                <option value="repair_at_client_location">{{ __('employee-place-order.repair_at_client_location') }}</option>
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.comments_additional_work') }}:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="additionalWorkText"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <button type="button" class="btn btn-primary btn-block placeOrderBtn" data-type="confirmed">{{ __('employee-globals.schedule') }}</button>
                    <button type="button" class="btn btn-info btn-block placeOrderBtn" data-type="quoted">{{ __('employee-place-order.send_as_an_offer') }}</button>
                    <button type="button" class="btn btn-secondary btn-block placeOrderBtn" data-type="drafted">{{ __('employee-place-order.save_as_draft') }}</button>

                    <h4>{{ __('employee-place-order.total_price') }}: &euro; <span id="totalPrice">0,00</span></h4>
                </div>
            </div>
        </div>
    </section>

    <!-- Hidden fields required for javascript -->
    <input type="hidden" id="fetchAddressRoute" value="{{ route('addresses.client') }}">
    <input type="hidden" id="fetchedAddressId" value="">
    <input type="hidden" id="fetchVehicleRoute" value="{{ route('licensePlateNumbers.vehicle') }}">
    <input type="hidden" id="fetchedlicensePlateNumberId" value="">
    <input type="hidden" id="fetchedServices" value="">
    <input type="hidden" id="saveOrderRoute" value="{{ route('orders.store') }}">
    <!-- Scripts -->
    @section('employee-scripts')
        @include('scripts.save-order')
        <script type="text/javascript" src="{{ asset('js/custom/employee-service-selection.js').'?v='.config('app.version', '1') }}"></script>
        <script type="text/javascript" src="{{ asset('js/custom/employee-create-order.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
