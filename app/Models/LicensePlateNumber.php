<?php

namespace App\Models;

use App\Models\Order;
use App\Utils\Curl;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LicensePlateNumber extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vehicle_id', 'license_plate_number', 'mandatory_service_expiry_date',
    ];

    /**
     * Format mandatory_service_expiry_date from yyyy-mm-dd to dd-mm-yyyy
     *
     * @return date $mandatory_service_expiry_date
     */
    public function mandatory_service_expiry_date()
    {
        return $this->mandatory_service_expiry_date
        ? Carbon::parse($this->mandatory_service_expiry_date)->format('d-m-Y') : '';
    }

    /**
     * Get the the vehicle for the license plate number.
     *
     * @return App\Models\Vehicle
     */
    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    } //end postalCode()

    /**
     * Get the orders for the license plate number.
     *
     * @return Collection
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    } //end orders()

    /**
     * Fetch Vehicle from API.
     *
     * @param string $licensePlateNumber
     * @return array
     */
    public static function fetch($licensePlateNumber)
    {

        $json = [];
        $json['response'] = false;
        $thisLicensePlateNumber = static::where('license_plate_number', $licensePlateNumber)->first();
        if ($thisLicensePlateNumber) {
            $vehicle = $thisLicensePlateNumber->vehicle;
            $json['services'] = $vehicle->services;
            $json['fuel_type'] = $vehicle->fuel_type;
            $json['brand'] = $vehicle->brand->name;
            $json['model'] = $vehicle->model;
            $json['build_year_start'] = $vehicle->build_year_start();
            $json['mandatory_service_expiry_date'] = $thisLicensePlateNumber->mandatory_service_expiry_date();
            $json['licensePlateNumberId'] = $thisLicensePlateNumber->id;
            $json['response'] = true;
        } else {
            $vehicle = Curl::fetchVehicle($licensePlateNumber);
            $json = $vehicle;
            if ($vehicle['response']) {
                // Add vehicle in DB to save time in future.
                $brand = Brand::createUnique($vehicle['brand']);
                $newVehicle = Vehicle::createUnique($brand->id, $vehicle['fuel_type'], $vehicle['model'],
                    $vehicle['build_year_start']->toDateString());
                $licensePlateNumber = static::create(
                    [
                        'vehicle_id' => $newVehicle->id,
                        'license_plate_number' => $licensePlateNumber,
                        'mandatory_service_expiry_date' => $vehicle['mandatory_service_expiry_date'],
                    ]
                );
                $json['build_year_start'] = $json['build_year_start']->format('d-m-Y');
                $json['mandatory_service_expiry_date'] = $json['mandatory_service_expiry_date'] ? $json['mandatory_service_expiry_date']->format('d-m-Y') : '';
                $json['licensePlateNumberId'] = $licensePlateNumber->id;
                $json['services'] = $newVehicle->services;
            }
        }
        return $json;
    } //end fetch()
}
