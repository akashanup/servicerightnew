<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Country;
use App\Models\Garage;
use App\Models\Order;
use App\Utils\PickupType;
use App\Utils\YajaraDataTable;
use Carbon\Carbon;
use DB;
use Lang;

class OrderController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator()
    {
        return request()->validate(
            [
                'salutation' => 'required',
                'name' => 'required|string|max:50',
                'phone' => 'required|string|max:15',
                'email' => 'required|string|email|max:50',
                'addressId' => 'nullable|numeric',
                'licensePlateNumberId' => 'nullable|numeric',
                'serviceIds' => 'required|string',
                'pickupDate' => 'required|string',
                'alternativePickupDate' => 'nullable|date',
                'pickupTime' => 'required|string|regex:/\d{2}:\d{2}-\d{2}:\d{2}/',
                'pickupType' => 'required|string',
                'orderStatus' => 'required|string',
                'additionalWork' => 'nullable|string',
            ]
        );
    } //end validator()

    /**
     * Display order creation form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'employees.create-order',
            [
                'countries' => Country::all(),
                'parentCategories' => Category::whereNull('parent_id')->get(),
                'brands' => Brand::all(),
                'fuelTypes' => json_decode(config('constants.FUEL_TYPES'), true),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        DB::beginTransaction();
        $json = Order::store($this->validator());
        DB::commit();
        return response()->json($json);
    }

    /**
     * Edit an order.
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        if (in_array($order->status, Order::EDITABLE_STATUS)) {
            $user = $order->user;
            $address = $order->address;
            $postalCode = $address->postalCode;
            $city = $postalCode->city;
            $country = $city->state->country;
            $licensePlateNumber = $order->licensePlateNumber;
            $vehicle = $licensePlateNumber->vehicle;
            return view(
                'employees.order-edit',
                [
                    'order' => $order,
                    'user' => $user,
                    'address' => $address,
                    'postalCode' => $postalCode,
                    'city' => $city,
                    'country' => $country,
                    'licensePlateNumber' => $licensePlateNumber,
                    'vehicle' => $vehicle,
                    'otherCountries' => Country::where('id', '!=', $country->id)->get(),
                    'parentCategories' => Category::whereNull('parent_id')->get(),
                    'brands' => Brand::all(),
                    'fuelTypes' => json_decode(config('constants.FUEL_TYPES'), true),
                    'orderServiceIds' => $order->services->pluck('id')->toArray(),
                    'pickupTypes' => PickupType::get(),
                    'callbackCount' => $order->histories()->where('comment', Order::CALLBACK_REQUESTED)->count(),
                ]
            );
        }
        return $this->sendResponse(Lang::get('globals.request_error_message'));
    }

    /**
     * Update resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id)
    {
        $order = Order::find($id);
        if (in_array($order->status, Order::EDITABLE_STATUS)) {
            DB::beginTransaction();
            $json = $order->updateOrder($this->validator());
            DB::commit();
            return response()->json($json);
        }
        return $this->sendResponse(Lang::get('globals.request_error_message'));
    }

    /**
     * Listing of order based on status.
     *
     * @param string $status
     * @return \Illuminate\Http\Response
     */
    public function status($status)
    {
        if (in_array($status, Order::SHOWABLE_STATUS)) {
            return view('employees.order-status', ['thisStatus' => $status]);
        }
        return redirect()->route('home')->withErrors(Lang::get('globals.request_error_message'));
    }

    /**
     * Listing of order based on status.
     *
     * @param string $status
     * @return array
     */
    public function statusData($status, $orderId = null)
    {
        if (in_array($status, Order::SHOWABLE_STATUS)) {
            return YajaraDataTable::orders(Order::ordersData($status, $orderId));
        }
        return $this->sendResponse(Lang::get('globals.request_error_message'));
    }

    /**
     * Update lock status of an order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function lock($id)
    {
        DB::beginTransaction();
        $order = Order::find($id);
        if ($order->status == Order::CONFIRMED) {
            $json = $order->lockStatus();
            DB::commit();
            return response()->json($json);
        }
        return $this->sendResponse(Lang::get('globals.request_error_message'));
    }

    /**
     * Complete an order request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function completeRequest($id)
    {
        $order = Order::find($id);
        if ($order->status == Order::ACCEPTED_BY_GARAGE) {
            $json = $order->user->sendOrderCompleteRequestNotification($order);
            return response()
                ->json(['response' => true, 'message' => Lang::get('globals.email_has_been_sent')]);
        }
        return $this->sendResponse(Lang::get('globals.request_error_message'));
    }

    /**
     * Complete an order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function complete()
    {
        $order = Order::find(request('orderId'));
        if ($order->status == Order::ACCEPTED_BY_GARAGE) {
            DB::beginTransaction();
            $validatedData = request()->validate(
                [
                    'orderId' => 'required|numeric',
                    'status' => 'required|string',
                    'type' => 'nullable|string',
                ]
            );
            $json = $order->completeOrder($validatedData);
            DB::commit();
            return response()->json($json);
        }
        return $this->sendResponse(Lang::get('globals.request_error_message'));
    }

    /**
     * Assign an order to a garage.
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function assign($id)
    {
        $order = Order::find($id);
        if ($order->status == Order::CONFIRMED) {
            $order->checkClientLatLng();
            $licensePlateNumber = $order->licensePlateNumber;
            $vehicle = $licensePlateNumber->vehicle;
            $user = $order->user;
            $address = $order->address;
            $postalCode = $address->postalCode;
            return view(
                'employees.order-assign',
                [
                    'order' => $order,
                    'licensePlateNumber' => $licensePlateNumber,
                    'vehicle' => $vehicle,
                    'user' => $user,
                    'address' => $address,
                    'postalCode' => $postalCode,
                    'parentCategories' => Category::whereNull('parent_id')->get(),
                    'allCategories' => json_encode(Category::select('id', 'name')->get()->toArray()),
                    'pickupType' => PickupType::get($order->pickup_type),
                    'garageTypes' => Garage::whereNotNull('type')->select('type')->distinct()->get(),
                    'currentDate' => Carbon::now()->toDateString(),
                    'buildYearStart' => Carbon::parse($vehicle->build_year_start)->year,
                    'mandatoryServiceExpiryDate' => $licensePlateNumber->mandatory_service_expiry_date(),
                    'pickupDate' => $order->pickup_date(),
                    'alternativePickupDate' => $order->alternative_pickup_date(),
                    'notesData' => $order->notesData(),
                ]
            );
        }
        return $this->sendResponse(Lang::get('globals.request_error_message'));
    }

    /**
     * Update price of an order due to additional work
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePrice()
    {
        $order = Order::find(request('orderId'));
        if ($order->status == Order::ACCEPTED_BY_GARAGE) {
            DB::beginTransaction();
            $validatedData = request()->validate(
                [
                    'orderId' => 'required|numeric',
                    'invoice' => 'required|file|mimes:pdf,jpg,jpeg,gif,png',
                    'additionalWork' => 'nullable|string',
                    'totalPrice' => 'required|string',
                    'note' => 'nullable|string',
                ]
            );

            $json = $order->updatePrice($validatedData);
            DB::commit();
            return response()->json($json);
        }
        return $this->sendResponse(Lang::get('globals.request_error_message'));
    }
}
