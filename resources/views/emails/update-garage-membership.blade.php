@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $garageName }},
	<br/>
	<br/>
	{{ __('manage-garage-email.email_text_17') }} {{ $memberExpiry }}
	{{ __('manage-garage-email.email_text_18') }}
	<br>
	<br/>
	<a href="{{ route('home') }}" style="padding:15px; font-size:14px; text-decoration:none; color:white; display:inline-block; background:#95b41c;" target="_blank">
            {{ __('globals.login') }} &raquo;
    </a>
    <br>
    <br>

    {{ __('manage-garage-email.email_text_15') }} {{ config('constants.CUSTOMER_CARE_NUMBER') }} {{ __('manage-garage-email.email_text_16') }} {{ config('constants.CUSTOMER_CARE_EMAIL') }}.
@endsection
