@extends('employees.content')

<!-- CSS -->
@section('employee-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('employee-content')
    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if($thisStatus == \App\Models\Order::QUOTED)
                        <h2>{{ __('employee-order-status.check_jobs') }}</h2>
                    @elseif($thisStatus == \App\Models\Order::CONFIRMED)
                        <h2>{{ __('employee-order-status.open_assignments') }}</h2>
                    @endif
                    <p>{{ __('employee-order-status.paragraph_text_1') }}</p>
                </div>
                <div class="col-md-12">
                    <span style="float: right;" id="showAllOrder">
                        <span class="btn btn-sm btn-primary">{{ __('globals.show_all') }}</span>
                    </span>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="orderTable">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>ID</th>
                                <th>{{ __('employee-order-status.shift') }}</th>
                                <th>{{ __('employee-order-status.customer_address') }}</th>
                                <th>{{ __('employee-place-order.date') }} {{ __('employee-order-status.shift') }}</th>
                                <th>{{ __('globals.price') }}</th>
                                <th>&nbsp;</th>
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="notesModel" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        {{ __('employee-order-status.notes_for') }} order
                        <label id="modalOrderId"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="notesDiv">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control" id="noteInpt"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-primary" id="saveNoteBtn">{{ __('globals.save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($thisStatus == \App\Models\Order::ACCEPTED_BY_GARAGE)
        <div class="modal fade" id="priceUpdateModal" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            {{ __('employee-order-status.other_work') }}
                            <label id="priceUpdateModalOrderId"></label>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label>
                                {{ __('employee-order-status.modal_text_1') }}
                                <span id="clientPhoneModal"></span>
                            </label>
                            <br>
                            <label>
                                {{ __('employee-order-status.modal_text_2') }} (incl. BTW)
                            </label>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label">{{ __('employee-order-status.copy_of_invoice') }}:</label>
                            <div>
                                <input class="form-control" type="file" id="invoiceModalInpt" placeholder="{{ __('employee-order-status.no_file_choosen') }}">
                            </div>
                            <span>{{ __('employee-order-status.modal_text_3') }}</span>
                            <label>Accepted formats: .pdf, .jpg, .jpeg, .gif and .png</label>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label">{{ __('employee-order-status.modal_text_4') }}:</label>
                            <div>
                                <textarea class="form-control" id="additionalWorkModalText"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label">{{ __('employee-order-status.modal_text_5') }}:</label>
                            <div>
                                <input class="form-control" type="number" min="0.00" step="0.10" id="priceDifferenceModalInpt" placeholder="0.00">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label">OR</label>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label">{{ __('employee-place-order.total_price') }}:</label>
                            <div>
                                <input class="form-control" type="number" min="0.00" step="0.10" id="priceModalInpt" placeholder="0.00">
                                <input type="hidden" id="currentPriceInpt">
                            </div>
                            <span>(Incl. BTW)</span>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-form-label">{{ __('employee-order-status.note') }} ServiceRight:</label>
                            <div>
                                <textarea class="form-control" id="noteModalInpt"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="pull-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                                <button type="button" class="btn btn-primary" id="priceUpdateBtn">{{ __('globals.save') }}</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="completeOrderModal" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            {{ __('employee-order-status.modal_text_6') }}
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="modalCompleteOrderId" value="">
                        <div class="row">
                            <h5>{{ __('employee-order-status.modal_text_7') }}</h5>
                        </div>
                        <div class="row">
                            <label>
                                {{ __('employee-place-order.license_plate') }}:
                                <span id="modalLicensePlateNumber"></span>
                            </label>
                        </div>
                        <div class="row">
                            <label>
                                Model:
                                <span id="modalModel"></span>
                            </label>
                        </div>
                        <div class="row">
                            <label>
                                {{ __('employee-place-order.maintenance') }} {{ __('employee-assign-order.on') }}:
                                <span id="modalPickupDate"></span>
                            </label>
                        </div>
                        <div class="row">
                            <label>
                                {{ __('employee-place-order.services_list') }}:
                                <ul id="modalServiceList">

                                </ul>
                            </label>
                        </div>
                        <div class="row">
                            <h3 style="color:#309ed4; margin:0px;">
                                {{ __('employee-place-order.total_price') }}: &euro; <span id="modalPriceInpt"></span> <span style="font-size:10px;">(Incl. BTW)</span>
                            </h3>
                            <br>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-primary btn-block completeOrderModalBtn" data-status="completed">{{ __('employee-order-status.confirm_agree') }}</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-secondary" id="cancelModalBtn">{{ __('globals.cancel') }}</button>
                            </div>
                        </div>
                        <div class="row">
                            <br>
                            <br>
                        </div>
                        <div class="row" id="orderCalcellationTypeDiv" style="display: none">
                            <div class="form-group col-sm-6">
                                <select class="form-control" id="cancellationTypeSelect">
                                    <option value="{{ \App\Models\Order::CANCELLED_WITH_NO_CANCELLATION_COST }}">{{ __('employee-order-status.cancelled_with_no_cancellation_cost') }}.</option>
                                    <option value="{{ \App\Models\Order::CANCELLED_GARAGE_GETS_CANCELLATION_COST }}">{{ __('employee-order-status.cancelled_garage_gets_cancellation_cost') }}.</option>
                                    <option value="{{ \App\Models\Order::CANCELLED_CLIENT_GETS_CANCELLATION_COST }}">{{ __('employee-order-status.cancelled_client_gets_cancellation_cost') }}.</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-secondary completeOrderModalBtn" data-status="cancelled">{{ __('globals.cancel') }}</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="priceUpdateRoute" value="{{ route('orders.updatePrice') }}">
        <input type="hidden" id="completeOrderRoute" value="{{ route('orders.complete') }}">
    @endif

    <input type="hidden" value="{{ route('orders.data', $thisStatus) }}" id="ordersDataRoute">
    <input type="hidden" value="{{ route('orderNotes.show') }}" id="fetchNoteRoute">
    <input type="hidden" value="{{ route('orderNotes.store') }}" id="storeNoteRoute">

    <!-- Scripts -->
    @section('employee-scripts')
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/custom/order-status.js').'?v='.config('app.version', '1') }}"></script>
        <script defer type="text/javascript" src="{{ asset('js/custom/order-lock.js').'?v='.config('app.version', '1') }}"></script>
        <script defer type="text/javascript" src="{{ asset('js/custom/order-note.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
