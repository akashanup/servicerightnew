<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $servername = config('constants.OLD_DB_SERVERNAME');
        $username = config('constants.OLD_DB_USERNAME');
        $password = config('constants.OLD_DB_PASSWORD');
        $dbname = config('constants.OLD_DB_NAME');

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        foreach (Category::all() as $key => $category) {
            $sql1 = "SELECT code, naam, categorie, prijs from aanbod WHERE branche LIKE 'autos' AND categorie LIKE '%" . $category->name . "%'";
            $result1 = $conn->query($sql1);
            while ($row1 = $result1->fetch_assoc()) {
                $sql2 = "SELECT naam, omschrijving from standaard_omschrijvingen WHERE branche LIKE 'autos' AND naam LIKE '" . $row1['code'] . "'";
                $result2 = $conn->query($sql2);
                while ($row2 = $result2->fetch_assoc()) {
                    DB::table('services')->insert(
                        [
                            'category_id' => $category->id,
                            'name' => $row1['naam'],
                            'code' => $row2['naam'],
                            'description' => $row2['omschrijving'] ? utf8_encode($row2['omschrijving']) : 'No data found.',
                            'slug' => str_slug($row1['naam'], '-'),
                            'fixed_price' => ($row1['prijs'] == '#PRICE#' ? 'no' : 'yes'),
                        ]
                    );
                }
            }
        }

        DB::table('services')->insert(
            [
                'category_id' => 5,
                'name' => 'Ophaal &amp; Aflever S...',
                'description' => 'No data found!',
                'slug' => 'ophaal-amp-aflever-s',
                'fixed_price' => 'no',
            ]
        );
        $conn->close();
    }
}
