<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Company;
use App\Models\Garage;
use App\Utils\YajaraDataTable;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Lang;

class GarageController extends Controller
{

    /**
     * Manage garages.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        return view(
            'admins.manage-garage',
            [
                'parentCategories' => Category::whereNull('parent_id')->get(),
                'companies' => Company::all(),
                'garageTypes' => Garage::whereNotNull('type')->select('type')->distinct()->get(),
            ]
        );
    }

    /**
     * Garages data.
     *
     * @return \Illuminate\Http\Response
     */
    public function data($garageId = null)
    {
        return YajaraDataTable::garages(Garage::garageData($garageId));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeOrder()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'garageId' => 'required|numeric',
                'orderId' => 'required|numeric',
                'status' => 'required|string',
            ]
        );

        $json = Garage::find(request('garageId'))->storeOrder($validatedData['orderId'], $validatedData['status']);
        DB::commit();
        return response()->json($json);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeNotes()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'garageId' => 'required|numeric',
                'notes' => 'nullable|string',
            ]
        );

        $garage = Garage::find(request('garageId'));
        $garage->update(['notes' => $validatedData['notes']]);
        DB::commit();
        return response()->json(Garage::fetchAll(null, [], [], $garage->id));
    }

    /**
     * Update a created resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCategories()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'garageId' => 'required|numeric',
                'categoryIds' => 'nullable|array',
            ]
        );

        $garage = Garage::find(request('garageId'));
        $garage->addCategories($validatedData['categoryIds']);
        DB::commit();
        return response()->json(Garage::fetchAll(null, [], [], $garage->id));
    }

    /**
     * Fetch all garages.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchAll()
    {
        return response()->json(
            Garage::fetchAll(
                request('member'),
                request('categories') ? request('categories') : [],
                request('types') ? request('types') : []
            )
        );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator()
    {
        return request()->validate(
            [
                'name' => 'required|string|max:100',
                'contactPerson' => 'required|string|max:50',
                'moneyBirdId' => 'required|string',
                'phone' => 'required|string|max:15',
                'email' => 'required|email|max:50',
                'website' => 'required|string|max:100',
                'completeAddress' => 'required|string',
                'postalCode' => 'required|string|max:10',
                'city' => 'required|string|max:50',
                'type' => 'required|string',
                'latitude' => 'required|max:50',
                'longitude' => 'required|max:50',
                'categories' => 'required|array|max:50',
                'companies' => 'required|array|max:50',
            ]
        );
    } //end validator()

    /**
     * Store a garages.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        DB::beginTransaction();
        $json = Garage::store($this->validator());
        DB::commit();
        return response()->json($json);
    }

    /**
     * Show details of a garage.
     *
     * @param integer $garageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($garageId)
    {
        return response()->json(Garage::find(request('garageId'))->garageDetails());
    }

    /**
     * Update a garage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        DB::beginTransaction();
        $json = Garage::store($this->validator(), Garage::find(request('garageId')));
        DB::commit();
        return response()->json($json);
    }

    /**
     * Delete a garage.
     *
     * @param integer $garageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($garageId)
    {
        DB::beginTransaction();
        Garage::find(request('garageId'))->update(
            [
                'status' => Garage::INACTIVE,
                'membership_status' => Garage::INACTIVE,
            ]
        );
        DB::commit();
        return response()->json(['response' => true, 'message' => Lang::get('globals.company_deleted')]);
    }

    /**
     * Show sale notes of a garage.
     *
     * @param integer $garageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function saleNotes($garageId)
    {
        return response()->json(Garage::find(request('garageId'))->saleNotesData());
    }

    /**
     * Store sale notes of a garage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSaleNote()
    {
        DB::beginTransaction();

        $validatedData = request()->validate(
            [
                'saleNote' => 'required|string',
            ]
        );

        $garage = Garage::find(request('garageId'));
        $note = $garage->addSaleNote($validatedData['saleNote']);
        $json = $garage->saleNotesData();
        DB::commit();
        return response()->json($json);
    }

    /**
     * Show sale notes of a garage.
     *
     * @param integer $garageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function stats($garageId)
    {
        return response()->json(Garage::find(request('garageId'))->stats());
    }

    /**
     * Create and send login credentials for garage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendDemo()
    {
        DB::beginTransaction();
        $json = Garage::find(request('garageId'))->sendDemo();
        DB::commit();
        return response()->json($json);
    }

    /**
     * Add membership for garage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMembership()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'discount' => 'nullable|numeric',
            ]
        );
        $json = Garage::find(request('garageId'))->createMembership($validatedData);
        DB::commit();
        return response()->json($json);
    }

    /**
     * Update membership for garage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMembership()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'discount' => 'nullable|numeric',
                'type' => 'required', Rule::in(['renew', 'cancel']),
                'memberExpiry' => 'nullable|string',
            ]
        );
        $json = Garage::find(request('garageId'))->updateMembership($validatedData);
        DB::commit();
        return response()->json($json);
    }

    /**
     * Activate membership for garage.
     *
     * @param string $token
     * @return \Illuminate\Http\Response
     */
    public function activateMembership($token)
    {
        $garage = Garage::where('activation_token', $token)->first();
        if ($garage) {
            DB::beginTransaction();
            $garage->updateMembershipDate();
            $garage->user->sendGarageConfirmMembership($garage);
            DB::commit();
            session()->flash('status', Lang::get('manage-garage-email.email_text_19'));
            return redirect()->route('login');
        }
        return redirect()->route('home')->withErrors(Lang::get('globals.link_expired'));
    }

    /**
     * Send recognition mail to garage user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function recognition()
    {
        $garage = Garage::find(request('garageId'));
        $garage->user->sendGarageRecognitionMail($garage);
        return response()->json(['response' => true, 'message' => Lang::get('globals.email_has_been_sent')]);
    }
}
