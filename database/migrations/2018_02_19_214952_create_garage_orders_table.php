<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGarageOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('garage_id');
            $table->enum('status', ['accepted_by_garage', 'rejected_by_garage', 'completed', 'cancelled_with_no_cancellation_cost', 'cancelled_garage_gets_cancellation_cost', 'cancelled_client_gets_cancellation_cost']);
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('garage_id')->references('id')->on('garages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garage_orders', function (Blueprint $table) {
            $table->dropForeign('garage_orders_order_id_foreign');
            $table->dropColumn('order_id');
            $table->dropForeign('garage_orders_garage_id_foreign');
            $table->dropColumn('garage_id');
        });
        Schema::dropIfExists('garage_orders');
    }
}
