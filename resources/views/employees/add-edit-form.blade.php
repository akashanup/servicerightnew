<form>
    <div class="form-group">
        <label for="employeeNameInpt">{{ __('employee-place-order.name') }}:</label>
        <input type="text" class="form-control employeeNameInpt">
    </div>
    <div class="form-group">
        <label for="employeeNicknameInpt">{{ __('users.nickname') }}:</label>
        <input type="text" class="form-control employeeNicknameInpt">
    </div>
    <div class="form-group">
        <label for="employeeEmailInpt">Email:</label>
        <input type="text" class="form-control employeeEmailInpt">
    </div>
    <div class="form-group">
        <label for="employeePasswordInpt">{{ __('users.password') }}:</label>
        <input type="password" class="form-control employeePasswordInpt" placeholder="******">
    </div>
    <div class="form-group">
        <label for="employeeStatusSlct">Status:</label>
        <select class="form-control employeeStatusSlct">
            <option value="{{ \App\Models\User::ACTIVE }}">{{ __('users.active') }}</option>
            <option value="{{ \App\Models\User::INACTIVE }}">{{ __('users.inactive') }}</option>
        </select>
    </div>
    <div class="form-group">
        <label for="employeeTypeSlct">Type:</label>
        <select class="form-control employeeTypeSlct">
            <option value="{{ \App\Models\User::SR_ADMIN }}">{{ __('users.admin') }}</option>
            <option value="{{ \App\Models\User::SR_EMPLOYEE }}">{{ __('users.employee') }}</option>
        </select>
    </div>
</form>
