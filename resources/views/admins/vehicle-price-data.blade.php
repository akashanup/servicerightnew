<section class="small-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>{{ __('globals.price') }} {{ $baseVehicle->brand->name }} {{ $baseVehicle->model }} {{ __('vehicles.manage') }}</h2>
			</div>
		</div>
	</div>
</section>

<section class="small-padding bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>{{ __('vehicles.basic_prices') }}</h3>
				<p>{{ __('vehicles.text1') }}</p> <p>{{ __('vehicles.text2') }}</p>
			</div>
			<div class="col-md-12">
				<table class="table">
					<tr>
						@foreach( $baseModelServicePrices as $key => $baseModelServicePrice)
							<td data-vehicleId="{{$baseVehicle->id}}" data-serviceId="{{ $baseModelServicePrice['serviceId'] }}">
								<label for="" class="form-label adjustTextLength">{{ $baseModelServicePrice['serviceName'] }}:</label>
								<input class="form-control serviceInpt servicePrice" type="number" step="0.1" min="0" value="{{ $baseModelServicePrice['price'] }}">
								<label for="" class="form-label">Diesel extra %:</label>
								<input class="form-control serviceInpt dieselPercentage" type="number" step="0.1" min="0" value="{{ $baseModelServicePrice['dieselPercentage'] }}">
							</td>
							@if(!($baseModelServicePrice['count']%4))
								</tr>
								<tr>
							@endif
						@endforeach
					</tr>
					<tr>
						<td>
                            <label for="" class="form-label">{{ __('vehicles.waterpump') }}</label>
                            @if($baseVehicle->waterpump_required == \App\Models\Vehicle::YES)
                            	<input type="checkbox" class="waterpompRequired" checked data-vehicleId="{{$baseVehicle->id}}">
                            @else
                            	<input type="checkbox" class="waterpompRequired" data-vehicleId="{{$baseVehicle->id}}">
                            @endif
                        </td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</section>

@foreach($childVehicles as $vehicle)
	<section class="small-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>{{ $vehicle['brandName'] }} {{ $vehicle['model'] }} ({{ $vehicle['buildYearStart'] }} - {{ $vehicle['buildYearEnd'] }})</h3>
					<p>{{ __('vehicles.text3') }}</p>
				</div>
				<div class="col-md-12">
					<table class="table">
						<tr>
							@foreach($vehicle['servicePrices'] as $key => $servicePrice)
								<td data-vehicleId="{{ $vehicle['id'] }}" data-serviceId="{{ $servicePrice['serviceId'] }}">
									<label for="" class="form-label adjustTextLength">{{ $servicePrice['serviceName'] }}:</label>
									<input class="form-control serviceInpt servicePrice" type="number" step="0.1" min="0" value="{{ $servicePrice['price'] == $baseModelServicePrices[$key]['price'] ? 0 : $servicePrice['price'] }}">
								</td>
								@if(!($servicePrice['count']%4))
									</tr>
									<tr>
								@endif
							@endforeach

						</tr>
						<tr>
							<td>
	                            <label for="" class="form-label">{{ __('vehicles.waterpump') }}</label>
	                            @if($vehicle['waterpumpRequired'] == \App\Models\Vehicle::YES)
	                            	<input type="checkbox" class="waterpompRequired" checked data-vehicleId="{{$vehicle['id']}}">
	                            @else
	                            	<input type="checkbox" class="waterpompRequired" data-vehicleId="{{$vehicle['id']}}">
	                            @endif
	                        </td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</section>
@endforeach
