<?php

include_once 'generate-file.php';

$dictionary = [
    'manage_price' => [
        'en' => 'Manage prices',
        'nl' => 'Prijzen beheren',
    ],
    'note' => [
        'en' => 'Note',
        'nl' => 'Let op',
    ],
    'there_are' => [
        'en' => 'There are',
        'nl' => 'Er zijn',
    ],
    'brands_without_prices' => [
        'en' => 'brands without prices',
        'nl' => 'merken zonder prijzen',
    ],
    'search_brand' => [
        'en' => 'Search prices on Brand',
        'nl' => 'Zoek prijzen op Merk',
    ],
    'search_license_plate' => [
        'en' => 'Search prices by license plate',
        'nl' => 'Zoek prijzen op kenteken',
    ],
    'edit_price' => [
        'en' => 'Edit prices',
        'nl' => 'Bewerk prijzen',
    ],
    'manage' => [
        'en' => 'manage',
        'nl' => 'beheren',
    ],
    'basic_prices' => [
        'en' => 'Basic prices',
        'nl' => 'Basisprijzen',
    ],
    'text1' => [
        'en' => 'If there are no prices for a specific year of construction, the system will fall back on the basic prices. Make sure that these are in any case filled in as much as possible.',
        'nl' => 'Als er geen prijzen zijn voor een specifiek bouwjaar dan valt het systeem terug op de basisprijzen. Zorg dus dat deze in ieder geval zo veel mogelijk zijn ingevuld.',
    ],
    'text2' => [
        'en' => 'For diesel cars, a surcharge is calculated on the basis of the percentage stated under each service. By default this is 5%',
        'nl' => 'Voor dieselautos wordt een toeslag berekend aan de hand van het percentage wat onder elke dienst staat. Standaard is dit 5%.',
    ],
    'text3' => [
        'en' => 'If prices for this year are different from the basic prices, the prices have to be entered here. If a price is not entered, the system falls back to the basic price.',
        'nl' => 'Wanneer prijzen voor dit bouwjaar verschillen van de basisprijzen dienen de prijzen hier te worden ingevuld. Als een prijs niet is ingevuld valt het systeem terug op de basisprijs.',
    ],
    'waterpump' => [
        'en' => 'Including water pump?',
        'nl' => 'Inclusief waterpomp?',
    ],
];

generateFile($dictionary, basename(__FILE__));
