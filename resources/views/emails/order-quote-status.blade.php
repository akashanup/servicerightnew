@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $salutation }} {{ $name }},
	<br/>
	<br/>
	{{ __('employee-place-order.order_quote_text_1') }} {{ $brand }} {{ $modelNumber }}
	{{ __('employee-place-order.order_quote_text_2') }} {{ $licensePlateNumber }}.
	<br/>
	<br/>
	{{ __('employee-place-order.services_list') }}:
	<ul>
		@foreach($servicesList as $service)
			<li>{{ $service }}</li>
		@endforeach
	</ul>
	<br>
	<b>{{ __('employee-place-order.total_price') }}: &euro; {{$totalPrice}} incl.BTW</b><br/>
	<br/>
	<a href="{{ $confirmQuoteUrl }}"> {{ __('employee-place-order.confirm_quote') }}</a>
	<br>
	{{ __('employee-place-order.order_quote_text_3') }}
	<br/><br/>
	{{ __('employee-place-order.order_quote_text_4') }}:
	<br/>
	<!-- AddThis Button BEGIN -->
	<a href="http://api.addthis.com/oexchange/0.8/forward/facebook/offer?pco=tbx32nj-1.0&amp;url=http%3A%2F%2Fwww.serviceright-#CFG_BRANCHE#.nl&amp;pubid=ra-50b8d0463b4b9c35" target="_blank" ><img src="#CFG_ROOT#/images/icons/social_media/facebook.png" border="0" alt="Facebook" /></a>
	<a href="http://api.addthis.com/oexchange/0.8/forward/twitter/offer?pco=tbx32nj-1.0&amp;url=http%3A%2F%2Fwww.serviceright-#CFG_BRANCHE#.nl&amp;pubid=ra-50b8d0463b4b9c35" target="_blank" ><img src="#CFG_ROOT#/images/icons/social_media/twitter.png" border="0" alt="Twitter" /></a>
	<a href="http://www.addthis.com/bookmark.php?source=tbx32nj-1.0&amp;=300&amp;pubid=ra-50b8d0463b4b9c35&amp;url=http%3A%2F%2Fwww.serviceright-#CFG_BRANCHE#.nl " target="_blank"  ><img src="#CFG_ROOT#/images/icons/social_media/addthis.png" border="0" alt="More..." /></a>
	<!-- AddThis Button END -->
@endsection
