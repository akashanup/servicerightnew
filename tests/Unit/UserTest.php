<?php

namespace Tests\Unit;

use App\Models\User;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class UserTest extends UnitTestCase
{

    /**
     * User createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueFirst()
    {
        $faker = Faker::create();
        $user = User::inRandomOrder()->first();
        $changedName = $faker->name;
        $data = User::createUnique($changedName, $user->email, $user->phone, $user->type);
        $this->assertEquals($changedName, $data->name);
    }

    /**
     * User createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueSecond()
    {
        $faker = Faker::create();
        $user = User::inRandomOrder()->first();
        $changedName = $faker->name;
        $changedPhone = '8527460000';
        $data = User::createUnique($changedName, $user->email, $changedPhone, $user->type);
        $this->assertEquals(array($changedName, $changedPhone), array($data->name, $data->phone));
    }

    /**
     * User createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueThird()
    {
        $faker = Faker::create();
        $name = $faker->name;
        $phone = '8527460000';
        $email = '123121@111.123';
        $data = User::createUnique($name, $email, $phone, 'client');
        $this->assertEquals(array($name, $phone, $email), array($data->name, $data->phone, $data->email));
    }

    /**
     * User createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueExceptionFirst()
    {
        $faker = Faker::create();
        $name = $faker->name;
        $phone = User::inRandomOrder()->first()->phone;
        $email = '123121@111.123';
        $this->expectException(\App\Exceptions\PhoneNumberAlreadyExistsException::class);
        $data = User::createUnique($name, $email, $phone, 'client');
    }

    /**
     * User createUnique test example.
     *
     * @return void
     */
    public function testCreateUniqueExceptionSecond()
    {
        $faker = Faker::create();
        $user = User::inRandomOrder()->first();
        $changedName = $faker->name;
        $changedPhone = User::where('id', '!=', $user->id)
            ->inRandomOrder()->first()->phone;
        $this->expectException(\App\Exceptions\PhoneNumberAlreadyExistsException::class);
        $data = User::createUnique($changedName, $user->email, $changedPhone, $user->type);
    }

    /**
     * User employees test example.
     *
     * @return void
     */
    public function testemployees()
    {
        $data = User::employees();
        $testData = User::whereIn('type', [User::SR_EMPLOYEE, User::SR_ADMIN])
            ->select('id', 'name', 'email', 'last_login', 'status', 'nickname')->get();
        $this->assertEquals($testData, $data->get());
    }
}
