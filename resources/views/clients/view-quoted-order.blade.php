@extends('employees.content')

@section('employee-content')
    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ __('client-review-order.review_order') }}</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>1. {{ __('employee-place-order.customer_data') }}</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.salutation') }}:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="salutationSelect">
                                @if($user->salutation == 'Mr.')
                                    <option value="Mr." selected>{{ __('employee-place-order.mr') }}.</option>
                                    <option value="Ms.">{{ __('employee-place-order.miss') }}.</option>
                                @else
                                    <option value="Mr.">{{ __('employee-place-order.mr') }}.</option>
                                    <option value="Ms." selected>{{ __('employee-place-order.miss') }}.</option>
                                @endif
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.name') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control formField" type="text" placeholder="{{ __('employee-place-order.name') }}..." id="nameInpt" value="{{ $user->name }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.phone_number') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control formField" type="tel" placeholder="{{ __('employee-place-order.phone_number') }}..." id="phoneInpt" value="{{ $user->phone }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Email:</label>
                        <div class="col-sm-8">
                            <input class="form-control formField" type="email" placeholder="Email..." id="emailInpt"  value="{{ $user->email }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">Country:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="countryInpt">
                                    <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Postcode + nr:</label>
                        <div class="col-sm-6">
                            <input class="form-control formField" type="text" placeholder="Postcode..." id="postalCodeInpt" value="{{$postalCode->postal_code}}">
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control homenr formField" type="text" placeholder="Nr.." id="houseNumberInpt" value="{{$address->house_number}}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.address') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" placeholder="{{ __('employee-place-order.address') }}..." id="addressInpt" value="{{ $address->complete_address }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.city') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" placeholder="{{ __('employee-place-order.city') }}..." id="cityInpt" value="{{ $city->name }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>2. {{ __('employee-place-order.data_auto') }}</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.license_plate') }}:</label>
                        <div class="col-sm-4">
                            <input class="form-control licence-plate formField" type="text" placeholder="{{ __('employee-place-order.license_plate') }}..." disabled="disabled" value=" {{ $licensePlateNumber->license_plate_number }}">
                        </div>
                        <div class="col-sm-4">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.brand') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" disabled="disabled" value="{{ $vehicle->brand->name }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">Model:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" disabled="disabled" value="{{ $vehicle->model }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.fuel') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" disabled="disabled" class="form-control" value="{{ $vehicle->fuel_type }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.construction_year') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control formField" type="text" placeholder="{{ __('employee-place-order.construction_year') }}..." disabled="disabled" value="{{ $vehicle->build_year_start() }}">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">APK verloopt op:</label>
                        <div class="col-sm-8">
                            <input class="form-control car-apk formField" type="text" placeholder="APK verloopt op..." disabled="disabled" value="{{ $licensePlateNumber->mandatory_service_expiry_date() }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding bg-gray">
        <div class="container service-selection">
            <div class="row">
                <h3>3. {{ __('employee-place-order.data_maintenance') }}</h3>
            </div>
            @foreach($order->services as $service)
                <div class="row">
                    <h4>{{ $service->name }} ( &euro; {{ $service->pivot->price }} )</h4>
                    {{ $service->description }}
                </div>
                <br/>
            @endforeach
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>4. {{ __('employee-place-order.overview') }} &amp; planning</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.date') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control datepicker formField" type="tel" value="{{ $order->pickup_date() }}" id="pickupDateInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.alternative') }} {{ __('employee-place-order.date') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control datepicker" type="tel" value="{{ $order->alternative_pickup_date() }}" id="alternativePickupDateInpt">
                        </div>
                        <label for="" class="col-sm-4 col-form-label">{{ __('employee-place-order.time') }}:</label>
                        <div class="col-sm-8">
                            <input class="form-control formField" type="tel" value="{{ $order->pickup_time }}" id="pickupTimeInpt">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <button type="button" class="btn btn-primary btn-block orderUpdateBtn" data-type="confirmed">{{ __('globals.confirm') }}</button>
                    <button type="button" class="btn btn-info btn-block orderUpdateBtn" data-type="cancelled">{{ __('globals.reject') }}</button>

                    <h4>{{ __('employee-place-order.total_price') }}: &euro; <span id="totalPrice">{{ $order->total_price }}</span></h4>
                </div>
            </div>
        </div>
    </section>

    <!-- Hidden fields required for javascript -->
    <input type="hidden" id="fetchAddressRoute" value="{{ route('addresses.client') }}">
    <input type="hidden" id="fetchedAddressId" value="{{ $order->address_id }}">
    <input type="hidden" id="fetchedlicensePlateNumberId" value="{{ $order->license_plate_number_id }}">
    <input type="hidden" id="confirmQuotedOrderRoute" value="{{ route('clients.confirmQuotedOrder', $order->verification_token) }}">

    <!-- Scripts -->
    @section('employee-scripts')
        <script type="text/javascript" src="{{ asset('js/custom/fetch-address.js').'?v='.config('app.version', '1') }}"></script>
        <script type="text/javascript" src="{{ asset('js/custom/order-masked-input.js').'?v='.config('app.version', '1') }}"></script>
        <script type="text/javascript" src="{{ asset('js/custom/client-confirm-order.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
