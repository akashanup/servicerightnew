@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $garageName }},
	<br>
	<br>
	<b>
		{{ __('manage-garage-email.email_text_36') }}
	</b>
	<br/>
	<b>
		{{ __('manage-garage-email.email_text_37') }}
	</b>
	<p>
		{{ __('manage-garage-email.email_text_38') }}
		{{ __('manage-garage-email.email_text_39') }} <a href="https://www.serviceright-autos.nl/{{$garageId}}/">{{ __('manage-garage-email.email_text_40') }}</a> {{__('manage-garage-email.email_text_41') }} <b>{{__('manage-garage-email.email_text_42') }}</b>{{__('manage-garage-email.email_text_43') }}.
		{{__('manage-garage-email.email_text_44') }}<b> {{__('manage-garage-email.email_text_45') }} </b> {{__('manage-garage-email.email_text_46') }}.
	</p>
	<p>
		{{__('manage-garage-email.email_text_47') }}:
	</p>
	<table>
		<tbody>
			<tr>
				<td>
					<div style="padding:10px; font-size:12px; width:300px; background:rgb(241,238,209); border:1px dotted rgb(221,216,164);">
					    <i>
					        &lt;a href="https://www.serviceright-autos.nl/{{$garageId}}/" title="Garage in {{$garageCity}}" target="_blank" &gt;&lt;img src="https://www.serviceright.nl/erkend_autos_medium.png"
					        alt="Garage in {{$garageCity}}" border="0" &gt;&lt;/a&gt;
					    </i>
					</div>
				</td>
				<td>
					&nbsp; <small>{{__('manage-garage-email.email_text_48') }}:</small><br>
					<a href="https://www.serviceright-autos.nl/{{$garageId}}/" title="Garage in {{$garageCity}}" target="_blank"><img src="https://www.serviceright.nl/erkend_autos_medium.png" alt="Garage in {{$garageCity}}" border="0"></a>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<table>
		<tbody>
			<tr>
				<td>
					<div style="padding:10px; font-size:12px; width:300px; background:rgb(241,238,209); border:1px dotted rgb(221,216,164);">
					    <i>
					        &lt;a href="https://www.serviceright-autos.nl/{{$garageId}}/" title="Garage in {{$garageCity}}" target="_blank" &gt;&lt;img src="https://www.serviceright.nl/erkend_autos_small.png"
					        alt="Garage in {{$garageCity}}" border="0" &gt;&lt;/a&gt;
					    </i>
					</div>
				</td>
				<td>
					&nbsp; <small>{{__('manage-garage-email.email_text_48') }}:</small><br>
					<a href="https://www.serviceright-autos.nl/{{$garageId}}/" title="Garage in {{$garageCity}}" target="_blank"><img src="https://www.serviceright.nl/erkend_autos_small.png" alt="Garage in {{$garageCity}}" border="0"></a>
				</td>
			</tr>
		</tbody>
	</table>
	<br><br>
	<i>{{__('manage-garage-email.email_text_49') }}
	{{__('manage-garage-email.email_text_50') }} <b>{{__('manage-garage-email.email_text_51') }}</b> {{__('manage-garage-email.email_text_52') }}.<br/>
	{{__('manage-garage-email.email_text_53') }}.<br/>
	{{__('manage-garage-email.email_text_54') }}: {{ config('constants.CUSTOMER_CARE_NUMBER') }}.</i>


@endsection
