@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $salutation }} {{ $name }},
	<br/>
	<br/>
	{{ __('employee-place-order.order_status_email_title') }}
	<br/>
	{{ __('employee-place-order.order_confirm_text_1') }}
	<br/>
	<b>{{ __('employee-place-order.order_confirm_text_2') }}</b>
	<br/><br/>
	@if($confirmOrderUrl)
		<a href="{{ $confirmOrderUrl }}"> {{ __('employee-place-order.order_confirm_text_3') }}</a>
	@endif
	<hr/>
	<b>{{ __('employee-place-order.your_data') }}:</b>
	<hr/>

	{{ $salutation }} {{ $name }},
	<br/>
	{{ $phone }}
	<br/>
	{{ $email }}
	<br/>
	{{ $address }} {{ $houseNumber }}
	<br/>
	{{ $postalCode }} {{ $city }}
	<br/>
	<br/>
	{{ __('employee-place-order.date') }}: {{ $pickUpDate }}
	<br/>
	{{ __('employee-place-order.time') }}: {{ $pickUpTime }}
	<br/>
	<br/>
	{{ __('employee-place-order.comments') }}
	<br/>
	{{ $additionalWork }}

	<hr/><b>{{ __('employee-place-order.car_data') }}:</b><hr/>

	{{ __('employee-place-order.license_plate') }}: {{ $licensePlateNumber }}
	<br/>
	{{ __('employee-place-order.brand') }}: {{ $brand }}
	<br/>
	Model: {{ $modelNumber }}
	<br/>
	<br/>
	{{ __('employee-place-order.order_confirm_text_4') }}
	<br/>
	{{ __('employee-place-order.order_confirm_text_5') }} {{ config('constants.CUSTOMER_CARE_NUMBER') }}
	<br/>
	<br/>
	@if($confirmOrderUrl)
		<a href="{{ $confirmOrderUrl }}"> {{ __('employee-place-order.order_confirm_text_3') }}</a>
		<br>
	@endif
	{{ __('employee-place-order.order_confirm_text_6') }}
	<br/>
	<br/>
@endsection
