<?php

use Illuminate\Database\Seeder;

class VehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $servername = config('constants.OLD_DB_SERVERNAME');
        $username = config('constants.OLD_DB_USERNAME');
        $password = config('constants.OLD_DB_PASSWORD');
        $dbname = config('constants.OLD_DB_NAME');

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        //Brands and Vehicles
        $sql1 = "SELECT DISTINCT merk from prijzen_autos";
        $result1 = $conn->query($sql1);
        while ($row1 = $result1->fetch_assoc()) {
            $brandId = DB::table('brands')->insertGetId(
                [
                    'name' => $row1["merk"],
                ]
            );
            // Find base model
            $baseSql = "SELECT * from prijzen_autos where merk ='" . $row1['merk'] . "' AND bouwjaar_min = '0000-00-00' AND bouwjaar_max = '0000-00-00'";
            $resultBaseSql = $conn->query($baseSql);
            while ($rowBaseSql = $resultBaseSql->fetch_assoc()) {
                // Create base model
                $servicesJson = static::createServiceJson($rowBaseSql);
                $baseVehicleId = DB::table('vehicles')->insertGetId(
                    [
                        'brand_id' => $brandId,
                        'fuel_type' => $rowBaseSql['brandstof'] ? $rowBaseSql['brandstof'] : 'Benzine',
                        'model' => $rowBaseSql['model'],
                        'services' => $servicesJson,
                        'diesel_percentage' => json_encode(config('constants.DEFAULT_DIESEL_PERCENTAGE')),
                    ]
                );
                $sql2 = "SELECT * from prijzen_autos where merk='" . $row1['merk'] . "' AND model = '" . $rowBaseSql['model'] . "' AND bouwjaar_min != '0000-00-00' AND bouwjaar_max != '0000-00-00'";
                $result2 = $conn->query($sql2);
                while ($row2 = $result2->fetch_assoc()) {
                    try {
                        if ($row2['brandstof']) {
                            $servicesJson = static::createServiceJson($row2, $servicesJson);
                            $vehicleId = DB::table('vehicles')->insertGetId(
                                [
                                    'brand_id' => $brandId,
                                    'fuel_type' => $row2['brandstof'],
                                    'model' => $row2['model'],
                                    'services' => $servicesJson,
                                    'build_year_start' => $row2['bouwjaar_min'],
                                    'build_year_end' => $row2['bouwjaar_max'],
                                    'parent_id' => $baseVehicleId,
                                    'waterpump_required' => $row2['prijs_inclusief_waterpomp'] == 'nee' ? 'no' : 'yes',
                                ]
                            );
                        }
                    } catch (\Exception $e) {
                        \Log::info("+++++VehiclesTableSeederStart++++");
                        \Log::info($e);
                        \Log::info("+++++VehiclesTableSeederEnd++++");
                    }
                }
            }
        }

        $conn->close();
    }

    public static function createServiceJson($data, $servicesJson = null)
    {
        $service = [
            "1" => ($data['prijs_kleine_beurt'] / 100), "2" => ($data['prijs_grote_beurt'] / 100), "3" => 0.00, "4" => 0.00, "5" => 0.00, "6" => 0.00, "7" => ($data['prijs_bougies_vervangen'] / 100), "8" => ($data['prijs_katalysator_vervangen'] / 100), "9" => ($data['prijs_remblokken_voor_vervangen'] / 100), "10" => ($data['prijs_koppeling_vervangen'] / 100),
            "11" => ($data['prijs_accu_vervangen'] / 100), "12" => 0.00, "13" => ($data['prijs_uitlaat_achterdemper_vervangen'] / 100), "14" => ($data['prijs_koppakking_vervangen'] / 100), "15" => ($data['prijs_distributieriem_vervangen'] / 100), "16" => 0.00, "17" => ($data['prijs_remschijven_achter_vervangen'] / 100), "18" => 0.00, "19" => ($data['prijs_schokdempers_achter_vervangen'] / 100), "20" => ($data['prijs_remblokken_achter_vervangen'] / 100),
            "21" => ($data['prijs_dynamo_vervangen'] / 100), "22" => ($data['prijs_remschijven_voor_vervangen'] / 100), "23" => 0.00, "24" => 0.00, "25" => 0.00, "26" => 0.00, "27" => ($data['prijs_olie_verversen'] / 100), "28" => ($data['prijs_waterpomp_vervangen'] / 100), "29" => 0.00, "30" => 0.00,
            "31" => 0.00, "32" => 0.00, "33" => 0.00, "34" => 0.00, "35" => 0.00, "36" => 0.00, "37" => 0.00, "38" => 0.00, "39" => 0.00, "40" => 0.00,
            "41" => 0.00, "42" => 0.00, "43" => 0.00, "44" => 0.00, "45" => 0.00, "46" => 0.00, "47" => 0.00, "48" => 0.00, "49" => 0.00, "50" => 0.00,
            "51" => 0.00, "52" => 0.00, "53" => 0.00, "54" => 0.00,
        ];

        if ($servicesJson && !array_filter($service)) {
            $service = $servicesJson;
        } else {
            $service = json_encode($service);
        }

        return $service;
    }
}
