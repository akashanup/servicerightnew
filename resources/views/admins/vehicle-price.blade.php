@extends('admins.content')

@section('admin-content')
	<div class="add-buildyear">
		<a href="#">+</a>
	</div>
	<div id="vehicleData">

	</div>

	<input type="hidden" id="vehicleId" value="{{ $vehicle }}">
	<input type="hidden" id="licensePlateNumber" value="{{ $licensePlateNumber }}">
	<input type="hidden" id="homeRoute" value="{{ route('home') }}">
	<input type="hidden" id="updateVehicleServicePrice" value="{{ route('vehicles.update-services') }}">
	<input type="hidden" id="inputSavedTxt" value="{{ __('globals.input_saved') }}">
	<input type="hidden" id="inputNotSavedTxt" value="{{ __('globals.input_not_saved') }}">

    <!-- Scripts -->
    @section('admin-scripts')
    	<script type="text/javascript" src="{{ asset('js/custom/vehicle-price.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
