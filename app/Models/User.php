<?php

namespace App\Models;

use App\Exceptions\PhoneNumberAlreadyExistsException;
use App\Models\Garage;
use App\Models\Order;
use App\Models\Service;
use App\Notifications\GarageActivationReminder;
use App\Notifications\GarageConfirmMembership;
use App\Notifications\GarageCredentials;
use App\Notifications\GarageDeactivateMembership;
use App\Notifications\GarageDemoCredentials;
use App\Notifications\GarageRecognition;
use App\Notifications\OrderCallbackRequest;
use App\Notifications\OrderCancelStatus;
use App\Notifications\OrderClientBringItself;
use App\Notifications\OrderCompleteRequest;
use App\Notifications\OrderConfirmStatus;
use App\Notifications\OrderPickUpAtClientLocation;
use App\Notifications\OrderQuoteStatus;
use App\Notifications\OrderServiceAtClientLocation;
use App\Notifications\UpdateGarageMembership;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Lang;
use PDF;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_login', 'phone', 'salutation', 'type', 'status', 'nickname',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const ACTIVE_STATUS = 'active';
    const NOT_VERIFIED = 'no';
    const VERIFIED = 'yes';

    const CLIENT = 'client';
    const GARAGE = 'garage_worker';
    const SR_EMPLOYEE = 'sr_employee';
    const SR_ADMIN = 'sr_admin';

    const ACTIVE = 'active';
    const INACTIVE = 'inactive';

    /**
     * Get the orders of the user.
     *
     * @return Collection
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    } //end orders()

    /**
     * Get the garage at the address.
     *
     * @return Collection
     */
    public function garage()
    {
        return $this->hasOne(Garage::class);
    } //end garage()

    /**
     * Confirm quoted order and update details.
     *
     * @param App\Model $order
     * @param array $data
     * @return json
     */
    public function confirmQuotedOrder($order, $data)
    {
        // Update order details.
        return $order->updateOrder($data);
    } //end confirmQuotedOrder()

    /**
     * Send the order status notification.
     *
     * @param  App\Models $order
     * @return void
     */
    public function sendOrderStatusNotification($order, $garage = null)
    {
        $licensePlateNumber = $order->licensePlateNumber;
        $vehicle = $licensePlateNumber->vehicle;

        switch ($order->status) {
            case Order::QUOTED:
                // Save verification token for client verification.
                $order->update(
                    [
                        'verification_token' => base64_encode(str_random(10) . $order->id . microtime()),
                        'client_verified' => static::NOT_VERIFIED,
                    ]
                );
                // Send quote email to client.
                $servicesList = Service::whereIn('id', $order->services()->pluck('order_services.service_id'))
                    ->pluck('name')->toArray();
                $this->notify(new OrderQuoteStatus(
                    $this->salutation, $this->name, $vehicle->brand->name, $vehicle->model,
                    $licensePlateNumber->license_plate_number, $order->total_price, $servicesList,
                    route('clients.phone', $order->verification_token))
                );

                break;
            case Order::CONFIRMED:
                $address = $order->address;
                $postalCode = $address->postalCode;
                $user = $order->user;
                $address = $order->address;
                $postalCode = $address->postalCode;
                $vehicle = $order->licensePlateNumber->vehicle;
                $filePath = storage_path(config('constants.PDF_STORAGE_PATH')) . '/' . $order->id . '.pdf';
                $pdf = PDF::loadView(
                    'pdf.order-confirm',
                    [
                        'order' => $order,
                        'user' => $user,
                        'address' => $address,
                        'postalCode' => $postalCode,
                        'vehicle' => $vehicle,
                    ]
                );
                $pdf->save($filePath);

                $this->notify(new OrderConfirmStatus(
                    $this->salutation, $this->name, $vehicle->brand->name, $vehicle->model,
                    $licensePlateNumber->license_plate_number, null, $this->phone, $this->email,
                    $address->complete_address, $address->house_number, $postalCode->postal_code, $postalCode->city->name,
                    $order->pickup_date(), $order->pickup_time, $order->additional_work, $filePath)
                );

                /*
                switch ($order->pickup_type) {
                case Order::PICKUP_FROM_CLIENT:
                $this->notify(new AdminOrderPickupFromClient(
                $this->salutation, $this->name, $vehicle->brand->name, $vehicle->model,
                $licensePlateNumber->license_plate_number, route('orders.confirm', $order->id), $this->phone, $this->email,
                $address->complete_address, $address->house_number, $postalCode->postal_code, $postalCode->city->name, $order->pickup_date, $order->pickup_time, $order->additional_work)
                );
                break;
                case Order::CLIENT_BRING_ITSELF:
                $this->notify(new AdminOrderClientBringItself(
                $this->salutation, $this->name, $vehicle->brand->name, $vehicle->model,
                $licensePlateNumber->license_plate_number, route('orders.confirm', $order->id), $this->phone, $this->email,
                $address->complete_address, $address->house_number, $postalCode->postal_code, $postalCode->city->name, $order->pickup_date, $order->pickup_time, $order->additional_work)
                );
                break;
                case Order::REPAIR_AT_LOCATION:
                $this->notify(new AdminOrderRepairAtLocation(
                $this->salutation, $this->name, $vehicle->brand->name, $vehicle->model,
                $licensePlateNumber->license_plate_number, route('orders.confirm', $order->id), $this->phone, $this->email,
                $address->complete_address, $address->house_number, $postalCode->postal_code, $postalCode->city->name, $order->pickup_date, $order->pickup_time, $order->additional_work)
                );
                break;

                default:
                # code...
                break;
                }
                 */
                break;
            case Order::CANCELLED:
                $this->notify(new OrderCancelStatus($this->salutation, $this->name, $order->id));
                break;
            case Order::ACCEPTED_BY_GARAGE:
                if ($this->type == static::CLIENT || $this->type == static::GARAGE) {
                    switch ($order->pickup_type) {
                        case Order::PICKUP_FROM_CLIENT:
                            $this->notify(new OrderPickUpAtClientLocation($this, $order, $garage));
                            break;
                        case Order::CLIENT_BRING_ITSELF:
                            $this->notify(new OrderClientBringItself($this, $order, $garage));
                            break;
                        case Order::REPAIR_AT_LOCATION:
                            $this->notify(new OrderServiceAtClientLocation($this, $order, $garage));
                            break;
                        default:
                            # code...
                            break;
                    }
                }
                break;
            default:
                # code...
                break;
        }
    } //end sendOrderStatusNotification()

    /**
     * Send the order callback notification.
     *
     * @param  App\Models $order
     * @return void
     */
    public function sendOrderCallbackNotification($order)
    {
        $this->notify(new OrderCallbackRequest($this->salutation, $this->name, $order->licensePlateNumber->license_plate_number,
            route('clients.phone', $order->verification_token)));
    } // end sendOrderCallbackNotification()

    /**
     * Send the complete order request.
     *
     * @param  App\Models $order
     * @return void
     */
    public function sendOrderCompleteRequestNotification($order)
    {
        $this->notify(new OrderCompleteRequest($this, $order));
    } // end sendOrderCompleteRequestNotification()

    /**
     * Send demo login credential to garage user.
     *
     * @param  integer $garageId
     * @param  string $password
     * @return void
     */
    public function sendGarageDemoCredentials($garageId, $password)
    {
        $this->notify(new GarageDemoCredentials($garageId, $password));
    } // end sendGarageDemoCredentials()

    /**
     * Send login credential to garage user.
     *
     * @param  App\Models $garage
     * @return void
     */
    public function sendGarageCredentials($garage)
    {
        $this->notify(new GarageCredentials($this, $garage));
    } // end sendGarageCredentials()

    /**
     * Send membership renewal mail.
     *
     * @param  App\Models $garage
     * @return void
     */
    public function updateGarageMembership($garage)
    {
        $this->notify(new UpdateGarageMembership($garage));
    } // end updateGarageMembership()

    /**
     * Send garage activation email.
     *
     * @param  App\Models $garage
     * @return void
     */
    public function sendGarageActivationReminder($garage)
    {
        $this->notify(new GarageActivationReminder($this, $garage));
    } // end sendGarageActivationReminder()

    /**
     * Send confirm membership email to garage.
     *
     * @param  App\Models $garage
     * @return void
     */
    public function sendGarageConfirmMembership($garage)
    {
        $this->notify(new GarageConfirmMembership($garage));
    } // end sendConfirmMembership()

    /**
     * Send deactivate membership email to garage.
     *
     * @param  App\Models $garage
     * @return void
     */
    public function sendGarageDeactivateMembership($garage)
    {
        $this->notify(new GarageDeactivateMembership($garage));
    } // end sendGarageDeactivateMembership()

    /**
     * Send recognition email to garage.
     *
     * @param  App\Models $garage
     * @return void
     */
    public function sendGarageRecognitionMail($garage)
    {
        $this->notify(new GarageRecognition($garage));
    } // end sendGarageRecognitionMail()

    /**
     * Create a user if its not found.
     *
     * @param string $name
     * @param string $email
     * @param string $phone
     * @param string $type
     * @param string $salutation
     * @return App\Models
     */
    public static function createUnique($name, $email, $phone, $type, $salutation = 'Mr.')
    {
        $thisUser = static::where('email', $email)->first();
        $tmpUser = static::where('phone', $phone)->first();
        if (!$thisUser) {
            // Check for the existing user with same phone
            if ($tmpUser) {
                throw new PhoneNumberAlreadyExistsException(Lang::get('globals.phone_already_exists'), 1);
            }
            $thisUser = static::create(
                [
                    'salutation' => $salutation,
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'type' => $type,
                    'status' => User::ACTIVE_STATUS,
                ]
            );
        } else {
            // Check for the existing user with same phone
            if (!$tmpUser || $thisUser->id == $tmpUser->id) {
                $thisUser->update(
                    [
                        'salutation' => $salutation,
                        'name' => $name,
                        'phone' => $phone,
                    ]
                );
            } else {
                throw new PhoneNumberAlreadyExistsException(Lang::get('globals.phone_already_exists'), 1);
            }
        }
        return $thisUser;
    } //end createUnique()

    /**
     * Get serviceright employees.
     *
     * @return array
     */
    public static function employees()
    {
        return static::whereIn('type', [static::SR_EMPLOYEE, static::SR_ADMIN])
            ->select('id', 'name', 'email', 'last_login', 'status', 'nickname');
    }

    /**
     * Update serviceright employees by admin.
     *
     * @param array $data employee data
     * @return boolean
     */
    public function adminUpdates($data)
    {
        $updateData = [
            'name' => $data['name'],
            'nickname' => $data['nickname'],
            'type' => $data['type'],
            'email' => $data['email'],
        ];

        if (auth()->user()->id != $data['id']) {
            $updateData = array_merge($updateData, ['status' => $data['status']]);
        }

        if ($data['password']) {
            $updateData = array_merge($updateData, ['password' => bcrypt($data['password'])]);
        }

        $this->update($updateData);
    }
}
