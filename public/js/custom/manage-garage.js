$(function(){
	var DECISION_TYPE_RENEW = 'renew';
	var DECISION_TYPE_CANCEL = 'cancel';
	var ACTION_EMAIL = 'email';

	$.fn.sendAjax = (garageId, modal, thisBtn, data) => {
		var url = thisBtn.attr('data-route');
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			thisBtn.prop('disabled', false);
			modal.modal('hide');
			if(successResponse.response) {
				$.fn.initGarageTable(garageId);
				$.blockUI({ message: successResponse.message, timeout: 2000,baseZ: 1005 });
			}
		}, (errorResponse) => {
			thisBtn.prop('disabled', false);
			modal.modal('hide');
		});
	};

	$.fn.initGarageTable = (garageId='') => {
		$('#garageTable').DataTable().destroy();
	    var table = $('#garageTable').DataTable(
		    {
		        processing: true,
		        serverSide: true,
		        ajax: $('#garageDataRoute').val() + (garageId ? ('/'+garageId) : ''),
		        columns: [
		        	{data: 'popups', orderable: false, searchable: false},
		            {data: 'id', name: 'garages.id'},
		            {data: 'name', name: 'garages.name'},
		            {data: 'location', name: 'addresses.complete_address', orderable: false},
		            {data: 'membership', name: 'garages.member_expiry', orderable: false},
		            {data: 'action', orderable: false, searchable: false}
		        ],
		        fnDrawCallback: () => {
		        	$(document).find('.popp').popover();
		        },
		        columnDefs: [
		            { width: '1%', targets: 0 },
		            { width: '1%', targets: 1 },
		            { width: '25%', targets: 2 },
		            { width: '20%', targets: 3 },
		            { width: '20%', targets: 4 },
		            { width: '33%', targets: 5 },
		        ],
		    },
		);
	};

	$.fn.openEditGarageModal = (modal, url) => {
		$.fn.ajaxBlockUI();
		$.fn.ajaxCall(REQUEST_TYPE_GET, url, {}, DATA_TYPE_JSON).then((successResponse) => {
			successResponse = successResponse[0];
			modal.find('.garageName').val(successResponse.name);
			modal.find('.garageContactPerson').val(successResponse.contactName);
			modal.find('.garageMoneybirdId').val(successResponse.moneybird_id);
			modal.find('.garagePhone').val(successResponse.phone);
			modal.find('.garageEmail').val(successResponse.email);
			modal.find('.garageWebsite').val(successResponse.website);
			modal.find('.garageCompleteAddress').val(successResponse.complete_address);
			modal.find('.garagePostalCode').val(successResponse.postal_code);
			modal.find('.garageCity').val(successResponse.city);
			modal.find('.garageType').val(successResponse.type);
			$.each(modal.find('.garageCategory'), function(i, v){
				if($.inArray($(v).val(), successResponse.categoryIds) != -1) {
					$(v).prop('checked', true);
				}
			});
			$.each(modal.find('.garageCompany'), function(i, v){
				if($.inArray($(v).val(), successResponse.companyIds) != -1) {
					$(v).prop('checked', true);
				}
			});
			$.unblockUI();
			modal.modal('show');
		}, (errorResponse) => {			
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	};

	$.fn.addEditGarage = (modal, thisBtn) => {
		if($.fn.validateRequest(modal)) {
			thisBtn.prop('disabled', true);
			var url = thisBtn.attr('data-route');
			var postalCode = modal.find('.garagePostalCode').val().trim();
			var address = modal.find('.garageCompleteAddress').val().trim();
			var city = modal.find('.garageCity').val().trim();
			var latitude = null;
			var longitude= null;
			var geocoder = new google.maps.Geocoder();
		    if (geocoder) {
		        geocoder.geocode({
		            'address': `${address}+${postalCode}+${city}`
		        }, function (results, status) {
		            if (status == google.maps.GeocoderStatus.OK) {
		                latitude = results[0].geometry.location.lat();
		                longitude = results[0].geometry.location.lng();
		                var categories = [];
		                modal.find('.garageCategory:checked').each(function() {
					        categories.push($(this).val());
					    });
					    var companies = [];
		                modal.find('.garageCompany:checked').each(function() {
					        companies.push($(this).val());
					    });
		                var data = {
							"name": modal.find('.garageName').val().trim(),
							"contactPerson": modal.find('.garageContactPerson').val().trim(),
							"moneyBirdId": modal.find('.garageMoneybirdId').val().trim(),
							"phone": modal.find('.garagePhone').val().trim(),
							"email": modal.find('.garageEmail').val().trim(),
							"website": modal.find('.garageWebsite').val().trim(),
							"completeAddress": address,
							"postalCode": postalCode,
							"latitude": latitude,
							"longitude": longitude,
							"city": city,
							"type": modal.find('.garageType').val(),
							"categories": categories,
							"companies": companies,
						};
						// To update the garage.
						var garageId = thisBtn.attr('data-id');
						garageId ? data.garageId = garageId : '';

						$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
							if (successResponse.response) {
								thisBtn.prop('disabled', false);
								modal.modal('hide');
								$.blockUI({ message: successResponse.message, timeout: 2000,baseZ: 1005 });
								$.fn.initGarageTable(garageId);
							}
						}, (errorResponse) => {
							thisBtn.prop('disabled', false);
							modal.modal('hide');
							$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
						});
		            } else {
		            	modal.modal('hide');
		            	$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		            }
		        });
		    }
		}
	};

	$.fn.openSaleNoteModal = (modal, url) => {
		$.fn.ajaxBlockUI();
		$.fn.ajaxCall(REQUEST_TYPE_GET, url, {}, DATA_TYPE_JSON).then((successResponse) => {
			$.unblockUI();
			$.fn.generateNoteContent(successResponse.saleNotes, modal.find('#saleNotesDiv'));
			modal.modal('show');
		}, (errorResponse) => {			
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	};

	$.fn.openStatsModal = (modal, url) => {
		$.fn.ajaxBlockUI();
		$.fn.ajaxCall(REQUEST_TYPE_GET, url, {}, DATA_TYPE_JSON).then((successResponse) => {
			$.unblockUI();
			var stats = successResponse.stats;
			modal.find('#statsModalGarageTotalOrder').html(stats.totalOrders);
			modal.find('#statsModalGarageAcceptedOrder').html(stats.acceptedOrders);
			modal.find('#statsModalGarageRevenue').html(stats.totalPrice);
			modal.find('#statsModalGarageTurnover').html(stats.totalAveragePrice);
			modal.find('#statsModalGarageTotalOrderLastYear').html(stats.totalOrdersLastYear);
			modal.find('#statsModalGarageAcceptedOrderLastYear').html(stats.acceptedOrdersLastYear);
			modal.find('#statsModalGarageRevenueLastYear').html(stats.totalPriceLastYear);
			modal.find('#statsModalGarageTurnoverLastYear').html(stats.totalAveragePriceLastYear);
			modal.modal('show');
		}, (errorResponse) => {			
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	};

	$(document).on('click', '.modalBtn',function(){
		var garageId = $(this).attr('data-id');
		var garageName = $(this).attr('data-name');
		var type = $(this).attr('data-type');
		var route = $(this).attr('data-route');
		var modal = null;
		switch (type) {
			case 'createPassword':
				modal = $('#startMembershipModal');
				modal.find('#membershipModalGarageName').html(garageName);
				modal.find('#membershipModalCreatePasswordBtn').attr('data-id', garageId);
				modal.modal('show');
				break;
			case 'renewPassword':
				modal = $('#extendMembershipModal');
				modal.find('.extendMembershipModalGarageName').html(garageName);
				modal.find('.extendMembershipModalDecisionBtn').attr('data-id', garageId);
				modal.find('#extendMembershipModalGarageExpiryInpt').val($(this).attr('data-garageExpiry'));
				modal.find('#extendMembershipModalGarageExpiryLbl').html($(this).attr('data-membership'));
				modal.modal('show');
				break;
			case 'stats':
				modal = $('#statsModal');
				modal.find('#statsModalGarageName').html(garageName);
				$.fn.openStatsModal(modal, route);
				break;
			case 'demo':
				modal = $('#demoGarageModal');
				modal.find('#demoGarageModalGarageName').html(garageName);
				modal.find('#demoGarageModalSendBtn').attr('data-id', garageId);
				modal.modal('show');
				break;
			case 'edit':
				modal = $(document).find('#editGarageModal');
				modal.find('#editGarageModalGarageName').html(garageName);
				modal.find('#editGarageModalSaveBtn').attr('data-id', garageId);
				$.fn.openEditGarageModal(modal, route);
				break;
			case 'delete':
				modal = $(document).find('#deleteGarageModal');
				modal.find('#deleteGarageModalGarageName').html(garageName);
				modal.find('#deleteGarageModaldeleteBtn').attr('data-route', route);
				modal.modal('show');
				break;
			case 'saleNote':
				modal = $(document).find('#saleNotesGarageModal');
				modal.find('#saleNotesGarageModalGarageName').html(garageName);
				modal.find('#saleNotesGarageModalSaveBtn').attr('data-id', garageId);
				$.fn.openSaleNoteModal(modal, route);
				break;
			case 'recognition':
				modal = $(document).find('#erkenningModal');
				var garageLink = `<a href="https://www.serviceright-autos.nl/${garageId}/"target="_blank"><img src="https://www.serviceright.nl/erkend_autos_small.png" alt="Garage " title="Garage Sluis"></a>`;
				modal.find('#garageLink').val(garageLink);
				modal.find('.actionBtn').attr('data-id', garageId);
				modal.modal('show');
				break;
			default:
				break;
		}
	});

	$(document).on('hidden.bs.modal', '#startMembershipModal', function(){
		var modal = $(this);
		modal.find('#membershipModalGarageName').html('');
		modal.find('#startMembershipModalDiscountInpt').val('0.00');
		modal.find('#membershipModalCreatePasswordBtn').removeAttr('data-id');
	});

	$(document).on('hidden.bs.modal', '#erkenningModal', function(){
		$(this).find('#garageLink').val('');
	});

	$(document).on('hidden.bs.modal', '#extendMembershipModal', function(){
		var modal = $(this);
		modal.find('.extendMembershipModalGarageName').html('');
		modal.find('#extendMembershipModalDiscountInpt').val('0.00');
		modal.find('#extendMembershipModalDeactivateBtn').removeAttr('data-id');
		modal.find('#extendMembershipModalRenewBtn').removeAttr('data-id');
		modal.find('#extendMembershipModalGarageExpiryInpt').val('');
		modal.find('#extendMembershipModalGarageExpiryLbl').html('');
	});

	$(document).on('hidden.bs.modal', '#createGarageModal, #editGarageModal', function(){
		$(this).find('form').find('.form-control').val('');
		$(this).find('form').find('.garageCategory').prop('checked', false);
		$(this).find('form').find('.garageCompany').prop('checked', false);
	});

	$(document).on('hidden.bs.modal', '#demoGarageModal', function(){
		var modal = $(this);
		modal.find('#demoGarageModalGarageName').html('');
		modal.find('#demoGarageModalSendBtn').removeAttr('data-id');
	});

	$(document).on('click', '#createGarageModalCreateBtn', function(){
		var modal = $(document).find('#createGarageModal');
		var thisBtn = $(this);
		$.fn.addEditGarage(modal, thisBtn);
	});

	$(document).on('click', '#editGarageModalSaveBtn', function(){
		var modal = $(document).find('#editGarageModal');
		var thisBtn = $(this);
		$.fn.addEditGarage(modal, thisBtn);
	});

	$(document).on('click', '#deleteGarageModaldeleteBtn', function(){
		var modal = $(document).find('#deleteGarageModal');
		var thisBtn = $(this);
		thisBtn.prop('disabled', true);
		var url = thisBtn.attr('data-route');
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify({}), DATA_TYPE_JSON).then((successResponse) => {
			if (successResponse.response) {
				thisBtn.prop('disabled', false);
				modal.modal('hide');
				$.blockUI({ message: successResponse.message, timeout: 2000,baseZ: 1005 });
				$.fn.initGarageTable();
			}
		}, (errorResponse) => {
			thisBtn.prop('disabled', false);
			modal.modal('hide');
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});

	$(document).on('click', '#saleNotesGarageModalSaveBtn', function(){
		var thisBtn = $(this);
		thisBtn.prop('disabled', true);
		var modal = $(document).find('#saleNotesGarageModal');
		var newNote = modal.find('#saleNotesGarageModalNewSaleNote');
		var garageId = thisBtn.attr('data-id');
		var data = {
			"garageId": garageId,
			"saleNote": newNote.val().trim()
		};
		var url = thisBtn.attr('data-route');
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			thisBtn.prop('disabled', false);
			if(successResponse.response) {
				$.fn.generateNoteContent(successResponse.saleNotes, modal.find('#saleNotesDiv'));
				newNote.val('');
				if($(document).find('.saleNotePopup').length) {
					$(`.saleNotePopup[data-id=${garageId}]`).addClass('primary-color');
				}
			}
		}, (errorResponse) => {
			thisBtn.prop('disabled', false);
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});

	$(document).on('click', '#demoGarageModalSendBtn', function(){
		var thisBtn = $(this);
		thisBtn.prop('disabled', true);
		var modal = $(document).find('#demoGarageModal');
		var garageId = thisBtn.attr('data-id');
		var data = {
			"garageId": garageId,
		};
		$.fn.sendAjax(garageId, modal, thisBtn, data);
	});

	$(document).on('click', '#membershipModalCreatePasswordBtn', function(){
		var thisBtn = $(this);
		thisBtn.prop('disabled', true);
		var modal = $(document).find('#startMembershipModal');
		var garageId = thisBtn.attr('data-id');
		var data = {
			"garageId": garageId,
			"discount": modal.find('#startMembershipModalDiscountInpt').val()
		};
		$.fn.sendAjax(garageId, modal, thisBtn, data);
	});

	$(document).on('change, keyup', '#startMembershipModalDiscountInpt', function(){
		var modal = $(document).find('#startMembershipModal');
		var discount = modal.find('#startMembershipModalDiscountInpt').val();
		discount = discount ? parseFloat(discount) : 0.00;
		modal.find('#startMembershipModalFinalPriceInpt').val(
			parseFloat(modal.find('#startMembershipModalPriceInpt').val()) - discount
		);
	});

	$(document).on('change, keyup', '#extendMembershipModalDiscountInpt', function(){
		var modal = $(document).find('#extendMembershipModal');
		var discount = modal.find('#extendMembershipModalDiscountInpt').val();
		discount = discount ? parseFloat(discount) : 0.00;
		modal.find('#extendMembershipModalFinalPriceInpt').val(
			parseFloat(modal.find('#extendMembershipModalPriceInpt').val()) - discount
		);
	});

	$(document).on('click', '.extendMembershipModalDecisionBtn', function(){
		var thisBtn = $(this);
		thisBtn.prop('disabled', true);
		var type = thisBtn.attr('data-type');
		var modal = $(document).find('#extendMembershipModal');
		var garageId = thisBtn.attr('data-id');
		var data = {
			"garageId": garageId,
			"type": type
		};
		if(type == 'renew') {
			data.discount = modal.find('#extendMembershipModalDiscountInpt').val(),
			data.memberExpiry = modal.find('#extendMembershipModalGarageExpiryInpt').val()
		}
		$.fn.sendAjax(garageId, modal, thisBtn, data);
	});

	$(document).on('click', '.actionBtn', function(){
		var modal = $(document).find('#erkenningModal');
		var thisBtn = $(this);
		var type = thisBtn.attr('data-type');
		var garageId = thisBtn.attr('data-id');
		if(type == ACTION_EMAIL){
			thisBtn.prop('disabled', true);
			var data = {"garageId": garageId};
			$.fn.sendAjax(garageId, modal, thisBtn, data);
		} else {
			// Sticker
		}
	});

	$('#showAllGarage').on('click', function(){
		$.fn.initGarageTable();
	});

	$.fn.initGarageTable();
});