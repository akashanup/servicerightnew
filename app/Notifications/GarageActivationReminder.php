<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GarageActivationReminder extends Notification
{
    /**
     * The garage.
     *
     * @var App\Models
     */
    public $garage;

    /**
     * The garage user.
     *
     * @var App\Models
     */
    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $garage)
    {
        $this->user = $user;
        $this->garage = $garage;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $garage = $this->garage;
        $user = $this->user;

        return (new MailMessage)
            ->view('emails.garage-activation-reminder', [
                'title' => '',
                'salutation' => $user->salutation,
                'name' => $user->name,
                'garageId' => $garage->id,
                'password' => $garage->activation_token,
                'token' => $garage->activation_token,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
