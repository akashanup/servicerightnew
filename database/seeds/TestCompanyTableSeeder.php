<?php

use App\Models\Company;
use Illuminate\Database\Seeder;

class TestCompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(Company::class, 5)->create();
        $companies = ['Bovag', 'Rdw', 'Focwa'];
        foreach ($companies as $key => $company) {
            Company::create([
                'name' => $company,
            ]);
        }
    }
}
