<?php

use App\Models\Order;
use Illuminate\Database\Seeder;

class TestOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Order::class, 500)->create();
    }
}
