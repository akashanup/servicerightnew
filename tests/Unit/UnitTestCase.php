<?php

namespace Tests\Unit;

use DB;
use Tests\TestCase;

class UnitTestCase extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }
    public function tearDown()
    {
        DB::rollBack();
        parent::tearDown();
    }
}
