<?php

include_once 'generate-file.php';

$dictionary = [
    'assign_to_garage' => [
        'en' => 'assign to a garage',
        'nl' => 'toewijzen aan garage',
    ],
    'company_type' => [
        'en' => 'Company type',
        'nl' => 'Type bedrijf',
    ],
    'members' => [
        'en' => 'Members',
        'nl' => 'Leden',
    ],
    'show_only_members' => [
        'en' => 'Show only members',
        'nl' => 'Alleen leden tonen',
    ],
    'lid' => [
        'en' => 'Member',
        'nl' => 'Lid',
    ],
    'payment_not_met' => [
        'en' => 'payment not yet met',
        'nl' => 'betaling nog niet voldaan',
    ],
    'with_priority' => [
        'en' => 'with priority',
        'nl' => 'met voorrang',
    ],
    'not_happy' => [
        'en' => 'not happy, money back',
        'nl' => 'niet goed geld terug',
    ],
    'vehicle_details' => [
        'en' => 'Vehicle details',
        'nl' => 'Autogegevens',
    ],
    'order_details' => [
        'en' => 'Order details',
        'nl' => 'Ordergegevens',
    ],
    'car' => [
        'en' => 'Car',
        'nl' => 'Auto',
    ],
    'customer' => [
        'en' => 'Customer',
        'nl' => 'Klant',
    ],
    'location' => [
        'en' => 'Location',
        'nl' => 'Locatie',
    ],
    'preferred_date' => [
        'en' => 'Preferred date',
        'nl' => 'Voorkeursdatum',
    ],
    'internal_notes' => [
        'en' => 'Internal notes',
        'nl' => 'Interne notities',
    ],
    'add_note' => [
        'en' => 'Add note',
        'nl' => 'Notitie toevoegen',
    ],
    'from' => [
        'en' => 'from',
        'nl' => 'uit',
    ],
    'on' => [
        'en' => 'On',
        'nl' => 'Op',
    ],
    'email_text_1' => [
        'en' => ',your maintenance is performed at ',
        'nl' => 'wordt uw onderhoud uitgevoerd bij ',
    ],
    'email_text_2' => [
        'en' => 'Your vehicle will be picked up between ',
        'nl' => 'Uw voertuig zal worden opgehaald tussen ',
    ],
    'service_point_data' => [
        'en' => 'Service point data',
        'nl' => 'Gegevens servicepunt',
    ],
    'email_text_3' => [
        'en' => 'You have the right to cancel your maintenance service at any time. As soon as the maintenance service has not been canceled in writing by you within 24 hours before the start of the maintenance service, we will be obliged to pay administration costs. (See article 7.1 from our general terms and conditions)',
        'nl' => 'U heeft het recht om uw onderhoudsservice op elk gewenst moment te annuleren. Zodra de onderhoudsservice niet binnen 24 uur voor de aanvang van de onderhoudsservice schriftelijk door u is geannuleerd, zijn wij genoodzaakt administratiekosten in rekening te brengen. (Zie artikel 7.1 uit onze algemene voorwaarden)',
    ],
    'email_text_4' => [
        'en' => 'If you want a different garage or do not agree with our terms and conditions then we would like to hear from you. If we are not allowed to receive a response from you on this subject, we assume that you agree with both the garage and the conditions.',
        'nl' => 'Indien u een andere garage wenst of niet akkoord gaat met onze algemene voorwaarden dan vernemen wij graag van u. Indien wij geen reactie van u mogen ontvangen hieromtrent gaan wij er vanuit dat u met zowel de garage als de voorwaarden akkoord gaat.',
    ],
    'email_text_5' => [
        'en' => 'Below you will find the details of the maintenance already confirmed by you',
        'nl' => 'Onderstaand de gegevens van het reeds door u bevestigd onderhoud',
    ],
    'customer_data' => [
        'en' => 'Customer data',
        'nl' => 'Gegevens klant',
    ],
    'vehicle_data' => [
        'en' => 'Vehicle data',
        'nl' => 'Gegevens auto',
    ],
    'maintenance_data' => [
        'en' => 'Maintenance data',
        'nl' => 'Gegevens onderhoud',
    ],
    'pickup_between' => [
        'en' => 'Pick up between',
        'nl' => 'Ophalen tussen',
    ],
    'email_text_6' => [
        'en' => 'If you have not yet had a telephone contact with the customer, it is best to call the customer via',
        'nl' => 'Indien u nog geen telefonisch contact heeft gehad met de klant kunt u het beste even bellen met de klant via',
    ],
    'email_text_7' => [
        'en' => 'to briefly review the assignment',
        'nl' => 'om de opdracht even kort door te nemen',
    ],
    'email_text_8' => [
        'en' => 'This amount will be paid in cash by the customer when you deliver the vehicle to the customer',
        'nl' => 'Dit bedrag wordt door de klant contant aan u voldaan wanneer u het voertuig bij de klant aflevert',
    ],
    'email_text_9' => [
        'en' => 'You can pay this amount in cash to the employee when your vehicle is delivered.',
        'nl' => 'Dit bedrag kunt u contant aan de medewerker voldoen wanneer uw voertuig wordt afgeleverd.',
    ],
    'email_text_10' => [
        'en' => 'Your vehicle is expected at ',
        'nl' => 'Uw voertuig wordt tussen ',
    ],
    'email_text_11' => [
        'en' => 'You can pay this amount to the garage when your vehicle picks up.',
        'nl' => 'Dit bedrag kunt u aan de garage voldoen wanneer uw voertuig ophaalt.',
    ],
    'at' => [
        'en' => 'at ',
        'nl' => 'verwacht bij ',
    ],
    'email_text_12' => [
        'en' => 'the work is carried out by',
        'nl' => 'worden de werkzaamheden uitgevoerd door ',
    ],
    'email_text_13' => [
        'en' => 'will between ',
        'nl' => 'zal tussen ',
    ],
    'email_text_14' => [
        'en' => 'at your location ',
        'nl' => 'bij u op locatie ',
    ],
    'email_text_15' => [
        'en' => 'carry out the work.',
        'nl' => 'de werkzaamheden uitvoeren.',
    ],
    'email_text_16' => [
        'en' => 'This amount must be paid to the garage after the work has been carried out.',
        'nl' => 'Dit bedrag dient u aan de garage te voldoen nadat de werkzaamheden zijn uitgevoerd.',
    ],
];

generateFile($dictionary, basename(__FILE__));
