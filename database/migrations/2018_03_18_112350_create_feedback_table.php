<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('rating')->nullable();
            $table->text('review')->nullable();
            $table->enum('status', ['new', 'accepted', 'declined', 'processed'])->default('new');
            $table->enum('social_media_post', ['yes', 'no'])->default('no');
            $table->unsignedInteger('order_id');
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feedback', function (Blueprint $table) {
            $table->dropForeign('feedback_order_id_foreign');
            $table->dropColumn('order_id');
        });
        Schema::dropIfExists('feedback');
    }
}
