<?php

use App\Models\Address;
use App\Models\Brand;
use App\Models\Category;
use App\Models\City;
use App\Models\Company;
use App\Models\Country;
use App\Models\LicensePlateNumber;
use App\Models\PostalCode;
use App\Models\Service;
use App\Models\User;
use App\Models\Vehicle;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GaragesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $servername = config('constants.OLD_DB_SERVERNAME');
        $username = config('constants.OLD_DB_USERNAME');
        $password = config('constants.OLD_DB_PASSWORD');
        $dbname = config('constants.OLD_DB_NAME');

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $countryId = Country::where('name', 'Netherlands')->first()->id;
        $stateId = DB::table('states')->insertGetId(
            [
                'name' => 'Not found',
                'country_id' => $countryId,
            ]
        );

        $fakeId = 23456;

        $sql = "SELECT id, naam, contact_persoon, adres, postcode, plaats, telefoon, ontvangen_opdrachten, email, website, aangesloten_bij, notes, gewenste_opdrachten, money_back_warranty, priority, lidmaatschap_verloopt_op, wachtwoord, notes_company, notes_sales, company_type, id_moneybird from bedrijven WHERE branche LIKE 'autos' AND id > " . $fakeId;

        $result = $conn->query($sql);
        while ($row = $result->fetch_assoc()) {
            // Check for address.
            $thisAddressId = static::getAddress($stateId, $row["plaats"], $row["postcode"], $row["adres"], null, 'garage', $conn);
            // Check for user
            $userId = static::getUser($row['email'], $row['telefoon'], $row['contact_persoon'], User::GARAGE);

            $garageId = $row['id'];
            $memberExpiry = ($row['lidmaatschap_verloopt_op'] && $row['lidmaatschap_verloopt_op'] != '0000-00-00') ? Carbon::parse($row['lidmaatschap_verloopt_op'])->toDateString() : null;
            $membershipStatus = ($memberExpiry && Carbon::parse($memberExpiry)->gte(Carbon::now())) ? 'active' : 'inactive';
            DB::table('garages')->insert(
                [
                    'id' => $garageId,
                    'name' => $row['naam'],
                    'website' => $row['website'],
                    'address_id' => $thisAddressId,
                    'user_id' => $userId,
                    'money_back_warranty' => $row['money_back_warranty'] ? $row['money_back_warranty'] : '0',
                    'priority' => $row['priority'] ? $row['priority'] : '0',
                    'member_expiry' => $memberExpiry,
                    'password' => $row['wachtwoord'] ? $row['wachtwoord'] : null,
                    'notes' => $row['notes'] ? $row['notes'] : null,
                    'type' => $row['company_type'] ? $row['company_type'] : null,
                    'additional_services' => $row['notes_company'],
                    'moneybird_id' => $row['id_moneybird'],
                    'membership_status' => $membershipStatus,
                ]
            );

            // Add categories for garage
            $categories = explode(',', $row['gewenste_opdrachten']);
            foreach ($categories as $category) {
                if ($category) {
                    $thisCategory = Category::where('name', $category)->first();
                    if (!$thisCategory) {
                        $thisCategoryId = DB::table('categories')
                            ->insertGetId(['name' => $category]);
                    } else {
                        $thisCategoryId = $thisCategory->id;
                    }
                    DB::table('garage_categories')->insert(
                        [
                            'category_id' => $thisCategoryId,
                            'garage_id' => $garageId,
                        ]
                    );
                }
            }

            // Add companies for garage
            $companies = explode(',', $row['aangesloten_bij']);
            foreach ($companies as $company) {
                if (in_array($company, ['Bovag', 'Rdw', 'Focwa'])) {
                    $thisCompany = Company::where('name', $company)->first();
                    if (!$thisCompany) {
                        $thisCompanyId = DB::table('companies')
                            ->insertGetId(['name' => $company]);
                    } else {
                        $thisCompanyId = $thisCompany->id;
                    }
                    DB::table('garage_companies')->insert(
                        [
                            'company_id' => $thisCompanyId,
                            'garage_id' => $garageId,
                        ]
                    );
                }
            }

            // Add sale note
            $saleNote = $row['notes_sales'];
            if ($saleNote) {
                DB::table('garage_sale_notes')->insert(
                    [
                        'garage_id' => $garageId,
                        'sale_note' => $saleNote,
                        'by_user_id' => 1,
                    ]
                );
            }
            $sql3 = "SELECT * from orders WHERE bedrijf_geaccepteerd = '" . $row['id'] . "' AND branche LIKE 'autos' AND datum NOT IN ('0000-00-00', '1970-01-01') AND plaatsings_datum NOT IN ('0000-00-00', '1970-01-01')";
            $result3 = $conn->query($sql3);
            while ($row3 = $result3->fetch_assoc()) {
                try {
                    $sql4 = "SELECT * from rejected_orders WHERE bid = '" . $row["id"] . "' AND oid = '" . $row3["id"] . "' LIMIT 1";
                    $result4 = $conn->query($sql4);
                    if ($result4->num_rows == 1) {
                        $status = 'rejected_by_garage';
                    } else {
                        $status = 'completed';
                    }

                    $sql5 = "SELECT * from kenteken_informatie WHERE kenteken = '" . $row3['kenmerk'] . "' LIMIT 1";
                    $result5 = $conn->query($sql5);
                    if ($result5->num_rows == 1) {
                        $row5 = $result5->fetch_assoc();
                        $sql6 = "SELECT * from prijzen_autos WHERE merk = '" . $row5['merk'] . "' AND model = '" . $row5['model'] . "' AND brandstof = '" . $row5['brandstof'] . "' AND bouwjaar_min = '" . $row5['bouwjaar'] . "' LIMIT 1";

                        $result6 = $conn->query($sql6);
                        if ($result6 && $result6->num_rows == 1) {
                            $row2 = $result6->fetch_assoc();
                            if (strlen($row5['bouwjaar']) > 3) {
                                $thisLicensePlateNumberId = static::getVehicle(
                                    $row3['kenmerk'], $row5['merk'],
                                    $row5['brandstof'], $row5['model'],
                                    ($row5['bouwjaar'] . '-01-01'), $row5['apk_verloop_datum'],
                                    static::createServiceJson($row2)
                                );

                                // Check for licensePlateNumber.
                                if ($thisLicensePlateNumberId) {
                                    $addressId = static::getAddress($stateId, $row3['plaats'], $row3['postcode'], $row3['adres'], $row3['huisnummer'], 'client', $conn);

                                    // Check for user
                                    $thisUserId = static::getUser($row3['email'], $row3['telefoon'], $row3['naam'],
                                        User::CLIENT);

                                    $sql7 = "SELECT prijzen from orders_prijzen where order_id = '" . $row3['id'] . "' LIMIT 1";
                                    $result7 = $conn->query($sql7);
                                    $prices = [];
                                    if ($result7->num_rows == 1) {
                                        $row7 = $result7->fetch_assoc();
                                        $prices = $row7['prijzen'] ? json_decode($row7['prijzen'], true) : 0.00;
                                    }

                                    $thisOrderId = $row3['id'];
                                    if ($row3['voltooid'] == '0000-00-00 00:00:00') {
                                        $status = 'accepted_by_garage';
                                    }
                                    $row3['datum'] = ($row3['datum'] != '0000-00-00' && $row3['datum'] != '1970-01-01') ? $row3['datum'] : $row3['plaatsings_datum'];

                                    DB::table('orders')->insert(
                                        [
                                            'id' => $thisOrderId,
                                            'user_id' => $thisUserId,
                                            'address_id' => $addressId,
                                            'license_plate_number_id' => $thisLicensePlateNumberId,
                                            'pickup_date' => Carbon::parse($row3['datum'])->toDateString(),
                                            'pickup_time' => $row3['tijd'],
                                            'pickup_type' => 'pickup_from_client',
                                            'status' => $status,
                                            'total_price' => (array_sum($prices) / 100),
                                        ]
                                    );

                                    foreach (explode(',', $row3['diensten']) as $service) {
                                        $service = Service::where('code', $service)->first();
                                        if ($service) {
                                            $price = array_key_exists($service->code, $prices) ? ($prices[$service] / 100) : 0.00;
                                            DB::table('order_services')->insert(
                                                [
                                                    'service_id' => $service->id,
                                                    'order_id' => $thisOrderId,
                                                    'price' => $price,
                                                    'created_at' => Carbon::parse($row3['datum'])->toDateTimeString(),
                                                    'updated_at' => Carbon::parse($row3['datum'])->toDateTimeString(),
                                                ]
                                            );
                                        }
                                    }

                                    DB::table('garage_orders')->insert(
                                        [
                                            'garage_id' => $garageId,
                                            'order_id' => $thisOrderId,
                                            'status' => $status,
                                            'created_at' => Carbon::parse($row3['voltooid'])->toDateTimeString(),
                                            'updated_at' => Carbon::parse($row3['voltooid'])->toDateTimeString(),
                                        ]
                                    );
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    \Log::info("+++++GaragesTableSeederStart++++");
                    \Log::info($e);
                    \Log::info("+++++GaragesTableSeederEnd++++");
                }
            }
        }

        // Store orders which are in confirmed state but didn't reach to garages till now.
        $sql3 = "SELECT * from orders WHERE bedrijf_geaccepteerd = '0' AND branche LIKE 'autos' AND datum NOT IN ('0000-00-00', '1970-01-01') AND plaatsings_datum NOT IN ('0000-00-00', '1970-01-01')";
        $result3 = $conn->query($sql3);
        while ($row3 = $result3->fetch_assoc()) {
            try {
                $status = 'confirmed';
                $sql5 = "SELECT * from kenteken_informatie WHERE kenteken = '" . $row3['kenmerk'] . "' LIMIT 1";
                $result5 = $conn->query($sql5);
                if ($result5->num_rows == 1) {
                    $row5 = $result5->fetch_assoc();
                    $sql6 = "SELECT * from prijzen_autos WHERE merk = '" . $row5['merk'] . "' AND model = '" . $row5['model'] . "' AND brandstof = '" . $row5['brandstof'] . "' AND bouwjaar_min = '" . $row5['bouwjaar'] . "' LIMIT 1";

                    $result6 = $conn->query($sql6);
                    if ($result6 && $result6->num_rows == 1) {
                        $row2 = $result6->fetch_assoc();
                        if (strlen($row5['bouwjaar']) > 3) {
                            $thisLicensePlateNumberId = static::getVehicle(
                                $row3['kenmerk'], $row5['merk'],
                                $row5['brandstof'], $row5['model'],
                                ($row5['bouwjaar'] . '-01-01'), $row5['apk_verloop_datum'],
                                static::createServiceJson($row2)
                            );

                            // Check for licensePlateNumber.
                            if ($thisLicensePlateNumberId) {
                                $addressId = static::getAddress($stateId, $row3['plaats'], $row3['postcode'], $row3['adres'], $row3['huisnummer'], 'client', $conn);

                                // Check for user
                                $thisUserId = static::getUser($row3['email'], $row3['telefoon'], $row3['naam'],
                                    User::CLIENT);

                                $sql7 = "SELECT prijzen from orders_prijzen where order_id = '" . $row3['id'] . "' LIMIT 1";
                                $result7 = $conn->query($sql7);
                                $prices = [];
                                if ($result7->num_rows == 1) {
                                    $row7 = $result7->fetch_assoc();
                                    $prices = $row7['prijzen'] ? json_decode($row7['prijzen'], true) : 0.00;
                                }

                                $thisOrderId = $row3['id'];

                                $row3['datum'] = ($row3['datum'] != '0000-00-00' && $row3['datum'] != '1970-01-01') ? $row3['datum'] : $row3['plaatsings_datum'];

                                DB::table('orders')->insert(
                                    [
                                        'id' => $thisOrderId,
                                        'user_id' => $thisUserId,
                                        'address_id' => $addressId,
                                        'license_plate_number_id' => $thisLicensePlateNumberId,
                                        'pickup_date' => Carbon::parse($row3['datum'])->toDateString(),
                                        'pickup_time' => $row3['tijd'],
                                        'pickup_type' => 'pickup_from_client',
                                        'status' => $status,
                                        'total_price' => (array_sum($prices) / 100),
                                    ]
                                );

                                foreach (explode(',', $row3['diensten']) as $service) {
                                    $service = Service::where('code', $service)->first();
                                    if ($service) {
                                        $price = array_key_exists($service->code, $prices) ? ($prices[$service] / 100) : 0.00;
                                        DB::table('order_services')->insert(
                                            [
                                                'service_id' => $service->id,
                                                'order_id' => $thisOrderId,
                                                'price' => $price,
                                                'created_at' => Carbon::parse($row3['datum'])->toDateTimeString(),
                                                'updated_at' => Carbon::parse($row3['datum'])->toDateTimeString(),
                                            ]
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                \Log::info("+++++GaragesTableSeederStart++++");
                \Log::info($e);
                \Log::info("+++++GaragesTableSeederEnd++++");
            }
        }
    }

    public static function getAddress($stateId, $city, $postalCode, $address, $houseNumber, $type, $conn)
    {
        $completeAddress = utf8_encode($address);
        $address = $conn->real_escape_string(str_replace(' ', '+', $address . ',' . $postalCode . ',NL'));
        $sql2 = "SELECT * from geocodes WHERE name = '" . $address . "' LIMIT 1";
        $result2 = $conn->query($sql2);
        $latitude = null;
        $longitude = null;
        if ($result2->num_rows == 1) {
            $result2 = $result2->fetch_assoc();
            $latitude = $result2['latitude'];
            $longitude = $result2['longitude'];
        }

        $city = City::createUnique($stateId, $city);
        $postalCode = PostalCode::createUnique($city->id, $postalCode);
        $thisAddress = Address::where('complete_address', $completeAddress);
        if ($houseNumber) {
            $thisAddress = $thisAddress->where('house_number', $houseNumber);
        }
        if ($latitude && $longitude) {
            $thisAddress = $thisAddress->where('latitude', $latitude)->where('longitude', $longitude);
        }
        $thisAddress = $thisAddress->first();
        if (!$thisAddress) {
            $thisAddressId = DB::table('addresses')->insertGetId(
                [
                    'postal_code_id' => $postalCode->id,
                    'type' => $type,
                    'complete_address' => $completeAddress,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'house_number' => $houseNumber,
                ]
            );
            return $thisAddressId;
        }
        return $thisAddress->id;
    }

    public static function getUser($email, $phone, $contact_persoon, $type)
    {
        $user = User::where('email', $email)->orWhere('phone', $phone)->first();
        if (!$user) {
            $userId = DB::table('users')->insertGetId(
                [
                    'salutation' => 'Mr.',
                    'name' => $contact_persoon,
                    'email' => $email,
                    'phone' => $phone,
                    'type' => $type,
                    'status' => 'active',
                ]
            );

            return $userId;
        }
        return $user->id;
    }

    public static function getVehicle($licensePlateNumber, $brand, $fuel_type, $model, $build_year_start, $mandatory_service_expiry_date, $services)
    {
        $thisLicensePlateNumber = LicensePlateNumber::where('license_plate_number', $licensePlateNumber)->first();
        if ($thisLicensePlateNumber) {
            return $thisLicensePlateNumber->id;
        } elseif ($mandatory_service_expiry_date && ($mandatory_service_expiry_date != '0000-00-00') && ($build_year_start != '0')) {
            $brand = Brand::createUnique($brand);
            $newVehicle = Vehicle::createUnique($brand->id, $fuel_type, $model, $build_year_start);
            $licensePlateNumber = LicensePlateNumber::create(
                [
                    'vehicle_id' => $newVehicle->id,
                    'license_plate_number' => $licensePlateNumber,
                    'mandatory_service_expiry_date' => Carbon::parse($mandatory_service_expiry_date),
                ]
            );
            if ($services) {
                $tempServices = json_decode($services, true);
                if (!array_filter($tempServices)) {
                    $parent = Vehicle::find($newVehicle->parent_id);
                    $services = $parent->services;
                }
                $newVehicle->update(['services' => $services]);
            }
            return $licensePlateNumber->id;
        } else {
            $licensePlateNumber = LicensePlateNumber::fetch($licensePlateNumber);
            return array_key_exists('licensePlateNumberId', $licensePlateNumber) ? $licensePlateNumber['licensePlateNumberId'] : null;
        }
    }

    public static function createServiceJson($data)
    {
        $service = [
            "1" => ($data['prijs_kleine_beurt'] / 100), "2" => ($data['prijs_grote_beurt'] / 100), "3" => 0.00, "4" => 0.00, "5" => 0.00, "6" => 0.00, "7" => ($data['prijs_bougies_vervangen'] / 100), "8" => ($data['prijs_katalysator_vervangen'] / 100), "9" => ($data['prijs_remblokken_voor_vervangen'] / 100), "10" => ($data['prijs_koppeling_vervangen'] / 100),
            "11" => ($data['prijs_accu_vervangen'] / 100), "12" => 0.00, "13" => ($data['prijs_uitlaat_achterdemper_vervangen'] / 100), "14" => ($data['prijs_koppakking_vervangen'] / 100), "15" => ($data['prijs_distributieriem_vervangen'] / 100), "16" => 0.00, "17" => ($data['prijs_remschijven_achter_vervangen'] / 100), "18" => 0.00, "19" => ($data['prijs_schokdempers_achter_vervangen'] / 100), "20" => ($data['prijs_remblokken_achter_vervangen'] / 100),
            "21" => ($data['prijs_dynamo_vervangen'] / 100), "22" => ($data['prijs_remschijven_voor_vervangen'] / 100), "23" => 0.00, "24" => 0.00, "25" => 0.00, "26" => 0.00, "27" => ($data['prijs_olie_verversen'] / 100), "28" => ($data['prijs_waterpomp_vervangen'] / 100), "29" => 0.00, "30" => 0.00,
            "31" => 0.00, "32" => 0.00, "33" => 0.00, "34" => 0.00, "35" => 0.00, "36" => 0.00, "37" => 0.00, "38" => 0.00, "39" => 0.00, "40" => 0.00,
            "41" => 0.00, "42" => 0.00, "43" => 0.00, "44" => 0.00, "45" => 0.00, "46" => 0.00, "47" => 0.00, "48" => 0.00, "49" => 0.00, "50" => 0.00,
            "51" => 0.00, "52" => 0.00, "53" => 0.00, "54" => 0.00,
        ];

        return json_encode($service);
    }
}
