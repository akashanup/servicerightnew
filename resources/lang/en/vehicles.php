<?php

return [
"manage_price" => "Manage prices",
"note" => "Note",
"there_are" => "There are",
"brands_without_prices" => "brands without prices",
"search_brand" => "Search prices on Brand",
"search_license_plate" => "Search prices by license plate",
"edit_price" => "Edit prices",
"manage" => "manage",
"basic_prices" => "Basic prices",
"text1" => "If there are no prices for a specific year of construction, the system will fall back on the basic prices. Make sure that these are in any case filled in as much as possible.",
"text2" => "For diesel cars, a surcharge is calculated on the basis of the percentage stated under each service. By default this is 5%",
"text3" => "If prices for this year are different from the basic prices, the prices have to be entered here. If a price is not entered, the system falls back to the basic price.",
"waterpump" => "Including water pump?",
];