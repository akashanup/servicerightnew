<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
    <page backleft="20mm" backbottom="14mm">
        <div id="logo" style="position: absolute; top: 48px;">
            <img src="{{ asset('images/logo/sr-logo.png') }}" alt="Yellow Hats" width="300">
        </div>
        <div id="adres" style="position: absolute; left: 450px; top: 63px; font:arial; color:#767676;">
            {{ $user->salutation }} {{ $user->name }}<br/>
            {{ $address->complete_address }} {{ $address->house_number }}<br/>
            {{ $postalCode->postal_code }} {{ $postalCode->city->name }}<br/>
        </div>
        <div id="order" style="font:arial; color:#767676;">
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <h2>Order</h2>
            <p>
            	{{ __('employee-order-status.paragraph_text_2') }}
            	<b>{{ $vehicle->brand->name}} - {{ $vehicle->model}}</b>
            	{{ __('employee-place-order.order_confirm_text_2') }} <b>{{ $order->licensePlateNumber->license_plate_number}}</b>.
            </p>
        </div>

        @foreach($order->services as $service)
            <div style="width:600px; font:arial; color:#767676;">
                <h3>{{ $service->name }} ( &euro; {{ $service->pivot->price }} )</h3>
                {{ $service->description }}
            </div>
            <br/>
        @endforeach

        <div style="border:1px solid #fff; border-top:1px solid #ededed; border-bottom:1px solid #ededed; padding-top:10px; padding-bottom:10px;">
            <h3 style="color:#309ed4; margin:0px;">{{ __('employee-place-order.total_price') }}: &euro; {{ $order->total_price}} <span style="font-size:12px;">(Incl. 21% BTW)</span></h3>
        </div>

    </page>
    <footer>
        <div style="padding-left:50px; padding-right:50px; font-size:12px; color:#a5a5a5;">
            <b>{{ __('employee-order-status.paragraph_text_3') }}</b><br/>
            {{ __('employee-order-status.paragraph_text_4') }}
        </div>
    </footer>
</body>
</html>
