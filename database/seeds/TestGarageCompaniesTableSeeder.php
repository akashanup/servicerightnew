<?php

use App\Models\GarageCompany;
use Illuminate\Database\Seeder;

class TestGarageCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(GarageCompany::class, 25)->create();
    }
}
