<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Order;
use App\Models\User;
use DB;
use Lang;

class ClientController extends Controller
{
    /**
     * The order.
     *
     * @var string
     */
    public $order;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->getOrderFromToken();
    }

    /**
     * Get the order from request token
     *
     * @return void
     */
    public function getOrderFromToken()
    {
        $this->order = Order::where('verification_token', request()->route('token'))->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $token
     * @return \Illuminate\Http\Response
     */
    public function phone($token)
    {
        return view('clients.phone', ['order' => $this->order]);
    }

    /**
     * Verifies the client for the order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verify($token)
    {
        $user = User::where('phone', request('phone'))->first();
        if ($user && $this->order->user->id == $user->id) {
            $this->order->update(['client_verified' => User::VERIFIED]);
            return redirect()->route('clients.viewQuotedOrder', $this->order->verification_token);
        } else {
            return redirect()->route('home')->withErrors(Lang::get('globals.request_error_message'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewQuotedOrder($token)
    {
        if ($this->order->client_verified == User::VERIFIED) {
            $order = $this->order;
            $user = $order->user;
            $address = $order->address;
            $postalCode = $address->postalCode;
            $city = $postalCode->city;
            $country = $city->state->country;
            $licensePlateNumber = $order->licensePlateNumber;
            $vehicle = $licensePlateNumber->vehicle;
            $countries = Country::where('id', '!=', $country->id)->get();
            return view(
                'clients.view-quoted-order',
                [
                    'order' => $order,
                    'user' => $user,
                    'address' => $address,
                    'postalCode' => $postalCode,
                    'city' => $city,
                    'country' => $country,
                    'licensePlateNumber' => $licensePlateNumber,
                    'vehicle' => $vehicle,
                    'countries' => $countries,
                ]
            );
        } else {
            return redirect()->route('home')->withErrors(Lang::get('globals.link_expired'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmQuotedOrder()
    {
        DB::beginTransaction();

        $validatedData = request()->validate(
            [
                'salutation' => 'required',
                'name' => 'required|string|max:50',
                'phone' => 'required|string|max:15',
                'email' => 'required|string|email|max:50',
                'addressId' => 'nullable|numeric',
                'licensePlateNumberId' => 'nullable|numeric',
                'pickupDate' => 'required|date',
                'alternativePickupDate' => 'nullable|date',
                'pickupTime' => 'required|string|regex:/\d{2}:\d{2}-\d{2}:\d{2}/',
                'orderStatus' => 'required|string',
            ]
        );

        $json = $this->order->user->confirmQuotedOrder($this->order, $validatedData);
        DB::commit();
        return response()->json($json);
    }
}
