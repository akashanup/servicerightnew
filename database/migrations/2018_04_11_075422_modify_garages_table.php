<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyGaragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages', function (Blueprint $table) {
            // Since it has enum columns so schema builder wont work.
            // DB::statement is a workaround for this.
            DB::statement('ALTER TABLE garages CHANGE name name VARCHAR(100)');
            DB::statement('ALTER TABLE garages CHANGE website website VARCHAR(100)');
            DB::statement('ALTER TABLE garages CHANGE activation_token activation_token VARCHAR(100)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages', function (Blueprint $table) {
            DB::statement('ALTER TABLE garages CHANGE name name VARCHAR(255)');
            DB::statement('ALTER TABLE garages CHANGE website website VARCHAR(255)');
            DB::statement('ALTER TABLE garages CHANGE activation_token activation_token VARCHAR(255)');
        });
    }
}
