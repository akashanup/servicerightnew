<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Utils\RedirectUser;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $type = auth()->user()->type;
        if ($type == User::SR_ADMIN) {
            return $next($request);
        } else {
            return redirect(RedirectUser::getLoginRedirectUrl($type));
        }
    }
}
