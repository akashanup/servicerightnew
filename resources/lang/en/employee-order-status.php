<?php

return [
"check_jobs" => "Check jobs",
"paragraph_text_1" => "The assignments come in here and are automatically updated. You can leave this screen open and carry out assignments at any time.",
"paragraph_text_2" => "You will receive the order for the execution of the work as below
described for the vehicle",
"paragraph_text_3" => "ServiceRight takes care of your car maintenance!",
"paragraph_text_4" => "The purpose of ServiceRight is to be as friendly, cost efficient
            and easy to offer possible car service to our customers. At
            ServiceRight guarantees the warranty and saves you guaranteed
            comparison with the main dealer prices. Your vehicle will also be free
            picked up for your desired service and brought back to
            your home or workplace. You will be guided by one of our employees during each step
            kept informed.",
"shift" => "Shift",
"notes_for" => "Notes for",
"customer_address" => "Customer address",
"open_assignments" => "Open assignments",
"check" => "Check",
"assign" => "Assign",
"edit" => "Edit",
"complete" => "COMPLETE",
"order_unlock_text" => "Order locked, only you can process this order. Click to unlock.",
"order_lock_text" => "Lock order, only you can process this order. Click to lock.",
"email_text_1" => "is the maintenance of your vehicle with the license plate",
"email_text_2" => "maintained by ",
"email_text_3" => "We understand only too well that you would rather spend the amount of maintenance on your vehicle elsewhere. That is why you now have the chance to reclaim your entire invoice amount!
Click on the link below, you only have to fill in the amount to participate.",
"email_text_4" => "Win your invoice amount back!",
"other_work" => "Other work for order ",
"modal_text_1" => "For additional work you must first contact the customer via ",
"modal_text_2" => "Other activities must be mentioned below ",
"copy_of_invoice" => "Copy of invoice",
"no_file_choosen" => "No file chosen",
"modal_text_3" => "Note: Account is missing. Invoice still has to be uploaded.",
"modal_text_4" => "And / or enter the description of the other activities",
"note" => "Note ",
"modal_text_5" => "Costs for the other activities",
"modal_text_6" => "Finish assignment",
"modal_text_7" => "The maintenance for the vehicle has been performed.",
"confirm_agree" => "Confirm and agree ",
"cancelled_with_no_cancellation_cost" => "No Handling costs",
"cancelled_garage_gets_cancellation_cost" => "Charges handling costs at the company",
"cancelled_client_gets_cancellation_cost" => "Charges handling costs to the customer",
];