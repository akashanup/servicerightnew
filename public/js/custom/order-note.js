$(function(){

	$(document).on('click', '.orderNote', function(){
		var orderId = $(this).attr('data-id');
		var data = {
			"orderId": orderId
		};
		var url = $('#fetchNoteRoute').val();
		$.fn.ajaxBlockUI();
		$.fn.ajaxCall(REQUEST_TYPE_GET, url, data, DATA_TYPE_JSON).then((successResponse) => {
			if(successResponse.response) {
				$('#modalOrderId').html(orderId);
				$.fn.generateNoteContent(successResponse.notes, $(document).find('#notesDiv'));
				$.unblockUI();
				$('#notesModel').modal('show');
			} else {
				$.unblockUI();
				$.blockUI({ message: successResponse.message, timeout: 2000,baseZ: 1005 });
			}
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});


	$(document).on('click','#saveNoteBtn', () => {
		var orderId = $('#modalOrderId').html().trim();
		var data = {
			"orderId": orderId,
			"note": $('#noteInpt').val().trim()
		};
		var url = $('#storeNoteRoute').val();
		$.fn.ajaxBlockUI();
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			if(successResponse.response) {
				$.fn.generateNoteContent(successResponse.notes, $(document).find('#notesDiv'));
				$('#noteInpt').val('');
				if($('.orderNote').length) {
					$(`.orderNote[data-id=${orderId}]`).addClass('primary-color');
				}
			}
			$.unblockUI();
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});
});