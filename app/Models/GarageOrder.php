<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GarageOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'garage_id', 'order_id', 'status',
    ];
}
