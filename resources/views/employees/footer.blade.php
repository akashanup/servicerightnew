<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 widgets-links">

			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 copyrights text-center">
				<p><a href="#">{{ __('employee-globals.terms_and_conditions') }}</a> | KVK 54758718 | <a href="#">{{ __('employee-globals.privacy_policy') }}</a> | <a href="#">{{ __('employee-globals.contact') }}</a></p>
				<p>Serviceright<sup>&reg;</sup> {{ __('employee-globals.cars') }}, Serviceright<sup>&reg;</sup> {{ __('employee-globals.couriers') }} &amp; Serviceright<sup>&reg;</sup> {{ __('employee-globals.financial') }} {{ __('employee-globals.are_a_part_of') }} ServiceRight<sup>&reg;</sup> | &copy; {{ date('Y') }} {{ __('employee-globals.all_rights_reserved') }}.</p>
            </div>
        </div>
    </div>
</footer>
