<?php

namespace App\Notifications;

use App\Models\Service;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class OrderClientBringItself extends Notification
{
    use Queueable;

    /**
     * User.
     *
     * @var App\Models
     */
    public $user;

    /**
     * Order
     *
     * @var App\Models
     */
    public $order;

    /**
     * Garage.
     *
     * @var App\Models
     */
    public $garage;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $order, $garage)
    {
        $this->user = $user;
        $this->order = $order;
        $this->garage = $garage;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = $this->user;
        $order = $this->order;
        $garage = $this->garage;
        $garageAddress = $garage->address;
        $garagePostalCode = $garageAddress->postalCode;
        $garageUser = $garage->user;
        $userAddress = $order->address;
        $userPostalCode = $userAddress->postalCode;
        $licensePlateNumber = $order->licensePlateNumber;
        $vehicle = $licensePlateNumber->vehicle;
        $servicesList = Service::whereIn('id', $order->services()
                ->pluck('order_services.service_id'))->pluck('name')->toArray();
        $data = [
            'title' => Lang::get('employee-place-order.order_status_email_title'),
            'salutation' => $user->salutation,
            'name' => $user->name,
            'email' => $user->email,
            'houseNumber' => $userAddress->house_number,
            'completeAddress' => $userAddress->complete_address,
            'postalCode' => $userPostalCode->postal_code,
            'city' => $userPostalCode->city->name,
            'pickupDate' => $order->pickup_date,
            'pickupTime' => $order->pickup_time,
            'totalPrice' => $order->total_price,
            'servicesList' => $servicesList,
            'garageName' => $garage->name,
            'phone' => $user->phone,
        ];

        switch ($this->user->type) {
            case User::CLIENT:
                return (new MailMessage)->view(
                    'emails.order-client-bring-itself-client',
                    array_merge(
                        $data,
                        [
                            'garageLogo' => $garage->logo,
                            'garageAddress' => $garageAddress->complete_address,
                            'garagePostalCode' => $garagePostalCode->postal_code,
                            'garageCity' => $garagePostalCode->city->name,
                            'garageEmail' => $garageUser->email,
                            'garagePhone' => $garageUser->phone,
                        ]
                    )
                );
                break;
            case User::GARAGE:
                return (new MailMessage)->view(
                    'emails.order-client-bring-itself-garage',
                    array_merge(
                        $data,
                        [
                            'additionalWork' => $order->additional_work,
                            'orderId' => $order->id,
                            'brand' => $vehicle->brand->name,
                            'model' => $vehicle->model,
                            'licensePlateNumber' => $licensePlateNumber->license_plate_number,
                        ]
                    )
                );
                break;

            default:
                # code...
                break;
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
