<?php

namespace App\Models;

use App\Models\Category;
use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    const NO = 'no';

    /**
     * Get the category of the service.
     *
     * @return App\Models\Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    } //end category()

    /**
     * Get the order services for the order.
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_services', 'service_id', 'order_id')->withPivot('id', 'price')
            ->withTimestamps();
    } //end orders()
}
