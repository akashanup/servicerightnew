$(function(){

	$.fn.toggleCarPickUpBtn = ($toggle=true) => {
		$toggle ? $('#carPickUpBtn').attr('disabled', true) :  $('#carPickUpBtn').removeAttr('disabled');
	};

	$.fn.disableVehicleFields = (message) => {
		$.fn.toggleCarPickUpBtn();
		$('#licensePlateInpt').val('');
		$('#carBrandInpt').val('');
		$('#carModelInpt').val('');
		$('#carFuelTypelInpt').val('');
		$('#buildYearInpt').val('');
		$('#mandatoryServiceExpiryDateInpt').val('');
		$('#fetchedlicensePlateNumberId').val('');
		$('#fetchedServices').val('');
		$('#carBrandInpt, #carModelInpt, #carFuelTypelInpt').css('display', 'none');
		$('#carBrandSelect, #carModelSelect, #carFuelTypeSelect').css('display', 'block');
		$('#buildYearInpt').removeAttr('disabled');
		$('#mandatoryServiceExpiryDateInpt').removeAttr('disabled');
		$.unblockUI();
		$.blockUI({ message: message, timeout: 2000,baseZ: 1005 });
	};

	$('#carPickUpBtn').on('click', () => {
		// Refresh service list for the car.
		$('.serviceCheckBox').removeAttr('checked');
		$('#totalPrice').html('0.00');
		$('.form-check').find('.price-hidden').hide();

		var data = {
			"licensePlateNumber": $('#licensePlateInpt').val().trim(),
		};
		var url = $('#fetchVehicleRoute').val();

		$.fn.ajaxBlockUI();

		$.fn.ajaxCall(REQUEST_TYPE_GET, url, data, DATA_TYPE_JSON).then((successResponse) => {
			if (successResponse.response) {
				$('#carBrandSelect, #carModelSelect, #carFuelTypeSelect').css('display', 'none');
				$('#carBrandInpt').val(successResponse.brand);
				$('#carModelInpt').val(successResponse.model);
				$('#carFuelTypelInpt').val(successResponse.fuel_type);
				$('#buildYearInpt').val(successResponse.build_year_start);
				$('#mandatoryServiceExpiryDateInpt').val(successResponse.mandatory_service_expiry_date);
				$('#carBrandInpt, #carModelInpt, #carFuelTypelInpt').css('display', 'block');
				$('#buildYearInpt').attr('disabled', true);
				$('#mandatoryServiceExpiryDateInpt').attr('disabled', true);
				$('#fetchedlicensePlateNumberId').val(successResponse.licensePlateNumberId);
				$('#fetchedServices').val(successResponse.services);
				$.unblockUI();
			} else {
				$.fn.disableVehicleFields(successResponse.message);
			}
		}, (errorResponse) => {
				$.fn.disableVehicleFields(AJAX_ERROR_MESSAGE);
		});
		
	});

	$('#licensePlateInpt').on('keyup paste', (e) => {
		var licensePlateValue = $('#licensePlateInpt').val().trim();
		if (licensePlateValue.length < 3) {
			$.fn.toggleCarPickUpBtn();
		}
		var keyCode = e.keyCode;
		if ( ( $.fn.allowedKey(keyCode) || keyCode == undefined) && (licensePlateValue.length > 2) ){
			$.fn.toggleCarPickUpBtn(false);
		}
	});

	$('#carBrandSelect').on('change', function(){
		if(!$(this).val().length) {
			$('#carModelSelect').attr('disabled', true);
		} else {
			//Fetch values
			$('#carModelSelect').removeAttr('disabled');
		}
	});
});