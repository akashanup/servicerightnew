<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            // Since it has enum columns so schema builder wont work.
            // DB::statement is a workaround for this.
            DB::statement('ALTER TABLE addresses CHANGE house_number house_number VARCHAR(10)');
            DB::statement('ALTER TABLE addresses CHANGE latitude latitude VARCHAR(50)');
            DB::statement('ALTER TABLE addresses CHANGE longitude longitude VARCHAR(50)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            DB::statement('ALTER TABLE addresses CHANGE house_number house_number VARCHAR(255)');
            DB::statement('ALTER TABLE addresses CHANGE latitude latitude VARCHAR(255)');
            DB::statement('ALTER TABLE addresses CHANGE longitude longitude VARCHAR(255)');
        });
    }
}
