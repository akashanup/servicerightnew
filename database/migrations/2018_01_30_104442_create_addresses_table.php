<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('postal_code_id');
            $table->string('house_number');
            $table->enum('type', ['client', 'garage', 'employee']);
            $table->text('complete_address')->nullable();
            $table->timestamps();
            $table->foreign('postal_code_id')->references('id')->on('postal_codes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropForeign('addresses_postal_code_id_foreign');
            $table->dropColumn('postal_code_id');
        });
        Schema::dropIfExists('addresses');
    }
}
