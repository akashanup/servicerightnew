<?php

include_once 'generate-file.php';

$dictionary = [
    'check' => [
        'en' => 'Check',
        'nl' => 'Opdracht',
    ],
    'callback_request' => [
        'en' => 'Callback request',
        'nl' => 'Terugbelverzoek',
    ],
    'assignment' => [
        'en' => 'assignment',
        'nl' => 'Terugbelverzoek',
    ],
];

generateFile($dictionary, basename(__FILE__));
