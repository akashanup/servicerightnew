<?php

return [
"finder_company" => "Find company",
"login_aldoc" => "Login Aldoc",
"oil_content" => "Oil content",
"looking_for_a_type" => "looking for a type",
"calc_oil_price" => "Calculate oil price",
"calc_tyre_price" => "Calculate tire price",
"exercises" => "Exercises",
"schedule" => "Schedule",
"to_check" => "To check",
"open" => "Open",
"accepted" => "Accepted",
"rounded" => "Completed",
"company_management" => "Company management",
"references" => "References",
"complaints" => "Complaints",
"prices" => "Prices",
"edit_staff" => "Edit staff",
"settings" => "Settings",
"log_out" => "Log out",
"terms_and_conditions" => "Terms and Conditions",
"privacy_policy" => "Privacy policy",
"contact" => "Contact",
"cars" => "cars",
"couriers" => "couriers",
"financial" => "financial",
"are_a_part_of" => "are a part of",
"all_rights_reserved" => "All rights reserved",
];