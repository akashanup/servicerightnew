<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLockOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lock_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['locked', 'unlocked']);
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('to_user_id');
            $table->unsignedInteger('by_user_id');
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('to_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('by_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lock_orders', function (Blueprint $table) {
            $table->dropForeign('lock_orders_order_id_foreign');
            $table->dropColumn('order_id');
            $table->dropForeign('lock_orders_to_user_id_foreign');
            $table->dropColumn('to_user_id');
            $table->dropForeign('lock_orders_by_user_id_foreign');
            $table->dropColumn('by_user_id');
        });
        Schema::dropIfExists('lock_orders');
    }
}
