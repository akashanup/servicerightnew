<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['car', 'bus', 'truck', 'motorbike'])->default('car');
            $table->unsignedInteger('brand_id');
            $table->enum('fuel_type', ['Benzine', 'Diesel', 'G3', 'Elektriciteit', 'Hybride']);
            $table->string('model_number');
            $table->string('image')->nullable();
            $table->timestamps();
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropForeign('vehicles_brand_id_foreign');
            $table->dropColumn('brand_id');
        });
        Schema::dropIfExists('vehicles');
    }
}
