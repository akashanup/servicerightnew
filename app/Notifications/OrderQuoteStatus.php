<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class OrderQuoteStatus extends Notification
{
    use Queueable;

    /**
     * The user's salutation.
     *
     * @var string
     */
    public $salutation;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * The vehicle's brand.
     *
     * @var string
     */
    public $brand;

    /**
     * The vehicle's model number.
     *
     * @var string
     */
    public $modelNumber;

    /**
     * The vehicle's licensePlateNumber.
     *
     * @var string
     */
    public $licensePlateNumber;

    /**
     * The order's total price.
     *
     * @var string
     */
    public $totalPrice;

    /**
     * The order quote url.
     *
     * @var string
     */
    public $confirmQuoteUrl;

    /**
     * The services list.
     *
     * @var array
     */
    public $servicesList;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        $salutation,
        $name,
        $brand,
        $modelNumber,
        $licensePlateNumber,
        $totalPrice,
        $servicesList,
        $confirmQuoteUrl
    ) {
        $this->salutation = $salutation;
        $this->name = $name;
        $this->brand = $brand;
        $this->modelNumber = $modelNumber;
        $this->licensePlateNumber = $licensePlateNumber;
        $this->totalPrice = $totalPrice;
        $this->confirmQuoteUrl = $confirmQuoteUrl;
        $this->servicesList = $servicesList;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->view('emails.order-quote-status', [
                'title' => Lang::get('employee-place-order.order_status_email_title'),
                'salutation' => $this->salutation,
                'name' => $this->name,
                'brand' => $this->brand,
                'modelNumber' => $this->modelNumber,
                'licensePlateNumber' => $this->licensePlateNumber,
                'totalPrice' => $this->totalPrice,
                'confirmQuoteUrl' => $this->confirmQuoteUrl,
                'servicesList' => $this->servicesList,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
