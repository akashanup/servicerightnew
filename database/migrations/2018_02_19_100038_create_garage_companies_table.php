<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGarageCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('garage_id');
            $table->unsignedInteger('company_id');
            $table->timestamps();
            $table->foreign('garage_id')->references('id')->on('garages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garage_companies', function (Blueprint $table) {
            $table->dropForeign('garage_companies_garage_id_foreign');
            $table->dropColumn('garage_id');
            $table->dropForeign('garage_companies_company_id_foreign');
            $table->dropColumn('company_id');
        });
        Schema::dropIfExists('garage_companies');
    }
}
