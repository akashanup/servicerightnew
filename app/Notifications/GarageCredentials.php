<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GarageCredentials extends Notification
{
    use Queueable;

    /**
     * The garage.
     *
     * @var App\Models
     */
    public $garage;

    /**
     * The garage user.
     *
     * @var App\Models
     */
    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $garage)
    {
        $this->user = $user;
        $this->garage = $garage;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $garage = $this->garage;
        $user = $this->user;

        //Create and update Garage activation token.
        $token = $garage->generateActivationToken();
        //Create and update Garage Password.
        $password = $garage->updatePassword($token);

        return (new MailMessage)
            ->view('emails.garage-credentials', [
                'title' => '',
                'salutation' => $user->salutation,
                'name' => $user->name,
                'garageName' => $garage->name,
                'garageId' => $garage->id,
                'password' => $password,
                'token' => $token,
                'membershipCost' => $garage->memberships()->latest()->first()->final_price,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
