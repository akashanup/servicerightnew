<?php

include_once 'generate-file.php';

$dictionary = [
    'manage_references' => [
        'en' => 'Manage references',
        'nl' => 'Referenties beheren',
    ],
    'feedback_social_media' => [
        'en' => 'Have the customer place this reference on Social Media.',
        'nl' => 'Laat de klant deze referentie op Social Media plaatsen.',
    ],
];

generateFile($dictionary, basename(__FILE__));
