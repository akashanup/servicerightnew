<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class OrderCompleteRequest extends Notification
{
    use Queueable;

    /**
     * User.
     *
     * @var App\Models
     */
    public $user;

    /**
     * Order
     *
     * @var App\Models
     */
    public $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $order)
    {
        $this->user = $user;
        $this->order = $order;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.order-complete-request',
            [
                'title' => Lang::get('employee-place-order.order_status_email_title'),
                'salutation' => $this->user->salutation,
                'name' => $this->user->name,
                'pickupDate' => $this->order->pickup_date,
                'licensePlateNumber' => $this->order->licensePlateNumber->license_plate_number,
                'garageName' => $this->order->garage()->name,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
