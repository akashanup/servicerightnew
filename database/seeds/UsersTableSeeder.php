<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'salutation' => 'Mr.',
                'name' => 'Admin',
                'email' => 'akasha@mindfiresolutions.com',
                'phone' => '8800997755',
                'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
                'type' => 'sr_admin',
                'status' => 'active',
                'remember_token' => str_random(10),
            ]
        );

        DB::table('users')->insert(
            [
                'salutation' => 'Mr.',
                'name' => 'Akash Anup',
                'email' => 'akashanup.1993@gmail.com',
                'phone' => '9900997755',
                'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
                'type' => 'sr_employee',
                'status' => 'active',
                'remember_token' => str_random(10),
            ]
        );

        DB::table('users')->insert(
            [
                'salutation' => 'Mr.',
                'name' => 'Damon Salvatore',
                'email' => 'akashanup.1993@hotmail.com',
                'phone' => '8527460334',
                'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
                'type' => 'sr_employee',
                'status' => 'active',
                'remember_token' => str_random(10),
            ]
        );

        //factory(User::class, 40)->create();
    }
}
