<?php

include_once 'generate-file.php';

$dictionary = [
    'check_jobs' => [
        'en' => 'Check jobs',
        'nl' => 'Opdrachten controleren',
    ],
    'paragraph_text_1' => [
        'en' => 'The assignments come in here and are automatically updated. You can leave this screen open and carry out assignments at any time.',
        'nl' => 'De opdrachten komen hier binnen en worden automatisch geupdate. U kunt dit scherm open laten staan en te allen tijde opdrachten doorvoeren.',
    ],
    'paragraph_text_2' => [
        'en' => 'You will receive the order for the execution of the work as below
described for the vehicle',
        'nl' => 'Hierbij ontvangt u de order voor het laten uitvoeren van de werkzaamheden zoals onderstaand
omschreven voor het voertuig',
    ],
    'paragraph_text_3' => [
        'en' => 'ServiceRight takes care of your car maintenance!',
        'nl' => 'ServiceRight verzorgd uw auto-onderhoud!',
    ],
    'paragraph_text_4' => [
        'en' => 'The purpose of ServiceRight is to be as friendly, cost efficient
            and easy to offer possible car service to our customers. At
            ServiceRight guarantees the warranty and saves you guaranteed
            comparison with the main dealer prices. Your vehicle will also be free
            picked up for your desired service and brought back to
            your home or workplace. You will be guided by one of our employees during each step
            kept informed.',
        'nl' => 'Het doel van ServiceRight is om een zo vriendelijk, kosten efficiënt
            en eenvoudig mogelijke auto service te bieden aan onze klanten. Bij
            ServiceRight waarborgen we de garantie en bespaart u gegarandeerd in
            vergelijking met de hoofddealer prijzen. Ook wordt uw voertuig gratis
            opgehaald voor uw gewenste service en gratis weer terug gebracht naar
            uw huis of werkplek. U wordt tijdens elke stap door één van onze medewerkers
            op de hoogte gehouden.',
    ],
    'shift' => [
        'en' => 'Shift',
        'nl' => 'Dienst',
    ],
    'notes_for' => [
        'en' => 'Notes for',
        'nl' => 'Notities',
    ],
    'customer_address' => [
        'en' => 'Customer address',
        'nl' => 'Adres klant',
    ],
    'open_assignments' => [
        'en' => 'Open assignments',
        'nl' => 'Openstaande opdrachten',
    ],
    'check' => [
        'en' => 'Check',
        'nl' => 'Controleren',
    ],
    'assign' => [
        'en' => 'Assign',
        'nl' => 'Toewijzen',
    ],
    'edit' => [
        'en' => 'Edit',
        'nl' => 'Bewerken',
    ],
    'complete' => [
        'en' => 'COMPLETE',
        'nl' => 'AFRONDEN',
    ],
    'order_unlock_text' => [
        'en' => 'Order locked, only you can process this order. Click to unlock.',
        'nl' => 'Opdracht gelockt, alleen jij kunt deze opdracht verwerken. Klik om te unlocken.',
    ],
    'order_lock_text' => [
        'en' => 'Lock order, only you can process this order. Click to lock.',
        'nl' => 'Lock opdracht, alleen jij kunt deze opdracht verwerken. Klik om te locken.',
    ],
    'email_text_1' => [
        'en' => 'is the maintenance of your vehicle with the license plate',
        'nl' => 'is het onderhoud van uw voertuig met het kenteken',
    ],
    'email_text_2' => [
        'en' => 'maintained by ',
        'nl' => 'onderhouden door ',
    ],
    'email_text_3' => [
        'en' => 'We understand only too well that you would rather spend the amount of maintenance on your vehicle elsewhere. That is why you now have the chance to reclaim your entire invoice amount!
Click on the link below, you only have to fill in the amount to participate.',
        'nl' => 'Wij begrijpen maar al te goed dat u het bedrag van het onderhoud aan uw voertuig liever ergens anders aan besteed. Daarom maakt u nu kans om uw gehele factuurbedrag terug te winnen!
Klik op de onderstaande link, u hoeft enkel het bedrag in te vullen om deel te nemen.',
    ],
    'email_text_4' => [
        'en' => 'Win your invoice amount back!',
        'nl' => 'Win uw factuurbedrag terug!',
    ],
    'other_work' => [
        'en' => 'Other work for order ',
        'nl' => 'Overige werkzaamheden order ',
    ],
    'modal_text_1' => [
        'en' => 'For additional work you must first contact the customer via ',
        'nl' => 'Voor meerwerk moet u eerst contact opnemen met de klant via ',
    ],
    'modal_text_2' => [
        'en' => 'Other activities must be mentioned below ',
        'nl' => 'Overige werkzaamheden dienen hieronder vermeld te worden ',
    ],
    'copy_of_invoice' => [
        'en' => 'Copy of invoice',
        'nl' => 'Kopie van factuur',
    ],
    'no_file_choosen' => [
        'en' => 'No file chosen',
        'nl' => 'Geen bestand gekozen',
    ],
    'modal_text_3' => [
        'en' => 'Note: Account is missing. Invoice still has to be uploaded.',
        'nl' => 'Let Op: Rekening ontbreekt. Factuur dient nog te worden geupload.',
    ],
    'modal_text_4' => [
        'en' => 'And / or enter the description of the other activities',
        'nl' => 'En/of vul de omschrijving van de overige werkzaamheden in',
    ],
    'note' => [
        'en' => 'Note ',
        'nl' => 'Opmerking ',
    ],
    'modal_text_5' => [
        'en' => 'Costs for the other activities',
        'nl' => 'Kosten voor de overige werkzaamheden',
    ],
    'modal_text_6' => [
        'en' => 'Finish assignment',
        'nl' => 'Opdracht Afronden',
    ],
    'modal_text_7' => [
        'en' => 'The maintenance for the vehicle has been performed.',
        'nl' => 'Het onderhoud voor het volgende voertuig is verricht.',
    ],
    'confirm_agree' => [
        'en' => 'Confirm and agree ',
        'nl' => 'Bevesting en akkoord ',
    ],
    'cancelled_with_no_cancellation_cost' => [
        'en' => 'No Handling costs',
        'nl' => 'Geen Handelingskosten',
    ],
    'cancelled_garage_gets_cancellation_cost' => [
        'en' => 'Charges handling costs at the company',
        'nl' => 'Handelingskosten bij het bedrijf in rekening brengen',
    ],
    'cancelled_client_gets_cancellation_cost' => [
        'en' => 'Charges handling costs to the customer',
        'nl' => 'Handelingskosten bij de klant in rekening brengen',
    ],
];

generateFile($dictionary, basename(__FILE__));
