<?php

use App\Models\Country;
use App\Models\State;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(State::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->state,
        'country_id' => Country::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
