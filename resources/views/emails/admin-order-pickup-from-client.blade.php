@extends('emails.html-template')

@section('content')
Geachte #AANHEF# #NAAM#,<br/>
<br/>
Op #DAG# #DATUM# wordt uw onderhoud uitgevoerd bij #BEDRIJF_NAAM#.<br/>
Uw voertuig zal worden opgehaald tussen #TIJD# uur op #ADRES# #HUISNUMMER#.<br/>
<br/>
<table width="700"><tr>
	<td width="320"><img alt="logo servicepunt" src="#CFG_ROOT#/images/bedrijven/#BEDRIJF_IMAGE#.jpg"></td>
	<td width="380">
		Gegevens servicepunt:<br/>
		<h2 style="margin:0px;">#BEDRIJF_NAAM#</h2>
		#BEDRIJF_ADRES#<br/>
		#BEDRIJF_POSTCODE# #BEDRIJF_PLAATS#<br/>
		[E] <a href="mailto:#BEDRIJF_EMAIL#">#BEDRIJF_EMAIL#</a><br/>
		<b>[T] #BEDRIJF_TELEFOON#</b><br/>
	</td>
<tr></table>
<br/>
Werkzaamheden:<br/>
#DIENSTEN_LIJST#
-------------- +<br/>
<b>Totaal: &euro; #BEDRAG_TOTAAL# incl.BTW</b><br/>
<small>Dit bedrag kunt u contant aan de medewerker voldoen wanneer uw voertuig wordt afgeleverd.</small>
<br/><br/>
<small>U heeft het recht om uw onderhoudsservice op elk gewenst moment te annuleren. Zodra de onderhoudsservice niet binnen 24 uur voor de aanvang van de onderhoudsservice schriftelijk door u is geannuleerd, zijn wij genoodzaakt administratiekosten (a &euro;9,-) in rekening te brengen. (Zie artikel 7.1 uit onze algemene voorwaarden)
<br/><br/>
Indien u een andere garage wenst of niet akkoord gaat met onze algemene voorwaarden dan vernemen wij graag van u. Indien wij geen reactie van u mogen ontvangen hieromtrent gaan wij er vanuit dat u met zowel de garage als de voorwaarden akkoord gaat.
</small>
@endsection
