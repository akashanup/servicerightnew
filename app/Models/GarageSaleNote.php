<?php

namespace App\Models;

use App\Models\Garage;
use Illuminate\Database\Eloquent\Model;

class GarageSaleNote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sale_note', 'garage_id', 'by_user_id',
    ];

    /**
     * Get the garage of the note.
     */
    public function garage()
    {
        return $this->belongsTo(Garage::class);
    }
}
