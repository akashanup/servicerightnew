<?php

return [
"inactive_account" => "Your account is inactive. Please contact our support staff.",
"last_login" => "Last login",
"nickname" => "Nickname",
"active" => "Active",
"inactive" => "Inactive",
"admin" => "Admin",
"employee" => "Employee",
"password" => "Password",
"add_staff" => "Add staff",
];