<?php

namespace App\Utils;

use Lang;

class PickupType
{

    /**
     * Pickup types
     *
     * @param  string $type
     * @return array
     */
    public static function get($type = null)
    {
        $types = [
            'pickup_from_client' => Lang::get('employee-place-order.pickup_from_client'),
            'client_brings_itself' => Lang::get('employee-place-order.client_brings_itself'),
            'repair_at_client_location' => Lang::get('employee-place-order.repair_at_client_location'),
        ];

        return $type ? $types[$type] : $types;
    } // end get
} //end class
