<?php

namespace App\Http\Controllers;

use App\Models\Order;
use DB;

class OrderNoteController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        $json = [];
        $json['response'] = true;
        $json['notes'] = Order::find(request()->input('orderId'))->notesData();
        return response()->json($json);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        DB::beginTransaction();

        $validatedData = request()->validate(
            [
                'orderId' => 'required|numeric',
                'note' => 'required|string',
            ]
        );

        $order = Order::find(request()->input('orderId'));
        $note = $order->addNote($validatedData['note']);
        $json = [];
        $json['response'] = true;
        $json['notes'] = $order->notesData();
        DB::commit();
        return response()->json($json);
    }
}
