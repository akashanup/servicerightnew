<?php

namespace App\Models;

use App\Models\PostalCode;
use App\Models\State;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'state_id',
    ];

    /**
     * Get the state of the city.
     *
     * @return App\Models\State
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    } //end state()

    /**
     * Get the postal codes of the city.
     *
     * @return Collection
     */
    public function postalCodes()
    {
        return $this->hasMany(PostalCode::class);
    } //end postalCodes()

    /**
     * Create a city if its not found.
     *
     * @param integer $stateId
     * @param string $city
     * @return App\Models
     */
    public static function createUnique($stateId, $city)
    {
        $thisCity = static::where('name', $city)->where('state_id', $stateId)->first();
        if (!$thisCity) {
            $thisCity = static::create(
                [
                    'name' => $city,
                    'state_id' => $stateId,
                ]
            );
        }
        return $thisCity;
    } //end createUnique()
}
