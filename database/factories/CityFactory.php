<?php

use App\Models\City;
use App\Models\State;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(City::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'state_id' => State::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
