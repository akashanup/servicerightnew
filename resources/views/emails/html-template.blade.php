<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>E-mail</title>
		<!--add Hotmail fixes, background colour in Hotmail is specificed on .ExternalClass-->
		<style type="text/css">
			.ReadMsgBody {width: 100%;}
			.ExternalClass {width: 100%; background-color: rgb(48, 158, 212) !important;}
		</style>
	</head>
	<body width="100%">
		<center>
			<table cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td style="background: rgb(231, 231, 231) none repeat scroll 0%; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;">
							<table align="center" cellpadding="0" cellspacing="0" width="756">
								<tbody>
									<tr>
										<td>
											<img src="{{ asset('images/header_autos.jpg') }}" style="display:block;" alt="header">
										</td>
									</tr>
									<tr>
										<td style="padding: 20px; background-color: rgb(48, 158, 212); color:white; text-align: left; font-family: 'arial'; font-style: normal; font-variant: normal; font-weight: bold; font-size: 20px; line-height: normal; font-size-adjust: none; font-stretch: normal;">
											{{ $title }}
										</td>
									</tr>
									<tr>
										<td style="padding: 20px; background-color: rgb(255, 255, 255); color:rgb(70, 70, 70); vertical-align: top; text-align: left; font-family: 'arial'; font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: normal; font-size-adjust: none; font-stretch: normal;">
												@yield('content')
												<br/><br/>
												{{ __('globals.sincerely') }},
												<br/>
												{{ auth()->user() ? auth()->user()->name : 'ServiceRight' }}
												<br>
												{{ config('app.name', 'ServiceRight') }}
												<br>
												[W]
												<a href="{{ config('app.url') }}" target="_blank">
														{{ config('app.url') }}
												</a>
												<br/><br/>
												<small>
													{{ __('globals.terms_and_conditions') }}:
													<br/>
													<a href="https://www.serviceright-autos.nl/algemene_voorwaarden_bedrijven">
														{{ __('globals.companies') }}
													</a>,
													<a href="https://www.serviceright-autos.nl/voorwaarden">
														{{ __('globals.consumers') }}
													</a>
												</small>

										</td>
									</tr>
									<tr>
										<td style="background: rgb(231, 231, 231) none repeat scroll 0%; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;">
											&nbsp;
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</center>
	</body>
</html>
