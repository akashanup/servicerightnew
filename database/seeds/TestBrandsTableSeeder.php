<?php

use App\Models\Brand;
use Illuminate\Database\Seeder;

class TestBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Brand::class, 5)->create();
    }
}
