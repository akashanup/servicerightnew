<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            // Since it has enum columns so schema builder wont work.
            // DB::statement is a workaround for this.
            DB::statement('ALTER TABLE vehicles CHANGE model model VARCHAR(50)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            DB::statement('ALTER TABLE vehicles CHANGE model model VARCHAR(255)');
        });
    }
}
