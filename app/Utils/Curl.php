<?php

namespace App\Utils;

use App\Exceptions\CurlException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Lang;

class Curl
{
    const REQUEST_GET = 'GET';

    /**
     * Fetch Address from API.
     *
     * @param  string $postcode
     * @param  string $huisnummer
     * @param  string $site (optional)
     * @return array
     */
    public static function fetchAddress($postalCode, $houseNumber)
    {
        $response = [];
        $response['response'] = false;
        $response['message'] = Lang::get('globals.address_not_found');

        $errorMessages = json_decode(config('constants.NL_POSTAL_CODE_ERRORS'), true);

        $link = config('constants.NL_POSTAL_CODE') . $postalCode . "/" . $houseNumber;
        $client = new Client();
        $res = $client->request(static::REQUEST_GET, $link);
        $data = $res->getBody()->getContents();
        $state = null;
        $city = null;
        $address = null;

        if ($data) {
            $data = explode('<title>', $data);
            $data = explode('</title>', $data[1]);
            if (!in_array($data[0], $errorMessages)) {
                $state = explode('Provincie</dt><dd>', $data[1]);
                $state = explode('</dd>', $state[1]);
                $state = $state[0];

                $city = explode('Woonplaats</dt><dd>', $data[1]);
                $city = explode('</dd>', $city[1]);
                $city = $city[0];

                $address = explode('Straatnaam</dt><dd>', $data[1]);
                $address = explode('</dd>', $address[1]);
                $address = $address[0];

                // Throw an exception if state or city or address isn't found.
                if ((!$state && $city && $address)) {
                    throw new CurlException(Lang::get('globals.address_curl_exception'), 1);
                }

                // Fetch latitude and longitude.
                $latLang = static::fetchLatLang($postalCode);
                $response['latitude'] = $latLang['lat'];
                $response['longitude'] = $latLang['lng'];
                $response['complete_address'] = $address;
                $response['state'] = $state;
                $response['city'] = $city;
                $response['response'] = true;
                $response['message'] = null;
            } else {
                throw new CurlException(Lang::get('globals.address_curl_exception'), 1);
            }
        }

        return $response;
    } //end fetchAddress()

    /**
     * Fetch Vehicle from API.
     *
     * @param  string $licensePlateNumber
     * @return array
     */
    public static function fetchVehicle($licensePlateNumber)
    {
        $response = [];
        $response['response'] = false;
        $response['message'] = Lang::get('globals.vehicle_not_found');
        $link = $url = config('constants.NL_LICENSE_PLATE_NUMBER') . $licensePlateNumber;

        $client = new Client();
        $res = $client->request(static::REQUEST_GET, $link);
        $data = $res->getBody()->getContents();

        $brand = null;
        $fuel_type = null;
        $model = null;
        $build_year = null;
        $mandatory_service_expiry_date = null;

        if ($data && strrpos($data, 'Resultaat Kentekencheck')) {
            $brand = explode('Merk</td>', $data);
            $brand = array_key_exists('1', $brand) ? explode('</td>', $brand[1]) : null;
            $brand = ($brand && array_key_exists('1', $brand)) ? explode('>', $brand[1]) : null;
            $brand = ($brand && array_key_exists('1', $brand)) ? $brand[1] : null;

            $fuel_type = explode('Brandstof</td>', $data);
            $fuel_type = array_key_exists('1', $fuel_type) ? explode('</td>', $fuel_type[1]) : null;
            $fuel_type = ($fuel_type && array_key_exists('1', $fuel_type)) ? explode('>', $fuel_type[1]) : null;
            $fuel_type = ($fuel_type && array_key_exists('1', $fuel_type)) ? $fuel_type[1] : null;

            $model = explode('Model</td>', $data);
            $model = array_key_exists('1', $model) ? explode('</td>', $model[1]) : null;
            $model = ($model && array_key_exists('1', $model)) ? explode('>', $model[1]) : null;
            $model = ($model && array_key_exists('1', $model)) ? $model[1] : null;

            $build_year_start = explode('Datum eerste toelating</td>', $data);
            $build_year_start = array_key_exists('1', $build_year_start) ? explode('</td>', $build_year_start[1]) : null;
            $build_year_start = ($build_year_start && array_key_exists('1', $build_year_start)) ? explode('>', $build_year_start[1]) : null;
            $build_year_start = ($build_year_start && array_key_exists('1', $build_year_start)) ? $build_year_start[1] : null;

            $mandatory_service_expiry_date = explode('APK vervaldatum</td>', $data);
            $mandatory_service_expiry_date = array_key_exists('1', $mandatory_service_expiry_date)
            ? explode('</td>', $mandatory_service_expiry_date[1]) : null;
            $mandatory_service_expiry_date = ($mandatory_service_expiry_date
                && array_key_exists('1', $mandatory_service_expiry_date))
            ? explode('>', $mandatory_service_expiry_date[1]) : null;
            $mandatory_service_expiry_date = ($mandatory_service_expiry_date
                && array_key_exists('1', $mandatory_service_expiry_date)) ? $mandatory_service_expiry_date[1] : null;

            if (!($brand && $fuel_type && $model && $build_year_start)) {
                // Throw an exception if brand or fuel type or model number or build year or mandatory service expiry date isn't found.
                throw new CurlException(Lang::get('globals.license_number_plate_curl_exception'), 1);
            }

            $response['brand'] = $brand;
            $response['fuel_type'] = $fuel_type;
            $response['model'] = $model;
            $response['build_year_start'] = Carbon::createFromFormat('d/m/Y', $build_year_start);
            $response['mandatory_service_expiry_date'] = $mandatory_service_expiry_date ?
            Carbon::createFromFormat('d/m/Y', $mandatory_service_expiry_date) : null;
            $response['response'] = true;
            $response['message'] = null;
        } elseif (!($data && strrpos($data, 'Ongeldig kenteken'))) {
            // Throw an exception if either data isn't found or data doesn't have Ongeldig kenteken.
            throw new CurlException(Lang::get('globals.license_number_plate_curl_exception'), 1);
        }

        return $response;
    } //end fetchVehicle()

    /**
     * Fetch Latitude and Longitude
     *
     * @param  string $postalCode postalCode of a address
     * @return array
     */
    public static function fetchLatLang($postalCode)
    {
        $link = config('constants.GOOGLE_LAT_LONG') . $postalCode;
        $client = new Client();
        $res = $client->request(static::REQUEST_GET, $link);
        $data = $res->getBody()->getContents();
        $result = json_decode($data, true);
        return $result['results'][0]['geometry']['location'];
    }
} //end class
