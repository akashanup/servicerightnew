@extends('admins.content')

<!-- CSS -->
@section('admin-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('admin-content')
    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ __('employee-globals.company_management') }}</h2>
                    <span data-toggle="modal" data-target="#createGarageModal" style="float: left;">
                        <span data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Bewerk bedrijf" class="btn btn-sm btn-primary">{{ __('manage-garage.add_company') }}</span>
                    </span>
                    <span style="float: right;" id="showAllGarage">
                        <span class="btn btn-sm btn-primary">{{ __('globals.show_all') }}</span>
                    </span>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped" id="garageTable">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>ID</th>
                                <th>{{ __('manage-garage.company') }}</th>
                                <th>{{ __('employee-assign-order.location') }}</th>
                                <th>{{ __('manage-garage.membership') }}</th>
                                <th>&nbsp;</th>
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="createGarageModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('manage-garage.add_company') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="row">
                                @include('admins.garage-create-edit-modal-body')
                                <label>{{ __('manage-garage.services') }}:</label><br>
                                @foreach($parentCategories as $category)
                                    <div class="col-md-6">
                                        <input class="form-check-input garageCategory" type="checkbox" id="add_cat_{{ $category->id }}" value="{{ $category->id }}">
                                        <label class="form-check-label" for="add_cat_{{ $category->id }}">{{ $category->name }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <label>{{ __('manage-garage.affiliated_with') }}:</label><br>
                                @foreach($companies as $company)
                                    <div class="col-md-6">
                                        <input class="form-check-input garageCompany" type="checkbox" id="add_comp_{{ $company->id }}" value="{{ $company->id }}">
                                        <label class="form-check-label" for="add_comp_{{ $company->id }}">{{ $company->name }}</label>
                                    </div>
                                @endforeach
                                <br>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-primary" id="createGarageModalCreateBtn" data-route="{{ route('garages.store') }}">{{ __('globals.save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editGarageModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <label id="editGarageModalGarageName"></label>
                        {{ __('manage-garage.edit') }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            @include('admins.garage-create-edit-modal-body')
                            <div class="row">
                                <label>{{ __('manage-garage.services') }}:</label><br>
                                @foreach($parentCategories as $category)
                                    <div class="col-md-6">
                                        <input class="form-check-input garageCategory" type="checkbox" id="edit_cat_{{ $category->id }}" value="{{ $category->id }}">
                                        <label class="form-check-label" for="edit_cat_{{ $category->id }}">{{ $category->name }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <label>{{ __('manage-garage.affiliated_with') }}:</label><br>
                                @foreach($companies as $company)
                                    <div class="col-md-6">
                                        <input class="form-check-input garageCompany" type="checkbox" id="edit_comp_{{ $company->id }}" value="{{ $company->id }}">
                                        <label class="form-check-label" for="edit_comp_{{ $company->id }}">{{ $company->name }}</label>
                                    </div>
                                @endforeach
                                <br>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-primary" data-route="{{ route('garages.update') }}" id="editGarageModalSaveBtn">{{ __('globals.save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteGarageModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <label id="deleteGarageModalGarageName"></label>
                        {{ __('manage-garage.delete') }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>{{ __('manage-garage.modal_text_2') }}</p>
                            <p>{{ __('manage-garage.modal_text_6') }}</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-primary" id="deleteGarageModaldeleteBtn">{{ __('globals.delete') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="saleNotesGarageModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <label id="saleNotesGarageModalGarageName"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="saleNotesDiv">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control" id="saleNotesGarageModalNewSaleNote"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-primary" data-route="{{ route('garages.store-sale-notes') }}" id="saleNotesGarageModalSaveBtn">{{ __('globals.save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="statsModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <label id="statsModalGarageName"></label>
                        {{ __('manage-garage.statistics') }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>{{ __('globals.total') }}</strong><br>
                                {{ __('manage-garage.total_orders') }}:<br>
                                {{ __('manage-garage.accepted_orders') }}:<br>
                                {{ __('manage-garage.revenue') }}:<br>
                                {{ __('manage-garage.average_turnover') }}:
                            </p>

                            <p><strong>{{ __('globals.last_year') }}</strong><br>
                                {{ __('manage-garage.total_orders') }}:<br>
                                {{ __('manage-garage.accepted_orders') }}:<br>
                                {{ __('manage-garage.revenue') }}:<br>
                                {{ __('manage-garage.average_turnover') }}:
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p><br>
                                <span id="statsModalGarageTotalOrder"></span><br>
                                <span id="statsModalGarageAcceptedOrder"></span><br>
                                &euro; <span id="statsModalGarageRevenue"></span><br>
                                &euro; <span id="statsModalGarageTurnover"></span> (Per factuur)
                            </p>

                            <p><br>
                                <span id="statsModalGarageTotalOrderLastYear"></span><br>
                                <span id="statsModalGarageAcceptedOrderLastYear"></span><br>
                                &euro; <span id="statsModalGarageRevenueLastYear"></span><br>
                                &euro; <span id="statsModalGarageTurnoverLastYear"></span> (Per factuur)
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="demoGarageModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        {{ __('manage-garage.modal_text_4') }}
                        <label id="demoGarageModalGarageName"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>{{ __('manage-garage.modal_text_5') }}?</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-primary" data-route="{{ route('garages.send-demo') }}" id="demoGarageModalSendBtn">{{ __('globals.send') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="startMembershipModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        {{ __('manage-garage.create_password') }} - <label id="membershipModalGarageName"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>{{ __('manage-garage.modal_text_1') }}</p>
                        </div>
                        <label class="col-sm-4 col-form-label">{{ __('globals.price') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" id="startMembershipModalPriceInpt" class="form-control" disabled="disabled" value="{{ config('constants.GARAGE_MEMBERSHIP_PRICE') }}">
                        </div>
                        <label class="col-sm-4 col-form-label">{{ __('globals.discount') }}:</label>
                        <div class="col-sm-8">
                            <input  type="number" min="0.00" id="startMembershipModalDiscountInpt" step="0.10" class="form-control" placeholder="0.00">
                        </div>
                        <label class="col-sm-4 col-form-label">{{ __('manage-garage.new_ones') }} {{ __('globals.price') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" id="startMembershipModalFinalPriceInpt" class="form-control" disabled="disabled" value="{{ config('constants.GARAGE_MEMBERSHIP_PRICE') }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-primary" data-route="{{ route('garages.add-membership') }}" id="membershipModalCreatePasswordBtn" style="width: 50%">{{ __('manage-garage.create_password') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="extendMembershipModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        {{ __('manage-garage.membership') }}
                        <label class="extendMembershipModalGarageName"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                {{ __('manage-garage.membership') }}
                                <label id="extendMembershipModalGarageExpiryLbl"></label>
                            </p>
                        </div>
                        <label class="col-sm-12 col-form-label">{{ __('manage-garage.modal_text_3') }}:</label>
                        <div class="col-sm-12">
                            <input class="form-control datepicker" type="tel" id="extendMembershipModalGarageExpiryInpt">
                        </div>

                        <label class="col-sm-4 col-form-label">{{ __('globals.price') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" id="extendMembershipModalPriceInpt" class="form-control" disabled="disabled" value="{{ config('constants.GARAGE_MEMBERSHIP_PRICE') }}">
                        </div>
                        <label class="col-sm-4 col-form-label">{{ __('globals.discount') }}:</label>
                        <div class="col-sm-8">
                            <input  type="number" min="0.00" id="extendMembershipModalDiscountInpt" step="0.10" class="form-control" placeholder="0.00">
                        </div>
                        <label class="col-sm-4 col-form-label">{{ __('manage-garage.new_ones') }} {{ __('globals.price') }}:</label>
                        <div class="col-sm-8">
                            <input type="text" id="extendMembershipModalFinalPriceInpt" class="form-control" disabled="disabled" value="{{ config('constants.GARAGE_MEMBERSHIP_PRICE') }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-info extendMembershipModalDecisionBtn" data-route="{{ route('garages.update-membership') }}" data-type="cancel">{{ __('globals.deactivate') }}</button>
                            <button type="button" class="btn btn-primary extendMembershipModalDecisionBtn" data-route="{{ route('garages.update-membership') }}" data-type="renew">{{ __('globals.renew') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="erkenningModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('manage-garage.recognition') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control" id="garageLink">
                            </textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('globals.cancel') }}</button>
                            <button type="button" class="btn btn-info actionBtn" data-route="{{route('garages.recognition')}}" data-type="email">E-mail</button>
                            <button type="button" class="btn btn-primary actionBtn" data-type="sticker">Sticker</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="garageDataRoute" value="{{ route('garages.data') }}">
    <!-- Scripts -->
    @section('admin-scripts')
        <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvQz45wQ5liYSUJ8HyAajVglYE0S_XlrM&libraries=geometry"></script>
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/custom/manage-garage.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
