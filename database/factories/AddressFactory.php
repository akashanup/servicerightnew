<?php

use App\Models\Address;
use App\Models\PostalCode;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Address::class, function (Faker $faker) {
    $type = ['client', 'garage'];
    return [
        'postal_code_id' => PostalCode::inRandomOrder()->first()->id,
        'house_number' => $faker->buildingNumber,
        'type' => $type[rand(0, 1)],
        'complete_address' => $faker->streetAddress . ',' . $faker->streetName,
        'latitude' => $faker->latitude(-90, 90),
        'longitude' => $faker->longitude(-180, 180),
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
