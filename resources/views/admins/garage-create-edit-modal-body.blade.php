<label class="col-sm-4 col-form-label">{{ __('manage-garage.company') }}</label>
<div class="col-sm-8">
    <input type="text" class="form-control required garageName">
</div>
<label class="col-sm-4 col-form-label">{{ __('manage-garage.contact_person') }}</label>
<div class="col-sm-8">
    <input type="text" class="form-control required garageContactPerson">
</div>
<label class="col-sm-4 col-form-label">Moneybird ID</label>
<div class="col-sm-8">
    <input type="text" class="form-control garageMoneybirdId">
</div>
<label class="col-sm-4 col-form-label">{{ __('employee-place-order.phone_number') }}</label>
<div class="col-sm-8">
    <input type="text" class="form-control required garagePhone">
</div>
<label class="col-sm-4 col-form-label">E-mail</label>
<div class="col-sm-8">
    <input type="text" class="form-control required garageEmail">
</div>
<label class="col-sm-4 col-form-label">Website</label>
<div class="col-sm-8">
    <input type="text" class="form-control required garageWebsite">
</div>
<label class="col-sm-4 col-form-label">{{ __('employee-place-order.address') }}</label>
<div class="col-sm-8">
    <input type="text" class="form-control required garageCompleteAddress">
</div>
<label class="col-sm-4 col-form-label">Postcode</label>
<div class="col-sm-8">
    <input type="text" class="form-control required garagePostalCode">
</div>
<label class="col-sm-4 col-form-label">{{ __('employee-place-order.city') }}</label>
<div class="col-sm-8">
    <input type="text" class="form-control required garageCity">
</div>
<label class="col-sm-4 col-form-label">Type</label>
<div class="col-sm-8">
    <select class="form-control garageType">
        @foreach($garageTypes as $type)
            <option value="{{ $type->type }}">{{ $type->type }}</option>
        @endforeach
    </select>
</div>
