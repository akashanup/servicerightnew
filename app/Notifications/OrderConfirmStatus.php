<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class OrderConfirmStatus extends Notification
{
    use Queueable;

    /**
     * The user's salutation.
     *
     * @var string
     */
    public $salutation;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * The vehicle's brand.
     *
     * @var string
     */
    public $brand;

    /**
     * The vehicle's model number.
     *
     * @var string
     */
    public $modelNumber;

    /**
     * The vehicle's licensePlateNumber.
     *
     * @var string
     */
    public $licensePlateNumber;

    /**
     * The order confirmation url.
     *
     * @var string
     */
    public $confirmOrderUrl;

    /**
     * The user's phone.
     *
     * @var array
     */
    public $phone;

    /**
     * The user's address.
     *
     * @var array
     */
    public $address;

    /**
     * The user's houseNumber.
     *
     * @var array
     */
    public $houseNumber;

    /**
     * The user's email.
     *
     * @var array
     */
    public $email;

    /**
     * The user's postalCode.
     *
     * @var array
     */
    public $postalCode;

    /**
     * The user's city.
     *
     * @var array
     */
    public $city;

    /**
     * The order's pickup date.
     *
     * @var array
     */
    public $pickUpDate;

    /**
     * The order's pickup time.
     *
     * @var array
     */
    public $pickUpTime;

    /**
     * The order's additional work.
     *
     * @var array
     */
    public $additionalWork;

    /**
     * File.
     *
     * @var string
     */
    public $file;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        $salutation,
        $name,
        $brand,
        $modelNumber,
        $licensePlateNumber,
        $confirmOrderUrl,
        $phone,
        $email,
        $address,
        $houseNumber,
        $postalCode,
        $city,
        $pickUpDate,
        $pickUpTime,
        $additionalWork,
        $file = null
    ) {
        $this->salutation = $salutation;
        $this->name = $name;
        $this->brand = $brand;
        $this->modelNumber = $modelNumber;
        $this->licensePlateNumber = $licensePlateNumber;
        $this->confirmOrderUrl = $confirmOrderUrl;
        $this->phone = $phone;
        $this->email = $email;
        $this->address = $address;
        $this->houseNumber = $houseNumber;
        $this->postalCode = $postalCode;
        $this->city = $city;
        $this->pickUpDate = $pickUpDate;
        $this->pickUpTime = $pickUpTime;
        $this->additionalWork = $additionalWork;
        $this->file = $file;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)->view(
            'emails.order-confirm-status',
            [
                'title' => Lang::get('employee-place-order.order_status_email_title'),
                'salutation' => $this->salutation,
                'name' => $this->name,
                'brand' => $this->brand,
                'modelNumber' => $this->modelNumber,
                'licensePlateNumber' => $this->licensePlateNumber,
                'confirmOrderUrl' => $this->confirmOrderUrl,
                'phone' => $this->phone,
                'email' => $this->email,
                'address' => $this->address,
                'houseNumber' => $this->houseNumber,
                'postalCode' => $this->postalCode,
                'city' => $this->city,
                'pickUpDate' => $this->pickUpDate,
                'pickUpTime' => $this->pickUpTime,
                'additionalWork' => $this->additionalWork,
            ]
        );

        if ($this->file) {
            $mail->attach($this->file);
        }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
