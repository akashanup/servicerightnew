@extends('employees.content')

@section('employee-content')
    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Order #{{ $order->id }} {{ __('employee-assign-order.assign_to_garage') }}</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row" style="margin-bottom: 2em;">
                        <div class="col-md-8">
                            <div id="country_canvas" style="width: 100%; height: 500px;">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3>Filters</h3>
                            <div class="row">
                                <h4>{{ __('employee-assign-order.company_type') }}:</h4>
                                @foreach($parentCategories as $category)
                                    <div class="col-md-6">
                                        <input class="form-check-input categoryCheckboxInpt" type="checkbox" id="{{ $category->id }}" value="{{ $category->name }}">
                                        <label class="form-check-label" for="{{ $category->id }}">{{ $category->name }}</label>
                                    </div>
                                @endforeach
                                <br>
                            </div>
                            <div class="row">
                                <h4>{{ __('employee-assign-order.members') }}:</h4>
                                <div class="col-md-12">
                                    <input class="form-check-input" type="checkbox" id="members_only"><label class="form-check-label" for="members_only"> {{ __('employee-assign-order.show_only_members') }}</label><br>
                                </div>
                            </div>
                            <div class="row">
                                <h4>Type:</h4>
                                @foreach($garageTypes as $type)
                                    <div class="col-md-12">
                                        <input class="form-check-input typeCheckboxInpt" id="{{ $type->type }}" type="checkbox" value="{{ $type->type }}">
                                        <label class="form-check-label" for="{{ $type->type }}">{{ $type->type }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <h4>Legenda:</h4>
                                <div class="row">
                                    <div style="float: left; padding: 10px; text-align: center;"><img src="{{ asset('images/icons/ok_grey.gif') }}"><br>Geen {{ __('employee-assign-order.lid') }}</div>
                                    <div style="float: left; padding: 10px; text-align: center;"><img src="{{ asset('images/icons/ok.gif') }}"><br>{{ __('employee-assign-order.members') }}</div>
                                    <div style="float: left; padding: 10px; text-align: center;"><img src="{{ asset('images/icons/ok_blue.gif') }}"><br>{{ __('employee-assign-order.members') }} ({{ __('employee-assign-order.payment_not_met') }})</div>
                                </div>
                                <div class="row">
                                    <div style="float: left; padding: 10px; text-align: center;"><img src="{{ asset('images/icons/ok_yellow.gif') }}"><br>{{ __('employee-assign-order.members') }} ({{ __('employee-assign-order.with_priority') }})</div>
                                    <div style="float: left; padding: 10px; text-align: center;"><img src="{{ asset('images/icons/ok_red.gif') }}"><br>{{ __('employee-assign-order.members') }} ({{ __('employee-assign-order.not_happy') }})</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>{{ __('employee-assign-order.vehicle_details') }}:</h3>
                            <p><strong>{{ __('employee-assign-order.car') }}:</strong> {{$vehicle->brand->name}} {{$vehicle->model}} ({{$vehicle->fuel_type}}) {{ __('employee-assign-order.from') }} {{ $buildYearStart }}<br>
                            <strong>{{ __('employee-place-order.license_plate') }}:</strong> {{ $licensePlateNumber->license_plate_number }}<br>
                            <strong>APK verloopt:</strong> {{ $mandatoryServiceExpiryDate }}</p>

                            <p><strong>{{ __('employee-assign-order.preferred_date') }}:</strong> {{ $pickupDate }}<br>
                            <strong>{{ __('employee-place-order.alternative') }} {{ __('employee-place-order.date') }}:</strong> {{ $alternativePickupDate }}<br>
                            <strong>{{ __('employee-place-order.time') }}:</strong> {{ $order->pickup_time }}</p>
                            <p><strong>{{ __('employee-place-order.pick_up_bring') }}:</strong> {{ $pickupType }}</p>

                            <p><strong>{{ __('employee-place-order.comments_additional_work') }}:</strong> {{ $order->additional_work }}</p>
                            <p><strong>{{ __('employee-assign-order.customer') }}:</strong> {{$user->salutation}} {{$user->name}}, {{$user->phone}}, {{$user->email}}<br>
                            <strong>{{ __('employee-assign-order.location') }}:</strong> {{$address->complete_address}}, {{$postalCode->postal_code}}, {{$postalCode->city->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <h3>{{ __('employee-assign-order.order_details') }}:</h3>
                            <table class="table table-responsive table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>Services</th>
                                        <th>{{ __('employee-globals.prices') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order->services as $service)
                                        <tr>
                                            <td>{{ $service->name }}</td>
                                            <td>{{ $service->pivot->price }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <h3>{{ __('employee-assign-order.internal_notes') }}:</h3>
                            <div class="row">
                                <div class="container" id="notesDiv">
                                    @foreach($notesData as $note)
                                        <div class="col-md-12 note">
                                            <p>{{ $note['note'] }}</p>
                                            <p class="noteUser">{{ $note['createdBy'] }} | {{ $note['createdAt'] }}</p>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-12">
                                    <textarea class="form-control" id="noteInpt"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary" id="saveNoteBtn">{{ __('employee-assign-order.add_note') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="orderModal" role="dialog" aria-hidden="true">
        <input type="hidden" value="" id="garageIdInpt">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Order #{{ $order->id }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="garageModalDiv">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-block btn-danger updateGarageOrderBtn" data-status="{{ \App\Models\Order::REJECTED_BY_GARAGE }}" data-dismiss="modal" id="rejectOrderBtn">{{ __('globals.rejected') }}</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-block btn-success updateGarageOrderBtn"  data-status="{{ \App\Models\Order::ACCEPTED_BY_GARAGE }}" id="acceptOrderBtn">{{ __('globals.accepted') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="{{ $currentDate }}" id="currentDate">
    <span id="modalOrderId" style="display: none">{{ $order->id }}</span>
    <input type="hidden" value="{{ route('orderNotes.store') }}" id="storeNoteRoute">
    <input type="hidden" value="{{ asset('images/icons/') }}" id="imageLoc">
    <input type="hidden" value="{{ $address->latitude }}" id="clientLatitude">
    <input type="hidden" value="{{ $address->longitude }}" id="clientLongitude">
    <input type="hidden" value="{{ route('garages.storeOrder') }}" id="garageOrderRoute">
    <input type="hidden" value="{{ route('garages.storeNotes') }}" id="garageNotesRoute">
    <input type="hidden" value="{{ route('garages.updateCategories') }}" id="garageCategoryRoute">
    <input type="hidden" value="{{ route('garages.fetchAll') }}" id="garageFetchAll">
    <input type="hidden" value="{{ $allCategories }}" id="allCategories">
    <!-- Scripts -->
    @section('employee-scripts')
        <script type="text/javascript" src="{{ asset('js/custom/order-note.js').'?v='.config('app.version', '1') }}"></script>
        <script src="{{ asset('js/custom/google-initmap.js').'?v='.config('app.version', '1') }}"></script>
        </script>
        <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvQz45wQ5liYSUJ8HyAajVglYE0S_XlrM&callback=initMap&libraries=geometry"></script>
        <script src="{{ asset('js/custom/employee-order-assign.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
