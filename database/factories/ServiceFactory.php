<?php

use App\Models\Category;
use App\Models\Service;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Service::class, function (Faker $faker) {
    return [
        'category_id' => Category::inRandomOrder()->first()->id,
        'name' => $faker->word,
        'code' => $faker->unique()->word,
        'slug' => $faker->word,
        'description' => $faker->text,
    ];
});
