<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use DB;
use Illuminate\Validation\Rule;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $type type of feedback
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        return view('admins.feedback', ['type' => $type]);
    }

    /**
     * Listing of new feedback based on type.
     *
     * @param string $type type of feedback
     * @param integer $draw pagination factor
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData($type, $draw = 1)
    {
        $feedbacks = Feedback::feedbacksData($type, $draw);
        return response()->json(
            [
                'response' => true,
                'feedback' => view(
                    'admins.feedback-data',
                    [
                        'draw' => $draw,
                        'feedbackCount' => $feedbacks['total'],
                        'feedbacks' => $feedbacks['feedbacks'],
                    ]
                )->render(),
            ]
        );
    }

    /**
     * Update a feedback.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'feedbackId' => 'required|numeric',
                'type' => ['required', Rule::in([Feedback::ASSIGN, Feedback::REJECT])],
                'socialMediaPost' => ['required', Rule::in([true, false])],
            ]
        );

        $feedback = Feedback::find(request('feedbackId'));
        $json = $feedback->updateStatus($validatedData);
        DB::commit();
        return response()->json($json);
    }
}
