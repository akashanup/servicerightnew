<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->renameColumn('company', 'name');
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->string('name', 50)->change();
            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropUnique('companies_name_unique');
            $table->string('name', 255)->change();
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->renameColumn('name', 'company');
        });
    }
}
