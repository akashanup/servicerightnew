<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\LicensePlateNumber;
use App\Models\Service;
use App\Models\Vehicle;
use Carbon\Carbon;
use DB;
use Lang;

class VehicleController extends Controller
{
    /**
     * Search vehicle based on brand or modal or license plate number.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vehicle::zeroservicePrice();
        return view(
            'admins.search-vehicles',
            [
                'vehicles' => $vehicles,
                'vehiclesCount' => sizeOf($vehicles),
                'brands' => Brand::all(),
            ]
        );
    }

    /**
     * Fetch all variants of a vehicle.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetch()
    {
        $validatedData = request()->validate(['brandId' => 'required|numeric']);

        $vehicles = Vehicle::where('brand_id', $validatedData['brandId'])->whereNull('parent_id')
            ->orderBy('build_year_start', 'DSC')->get();
        return response()->json(
            [
                'response' => true,
                'data' => view(
                    'admins.vehicle-model-data',
                    ['vehicles' => $vehicles])->render(),
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param integer $vehicle vehicle id
     * @param integer $licensePlateNumber vehicle's licensePlateNumber
     * @return \Illuminate\Http\Response
     */
    public function prices($vehicle, $licensePlateNumber = null)
    {
        return view('admins.vehicle-price', ['vehicle' => $vehicle, 'licensePlateNumber' => $licensePlateNumber]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param integer $vehicle vehicle id
     * @param integer $licensePlateNumber vehicle's licensePlateNumber
     * @return \Illuminate\Http\JsonResponse
     */
    public function pricesData($vehicle, $licensePlateNumber = null)
    {
        $json = [];
        if ($baseVehicle = Vehicle::getBaseModel($vehicle, $licensePlateNumber)) {
            $vehicles = Vehicle::where('parent_id', $baseVehicle->id)->where('fuel_type', Vehicle::BENZINE)
                ->orderBy('build_year_start', 'DSC')->get();
            $childVehicles = [];
            foreach ($vehicles as $vehicle) {
                $childVehicles[$vehicle->id]['brandName'] = $vehicle->brand->name;
                $childVehicles[$vehicle->id]['model'] = $vehicle->model;
                $childVehicles[$vehicle->id]['buildYearStart'] = Carbon::parse($vehicle->build_year_start)->year;
                $childVehicles[$vehicle->id]['buildYearEnd'] = $vehicle->build_year_end
                ? Carbon::parse($vehicle->build_year_end)->year
                : '';
                $childVehicles[$vehicle->id]['servicePrices'] = $vehicle->getServicePrices();
                $childVehicles[$vehicle->id]['waterpumpRequired'] = $vehicle->waterpump_required;
                $childVehicles[$vehicle->id]['id'] = $vehicle->id;
            }

            $baseModelServicePrices = $baseVehicle->getServicePrices();

            $json['data'] = view(
                'admins.vehicle-price-data',
                [
                    'baseVehicle' => $baseVehicle,
                    'childVehicles' => $childVehicles,
                    'baseModelServicePrices' => $baseModelServicePrices,
                ]
            )->render();
            $json['response'] = true;
        } else {
            $json['response'] = false;
            $json['redirect'] = route('vehicles.index');
        }
        return response()->json($json);
    }

    /**
     * Update service price of the vehicle(s).
     *
     * @return \Illuminate\Http\Response
     */
    public function updateServices()
    {
        DB::beginTransaction();
        $validatedData = request()->validate(
            [
                'vehicleId' => 'required|numeric',
                'service' => 'required|string',
            ]
        );
        $vehicle = Vehicle::find($validatedData['vehicleId']);
        $vehicle ? $vehicle->updateServices(json_decode($validatedData['service'], true))
        : $this->sendResponse(Lang::get('globals.vehicle_not_found'));

        DB::commit();
        return response()->json(['response' => true]);
    }
}
