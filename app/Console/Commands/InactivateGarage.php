<?php

namespace App\Console\Commands;

use App\Models\Garage;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InactivateGarage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'garage:inactivate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for the garages whose membership is expired.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Garage::where('membership_status', Garage::ACTIVE)
            ->where('member_expiry', '<', Carbon::now()->toDateString())
            ->update(['membership_status' => Garage::INACTIVE]);
    }
}
