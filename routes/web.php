<?php

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
], function () {
    Route::get('/', function () {
        return redirect()->route('login'); // view('employees.place_order');
    })->name('home');

    Auth::routes();

    Route::group(['middleware' => ['auth']], function () {
        Route::group(['middleware' => ['employee']], function () {
            Route::group(['middleware' => ['ajaxRequest']], function () {
                Route::get('/addresses/client', 'AddressController@client')->name('addresses.client');

                Route::get('license-plate-numbers/vehicle', 'LicensePlateNumberController@vehicle')
                    ->name('licensePlateNumbers.vehicle');

                Route::post('/garages/storeOrder', 'GarageController@storeOrder')->name('garages.storeOrder');
                Route::post('/garages/storeNotes', 'GarageController@storeNotes')->name('garages.storeNotes');
                Route::get('/garages/fetchAll', 'GarageController@fetchAll')->name('garages.fetchAll');
                Route::post('/garages/updateCategories', 'GarageController@updateCategories')
                    ->name('garages.updateCategories');

                Route::post('/orders/store', 'OrderController@store')->name('orders.store');
                Route::get('/orders/data/{status}/{orderId?}', 'OrderController@statusData')
                    ->name('orders.data');
                Route::post('/orders/updatePrice', 'OrderController@updatePrice')->name('orders.updatePrice');
                Route::post('/orders/complete', 'OrderController@complete')->name('orders.complete');
            });

            Route::get('/employees/dashboard', 'EmployeeController@dashboard')->name('employees.dashboard');
            Route::post('/employees/adminLogin', 'EmployeeController@adminLogin')->name('employees.adminLogin');

            Route::get('/orders/create', 'OrderController@create')->name('orders.create');
            Route::get('/orders/statuses/{status}', 'OrderController@status')->name('orders.statuses');

            Route::group(['middleware' => ['verifyOrderNotes']], function () {
                Route::get('/order-notes/show', 'OrderNoteController@show')->name('orderNotes.show');
                Route::post('/order-notes/store', 'OrderNoteController@store')->name('orderNotes.store');
            });

            Route::group(['middleware' => ['verifyLockOrder']], function () {
                Route::get('/orders/edit/{id}', 'OrderController@edit')->name('orders.edit');
                Route::post('/orders/update/{id}', 'OrderController@update')->name('orders.update');
                Route::post('/orders/lock/{id}', 'OrderController@lock')->name('orders.lock');
                Route::get('/orders/assign/{id}', 'OrderController@assign')->name('orders.assign');
                Route::post('/orders/completeRequest/{id}', 'OrderController@completeRequest')
                    ->name('orders.completeRequest');
            });
        });

        Route::group(['middleware' => ['admin']], function () {
            Route::group(['middleware' => ['ajaxRequest']], function () {
                Route::post('/garages/store', 'GarageController@store')->name('garages.store');
                Route::post('/garages/delete/{garageId}', 'GarageController@delete')->name('garages.delete');
                Route::post('/garages/update', 'GarageController@update')->name('garages.update');
                Route::get('/garages/sale-notes/{garageId}', 'GarageController@saleNotes')
                    ->name('garages.sale-notes');
                Route::post('/garages/store-sale-note', 'GarageController@storeSaleNote')
                    ->name('garages.store-sale-notes');
                Route::get('/garages/stats/{garageId}', 'GarageController@stats')->name('garages.stats');
                Route::post('/garages/send-demo', 'GarageController@sendDemo')->name('garages.send-demo');
                Route::post('/garages/add-membership', 'GarageController@addMembership')
                    ->name('garages.add-membership');
                Route::post('/garages/update-membership', 'GarageController@updateMembership')
                    ->name('garages.update-membership');
                Route::post('/garages/recognition', 'GarageController@recognition')
                    ->name('garages.recognition');

                Route::post('/feedbacks/update', 'FeedbackController@update')->name('feedbacks.update');

                Route::post('/employees/update', 'EmployeeController@update')->name('employees.update');
                Route::post('/employees/store', 'EmployeeController@store')->name('employees.store');

                Route::post('/vehicles/fetch', 'VehicleController@fetch')->name('vehicles.fetch');
            });

            Route::get('/employees', 'EmployeeController@index')->name('employees.index');
            Route::get('/employees/indexData', 'EmployeeController@indexData')->name('employees.indexData');
            Route::post('/employees/login', 'EmployeeController@login')->name('employees.login');
            Route::get('/employees/show/{id}', 'EmployeeController@show')->name('employees.show');

            Route::get('/feedbacks/{type}', 'FeedbackController@index')->name('feedbacks.index');
            Route::get('/feedbacks-data/{type}/{draw?}', 'FeedbackController@indexData')
                ->name('feedbacks.indexData');

            Route::get('/garages/manage', 'GarageController@manage')->name('garages.manage');
            Route::get('/garages/data/{garageId?}', 'GarageController@data')->name('garages.data');
            Route::get('/garages/edit/{garageId}', 'GarageController@edit')->name('garages.edit');

            Route::get('/vehicles/', 'VehicleController@index')->name('vehicles.index');
            Route::get('/vehicles/prices/{vehicle}/{licensePlateNumber?}', 'VehicleController@prices')
                ->name('vehicles.prices');
            Route::get('/vehicles/prices-data/{vehicle}/{licensePlateNumber?}', 'VehicleController@pricesData')
                ->name('vehicles.prices-data');
            Route::post('/vehicles/services/update', 'VehicleController@updateServices')
                ->name('vehicles.update-services');
        });
    });

    Route::group(['middleware' => ['verifyQuotedOrder']], function () {
        Route::get('/clients/orders/{token}/phone', 'ClientController@phone')->name('clients.phone');
        Route::post('/clients/verify/{token}', 'ClientController@verify')->name('clients.verify');
        Route::get('/clients/view-quoted-orders/{token}', 'ClientController@viewQuotedOrder')
            ->name('clients.viewQuotedOrder');
        Route::post('/clients/confirm-quoted-orders/{token}', 'ClientController@confirmQuotedOrder')
            ->name('clients.confirmQuotedOrder');
    });

    Route::get('/garages/activate-membership/{token}', 'GarageController@activateMembership')->name('garages.activate-membership');
});
