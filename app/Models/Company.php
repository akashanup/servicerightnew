<?php

namespace App\Models;

use App\Models\Garage;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the garages for the category.
     */
    public function garages()
    {
        return $this->belongsToMany(Garage::class, 'garage_companies', 'company_id', 'garage_id')
            ->withTimestamps();
    } //end garages()
}
