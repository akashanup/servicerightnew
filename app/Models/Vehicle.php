<?php

namespace App\Models;

use App\Models\Brand;
use App\Models\LicensePlateNumbers;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'brand_id', 'fuel_type', 'model', 'image', 'services', 'build_year_start', 'build_year_end',
        'parent_id', 'waterpump_required', 'diesel_percentage',
    ];

    const BENZINE = 'Benzine';
    const DIESEL = 'Diesel';
    const YES = 'yes';

    /**
     * Format build_year_start from yyyy-mm-dd to dd-mm-yyyy
     *
     * @return date $build_year_start
     */
    public function build_year_start()
    {
        return Carbon::parse($this->build_year_start)->format('d-m-Y');
    }

    /**
     * Format build_year_end from yyyy-mm-dd to dd-mm-yyyy
     *
     * @return date $build_year_end
     */
    public function build_year_end()
    {
        return Carbon::parse($this->build_year_end)->format('d-m-Y');
    }

    /**
     * Get the brand of the vehicle.
     *
     * @return App\Models\Brand
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    } //end brand()

    /**
     * Get the licensePlateNumbers of the vehicle.
     *
     * @return Collection
     */
    public function licensePlateNumbers()
    {
        return $this->hasMany(LicensePlateNumbers::class);
    } //end licensePlateNumbers()

    /**
     * Get the servicePrice of a service for the vehicle.
     *
     * @param integer $serviceId
     * @return string
     */
    public function servicePrice($serviceId)
    {
        $services = json_decode($this->services, true);
        return $services[$serviceId];
    } //end servicePrice()

    /**
     * Update the servicePrice of a service for the vehicle.
     *
     * @param array $serviceIds service ids with their price of a vehicle.
     * @param boolean zeroPrice whether the price of a service can be zero or not.
     * @return void
     */
    public function updateServices($serviceIds, $zeroPrice = false)
    {
        // Check whether it is a base model. If yes then update price of its variants also.
        if (!$this->parent_id) {
            $tmpBaseServices = json_decode($this->services, true);
            $baseServices = $tmpBaseServices;
            $variants = static::where('parent_id', $this->id)->get();
            $dieselPercentage = json_decode($this->diesel_percentage, true);
            foreach ($variants as $key => $variant) {
                $variantServices = json_decode($variant->services, true);
                foreach ($serviceIds as $serviceId) {
                    $variantServicePrice = $variantServices[$serviceId['serviceId']];
                    $baseServicePrice = $tmpBaseServices[$serviceId['serviceId']];
                    // Update base service price.
                    $baseServices[$serviceId['serviceId']] = $serviceId['price'];
                    // If the variant is of DIESEL type then deduct the diesel_percentage
                    if ($variant->fuel_type == static::DIESEL) {
                        $baseServicePrice = $baseServicePrice * (1 + ($dieselPercentage[$serviceId['serviceId']] / 100));
                    }

                    // Update diesel percentage for this service.
                    array_key_exists('dieselPercentage', $serviceId)
                    ? ($dieselPercentage[$serviceId['serviceId']] = $serviceId['dieselPercentage'])
                    : ($serviceId['dieselPercentage'] = $dieselPercentage[$serviceId['serviceId']]);

                    // If the initial price is same then update the price.
                    if ($variantServicePrice == $baseServicePrice) {
                        // If the variant is of DIESEL type then add the diesel_percentage
                        if ($variant->fuel_type == static::DIESEL) {
                            $serviceId['price'] = $serviceId['price'] * (1 + ($serviceId['dieselPercentage'] / 100));
                        }
                        $variantServices[$serviceId['serviceId']] = $serviceId['price'];
                    }
                }
                $variant->update(['services' => json_encode($variantServices)]);
            }
            $this->update(['diesel_percentage' => json_encode($dieselPercentage)]);
            $this->update(['services' => json_encode($baseServices)]);
        } else {
            $baseVehicle = static::find($this->parent_id);
            $baseServices = json_decode($baseVehicle->services, true);
            $services = json_decode($this->services, true);
            foreach ($serviceIds as $serviceId) {
                $price = $serviceId['price'];
                $price = ($price || $zeroPrice) ? $price : $baseServices[$serviceId['serviceId']];
                $services[$serviceId['serviceId']] = $price;
            }
            $this->update(['services' => json_encode($services)]);
        }
    } //end updateServices()

    /**
     * Create a vehicle if its not found.
     *
     * @param integer $brandId
     * @param string $fuelType
     * @param string $model
     * @param string $buildYearStart
     * @return App\Models
     */
    public static function createUnique($brandId, $fuelType, $model, $buildYearStart)
    {
        $thisVehicle = static::where('brand_id', $brandId)->where('model', $model)->where('fuel_type', $fuelType)
            ->where('build_year_start', $buildYearStart)->whereNotNull('parent_id')->first();

        if (!$thisVehicle) {
            // Make services for this vehicle based on similar kind of vehicle.
            $services = null;
            $parentId = null;
            // Find similar model.
            $baseVehicle = static::where('brand_id', $brandId)->where('model', $model)->whereNotNull('parent_id')
                ->where('fuel_type', $fuelType)->first();
            if ($baseVehicle) {
                $parentId = $baseVehicle->parent_id;
            } else {
                // Find base model.
                $baseVehicle = static::where('brand_id', $brandId)->where('model', $model)->whereNull('parent_id')->first();
                if (!$baseVehicle) {
                    // Initialize all service with 0 price.
                    foreach (Service::all() as $service) {
                        $services[$service->id] = '0.00';
                    }

                    // Create new base vehicle.
                    $baseVehicle = static::create(
                        [
                            'brand_id' => $brandId,
                            'fuel_type' => static::BENZINE,
                            'model' => $model,
                            'services' => json_encode($services),
                            'diesel_percentage' => json_encode(config('constants.DEFAULT_DIESEL_PERCENTAGE')),
                        ]
                    );
                }
                $parentId = $baseVehicle->id;
            }

            $thisVehicle = static::create(
                [
                    'brand_id' => $brandId,
                    'fuel_type' => $fuelType,
                    'model' => $model,
                    'services' => $baseVehicle->services,
                    'build_year_start' => $buildYearStart,
                    'build_year_end' => Carbon::parse($buildYearStart)->addYear(),
                    'parent_id' => $parentId,
                ]
            );
        }

        return $thisVehicle;
    } //end createUnique()

    /**
     * Get the vehicles whose price for each service is 0.
     *
     * @return array
     */
    public static function zeroservicePrice()
    {
        $vehicles = [];
        foreach (static::all() as $vehicle) {
            $servicePrice = json_decode($vehicle->services, true);
            if (!array_filter($servicePrice)) {
                // Include only the base models
                $parentId = $vehicle->parent_id;
                $key = $parentId ? $parentId : $vehicle->id;
                $vehicles[$key] = static::find($key);
            }
        }
        return $vehicles;
    } //end zeroservicePrice()

    /**
     * Get the base model of vehicle.
     *
     * @param integer $vehicle vehicle id
     * @param integer $licensePlateNumber vehicle's licensePlateNumber
     * @return App\Models\Vehicle
     */
    public static function getBaseModel($vehicle, $licensePlateNumber = null)
    {
        $baseVehicle = null;
        if ($licensePlateNumber) {
            $licensePlateNumber = LicensePlateNumber::where('license_plate_number', $licensePlateNumber)->first();
            if ($licensePlateNumber) {
                $vehicle = Vehicle::find($licensePlateNumber->vehicle_id);
                $baseVehicle = Vehicle::find($vehicle->parent_id);
            }
        } else {
            $baseVehicle = Vehicle::find($vehicle);
            // Check whether it is a base model or not. If not then find the base model.
            if ($baseVehicle && $baseVehicle->parent_id) {
                $baseVehicle = Vehicle::find($baseVehicle->parent_id);
            }
        }
        return $baseVehicle;
    } //end getBaseModel()

    /**
     * Get the price of services of vehicle.
     *
     * @return array
     */
    public function getServicePrices()
    {
        $servicePrices = [];
        $thisServicePrices = json_decode($this->services, true);
        $services = [];
        $count = 1;
        foreach ($thisServicePrices as $key => $thisService) {
            $service = Service::where('id', $key)->where('fixed_price', Service::NO)->first();
            if ($service) {
                $servicePrices[$key]['serviceId'] = $service->id;
                $servicePrices[$key]['serviceName'] = $service->name;
                $servicePrices[$key]['price'] = $thisService;
                $servicePrices[$key]['count'] = $count++;
                // Diesel percentage is for base models
                if (!$this->parent_id) {
                    $dieselPercentage = json_decode($this->diesel_percentage, true);
                    $servicePrices[$key]['dieselPercentage'] = $dieselPercentage[$service->id];
                }
            }
        }
        return $servicePrices;
    } //end getServicePrices()
}
