<?php

namespace App\Utils;

use App\Models\User;

class RedirectUser
{
    /**
     * Decides the login redirect url of a user based on user type.
     *
     * @param  string $userType
     * @return string $route
     */
    public static function getLoginRedirectUrl($userType)
    {
        $route = null;
        switch ($userType) {
            case User::CLIENT:
                # code...
                break;
            case User::GARAGE:
                # code...
                break;
            case User::SR_ADMIN:
                # for now
                $route = route('employees.dashboard');
                break;
            case User::SR_EMPLOYEE:
                $route = route('employees.dashboard');
                break;

            default:
                # Redirect to error page and logs him out.
                break;
        }
        return $route;
    } //end getLoginRedirectUrl()
} //end class
