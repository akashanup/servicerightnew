<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Lang;

class OrderCancelStatus extends Notification
{
    use Queueable;

    /**
     * The user's salutation.
     *
     * @var string
     */
    public $salutation;

    /**
     * The user's name.
     *
     * @var string
     */
    public $name;

    /**
     * The order id
     *
     * @var string
     */
    public $orderId;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        $salutation,
        $name,
        $orderId
    ) {
        $this->salutation = $salutation;
        $this->name = $name;
        $this->orderId = $orderId;
    } //end __construct()

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->view('emails.order-cancel-status', [
                'title' => Lang::get('employee-place-order.order_status_email_title'),
                'salutation' => $this->salutation,
                'name' => $this->name,
                'orderId' => $this->orderId,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
