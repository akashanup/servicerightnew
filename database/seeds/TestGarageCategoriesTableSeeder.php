<?php

use App\Models\GarageCategory;
use Illuminate\Database\Seeder;

class TestGarageCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(GarageCategory::class, 25)->create();
    }
}
