$(function(){

	const LOCKED = 'locked';
	const UNLOCKED = 'unlocked';

	$(document).on('click', '.orderLock', function(){
		var lock = $(this);
		var data = {};
		var url = $(this).attr('data-route');
		$.fn.ajaxBlockUI();
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			if(successResponse.response) {
				lock.attr('data-lock-status') == LOCKED 
				? lock.removeClass('fa-lock primary-color').addClass('fa-unlock').attr('data-lock-status', UNLOCKED)
				: lock.removeClass('fa-unlock').addClass('fa-lock primary-color').attr('data-lock-status', LOCKED);
				$.unblockUI();
			} else {
				$.unblockUI();
				$.blockUI({ message: successResponse.message, timeout: 2000,baseZ: 1005 });
			}
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});
	

	$('.popp').popover();

});