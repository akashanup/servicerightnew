$(function(){
	
	$('.orderUpdateBtn').on('click', function(){
		if ($.fn.validateRequest($('#userDetails')) && $.fn.validateRequest($('#vehicleDetails'))
			&& $.fn.validateRequest($('#orderDetails'))) {
			var services = $.fn.recalculate();
			var data = {
				"salutation": $('#salutationSelect').val(),
				"name": $('#nameInpt').val(),
				"phone": $('#phoneInpt').val(),
				"email": $('#emailInpt').val(),
				"addressId": $('#fetchedAddressId').val(),
				"licensePlateNumberId": $('#fetchedlicensePlateNumberId').val(),
				"serviceIds": JSON.stringify(services),
				"pickupDate": $('#pickupDateInpt').val(),
				"alternativePickupDate": $('#alternativePickupDateInpt').val(),
				"pickupTime": $('#pickupTimeInpt').val(),
				"pickupType": $('#pickupTypeSelect').val(),
				"additionalWork": $('#additionalWorkText').val(),
				"orderStatus": $(this).attr('data-type')
			};

			var url = $('#updateOrderRoute').val();
			$.fn.ajaxBlockUI();
			$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
				$.unblockUI();
				window.location = successResponse.route;
			}, (errorResponse) => {
				$.unblockUI();
				$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
			});
		}
	});
});