$(function(){

	$.fn.recalculate = function (){
        var sum = 0;
        var services = [];
        $("input[type=checkbox]:checked").each(function(){
            var itemValue = parseFloat($(this).closest('.form-check').find('.price-hidden input').val());
            sum += (itemValue ? itemValue : 0.00);
            var service = {
                "serviceId":$(this).val(),
                "price":itemValue
            }
            services.push(service);

        });

        sum = sum.toFixed(2);

        $("#totalPrice").html(sum);
        return services;
    };

    $.fn.showPrices = function(){
        $('.service-selection input[type=checkbox]:checked').each(function(){
            $(this).closest('.form-check').find('.price-hidden').show();
        });
    };

    $.fn.fetchPrice = function(checkbox, price=null){
        var servicePrices = $('#fetchedServices').val();
        if(servicePrices) {
            servicePrices = JSON.parse(servicePrices);
            var serviceId = checkbox.val();
            price = price ? price : servicePrices[serviceId];
            checkbox.closest('.form-check').find('.price-hidden').find('.servicePrice').val(price);
        }
    };
	

    $('.service-selection input:checkbox').on('click', function(){
        var thisElement = $(this);
        var selected = thisElement.is(':checked');
        if (selected == true){
            $.fn.fetchPrice(thisElement);
            thisElement.closest('.form-check').find('.price-hidden').show();
            $.fn.recalculate();
        }

        else
        {
            $.fn.fetchPrice(thisElement,0.00);
            thisElement.closest('.form-check').find('.price-hidden').hide();
            $.fn.recalculate();
        }
    });

    $('.service-selection .form-check-label').on('click', function(){
        var thisElement = $(this).closest('.form-check').find('input:checkbox');
        var checkBox = thisElement.is(':checked');

        if (checkBox == true) {
            $.fn.fetchPrice(thisElement, 0.00);
            thisElement.removeAttr('checked');
            thisElement.closest('.form-check').find('.price-hidden').hide();
        } else {
            $.fn.fetchPrice(thisElement);
            thisElement.prop('checked', true);
            thisElement.closest('.form-check').find('.price-hidden').show();
        }
        $.fn.recalculate();
    });

    $(".item-price").click(function() {
        $(this).select();
    });

    $(".item-price").on('change, keyup', function(){
        $.fn.recalculate();
    });

    $('.popp').popover();
    
	$.fn.showPrices();
   	$.fn.recalculate();
});