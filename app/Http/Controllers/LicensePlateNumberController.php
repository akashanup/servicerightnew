<?php

namespace App\Http\Controllers;

use App\Models\LicensePlateNumber;
use DB;

class LicensePlateNumberController extends Controller
{
    /**
     * Fetch vehicle based on license plate number.
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle()
    {
        DB::beginTransaction();
        $json = LicensePlateNumber::fetch(request()->query('licensePlateNumber'));
        DB::commit();
        return response()->json($json);
    }
}
