@extends('layouts.app')

@section('stylesheets')
	@yield('employee-css')
@endsection

@section('content')
    @include('employees.header')
    	@include('errors')
        @yield('employee-content')
    @include('employees.footer')
@endsection

@section('scripts')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    @yield('employee-scripts')
@endsection
