<?php

use App\Models\OrderService;
use Illuminate\Database\Seeder;

class TestOrderServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OrderService::class, 500)->create();
    }
}
