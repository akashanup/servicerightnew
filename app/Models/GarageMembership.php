<?php

namespace App\Models;

use App\Models\Garage;
use Illuminate\Database\Eloquent\Model;

class GarageMembership extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'garage_id', 'base_price', 'discount', 'final_price', 'start_date',
    ];

    /**
     * Get the garage for the membership.
     *
     * @return App\Models\Address
     */
    public function garage()
    {
        return $this->belongsTo(Garage::class);
    } //end address()
}
