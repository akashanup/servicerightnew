@extends('clients.content')

@section('client-content')
    <div class="container">
    	<div class="pannel panel-default">
    		<div class="panel-body">
		    	<form method="post" action="{{ route('clients.verify', $order->verification_token) }}">
		    		{{ csrf_field() }}
			    	<div class="form-group">
					    <label for="phone">Phone:</label>
					    <input type="text" name="phone" required>
					    <button type="submit" class="btn btn-success"> {{ __('globals.confirm') }} </button>
					</div>
		    	</form>
		    </div>
	    </div>
    </div>
@endsection
