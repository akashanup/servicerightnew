<div class="row references">
	@foreach($feedbacks as $feedback)
		<div class="col-md-6">
			<div class="reference{{\App\Models\Feedback::RATING_COLOR[ceil($feedback->rating) -1]}}">
				<div class="reference-top">
					<div class="row">
						<div class="col-md-6">
							<span class="reference-name">{{ $feedback->clientName }}</span>
							<span data-toggle="popover" data-placement="right" data-trigger="hover" data-content="{{ $feedback->clientEmail }}" class="popp adjustTextLength"> {{ $feedback->clientEmail }}</span>
							<p>
							<span data-toggle="popover" data-placement="right" data-trigger="hover" data-content="{{ $feedback->clientCity }}" class="popp adjustTextLength">
								{{ __('employee-place-order.city') }}: {{ $feedback->clientCity }}</span><br>
							Rating: {{ $feedback->rating }}/5 (#{{ $feedback->orderId }})</p>
						</div>
						<div class="col-md-6">
							<p><span data-toggle="popover" data-placement="right" data-trigger="hover" data-content="{{ $feedback->categories }}" class="popp adjustTextLength">
								{{ __('manage-garage.services') }}:
								{{ $feedback->categories }}
							</span><br>
							<span data-toggle="popover" data-placement="right" data-trigger="hover" data-content="{{ $feedback->garageName }}" class="popp adjustTextLength">
								{{ __('manage-garage.company') }}:
								{{ $feedback->garageName }}
							</span><br>
							Postcode: {{ $feedback->garagePostalCode }}<br>
							{{ __('globals.price') }}: &euro; {{ $feedback->orderPrice }}</p>
						</div>
					</div>
				</div>
				<div class="reference-content">
					<div class="row">
						<div class="col-md-12">
							<textarea class="form-control">{{$feedback->review}}</textarea>
						</div>
					</div>
				</div>
				<div class="reference-bottom">
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-primary left feedbackBtn" data-type="assign" data-id="{{$feedback->id}}">{{ __('employee-order-status.assign') }}</button>
							<button type="button" class="btn btn-secondary feedbackBtn" data-type="reject" data-id="{{$feedback->id}}">{{ __('globals.reject') }}</button>
						</div>
						<div class="col-md-12">
							<input type="checkbox" class="form-check-input"> {{ __('feedbacks.feedback_social_media') }}
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach
</div>

<div class="row">
	<div class="pull-right">
		@if($draw > 1)
			<button type="button" class="btn btn-sm btn-default feedbackPaginateBtn" id="prevFeedbackBtn" data-draw="{{ $draw - 1 }}">
				Previous
			</button>
		@endif
		@if($draw < ($feedbackCount/10) )
			<button type="button" class="btn btn-sm btn-default feedbackPaginateBtn" id="nextFeedbackBtn"  data-draw="{{ $draw + 1 }}">
				Next
			</button>
		@endif
	</div>
</div>
<input type="hidden" id="feedbackCount" value="{{ ($feedbackCount/10) }}">
