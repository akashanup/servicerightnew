var clientLatitude = parseInt($('#clientLatitude').val());
var clientLongitude = parseInt($('#clientLongitude').val());
//var clientLocation = {lat: 52.403280, lng: 5.273733};
var clientLocation = {
    lat: clientLatitude,
    lng: clientLongitude
};
var map = '';
var markers = [];
var infoWindows = [];
var allCategories = JSON.parse($('#allCategories').val());
//'https://www.serviceright-autos.nl/images/icons/ok';
var image = $('#imageLoc').val() + '/ok';

function makeGarageMarker(map, clientLatitude, clientLongitude, locations) {
  for (var i = 0; i < locations.length; i++)
    {
        var plaats   = locations[i][0];
        var lat      = locations[i][1];
        var long     = locations[i][2];
        var postcode = locations[i][3];
        var telefoon = locations[i][4];
        var name     = locations[i][5];
        var id       = locations[i][6];
        var notes    = locations[i][7];
        var icons    = locations[i][8];
        var mny_bck  = locations[i][9];
        var lid_tot  = locations[i][10];
        var priority  = locations[i][11];
        var password  = locations[i][12];
        var acceptedOrders = locations[i][13];
        var totalOrders = locations[i][14];
        var categoryIds = locations[i][15];
        categoryIds = categoryIds ? categoryIds.split(',').map(function(item) {
            return parseInt(item, 10);
        }) : categoryIds;

        var gewenste = '';
        $.each(allCategories, function(key, category){
          gewenste += '<input class="form-check-input garageCheckBoxInpt" type="checkbox" id="'+(i+key)+'"';
          if($.inArray(category.id, categoryIds) != -1) {
            gewenste += 'checked '; 
          }
          gewenste += 'data-garageId="'+id+'" data-categoryId="' + category.id + '" value="' + category.name + '">' +
                '<label class="form-check-label" for="'+(i+key)+'">'+
                  category.name+
                '</label><br/>';
        })
        
        var aantal   = acceptedOrders+'/'+totalOrders;
        
        var latlngset = new google.maps.LatLng(lat, long);

        lid_tot = (!lid_tot || lid_tot == '-0001-11-30') ? '' : lid_tot;
        var color = '';
        if(mny_bck == '1' && priority == '1') {
          color = '_yellow';
        } else if (mny_bck == '1') {
          color = '_red';
        } else if (!password && !lid_tot) {
          color = '_blue';
        } else if (lid_tot < $('#currentDate').val()) {
          color = '_grey';
        };

        if(telefoon=="")
            var telefoon = '<a href="#">Bedrijf bewerken &raquo;</a>';
          
        notes = notes ? notes : '';

        var p1 =  latlngset;
        var p2 =  new google.maps.LatLng(clientLatitude, clientLongitude);

        var distance = (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(1);        

        var content = '<table width="350">' +
                        '<tr>'+
                          '<td class="align_left larger" width="50%"><b>' + decodeURI(name) + '</b><br/>' +
                            'Afstand: ' + distance + ' km.<br/>' + postcode + ' ' + plaats + '<br/>[T] ' + telefoon + '<br/>' +
                            'Ontvangen opdrachten: ' + aantal + '<br/>' +
                            '<textarea style="height:80px; width:150px;">' + notes + '</textarea><br/>'+
                            '<input type="button" class="saveGarageNoteBtn" data-garageId="'+id+'" value="Opslaan">'+
                          '</td>'+
                          '<td class="align_left padding_left" width="50%">' + 
                            gewenste + icons + '<br/>'+
                            'Lid tot: ' + lid_tot + '<br/><br/>'+
                            '<a href="#">Bekijk profiel</a><br/><br/>'+
                            '<a href="javascript:void(0)" class="assignGarageOrderBtn" data-garageName="'+name+'" data-garageId="'+id+'">Opdracht aanbieden</a><br/>'+
                          '</td>'+
                        '</tr>'+
                      '</table>';

        var marker = null;
        var infowindow = null;
        if (markers[id] != undefined) {
            //Remove the marker from Map                  
            //markers[id].setMap(null);
            marker = markers[id];
            infowindow = infoWindows[id];
        } else {
            marker = new google.maps.Marker({
              map:map,title:plaats,position:latlngset,icon:image+color+".gif"
            });
            infowindow = new google.maps.InfoWindow();
        }

        var prev_infowindow = false;

        google.maps.event.addListener(marker,"click", (function(marker,content,infowindow){
            return function() {
                if( prev_infowindow ) 
                    prev_infowindow.close();
                     
                prev_infowindow = infowindow;
                infowindow.setContent(content);
                infowindow.open(map, marker);
            };
        })(marker,content,infowindow));

        //Update the markers array.
        markers[id] = marker;
        infoWindows[id] = infowindow;
    }
}

function initMap() {
    map = new google.maps.Map(document.getElementById('country_canvas'), 
        {
            zoom: 11,
            minZoom: 5,
            streetViewControl: false,
            "featureType": "road.highway",
            "elementType": "geometry",
            "styles":[{"stylers": [{ "saturation": -50 }]}],
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            gestureHandling: 'greedy',
            center: clientLocation
        }
    );  

    var clientMarker = new google.maps.Marker({
        position: clientLocation,
        map: map
    });

    var circle = new google.maps.Circle({
        map: map,
        radius: 9  * 1000,    // km = metres * 1000
        fillColor: "#AA0000",
        fillOpacity: 0.20,
        strokeColor: "#FF0000",
        strokeOpacity: 0.00001,
        strokeWeight: 0
    });

    circle.bindTo("center", clientMarker, "position");
    circle.setMap(map);

    // Location
    var locations = '';
    var url = $('#garageFetchAll').val();
    var categories = [];
    var types = [];  
    $('.typeCheckboxInpt:checked').each(function() {
        types.push($(this).val());
    });

    $('.categoryCheckboxInpt:checked').each(function() {
        categories.push($(this).attr('id'));
    });

    var data = {
        "categories": categories,
        "member": $("#members_only").is(':checked'),
        "types": types
    };

    markers = [];
    infoWindows = [];

    $.blockUI({ message: `<strong><b>Loading</b> <i class="fa fa-spinner fa-pulse fa-fw"></i></strong>`, baseZ: 1005 });
    $.ajax({
        type: 'GET',
        url: url,
        data: data,
        dataType: DATA_TYPE_JSON,
        success: function (data) {
            locations = data.locations;
            makeGarageMarker(map, clientLatitude, clientLongitude, locations);
            $.unblockUI();
        },
        error: function (data) {
          $.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
        }
    });
};

// For reference
// var plaats   = locations[i][0]; = city
// var lat      = locations[i][1]; = latitude
// var long     = locations[i][2]; = longitude
// var postcode = locations[i][3]; = postal code
// var telefoon = locations[i][4]; = phone number
// var name     = locations[i][5]; = name of garage
// var aangesloten = locations[i][6]; = garage is member of
// var gewenste = locations[i][7]; = which services does the garage take
// var aantal   = locations[i][8]; = amount of orders already gotten
// var id       = locations[i][9]; = id of garage
// var notes    = locations[i][10]; = general note of garage
// var icons    = locations[i][11]; = which  icon should be displayed on map
// var mny_bck  = locations[i][12]; = garage has money back guarantee or not
// var lid_tot  = locations[i][13]; = garage is member until this date
