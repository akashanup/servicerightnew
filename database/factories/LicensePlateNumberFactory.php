<?php

use App\Models\LicensePlateNumber;
use App\Models\Vehicle;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(LicensePlateNumber::class, function (Faker $faker) {
    return [
        'vehicle_id' => Vehicle::inRandomOrder()->first()->id,
        'license_plate_number' => $faker->unique()->randomNumber($nbDigits = 7, $strict = false),
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
