<?php

namespace App\Models;

use App\Models\Address;
use App\Models\Category;
use App\Models\Company;
use App\Models\GarageMembership;
use App\Models\GarageSaleNote;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Lang;

class Garage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'website', 'address_id', 'user_id', 'money_back_warranty', 'priority',
        'member_expiry', 'password', 'notes', 'status', 'additional_services', 'type',
        'moneybird_id', 'membership_status', 'activation_token', 'deactivate_requested',
    ];

    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
    const DEMO = 'demo';
    const CANCEL = 'cancel';
    const TRUE = 'true';

    /**
     * Format member_expiry from yyyy-mm-dd to dd-mm-yyyy
     *
     * @return date $member_expiry
     */
    public function member_expiry()
    {
        return Carbon::parse($this->member_expiry)->format('d-m-Y');
    }

    /**
     * Get the service categories for the garage.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'garage_categories', 'garage_id', 'category_id')
            ->withTimestamps();
    } //end categories()

    /**
     * Get the service categories for the garage.
     */
    public function companies()
    {
        return $this->belongsToMany(Company::class, 'garage_companies', 'garage_id', 'company_id')
            ->withTimestamps();
    } //end companies()

    /**
     * Get the orders for the garage.
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class, 'garage_orders', 'garage_id', 'order_id')
            ->withPivot('id', 'status')->withTimestamps();
    } //end orders()

    /**
     * Get the salenotes for the garage.
     */
    public function saleNotes()
    {
        return $this->hasMany(GarageSaleNote::class);
    } //end orders()

    /**
     * Get the membership details for the garage.
     */
    public function memberships()
    {
        return $this->hasMany(GarageMembership::class);
    } //end memberships()

    /**
     * Get the address of the garage.
     *
     * @return App\Models\Address
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    } //end address()

    /**
     * Get the user of the garage.
     *
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    } //end user()

    /**
     * Store garage order
     *
     * @param array $data
     * @param App\Models $garage
     * @return json $json
     */
    public static function store($data, $garage = null)
    {
        // Store address
        $address = Address::createUnique(
            null, $data['completeAddress'], Address::GARAGE_TYPE,
            $data['latitude'], $data['longitude'], $data['postalCode'], $data['city']
        );

        // Check for the existing user. If not found then add it.
        $user = User::createUnique($data['contactPerson'], $data['email'], $data['phone'], User::GARAGE);

        $garageData = [
            'name' => $data['name'],
            'website' => $data['website'],
            'type' => $data['type'],
            'address_id' => $address->id,
            'user_id' => $user->id,
            'moneybird_id' => $data['moneyBirdId'],
        ];
        $message = null;
        if ($garage) {
            // Update garage
            $garage->update($garageData);
            $message = Lang::get('globals.company_updated');
        } else {
            // Store Garage
            $garage = static::create($garageData);
            $message = Lang::get('globals.company_added');
        }

        $garage->addCategories($data['categories']);
        $garage->addCompanies($data['companies']);

        return ['response' => true, 'message' => $message];
    } // end store()

    /**
     * Store garage order
     *
     * @param integer $orderId
     * @param string $status
     * @return json $json
     */
    public function storeOrder($orderId, $status)
    {
        $json = [];
        $json['response'] = false;
        $order = Order::find($orderId);
        if ($order) {
            $this->orders()->attach($orderId, ['status' => $status]);

            // If garage accepts the order then update its status and send email to garage and client.
            if ($status == Order::ACCEPTED_BY_GARAGE) {
                $order->update(['status' => $status, 'locked_status' => Order::UNLOCKED]);
                $order->addHistory();
                // Garage
                $this->user->sendOrderStatusNotification($order, $this);
                // Client
                $order->user->sendOrderStatusNotification($order, $this);
                // Allow navigation
                $json['route'] = route('orders.statuses', $order->status);
            } else {
                // Update the marker
                $locations = static::fetchAll(null, [], [], $this->id);
                $json['locations'] = $locations['locations'];
            }
            $json['response'] = true;
        }
        return $json;
    } //end storeOrder()

    /**
     * Fetch all garages
     *
     * @param integer $orderId
     * @param array $categories (optional)
     * @param boolean $member
     * @return json $json
     */
    public static function fetchAll($member, $categories, $types, $garageId = null)
    {
        $json = [];
        $json['response'] = true;
        $locations = [];
        $index = 0;
        $imgServiceAutos = view(
            'employees.company-images',
            ['location' => config('constants.SERVICE_AUTOS_IMAGE')]
        )->render();
        $imgBovag = view(
            'employees.company-images',
            ['location' => config('constants.BOVAG_IMAGE')]
        )->render();
        $imgRdw = view(
            'employees.company-images',
            ['location' => config('constants.RDW_IMAGE')]
        )->render();
        $imgFocwa = view(
            'employees.company-images',
            ['location' => config('constants.FOCWA_IMAGE')]
        )->render();
        $comanyIcons = [
            'Bovag' => $imgBovag,
            'Rdw' => $imgRdw,
            'Focwa' => $imgFocwa,
        ];

        $garages = static::basicGarageDetails();
        $garages = $garages
            ->leftjoin('garage_companies', 'garages.id', 'garage_companies.garage_id')
            ->leftjoin('companies', 'garage_companies.company_id', 'companies.id');

        if (sizeof($categories)) {
            $garages = $garages->leftjoin('garage_categories as gc', 'garages.id', 'gc.garage_id')
                ->leftjoin('categories as c', 'gc.category_id', 'c.id')
                ->whereIn('c.id', $categories);
        }
        if (sizeof($types)) {
            $garages = $garages->whereIn('garages.type', $types);
        }
        if ($member == 'true') {
            $garages = $garages->whereNotNull('garages.member_expiry');
        }
        if ($garageId) {
            $garages = $garages->where('garages.id', $garageId);
        }

        $garages = $garages->select(
            [
                'garages.id as garageId',
                'garages.name as garageName',
                'garages.money_back_warranty',
                'garages.member_expiry',
                'garages.priority',
                'garages.password',
                'garages.notes',
                'users.phone',
                'cities.name as city',
                'postal_codes.postal_code',
                'addresses.latitude',
                'addresses.longitude',
                DB::raw('GROUP_CONCAT(DISTINCT categories.id) as categoryIds'),
                DB::raw('(Select count(*) from garage_orders where garage_orders.garage_id = garages.id group by garage_orders.garage_id) as totalOrders'),
                DB::raw("(select count(*) from garage_orders where garage_orders.garage_id = garages.id and garage_orders.status = '" . Order::COMPLETED . "' group by garage_orders.garage_id) as acceptedOrders"),
                DB::raw('GROUP_CONCAT(DISTINCT companies.name) as companyNames'),
            ]
        )->distinct('garages.id')->groupBy('garages.id')->orderBy('garages.id')->get()->toArray();

        foreach ($garages as $garage) {
            $icons = $imgServiceAutos;
            if ($garage['companyNames']) {
                foreach (explode(',', $garage['companyNames']) as $company) {
                    $icons .= $comanyIcons[$company];
                }
            }

            $locations[$index++] = [
                $garage['city'],
                $garage['latitude'],
                $garage['longitude'],
                $garage['postal_code'],
                $garage['phone'],
                $garage['garageName'],
                $garage['garageId'],
                $garage['notes'],
                $icons,
                $garage['money_back_warranty'],
                $garage['member_expiry'],
                $garage['priority'],
                $garage['password'],
                $garage['acceptedOrders'],
                $garage['totalOrders'],
                $garage['categoryIds'],
            ];
        }
        $json['locations'] = $locations;
        return $json;
    } //end fetchAll()

    /**
     * Add categories to the garages.
     *
     * @param  array $categoryIds
     * @return void
     */
    public function addCategories($categoryIds)
    {
        if ($this->categories->count()) {
            $this->categories()->detach();
        }

        foreach ($categoryIds as $categoryId) {
            $category = Category::find($categoryId);
            if ($category) {
                $this->categories()->attach($category->id);
            }
        }
    } //end addCategories()

    /**
     * Add membership to the garages.
     *
     * @param  number $discount
     * @param  number $basePrice
     * @return void
     */
    public function addMembership($discount)
    {
        $basePrice = config('constants.GARAGE_MEMBERSHIP_PRICE');
        $this->memberships()->create(
            [
                'base_price' => $basePrice,
                'discount' => $discount,
                'final_price' => ($basePrice - $discount),
            ]
        );
    } //end addCategories()

    /**
     * Update membership start and end date.
     *
     * @param  number $discount
     * @param  number $basePrice
     * @return void
     */
    public function updateMembershipDate($membershipValidity = null)
    {
        $currentDate = Carbon::now();
        // Update start date of the memebership
        $this->memberships()->latest()->first()->update(['start_date' => $currentDate->toDateString()]);
        // Update member expiry
        $membershipValidity = ($membershipValidity && Carbon::parse($membershipValidity)->gte($currentDate))
        ? Carbon::parse($membershipValidity)->toDateString()
        : $currentDate->addDays(
            config('constants.GARAGE_MEMBERSHIP_VALIDITY')
        )->toDateString();
        // Activation token can be used only for one time.
        $this->update(
            [
                'member_expiry' => $membershipValidity,
                'membership_status' => static::ACTIVE,
                'activation_token' => null,
            ]
        );
    } //end updateMembershipDate()

    /**
     * Add companies to the garages.
     *
     * @param  array $companyIds
     * @return void
     */
    public function addCompanies($companyIds)
    {
        if ($this->companies->count()) {
            $this->companies()->detach();
        }

        foreach ($companyIds as $companyId) {
            $company = Company::find($companyId);
            if ($company) {
                $this->companies()->attach($company->id);
            }
        }
    } //end addCompanies()

    /**
     * Manage garage data.
     *
     * @return json $json
     */
    public static function garageData($garageId = null)
    {
        $garages = static::basicGarageDetails();
        if ($garageId) {
            $garages = $garages->where('garages.id', $garageId);
        }
        $garages = $garages
            ->leftjoin('garage_sale_notes', 'garages.id', 'garage_sale_notes.garage_id')
            ->groupBy('garages.id')
            ->select(
                [
                    'garages.id',
                    'garages.name',
                    DB::raw('GROUP_CONCAT(DISTINCT categories.name SEPARATOR " | ") as categories'),
                    DB::raw('GROUP_CONCAT(DISTINCT garage_sale_notes.sale_note SEPARATOR " | ") as saleNotes'),
                    'addresses.complete_address',
                    'users.phone',
                    'garages.member_expiry',
                    'garages.notes',
                    'cities.name as city',
                    'postal_codes.postal_code',
                    'garages.membership_status',
                    'garages.deactivate_requested',
                ]
            );

        return $garages;
    } //end garageData()

    /**
     * Check whether the garage was/is a member or not.
     *
     * @param array $garage
     * @return string $html
     */
    public static function isRenewable($garage)
    {
        return ($garage->membership_status == static::ACTIVE ||
            ($garage->membership_status == static::INACTIVE && $garage->member_expiry));
    }

    /**
     * Get membership text.
     *
     * @param array $garage
     * @return string $html
     */
    public static function getMembershipText($garage)
    {
        $html = null;
        $member_expiry = $garage->member_expiry ? Carbon::parse($garage->member_expiry)->format('d-m-Y')
        : $garage->member_expiry;
        if ($garage->membership_status == static::ACTIVE) {
            $html = Lang::get('manage-garage.member_to') . ': ' . $member_expiry;
        } elseif ($garage->membership_status == static::INACTIVE && $garage->member_expiry) {
            $html = Lang::get('manage-garage.expired_on') . ': ' . $member_expiry;
        } else {
            $html = Lang::get('globals.not_a_member');
        }
        return $html;
    } // getMembershipText()

    /**
     * Get basic garage details.
     *
     */
    public static function basicGarageDetails()
    {
        return static::join('addresses', 'garages.address_id', 'addresses.id')
            ->join('postal_codes', 'addresses.postal_code_id', 'postal_codes.id')
            ->join('cities', 'postal_codes.city_id', 'cities.id')
            ->join('users', 'garages.user_id', 'users.id')
            ->leftjoin('garage_categories', 'garages.id', 'garage_categories.garage_id')
            ->leftjoin('categories', 'garage_categories.category_id', 'categories.id')
            ->where('garages.status', static::ACTIVE);
    } // end basicGarageDetails()

    /**
     * Show details of a garage.
     *
     * @return array
     */
    public function garageDetails()
    {
        return static::basicGarageDetails()
            ->leftjoin('garage_companies', 'garages.id', 'garage_companies.garage_id')
            ->leftjoin('companies', 'garage_companies.company_id', 'companies.id')
            ->where('garages.id', $this->id)
            ->select(
                'garages.name', 'users.name as contactName', 'garages.moneybird_id', 'users.phone', 'users.email',
                'garages.website', 'addresses.complete_address', 'postal_codes.postal_code', 'cities.name as city', 'garages.type',
                DB::raw('GROUP_CONCAT(DISTINCT categories.id) as categoryIds'),
                DB::raw('GROUP_CONCAT(DISTINCT companies.id) as companyIds')
            )
            ->groupBy('garages.id')
            ->get();
    } // end garageDetails()

    /**
     * Get sale note data in a particular format.
     *
     * @return array
     */
    public function saleNotesData()
    {
        $notes = [];
        foreach ($this->saleNotes()->orderBy('id', 'desc')->get() as $key => $note) {
            $notes[$key]['note'] = $note->sale_note;
            $notes[$key]['createdBy'] = User::find($note->by_user_id)->name;
            $notes[$key]['createdAt'] = Carbon::parse($note->created_at)->format('d-m-Y h:m:s');
        }
        return ['response' => true, 'saleNotes' => $notes];
    } //end saleNotesData()

    /**
     * Add note to the order.
     *
     * @param  text $note
     * @return App\Models
     */
    public function addSaleNote($note)
    {
        return $this->saleNotes()->create(
            [
                'sale_note' => $note,
                'by_user_id' => auth()->user()->id,
            ]
        );
    } //end addSaleNote()

    /**
     * Get stats of a garage in a particular format.
     *
     * @return array
     */
    public function stats()
    {
        $orders = $this->orders()->distinct('orders.id');
        $acceptedOrders = $orders->wherePivot('status', Order::COMPLETED);
        $totalPrice = $acceptedOrders->sum('total_price');
        $acceptedOrdersCount = $acceptedOrders->count();
        $totalAveragePrice = $acceptedOrdersCount ? number_format(($totalPrice / $acceptedOrdersCount), 2) : 0;

        $lastYearOrders = $this->orders()->distinct('orders.id')->wherePivot('created_at', '>', Carbon::now()->subYear());
        $lastYearAcceptedOrders = $lastYearOrders->wherePivot('status', Order::COMPLETED);
        $totalPriceLastYear = $lastYearAcceptedOrders->sum('total_price');
        $acceptedOrdersCountLastYear = $lastYearAcceptedOrders->count();
        $totalAveragePriceLastYear = $acceptedOrdersCountLastYear ? number_format(($totalPriceLastYear / $acceptedOrdersCountLastYear), 2) : 0;

        $stats = [];
        $stats['totalOrders'] = $orders->count();
        $stats['totalAveragePrice'] = $totalAveragePrice;
        $stats['totalPrice'] = $totalPrice;
        $stats['acceptedOrders'] = $acceptedOrdersCount;
        $stats['totalOrdersLastYear'] = $lastYearOrders->count();
        $stats['acceptedOrdersLastYear'] = $acceptedOrdersCountLastYear;
        $stats['totalPriceLastYear'] = $totalPriceLastYear;
        $stats['totalAveragePriceLastYear'] = $totalAveragePriceLastYear;

        return ['response' => true, 'stats' => $stats];
    } //end stats()

    /**
     * Create and send login credentials for garage.
     *
     * @return array
     */
    public function sendDemo()
    {
        $response = false;
        $message = null;
        if ($this->membership_status == static::INACTIVE) {
            // Update member expiry
            $this->update(
                [
                    'member_expiry' => Carbon::now()->addDays(config('constants.GARAGE_DEMO_MEMBERSHIP_VALIDITY'))
                        ->toDateString(),
                    'membership_status' => static::DEMO,
                ]
            );
            $password = $this->updatePassword();
            // Send password
            $this->user->sendGarageDemoCredentials($this->id, $password);
            $response = true;
            $message = Lang::get('manage-garage.login_credentials');
        }

        return ['response' => $response, 'message' => $message];
    } //end sendDemo()

    /**
     * Create membership for garage.
     *
     * @param array $data
     * @return array
     */
    public function createMembership($data)
    {
        $message = Lang::get('manage-garage.membership_already_active');
        // Check whether the membership is active or not. If not then add the membership.
        if ($this->membership_status != static::ACTIVE) {
            // A cronjob will run which will automatically deactivate the garage if the member_expiry < current_date.
            // Add membership
            $this->addMembership($data['discount']);
            // If membership status is demo then set it to inactive
            $this->update(['membership_status' => static::INACTIVE, 'member_expiry' => null]);
            $this->user->sendGarageCredentials($this);

            $message = Lang::get('manage-garage.membership_added');
        }
        return ['response' => true, 'message' => $message];
    } //end createMembership()

    /**
     * Update membership for garage.
     *
     * @param array $data
     * @return array
     */
    public function updateMembership($data)
    {
        $message = Lang::get('globals.request_error_message');

        if (static::isRenewable($this)) {
            if ($data['type'] == static::CANCEL) {
                $this->update(
                    [
                        'deactivate_requested' => static::TRUE,
                    ]
                );
                $message = $this->name . Lang::get('manage-garage.membership_deactivated');
                // Send deactivation mail to garage user.
                $this->user->sendGarageDeactivateMembership($this);
            } else {
                // Update membership
                $this->addMembership($data['discount']);
                $this->updateMembershipDate($data['memberExpiry']);
                $this->user->updateGarageMembership($this);
                $message = Lang::get('manage-garage.membership_updated');
            }
        }
        return ['response' => true, 'message' => $message];
    } //end updateMembership()

    /**
     * Update password for a garage.
     *
     * @param string $password
     * @return string $password
     */
    public function updatePassword($password = null)
    {
        // Create password for the garage.
        $password = $password ? $password : str_random(15);
        // Update password.
        $this->update(['password' => bcrypt($password)]);
        return $password;
    } // end updatePassword()

    /**
     * Generate Activation Token for a garage.
     *
     * @return string $password
     */
    public function generateActivationToken()
    {
        $activationToken = base64_encode(str_random(10) . $this->id . microtime());

        $this->update(['activation_token' => $activationToken]);
        return $activationToken;
    } // end generateActivationToken()
}
