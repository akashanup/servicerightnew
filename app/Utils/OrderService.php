<?php

namespace App\Utils;

use App\Exceptions\AddressNotFoundException;
use App\Exceptions\LicensePlateNumberNotFoundException;
use App\Models\Address;
use App\Models\LicensePlateNumber;
use App\Models\Vehicle;
use Lang;

class OrderService
{
    /**
     * Check for car exception.
     *
     * @param integer $licensePlateNumberId
     * @return App\Model
     */
    public static function getLicensePlateNumber($licensePlateNumberId)
    {
        // Check for license number Id. If not found then throw and exception.
        $licensePlateNumber = LicensePlateNumber::find($licensePlateNumberId);
        if (!$licensePlateNumber) {
            throw new LicensePlateNumberNotFoundException(Lang::get('globals.vehicle_not_found'), 1);
        }
        return $licensePlateNumber;
    } //end getLicensePlateNumber()

    /**
     * Check for address exception.
     *
     * @param integer $addressId
     * @return App\Model
     */
    public static function getAddress($addressId)
    {
        // Check for addressId. If not found then throw and exception.
        $address = Address::find($addressId);
        if (!$address) {
            throw new AddressNotFoundException(Lang::get('globals.address_not_found'), 1);
        }
        return $address;
    } //end getAddress()

    /**
     * Get total price of the order.
     *
     * @param array $serviceIds
     * @param integer $vehicleId
     * @return string $totalPrice
     */
    public static function getTotalPrice($serviceIds, $vehicleId)
    {
        $vehicle = Vehicle::find($vehicleId);
        $totalPrice = 0;
        foreach ($serviceIds as $serviceId) {
            $totalPrice += $vehicle->servicePrice($serviceId);
        }
        return $totalPrice;
    } //end totalPrice()
} //end class
