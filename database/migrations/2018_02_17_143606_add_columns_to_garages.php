<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToGarages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garages', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->string('moneybird_id')->nullable();
            $table->enum('money_back_warranty', ['0', '1'])->default('0');
            $table->enum('priority', ['0', '1'])->default('0');
            $table->date('member_expiry')->nullable();
            $table->string('password')->nullable();
            $table->text('notes')->nullable();
            $table->text('additional_services')->nullable();
            $table->enum('type',
                ['Bovag autobedrijf', 'SR cleaningbedrijf', 'Focwa schadeherstelbedrijf',
                    'Koerierbedrijf', 'SR autobedrijf', 'SR schadeherstelbedrijf', 'Transportbedrijf',
                    'Accountant', 'Adviseur']
            )->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garages', function (Blueprint $table) {
            $table->dropForeign('garages_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('money_back_warranty');
            $table->dropColumn('priority');
            $table->dropColumn('member_expiry');
            $table->dropColumn('password');
            $table->dropColumn('notes');
            $table->dropColumn('additional_services');
            $table->dropColumn('type');
            $table->dropColumn('moneybird_id');
        });
    }
}
