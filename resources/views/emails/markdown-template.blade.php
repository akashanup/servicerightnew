@component('mail::message')
{{-- Greeting --}}
# Hello!


{{-- Action Panel --}}

@component('mail::panel', ['color' => '#f2dede'])
{{ $exception }}
@endcomponent

@component('mail::panel', ['color' => '#f2dede'])
{{ $request }}
@endcomponent


{{-- Salutation --}}
{{ config('app.name') }}

@endcomponent
