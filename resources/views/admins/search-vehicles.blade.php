@extends('admins.content')

@section('admin-content')
    <section class="small-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>{{ __('vehicles.manage_price') }}</h2>
				</div>
			</div>
		</div>
	</section>

	<section class="small-padding bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>{{ __('vehicles.note') }}: {{ __('vehicles.there_are') }} {{ $vehiclesCount }} {{ __('vehicles.brands_without_prices') }}:
						@foreach($vehicles as $vehicle)
							<a href="{{ route('vehicles.prices', $vehicle->id) }}">{{ $vehicle->brand->name }} {{ $vehicle->model }}</a>&nbsp;&nbsp;
						@endforeach
				</div>
				<div class="col-md-6">
					<label for="" class="form-label">{{ __('vehicles.search_brand') }}:</label>
					<select class="form-control" id="brandSlct" >
							<option value="" disabled selected>
								{{ __('employee-place-order.select') }} {{ __('employee-place-order.brand') }}
							</option>
						@foreach($brands as $brand)
							<option value="{{ $brand->id }}">{{ $brand->name }}</option>
						@endforeach()
					</select>
					<label for="" class="form-label" >Model:</label>
					<select class="form-control" id="modelSlct" disabled="disabled">
					</select>
				</div>
				<div class="col-md-6">
					<label for="" class="form-label">{{ __('vehicles.search_license_plate') }}:</label>
					<input class="form-control" type="text" id="licensePlateNumberInpt" placeholder="{{ __('employee-place-order.license_plate') }}...">
					<label for="" class="form-label">&nbsp;</label>
					<button type="button" id="searchVehicleBtn" class="btn btn-primary" style="float: left; clear: left;">{{ __('vehicles.edit_price') }}</button>
				</div>
			</div>
		</div>
		<input type="hidden" value="{{ route('vehicles.fetch') }}" id="fetchVehicleRoute">
		<input type="hidden" value="{{ route('home') }}" id="homeRoute">
	</section>

    <!-- Scripts -->
    @section('admin-scripts')
    	<script type="text/javascript" src="{{ asset('js/custom/vehicle.js').'?v='.config('app.version', '1') }}"></script>
    @endsection

@endsection
