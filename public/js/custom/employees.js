$(function(){
	$.fn.initEmployeeTable = () => {
		$('#employeeTable').DataTable().destroy();
	    var table = $('#employeeTable').DataTable(
		    {
		        processing: true,
		        serverSide: true,
		        ajax: $('#employeesDataRoute').val(),
		        columns: [
		        	{data: 'login', orderable: false, searchable: false},
		            {data: 'id', name: 'users.id'},
		            {data: 'name', name: 'users.name'},
		            {data: 'nickname', name: 'users.nickname'},
		            {data: 'last_login', name: 'users.last_login'},
		            {data: 'status', name: 'users.status', orderable: false},
		            {data: 'action', orderable: false, searchable: false}
		        ],
		        fnDrawCallback: () => {
		        	$(document).find('.popp').popover();
		        },
		        columnDefs: [
		            { width: '1%', targets: 0 },
		            { width: '2%', targets: 1 },
		            { width: '20%', targets: 2 },
		            { width: '20%', targets: 3 },
		            { width: '20%', targets: 4 },
		            { width: '17%', targets: 5 },
		            { width: '20%', targets: 6 },
		        ],
		    },
		);
	};

	$(document).on('click', '.employeeLogin', function(){
		$.fn.ajaxBlockUI();
		var url = $('#employeeLoginRoute').val();
		var data = {
			"id": $(this).attr('data-id'),
		};
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			$.unblockUI();
			if(successResponse.response){
				window.location = successResponse.route;
			}
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});

	$(document).on('click', '.editEmployeeModalBtn', function(){
		$.fn.ajaxBlockUI();
		var data = {};
		$.fn.ajaxCall(REQUEST_TYPE_GET, $(this).attr('data-route'), data, DATA_TYPE_JSON).then((successResponse) => {
			$.unblockUI();
			var data = successResponse.data;
			var modal = $('#editEmployeeModal');
			modal.find('.employeeNameInpt').val(data.name);
			modal.find('.employeeNicknameInpt').val(data.nickname);
			modal.find('.employeeEmailInpt').val(data.email);
			modal.find('.employeeStatusSlct').val(data.status);
			modal.find('.employeeTypeSlct').val(data.type);
			modal.find('#updateEmployeeBtn').attr('data-id', data.id);
			if(successResponse.currentUser) {
				modal.find('.employeeStatusSlct').prop('disabled', true);
			}
			modal.modal('show');
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});

	$(document).on('hidden.bs.modal', '#editEmployeeModal, #addEmployeeModal', function(){
		var modal = $(this);
		modal.find('.employeeNameInpt').val('');
		modal.find('.employeeNicknameInpt').val('');
		modal.find('.employeeStatusSlct').val('');
		modal.find('.employeeTypeSlct').val('');
		modal.find('.employeeEmailInpt').val('');
		modal.find('.employeeStatusSlct').prop('disabled', false);
		modal.find('.employeePasswordInpt').val('');
		modal.find('#updateEmployeeBtn').removeAttr('data-id');
	});

	$('#updateEmployeeBtn').on('click', function(){
		var id = $(this).attr('data-id');

		if(id > 0) {
			$(this).prop('disabled', true);
			var modal = $('#editEmployeeModal');
			var url = $('#employeeUpdateRoute').val();
			var data = {
				"id": id,
				"name": modal.find('.employeeNameInpt').val(),
				"nickname": modal.find('.employeeNicknameInpt').val(),
				"status": modal.find('.employeeStatusSlct').val(),
				"type": modal.find('.employeeTypeSlct').val(),
				"email": modal.find('.employeeEmailInpt').val(),
				"password": modal.find('.employeePasswordInpt').val(),
			};
			$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
				$(this).prop('disabled', false);
				modal.modal('hide');
				$.blockUI({ message: successResponse.message, timeout: 2000,baseZ: 1005 });
				$.fn.initEmployeeTable();
			}, (errorResponse) => {
				$(this).prop('disabled', false);
				modal.modal('hide');
				$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
			});
		}
	});

	$('#addEmployeeModalBtn').on('click', () => {
		$('#addEmployeeModal').modal('show');
	});

	$('#addEmployeeBtn').on('click', function(){
		$(this).prop('disabled', true);
		var modal = $('#addEmployeeModal');
		var url = $('#employeeAddRoute').val();
		var data = {
			"name": modal.find('.employeeNameInpt').val(),
			"nickname": modal.find('.employeeNicknameInpt').val(),
			"status": modal.find('.employeeStatusSlct').val(),
			"type": modal.find('.employeeTypeSlct').val(),
			"email": modal.find('.employeeEmailInpt').val(),
			"password": modal.find('.employeePasswordInpt').val(),
		};
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			$(this).prop('disabled', false);
			modal.modal('hide');
			$.blockUI({ message: successResponse.message, timeout: 2000,baseZ: 1005 });
			$.fn.initEmployeeTable();
		}, (errorResponse) => {
			$(this).prop('disabled', false);
			modal.modal('hide');
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});

	$.fn.initEmployeeTable();
});