<?php

include_once 'generate-file.php';

$dictionary = [
    'review_order' => [
        'en' => 'Review your order',
        'nl' => 'Controleer uw bestelling',
    ],
];

generateFile($dictionary, basename(__FILE__));
