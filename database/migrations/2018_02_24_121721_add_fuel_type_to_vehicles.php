<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFuelTypeToVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->enum('fuel_type',
                [
                    'Benzine', 'Diesel', 'G3', 'Elektriciteit', 'Hybride Benzine',
                    'Hybride Diesel', 'CNG (Compressed Natural G', 'Overig',
                    'Alcohol', 'CNG', 'N.v.t', 'Waterstof', 'LPG',
                ]
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('fuel_type');
        });
    }
}
