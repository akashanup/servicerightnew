<?php

use App\Models\Address;
use App\Models\LicensePlateNumber;
use App\Models\Order;
use App\Models\User;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Order::class, function (Faker $faker) {
    $statuses = [
        'drafted', 'quoted', 'confirmed', 'cancelled', 'removed', 'accepted_by_garage', 'rejected_by_garage', 'completed',
    ];
    return [
        'address_id' => Address::where('type', Address::CLIENT_TYPE)->inRandomOrder()->first()->id,
        'user_id' => User::where('type', User::CLIENT)->inRandomOrder()->first()->id,
        'license_plate_number_id' => LicensePlateNumber::inRandomOrder()->first()->id,
        'pickup_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'pickup_type' => 'pickup_from_client',
        'total_price' => $faker->numberBetween($min = 1000, $max = 2000),
        'status' => $statuses[rand(0, 7)],
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
