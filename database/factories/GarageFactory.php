<?php

use App\Models\Address;
use App\Models\Garage;
use App\Models\User;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Garage::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'website' => $faker->url,
        'address_id' => Address::where('type', Address::GARAGE_TYPE)->inRandomOrder()->first()->id,
        'user_id' => User::where('type', User::GARAGE)->inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
