<?php

namespace App\Models;

use App\Models\Address;
use App\Models\City;
use Illuminate\Database\Eloquent\Model;

class PostalCode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'postal_code', 'city_id',
    ];

    /**
     * Get the city of the postal code.
     *
     * @return App\Models\City
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    } //end city()

    /**
     * Get the addresses of the postal code.
     *
     * @return Collection
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    } //end addresses()

    /**
     * Create a postal code if its not found.
     *
     * @param integer $cityId
     * @param string $postalCode
     * @return App\Modals
     */
    public static function createUnique($cityId, $postalCode)
    {
        $thisPostalCode = static::where('postal_code', $postalCode)->where('city_id', $cityId)->first();
        if (!$thisPostalCode) {
            $thisPostalCode = static::create(
                [
                    'postal_code' => $postalCode,
                    'city_id' => $cityId,
                ]
            );
        }
        return $thisPostalCode;
    } //end createUnique()
}
