 <!-- Error div-->
<div class="container" id="errorsDiv">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
@if($flash = session('status'))
        <div class="flash-message form-group">
            <div class="alert alert-success">
                <ul style="list-style: none;">
                    <li>{{$flash}}</li>
                </ul>
            </div>
        </div>
@endif
