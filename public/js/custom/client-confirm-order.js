$(function(){
	const borderColor 		=	'border-color';
	const color 			=	'red';

	$.fn.validateForm = () => {
		$('.formField').css('border-color', 'lightgrey');
		$response = false;
		if (!$('#nameInpt').val().length) {
			$('#nameInpt').focus();
			$('#nameInpt').css(borderColor, color);
		} else if((!$('#phoneInpt').val().length) || ($('#phoneInpt').val().length < 10)) {
			$('#phoneInpt').focus();
			$('#phoneInpt').css(borderColor, color);
		} else if(!$('#emailInpt').val().length) {
			$('#emailInpt').focus();
			$('#emailInpt').css(borderColor, color);
		} else if(!$('#postalCodeInpt').val().length) {
			$('#postalCodeInpt').focus();
			$('#postalCodeInpt').css(borderColor, color);
		} else if(!$('#houseNumberInpt').val().length) {
			$('#houseNumberInpt').focus();
			$('#houseNumberInpt').css(borderColor, color);
		} else if(!$('#pickupDateInpt').val().length) {
			$('#pickupDateInpt').focus();
			$('#pickupDateInpt').css(borderColor, color);
		} else if(!$('#pickupTimeInpt').val().length) {
			$('#pickupTimeInpt').focus();
			$('#pickupTimeInpt').css(borderColor, color);
		} else {
			$response = true;
		}
		return $response;
	};

	$('.orderUpdateBtn').on('click', function(){
		if ($.fn.validateForm()) {
			var data = {
				"salutation": $('#salutationSelect').val(),
				"name": $('#nameInpt').val(),
				"phone": $('#phoneInpt').val(),
				"email": $('#emailInpt').val(),
				"addressId": $('#fetchedAddressId').val(),
				"licensePlateNumberId": $('#fetchedlicensePlateNumberId').val(),
				"pickupDate": $('#pickupDateInpt').val(),
				"alternativePickupDate": $('#alternativePickupDateInpt').val(),
				"pickupTime": $('#pickupTimeInpt').val(),
				"orderStatus": $(this).attr('data-type')
			};

			var url = $('#confirmQuotedOrderRoute').val();
			$.fn.ajaxBlockUI();
			$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
				$.unblockUI();
				window.location = successResponse.route;
			}, (errorResponse) => {
				$.unblockUI();
				$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
			});
		}
	});
});