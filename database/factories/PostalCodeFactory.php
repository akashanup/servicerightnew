<?php

use App\Models\City;
use App\Models\PostalCode;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(PostalCode::class, function (Faker $faker) {
    return [
        'postal_code' => $faker->unique()->postcode,
        'city_id' => City::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
