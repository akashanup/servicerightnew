<?php

include_once 'generate-file.php';

$dictionary = [
    'inactive_account' => [
        'en' => 'Your account is inactive. Please contact our support staff.',
        'nl' => 'Uw account is inactief. Neem contact op met onze ondersteuningsmedewerkers.',
    ],
    'last_login' => [
        'en' => 'Last login',
        'nl' => 'Laatst ingelogd',
    ],
    'nickname' => [
        'en' => 'Nickname',
        'nl' => 'Bijnaam',
    ],
    'active' => [
        'en' => 'Active',
        'nl' => 'Actief',
    ],
    'inactive' => [
        'en' => 'Inactive',
        'nl' => 'Inactief',
    ],
    'admin' => [
        'en' => 'Admin',
        'nl' => 'Beheerder',
    ],
    'employee' => [
        'en' => 'Employee',
        'nl' => 'Werknemer',
    ],
    'password' => [
        'en' => 'Password',
        'nl' => 'Wachtwoord',
    ],
    'add_staff' => [
        'en' => 'Add staff',
        'nl' => 'Voeg personeel toe',
    ],
];

generateFile($dictionary, basename(__FILE__));
