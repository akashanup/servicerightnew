<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ExceptionEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The exception that occurred.
     *
     * @var string
     */
    public $exception;

    /**
     * The request that was sent.
     *
     * @var string
     */
    public $request;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($exception, $request)
    {
        $this->request = $request;
        $this->exception = $exception;
    } //end __construct()

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.markdown-template', ['exception' => $this->exception, 'request' => $this->request]);
    }
}
