@extends('employees.content')

<!-- CSS -->
@section('employee-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('employee-content')
    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ __('employee-globals.edit_staff') }}</h2>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-primary" id="addEmployeeModalBtn">{{ __('users.add_staff') }}</button>
                </div>
            </div>
        </div>
    </section>

    <section class="small-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="employeeTable">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>ID</th>
                                <th>{{ __('employee-place-order.name') }}</th>
                                <th>{{ __('users.nickname') }}</th>
                                <th>{{ __('users.last_login') }}</th>
                                <th>Status</th>
                                <th>{{ __('employee-order-status.edit') }}</th>
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <div class="modal fade" id="editEmployeeModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        {{ __('employee-globals.edit_staff') }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('employees.add-edit-form')
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-primary btn-block" id="updateEmployeeBtn">{{ __('globals.save') }}</button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-secondary btn-block">{{ __('globals.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="addEmployeeModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        {{ __('users.add_staff') }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('employees.add-edit-form')
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-primary btn-block" id="addEmployeeBtn">{{ __('globals.save') }}</button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-secondary btn-block">{{ __('globals.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="{{ route('employees.indexData') }}" id="employeesDataRoute">
    <input type="hidden" value="{{ route('employees.login') }}" id="employeeLoginRoute">
    <input type="hidden" value="{{ route('employees.update') }}" id="employeeUpdateRoute">
    <input type="hidden" value="{{ route('employees.store') }}" id="employeeAddRoute">

    <!-- Scripts -->
    @section('employee-scripts')
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
        <script defer type="text/javascript" src="{{ asset('js/custom/employees.js').'?v='.config('app.version', '1') }}"></script>
    @endsection
@endsection
