<header id="navbar-spy" class="full-header header-5">
	<div id="top-bar" class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 hidden-xs">
					<ul class="list-inline top-contact" style="width: 100%; float: left;">
						<li><p><a href="#">{{ __('employee-globals.finder_company') }}</a></p></li>
						<li><p><a href="#" target="_blank">{{ __('employee-globals.login_aldoc') }} 1</a></p></li>
						<li><p><a href="#" target="_blank">{{ __('employee-globals.login_aldoc') }} 2</a></p></li>
						<li><p><a href="#" target="_blank">{{ __('employee-globals.oil_content') }} &amp; {{ __('employee-globals.looking_for_a_type') }}</a></p></li>
						<li><p><a href="http://www.smeerwinkel.nl/" target="_blank">{{ __('employee-globals.calc_oil_price') }}</a></p></li>
						<li><p><a href="http://www.oponeo.nl/" target="_blank">{{ __('employee-globals.calc_tyre_price') }}</a></p></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav id="primary-menu" class="navbar navbar-fixed-top style-1">
		<div class="row">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="logo" href="{{ route('login') }}">
						<img src="{{ asset('images/logo/sr-logo.png') }}" alt="Yellow Hats">
					</a>
				</div>

				<div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">
						@if($userType == \App\Models\User::SR_ADMIN || $userType == \App\Models\User::SR_EMPLOYEE)
							<li class="has-dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">{{ __('employee-globals.exercises') }}</a>
								<ul class="dropdown-menu">
									<li><a href="{{ route('orders.create') }}">{{ __('employee-globals.schedule') }}</a></li>
									<li><a href="{{ route('orders.statuses', \App\Models\Order::QUOTED) }}">{{ __('employee-globals.to_check') }} ({{ $quotedOrdersCount }})</a></li>
									<li><a href="{{ route('orders.statuses', \App\Models\Order::CONFIRMED) }}">{{ __('employee-globals.open') }} ({{ $confirmedOrdersCount }})</a></li>
									<li><a href="{{ route('orders.statuses', \App\Models\Order::ACCEPTED_BY_GARAGE) }}">{{ __('employee-globals.accepted') }} ({{ $acceptedByGarageOrdersCount }})</a></li>
									<li><a href="{{ route('orders.statuses', \App\Models\Order::COMPLETED) }}">{{ __('employee-globals.rounded') }} ({{ $completedOrdersCount }})</a></li>
								</ul>
							</li>
						@endif
						@if($userType == \App\Models\User::SR_ADMIN)
							<li class="has-dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">Admin</a>
								<ul class="dropdown-menu">
									<li><a href="{{ route('garages.manage') }}">{{ __('employee-globals.company_management') }}</a></li>
									<li><a href="{{ route('feedbacks.index', \App\Models\Feedback::REFERENCES) }}">{{ __('employee-globals.references') }}</a></li>
									<li><a href="{{ route('feedbacks.index', \App\Models\Feedback::COMPLAINTS) }}">{{ __('employee-globals.complaints') }}</a></li>
									<li><a href="{{ route('vehicles.index') }}">{{ __('employee-globals.prices') }}</a></li>
									<li><a href="{{ route('employees.index') }}">{{ __('employee-globals.edit_staff') }}</a></li>
								</ul>
							</li>
						@endif()
						@if($authenticatedUser)
							<li class="has-dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">Account</a>
								<ul class="dropdown-menu">
									<li><a href="#">{{ __('employee-globals.settings') }}</a></li>
									<li>
										<a href="{{ route('logout') }}"
										onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
											{{ __('employee-globals.log_out') }}
										</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                        {{ csrf_field() }}
	                                    </form>
									</li>
								</ul>
							</li>
						@endif
						@if(session('admin'))
							<li class="has-dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle" id="adminLoginBtn" data-route="{{ route('employees.adminLogin') }}">Admin</a>
							</li>
						@endif
					</ul>
				</div>
			</div>
		</div>
	</nav>
</header>
