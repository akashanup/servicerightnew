<?php

namespace Tests\Unit;

use App\Models\Feedback;
use DB;
use Illuminate\Support\Collection;
use Tests\Unit\UnitTestCase;

class FeedbackTest extends UnitTestCase
{
    /**
     * Feedback feedbacksData test example.
     *
     * @return void
     */
    public function testFeedbacksDataWithZeroFeedbackForReferences()
    {
        // Change the status of all feedbacks to anything other than new so that no feedback is returned.
        DB::table('feedback')->where('rating', '>=', 3)->update(['status' => 'processed']);
        $data = Feedback::feedbacksData(Feedback::REFERENCES);
        $this->assertEquals(['total' => 0, 'feedbacks' => new Collection()], $data);
    }

    /**
     * Feedback feedbacksData test example.
     *
     * @return void
     */
    public function testFeedbacksDataWithZeroFeedbackForComplaints()
    {
        // Change the status of all feedbacks to anything other than new so that no feedback is returned.
        DB::table('feedback')->where('rating', '<', 3)->update(['status' => 'processed']);
        $data = Feedback::feedbacksData(Feedback::COMPLAINTS);
        $this->assertEquals(['total' => 0, 'feedbacks' => new Collection()], $data);
    }

    /**
     * Feedback feedbacksData test example.
     *
     * @return void
     */
    public function testFeedbacksDataWithOutOfBoundPaginationForReferences()
    {
        $feedbacks = Feedback::where('rating', '>=', 3)->get();
        $data = Feedback::feedbacksData(Feedback::REFERENCES, (Feedback::all()->count()));
        $this->assertEquals(['total' => $feedbacks->count(), 'feedbacks' => new Collection()], $data);
    }

    /**
     * Feedback feedbacksData test example.
     *
     * @return void
     */
    public function testFeedbacksDataWithOutOfBoundPaginationForComplaints()
    {
        $feedbacks = Feedback::where('rating', '<', 3)->get();
        $data = Feedback::feedbacksData(Feedback::COMPLAINTS, (Feedback::all()->count()));
        $this->assertEquals(['total' => $feedbacks->count(), 'feedbacks' => new Collection()], $data);
    }

    /**
     * Feedback feedbacksData test example.
     *
     * @return void
     */
    public function testFeedbacksDataForFirstPassForReferences()
    {
        $feedbacks = Feedback::where('rating', '>=', 3)->where('feedback.status', Feedback::NEW_STATUS);
        $total = $feedbacks->count();
        $feedbacks = $feedbacks
            ->join('orders', 'feedback.order_id', 'orders.id')
            ->join('users', 'orders.user_id', 'users.id')
            ->join('addresses as addresses1', 'orders.address_id', 'addresses1.id')
            ->join('postal_codes as postal_codes1', 'addresses1.postal_code_id', 'postal_codes1.id')
            ->join('cities', 'postal_codes1.city_id', 'cities.id')
            ->join('garage_orders', 'orders.id', 'garage_orders.order_id')
            ->join('garages', 'garage_orders.garage_id', 'garages.id')
            ->join('addresses as addresses2', 'orders.address_id', 'addresses2.id')
            ->join('postal_codes as postal_codes2', 'addresses2.postal_code_id', 'postal_codes2.id')
            ->join('garage_categories', 'garages.id', 'garage_categories.garage_id')
            ->join('categories', 'garage_categories.category_id', 'categories.id')
            ->groupBy('feedback.id')
            ->groupBy('garages.id')
            ->select(
                'users.name as clientName',
                'users.email as clientEmail',
                'cities.name as clientCity',
                DB::raw('GROUP_CONCAT(DISTINCT categories.name SEPARATOR " | ") as categories'),
                'garages.name as garageName',
                'postal_codes2.postal_code as garagePostalCode',
                'orders.total_price as orderPrice',
                'feedback.rating',
                'feedback.review',
                'feedback.order_id as orderId',
                'feedback.id'
            )
            ->skip(0)
            ->take(10)
            ->get()
            ->toArray();
        $data = Feedback::feedbacksData(Feedback::REFERENCES);
        $this->assertEquals(['total' => $total, 'feedbacks' => $feedbacks], ['total' => $data['total'], 'feedbacks' => json_decode(json_encode($data['feedbacks']), true)]);
    }

    /**
     * Feedback feedbacksData test example.
     *
     * @return void
     */
    public function testFeedbacksDataForFirstPassForComplaints()
    {
        $feedbacks = Feedback::where('rating', '<', 3)->where('feedback.status', Feedback::NEW_STATUS);
        $total = $feedbacks->count();
        $feedbacks = $feedbacks
            ->join('orders', 'feedback.order_id', 'orders.id')
            ->join('users', 'orders.user_id', 'users.id')
            ->join('addresses as addresses1', 'orders.address_id', 'addresses1.id')
            ->join('postal_codes as postal_codes1', 'addresses1.postal_code_id', 'postal_codes1.id')
            ->join('cities', 'postal_codes1.city_id', 'cities.id')
            ->join('garage_orders', 'orders.id', 'garage_orders.order_id')
            ->join('garages', 'garage_orders.garage_id', 'garages.id')
            ->join('addresses as addresses2', 'orders.address_id', 'addresses2.id')
            ->join('postal_codes as postal_codes2', 'addresses2.postal_code_id', 'postal_codes2.id')
            ->join('garage_categories', 'garages.id', 'garage_categories.garage_id')
            ->join('categories', 'garage_categories.category_id', 'categories.id')
            ->groupBy('feedback.id')
            ->groupBy('garages.id')
            ->select(
                'users.name as clientName',
                'users.email as clientEmail',
                'cities.name as clientCity',
                DB::raw('GROUP_CONCAT(DISTINCT categories.name SEPARATOR " | ") as categories'),
                'garages.name as garageName',
                'postal_codes2.postal_code as garagePostalCode',
                'orders.total_price as orderPrice',
                'feedback.rating',
                'feedback.review',
                'feedback.order_id as orderId',
                'feedback.id'
            )
            ->skip(0)
            ->take(10)
            ->get()
            ->toArray();
        $data = Feedback::feedbacksData(Feedback::COMPLAINTS);
        $this->assertEquals(['total' => $total, 'feedbacks' => $feedbacks], ['total' => $data['total'], 'feedbacks' => json_decode(json_encode($data['feedbacks']), true)]);
    }

    /**
     * Feedback updateStatus test example.
     *
     * @return void
     */
    public function testupdateStatusWithAssignAndPost()
    {
        $feedback = Feedback::inRandomOrder()->first();
        $data = $feedback->updateStatus(
            array(
                'type' => 'assign',
                'socialMediaPost' => true,
            )
        );
        $this->assertEquals(['response' => true], $data);
    }

    /**
     * Feedback updateStatus test example.
     *
     * @return void
     */
    public function testupdateStatusWithAssignAndNoPost()
    {
        $feedback = Feedback::inRandomOrder()->first();
        $data = $feedback->updateStatus(
            array(
                'type' => 'assign',
                'socialMediaPost' => false,
            )
        );
        $this->assertEquals(['response' => true], $data);
    }

    /**
     * Feedback updateStatus test example.
     *
     * @return void
     */
    public function testupdateStatusWithRejectAndPost()
    {
        $feedback = Feedback::inRandomOrder()->first();
        $data = $feedback->updateStatus(
            array(
                'type' => 'reject',
                'socialMediaPost' => true,
            )
        );
        $this->assertEquals(['response' => true], $data);
    }

    /**
     * Feedback updateStatus test example.
     *
     * @return void
     */
    public function testupdateStatusWithRejectAndNoPost()
    {
        $feedback = Feedback::inRandomOrder()->first();
        $data = $feedback->updateStatus(
            array(
                'type' => 'reject',
                'socialMediaPost' => false,
            )
        );
        $this->assertEquals(['response' => true], $data);
    }
}
