<?php

namespace Tests\Unit;

use App\Models\LicensePlateNumber;
use Faker\Factory as Faker;
use Tests\Unit\UnitTestCase;

class LicensePlateNumberTest extends UnitTestCase
{
    /**
     * LicensePlateNumber fetch test example.
     *
     * @return void
     */
    public function testFetchFirst()
    {
        $licensePlateNumber = LicensePlateNumber::inRandomOrder()->first();
        $vehicle = $licensePlateNumber->vehicle;
        $data = LicensePlateNumber::fetch($licensePlateNumber->license_plate_number);

        $this->assertEquals(
            array(
                'services' => $vehicle->services,
                'fuel_type' => $vehicle->fuel_type,
                'brand' => $vehicle->brand->name,
                'model' => $vehicle->model,
                'build_year_start' => $vehicle->build_year_start(),
                'mandatory_service_expiry_date' => $licensePlateNumber->mandatory_service_expiry_date(),
                'licensePlateNumberId' => $licensePlateNumber->id,
                'response' => true,
            ),
            $data
        );
    }
    /**
     * LicensePlateNumber fetch test example.
     *
     * @return void
     */
    public function testFetchSecond()
    {
        $licensePlateNumber = '00-JBD-9';
        $data = LicensePlateNumber::fetch($licensePlateNumber);
        $licensePlateNumber = LicensePlateNumber::where('license_plate_number', $licensePlateNumber)->first();
        $vehicle = $licensePlateNumber->vehicle;
        $this->assertEquals(
            array(
                'services' => $vehicle->services,
                'fuel_type' => $vehicle->fuel_type,
                'brand' => $vehicle->brand->name,
                'model' => $vehicle->model,
                'build_year_start' => $vehicle->build_year_start(),
                'mandatory_service_expiry_date' => $licensePlateNumber->mandatory_service_expiry_date(),
                'licensePlateNumberId' => $licensePlateNumber->id,
                'response' => true,
                'message' => null,
            ),
            $data
        );
    }

    /**
     * LicensePlateNumber fetch test example.
     *
     * @return void
     */
    public function testFetchException()
    {
        $licensePlateNumber = Faker::create()->word;
        $this->expectException(\App\Exceptions\CurlException::class);
        $data = LicensePlateNumber::fetch($licensePlateNumber);
    }
}
