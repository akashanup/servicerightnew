<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicensePlateNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_plate_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vehicle_id');
            $table->string('license_plate_number');
            $table->date('build_year')->nullable();
            $table->date('mandatory_service_expiry_date')->nullable();
            $table->timestamps();
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('license_plate_numbers', function (Blueprint $table) {
            $table->dropForeign('license_plate_numbers_vehicle_id_foreign');
            $table->dropColumn('vehicle_id');
        });
        Schema::dropIfExists('license_plate_numbers');
    }
}
