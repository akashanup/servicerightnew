<?php

use App\Models\Garage;
use App\Models\GarageOrder;
use App\Models\Order;
use Illuminate\Database\Seeder;

class TestGarageOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(GarageOrder::class, 150)->create();
        $garageOrders = Order::whereIn(
            'status',
            [Order::ACCEPTED_BY_GARAGE, Order::REJECTED_BY_GARAGE, Order::COMPLETED]
        )->get();
        foreach ($garageOrders as $key => $garageOrder) {
            GarageOrder::create([
                'status' => $garageOrder->status,
                'order_id' => $garageOrder->id,
                'garage_id' => Garage::inRandomOrder()->first()->id,
                'created_at' => $garageOrder->created_at,
                'updated_at' => $garageOrder->updated_at,
            ]);
        }
    }
}
