<?php

namespace App\Models;

use App\Models\City;
use App\Models\Country;
use App\Models\Garage;
use App\Models\Order;
use App\Models\PostalCode;
use App\Models\State;
use App\Models\User;
use App\Utils\Curl;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'postal_code_id', 'house_number', 'complete_address', 'type', 'latitude', 'longitude',
    ];

    const GARAGE_TYPE = 'garage';
    const CLIENT_TYPE = 'client';

    const NETHERLANDS = 'Netherlands';
    /**
     * Get the the postal code of the address.
     *
     * @return App\Models\PostalCode
     */
    public function postalCode()
    {
        return $this->belongsTo(PostalCode::class);
    } //end postalCode()

    /**
     * Get the orders for the address.
     *
     * @return Collection
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    } //end orders()

    /**
     * Get the garage at the address.
     *
     * @return Collection
     */
    public function garage()
    {
        return $this->hasOne(Garage::class);
    } //end garage()

    /**
     * Fetch address from API.
     *
     * @param array $location
     * @return array
     */
    public static function fetch($location)
    {
        $json = [];
        $json['response'] = false;
        $addresses = static::where('house_number', $location['houseNumber'])->get();
        foreach ($addresses as $address) {
            $postalCode = $address->postalCode;
            if (($postalCode->postal_code == $location['postalCode'])
                && ($postalCode->city->state->country_id == $location['countryId'])) {
                $json['complete_address'] = $address->complete_address;
                $city = $postalCode->city;
                $json['city'] = $city->name;
                $json['state'] = $city->state->name;
                $json['addressId'] = $address->id;
                $json['response'] = true;
            }
        }

        // Curl only for Netherands
        $countryNetherands = Country::where('name', static::NETHERLANDS)->first();
        if (!$json['response'] && $countryNetherands && ($location['countryId'] == $countryNetherands->id)) {
            $address = Curl::fetchAddress($location['postalCode'], $location['houseNumber']);
            $json = $address;
            if ($address['response']) {
                // Add address in DB to save time in future.
                $address = static::createUnique(
                    $location['houseNumber'], $address['complete_address'], User::CLIENT,
                    $address['latitude'], $address['longitude'], $location['postalCode'], $address['city'], $address['state']
                );
                $json['addressId'] = $address->id;
            }
        }

        return $json;
    } //end fetch()

    /**
     * Create an address if its not found.
     *
     * @param string $houseNumber
     * @param string $completeAddress
     * @param string $type
     * @param string $latitude
     * @param string $longitude
     * @param string $postalCode
     * @param string $city
     * @param string $state
     * @return App\Models
     */
    public static function createUnique($houseNumber, $completeAddress, $type, $latitude, $longitude, $postalCode, $city, $state = null)
    {
        if ($state) {
            $country = Country::where('name', static::NETHERLANDS)->first();
            $state = State::createUnique($country->id, $state);
        } else {
            $state = State::where('name', State::NOT_FOUND)->first();
        }
        $city = City::createUnique($state->id, $city);
        $postalCode = PostalCode::createUnique($city->id, $postalCode);
        $address = static::where('postal_code_id', $postalCode->id)->where('house_number', $houseNumber)
            ->where('complete_address', $completeAddress)->where('type', $type)->first();
        if (!$address) {
            $address = static::create(
                [
                    'postal_code_id' => $postalCode->id,
                    'house_number' => $houseNumber,
                    'complete_address' => $completeAddress,
                    'type' => $type,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ]
            );
        }
        return $address;
    } //end createUnique()
}
