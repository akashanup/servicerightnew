$(function(){

	$('#brandSlct').on('change', function(){
		$.fn.ajaxBlockUI();
		var data = {
			"brandId": $(this).val()
		};
		var url = $('#fetchVehicleRoute').val();
		$.fn.ajaxCall(REQUEST_TYPE_POST, url, JSON.stringify(data), DATA_TYPE_JSON).then((successResponse) => {
			var model = $('#modelSlct');
			model.html(successResponse.data);
			model.prop('disabled', false)
			$.unblockUI();
		}, (errorResponse) => {
			$.unblockUI();
			$.blockUI({ message: AJAX_ERROR_MESSAGE, timeout: 2000,baseZ: 1005 });
		});
	});

	$('#searchVehicleBtn').on('click', () => {
		var model = $('#modelSlct').val();
		var licensePlateNumber = $('#licensePlateNumberInpt').val();
		licensePlateNumber = licensePlateNumber ? `/${licensePlateNumber}` : '';
		if(model || licensePlateNumber) {
			var homeRoute = $('#homeRoute').val();
			window.location = `${homeRoute}/vehicles/prices/${model}${licensePlateNumber}`;
		} else {
			$('#modelSlct').focus();
			$('#modelSlct').css(BORDER_COLOR, RED_COLOR);
		}
		
	});
});