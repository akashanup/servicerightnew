@extends('emails.html-template')

@section('content')
	{{ __('globals.dear') }} {{ $salutation }} {{ $name }},
	<br/>
	<br/>
	{{ __('employee-assign-order.on') }} {{ $pickupDate }} {{__('employee-assign-order.email_text_12') }} {{ $garageName }}.
	<br/>
	{{ $garageName }} {{__('employee-assign-order.email_text_13') }} {{ $pickupTime }} {{__('employee-assign-order.email_text_14') }} {{ $houseNumber }}, {{ $completeAddress }}, {{ $postalCode }}, {{ $city }}, {{__('employee-assign-order.email_text_15') }}
	<br/>

	<br/>
	<table width="700">
		<tr>
			<td width="320">
				<img alt="logo servicepunt" src="{{$garageLogo}}">
			</td>
			<td width="380">
				{{__('employee-assign-order.service_point_data') }}:<br/>
				<h2 style="margin:0px;">{{ $garageName }}</h2>
				{{ $garageAddress }}<br/>
				{{ $garagePostalCode }} {{ $garageCity }}<br/>
				[E] <a href="mailto:{{ $garageEmail }}">{{ $garageEmail }}</a><br/>
				<b>[T] {{ $garagePhone }}</b><br/>
			</td>
		</tr>
	</table>
	<br/>
	{{ __('employee-place-order.services_list') }}:
	<ul>
		@foreach($servicesList as $service)
			<li>{{ $service }}</li>
		@endforeach
	</ul>
	</br>
	<b>{{ __('employee-place-order.total_price') }}: &euro; {{$totalPrice}} incl.BTW</b><br/>
	<br/>

	<small>{{__('employee-assign-order.email_text_16') }}</small>
	<br/><br/>
	<small>{{__('employee-assign-order.email_text_3') }}
	<br/><br/>
	{{__('employee-assign-order.email_text_4') }}
	</small>

@endsection
