<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Send response based on request type.
     *
     * @return mixed
     */
    public function sendResponse($message)
    {
        if (request()->ajax()) {
            $json['response'] = false;
            $json['message'] = $message;
            return response()->json($json);
        } else {
            return redirect()->route('home')->withErrors($message);
        }
    } //end sendResponse()
}
