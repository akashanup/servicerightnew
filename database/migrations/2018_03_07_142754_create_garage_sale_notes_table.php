<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGarageSaleNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage_sale_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('garage_id');
            $table->unsignedInteger('by_user_id');
            $table->text('sale_note');
            $table->timestamps();
            $table->foreign('garage_id')->references('id')->on('garages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('by_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('garage_sale_notes', function (Blueprint $table) {
            $table->dropForeign('garage_sale_notes_garage_id_foreign');
            $table->dropColumn('garage_id');
            $table->dropForeign('garage_sale_notes_by_user_id_foreign');
            $table->dropColumn('by_user_id');
        });
        Schema::dropIfExists('garage_sale_notes');
    }
}
