<?php

namespace App\Exceptions;

use Exception;
use Lang;

class PhoneNumberAlreadyExistsException extends Exception
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        $json = [
            "response" => false,
            "errors" => ["error" => [Lang::get('globals.phone_already_exists')]],
        ];
        return response()->json($json, 422);
    }
}
