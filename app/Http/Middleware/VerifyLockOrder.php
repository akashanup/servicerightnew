<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Closure;
use Lang;

class VerifyLockOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $controller = new Controller;
        $order = Order::find(request()->route('id'));
        if ($order) {
            if ($order->verifyLock()) {
                return $next($request);
            }
            return $controller->sendResponse(Lang::get('globals.unauthorized_text'));
        }
        return $controller->sendResponse(Lang::get('globals.request_error_message'));
    }
}
