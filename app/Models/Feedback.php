<?php

namespace App\Models;

use App\Models\Order;
use DB;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'rating', 'review', 'status', 'social_media_post',
    ];

    const RATING_COLOR = [' red', ' red', ' orange', ' blue', ' green'];
    const REFERENCES = 'references';
    const COMPLAINTS = 'complaints';
    const REFERENCES_RATING_MIN = 3;
    const COMPLAINTS_RATING_MAX = 2;
    const RECORD_MULTIPLIER = 10;
    const NEW_STATUS = 'new';
    const ASSIGN = 'assign';
    const REJECT = 'reject';
    const ACCEPTED_STATUS = 'accepted';
    const DECLINED_STATUS = 'declined';
    const PROCESSED_STATUS = 'processed';
    const YES = 'yes';
    const NO = 'no';

    /**
     * Get the the order for the feedback.
     *
     * @return App\Models\User
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    } //end order()

    /**
     * Feedback data based on type.
     *
     * @param string $type type of feedback
     * @param integer $draw pagination factor
     * @return array
     */
    public static function feedbacksData($type, $draw = 1)
    {
        $feedbacks = DB::table('feedback')->where('feedback.status', static::NEW_STATUS);

        $feedbacks = ($type == static::REFERENCES ? $feedbacks->where('feedback.rating', '>=', static::REFERENCES_RATING_MIN)
            : $feedbacks->where('feedback.rating', '<=', static::COMPLAINTS_RATING_MAX));
        $total = $feedbacks->count();
        $feedbacks = $feedbacks
            ->join('orders', 'feedback.order_id', 'orders.id')
            ->join('users', 'orders.user_id', 'users.id')
            ->join('addresses as addresses1', 'orders.address_id', 'addresses1.id')
            ->join('postal_codes as postal_codes1', 'addresses1.postal_code_id', 'postal_codes1.id')
            ->join('cities', 'postal_codes1.city_id', 'cities.id')
            ->join('garage_orders', 'orders.id', 'garage_orders.order_id')
            ->join('garages', 'garage_orders.garage_id', 'garages.id')
            ->join('addresses as addresses2', 'orders.address_id', 'addresses2.id')
            ->join('postal_codes as postal_codes2', 'addresses2.postal_code_id', 'postal_codes2.id')
            ->join('garage_categories', 'garages.id', 'garage_categories.garage_id')
            ->join('categories', 'garage_categories.category_id', 'categories.id')
            ->groupBy('feedback.id')
            ->groupBy('garages.id')
            ->select(
                'users.name as clientName',
                'users.email as clientEmail',
                'cities.name as clientCity',
                DB::raw('GROUP_CONCAT(DISTINCT categories.name SEPARATOR " | ") as categories'),
                'garages.name as garageName',
                'postal_codes2.postal_code as garagePostalCode',
                'orders.total_price as orderPrice',
                'feedback.rating',
                'feedback.review',
                'feedback.order_id as orderId',
                'feedback.id'
            )
            ->skip(($draw - 1) * (static::RECORD_MULTIPLIER))
            ->take(static::RECORD_MULTIPLIER)
            ->get();

        return ['total' => $total, 'feedbacks' => $feedbacks];
    } //end feedbacksData()

    /**
     * Update feedback status.
     *
     * @param array $data feedback data
     * @return array
     */
    public function updateStatus($data)
    {
        $this->update(
            [
                'status' => $data['type'] == static::ASSIGN
                ? ($this->rating >= static::REFERENCES_RATING_MIN ? static::ACCEPTED_STATUS : static::PROCESSED_STATUS)
                : static::DECLINED_STATUS,
                'social_media_post' => $data['socialMediaPost'] ? static::YES : static::NO,

            ]
        );

        return ['response' => true];
    }
}
