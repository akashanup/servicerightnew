<!DOCTYPE html>
<html dir="ltr" lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="author" content="ServiceRight" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="construction html5 template">
        <link href="{{ asset('images/favicon/favicon.ico') }}" rel="icon">

        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700%7CRaleway:100,200,300,400,500,600,700,800%7CDroid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

        <!-- Styles -->
        <link href="{{ asset('css/external.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom-admin.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('revolution/css/settings.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('revolution/css/layers.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('revolution/css/navigation.css') }}" rel="stylesheet" type="text/css">
        @yield('stylesheets')
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
        <!--[if lt IE 9]>
              <script src="js/html5shiv.js"></script>
              <script src="js/respond.min.js"></script>
            <![endif]-->
        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body>
        @yield('content')
        <input type="hidden" id="requestErrorMessageInpt" value="{{ __('globals.request_error_message') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/jquery-2.1.1.min.js') }}"></script>
        <script src="{{ asset('js/plugins.js').'?v='.config('app.version', '1') }}"></script>
        <script src="{{ asset('js/functions.js').'?v='.config('app.version', '1') }}"></script>
        <script src="{{ asset('js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('js/custom/app.js').'?v='.config('app.version', '1') }}"></script>
        @yield('scripts')
    </body>
</html>
