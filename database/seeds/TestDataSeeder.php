<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TestCountriesTableSeeder::class);
        $this->call(TestStatesTableSeeder::class);
        $this->call(TestCitiesTableSeeder::class);
        $this->call(TestPostalCodesTableSeeder::class);
        $this->call(TestCategoriesTableSeeder::class);
        $this->call(TestCompanyTableSeeder::class);
        $this->call(TestServicesTableSeeder::class);
        $this->call(TestBrandsTableSeeder::class);
        $this->call(TestVehiclesTableSeeder::class);
        $this->call(TestLicensePlateNumbersTableSeeder::class);
        $this->call(TestUsersTableSeeder::class);
        $this->call(TestAddressesTableSeeder::class);
        $this->call(TestGaragesTableSeeder::class);
        $this->call(TestGarageCategoriesTableSeeder::class);
        $this->call(TestGarageCompaniesTableSeeder::class);
        $this->call(TestOrdersTableSeeder::class);
        $this->call(TestGarageOrdersTableSeeder::class);
        $this->call(TestOrderServicesTableSeeder::class);
        $this->call(FeedbackTableSeeder::class);
    }
}
